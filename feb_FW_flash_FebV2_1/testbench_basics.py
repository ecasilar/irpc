# -*- coding: utf-8 -*-
"""
Created on Wed Apr  7 08:22:24 2021

@author: luciol
"""

import uhal
import time
import sys
import os
import types
import argparse
from scurve import *
from TDC_plot import*
from fc7 import *
from testbench_calibrate_asic import * 
from tools_box import *
from feb_v2_cycloneV_r1 import *


class testbench_basics:
    
    def __init__(self):
        value_fpga0_top=[7,19,24,15,25,23,30,24,28,37,37,36,43,49,57,59]
        strip_ref_fpga0_top = ["r_strip_8","strip_8","r_strip_9","strip_9","r_strip_10","strip_10","r_strip_11","strip_11","r_strip_12","strip_12","r_strip_13","strip_13","r_strip_14","strip_14","r_strip_15","strip_15"]
        
        value_fpga0_bottom=[7,19,24,15,25,23,30,24,28,37,37,36,43,49,57,59]
         strip_ref_fpga0_top = ["r_strip_0","strip_0","r_strip_1","strip_1","r_strip_2","strip_2","r_strip_3","strip_3","r_strip_4","strip_4","r_strip_5","strip_5","r_strip_6","strip_6","r_strip_7","strip_7"]
        
        value_fpga1_top=[7,19,24,15,25,23,30,24,28,37,37,36,43,49,57,59]
        strip_ref_fpga1_top = ["r_strip_24","strip_24","r_strip_25","strip_25","r_strip_26","strip_26","r_strip_27","strip_27","r_strip_28","strip_28","r_strip_29","strip_29","r_strip_30","strip_30","r_strip_31","strip_31"]

        value_fpga1_bottom=[7,19,24,15,25,23,30,24,28,37,37,36,43,49,57,59]
        strip_ref_fpga1_bottom = ["r_strip_16","strip_16","r_strip_17","strip_17","r_strip_18","strip_18","r_strip_19","strip_19","r_strip_20","strip_20","r_strip_21","strip_21","r_strip_22","strip_22","r_strip_23","strip_23"]

        value_fpga2_top=[7,19,24,15,25,23,30,24,28,37,37,36,43,49,57,59]
        strip_ref_fpga2_top = ["r_strip_40","strip_40","r_strip_41","strip_41","r_strip_42","strip_42","r_strip_43","strip_43","r_strip_44","strip_44","r_strip_45","strip_45","r_strip_46","strip_46","r_strip_47","strip_47"]
        
        value_fpga2_bottom=[7,19,24,15,25,23,30,24,28,37,37,36,43,49,57,59]
        strip_ref_fpga2_bottom = ["r_strip_32","strip_32","r_strip_33","strip_33","r_strip_34","strip_34","r_strip_35","strip_35","r_strip_36","strip_36","r_strip_37","strip_37","r_strip_38","strip_38","r_strip_39","strip_39"]

    def get_trigger_windows(self, window_time_ns = 1):
        
        #Window config.
        self.fc7.set_time_window(window_time)
        self.fc7.set_short_wdw_delay(0)# No delay
                
        cpt_timeout = 0
        self.fc7.start_TDC_counters()
        while(self.fc7.get_calibration_done()!=1):
            if cpt_timeout==10:
                self.fc7.stop_TDC_counters()
                return -1
            else:
                time.sleep(0.2)
                cpt_timeout = cpt_timeout + 1
        r=self.fc7.get_counter_trigger()
        self.fc7.stop_TDC_counters()
        return r
    
    # find delay of the 100 ns window for maximum trigger efficiency
    def find_short_wdw(self):
        cpt_timeout = 0
        print("start tdc acquisition")
        for i in range(30,50):
            self.fc7.set_short_wdw_delay(i)
            list_i=[]
            for j in range(100):
                self.fc7.set_trigger_out_ch2(1) #send one trigger channel 2

                if self.fc7.get_calibration_done() == 1 :
                    r=self.fc7.get_counter_trigger()
                    list_i.append(r)
                    self.fc7.set_trigger_out_ch2(0) 
            print("décalage (x25ns) ",i)
            print("nombre de 1 sur 500 tests : ",list_i.count(1))
    
    def dac_6b_adjusted_config(self,threashold_50_list):
        threashold_list = []
        #threashold equalisation
        for ref,threashold_50_i in threashold_50_list.items():
            threashold_list.append(threashold_50_i[0])
        # 10bits inputDAC th 50% value
        th_mean = mean(threashold_list)
        ### 6b DAC value
        for strip, th in threashold_50_list.items():
            adjust_n = ((th_mean - th[0])*0.92 + 32*1.5)/1.5
            project.feb.configure_6b_dac(strip, int(adjust_n)) # adjust 6bit DAC
        
    ### Used to find PetiROC / TDC channels efficiency
    def pedestal_efficiency_search(self,strip_ref, first,all_asic = True, all_TDC = False):
                
        ######################################Asic/TDC Config.############################################
        all_strips = [x for x in range(0,31,2)]
        ### Active all ASIC channels
        if all_asic == True:
            self.feb.asic.init(False) 
            self.feb.asic.disable_channel("fpga_1","top",all_strips)
            self.feb.asic.enable_channel("fpga_1","top",all_strips)
            self.feb.asic.configure("fpga_1","all")
            print("ASIC CHANNEL EN TOP: {}".format(project.feb.asic.get_enable_channel("fpga_1","top")))

        all_channels = [x for x in range(0,34)]
        ### Active all TDC channels
        if all_TDC==True:
            self.feb.tdc.set_enable("fpga_1")
            #Configure TDCs
            self.feb.tdc.channel_disable("fpga_1",all_channels)
            self.feb.tdc.channel_enable("fpga_1",all_channels)
            self.feb.tdc.set_cmd_valid("fpga_1")
            #Status
            print(self.feb.tdc.get_enable_status("fpga_1"))
            print(self.feb.tdc.get_channel_status("fpga_1"))
        ##################################################################################################
        
         
        ### CHANNEL / CHANNEL scanning #################################################
        for strip in strip_ref:
            print("#############################################")
            print("#### ",strip)
            # project.feb.init_configuration([strip],False) #enable asic channel (mask) and TDC channel
            scurve = []
            
            ######################################Asic/TDC Config.############################################
            #############################
            ### Asic channel / channel
            if all_asic == False :
                self.feb.asic.init(False) 
                self.feb.asic.disable_channel("fpga_1","top",all_strips)
                self.feb.asic.enable_channel("fpga_1","top",all_strips)
                self.feb.asic.configure("fpga_1","all")
                print("ASIC CHANNEL EN TOP: {}".format(project.feb.asic.get_enable_channel("fpga_1","top")))
                
            #############################
            ### TDc channel / channel
            if all_TDC == False : 
                self.feb.tdc.set_enable("fpga_1")
                all_channels = [x for x in range(0,34)]
                channel = self.feb.lookuptable_strips[strip][project.feb.index_lut_tdc_channel]
                print(channel)
                #Configure TDCs
                self.feb.tdc.channel_disable("fpga_1",all_channels)
                self.feb.tdc.channel_enable("fpga_1",[channel])
                self.feb.tdc.set_cmd_valid("fpga_1")
                #Status
                print(self.feb.tdc.get_enable_status("fpga_1"))
                print(self.feb.tdc.get_channel_status("fpga_1"))
            ##################################################################################################
            
            ### efficiency caluculation ######################################################################
            # Detection of a trigger within a 100 ns window
            ##################################################################################################
            threshold_min = 300
            threshold_current = threshold_min
            #############################
            eff_list = []
            efficacity = self.get_trigger_efficiency(strip,threshold_current) # return efficiency / threashold
            eff_list.append([efficacity, threshold_current])
            
            ### Extend window detection 100 value after 0
            cpt = 0

            while(efficacity != 0 or cpt > 300):
                cpt = cpt+1
                threshold_current = threshold_current + 1
                efficacity = self.get_trigger_efficiency(strip,threshold_current)
                eff_list.append([efficacity, threshold_current])
                scurve.append((threshold_current,efficacity))
                print(eff_list)


            if first:
                with open("efficacity_scurve.raw", "w") as outfile:
                    outfile.write("#{},{},asic_{},ch_{},tdc_{}\n".format(strip,project.feb.lookuptable_strips[strip][0],project.feb.lookuptable_strips[strip][1],project.feb.lookuptable_strips[strip][2],project.feb.lookuptable_strips[strip][3]))
                    for x in scurve:
                        outfile.write((str(x[0]) + "," +str(x[1]) + "\n"))
                        first=False
            else:
                with open("efficacity_scurve.raw", "a") as outfile:
                    outfile.write("#{},{},asic_{},ch_{},tdc_{}\n".format(strip,project.feb.lookuptable_strips[strip][0],project.feb.lookuptable_strips[strip][1],project.feb.lookuptable_strips[strip][2],project.feb.lookuptable_strips[strip][3]))
                    for x in scurve:
                        outfile.write((str(x[0]) + "," +str(x[1]) + "\n"))
                        
    def pedestal_search(self,strip_ref, first, all_asic = False,time_window=1):
        
        ######################################Asic/TDC Config.############################################
        fpga = self.feb.lookuptable_strips[strip_ref[0]][project.feb.index_lut_fpga]
        asic = self.feb.lookuptable_strips[strip_ref[0]][project.feb.index_lut_asic]
        
        all_strips = [x for x in range(0,31,2)]
        ### Active all ASIC channels
        if all_asic == True:
            project.feb.asic.init(False)
            self.feb.asic.disable_channel(fpga,asic,all_strips)
            self.feb.asic.enable_channel(fpga,asic,all_strips)
            self.feb.asic.configure(fpga,"all")
            print("ASIC ", asic,"of FPGA",fpga," CHANNEL enable: {}".format(project.feb.asic.get_enable_channel(fpga,asic)))
                        
        for strip in strip_ref: 
            print("#############################################")
            print("#### ",strip)
                       
           ### Active selected ASIC channels
            if all_asic == False:
                self.feb.init_configuration([strip],False) #enable asic channel (mask) and TDC channel
                # self.feb.asic.disable_channel(fpga,asic,all_strips)
                # self.feb.asic.enable_channel(fpga,asic,strip)
                # self.feb.asic.configure(fpga,"all")
            
            ### Init configuration
            threshold_min = 100
            threshold_current = threshold_min
            project.feb.configure_6b_dac(strip,32) #First step : 6b-dac set to middle range (32)
            project.feb.configure_10b_dac(strip,threshold_current)
            
            print("Find out Threshold range...")
             ###Threashold min
            while(project.get_trigger_windows(time_window)>1800):
                print("nombre données : {}" .format(project.get_trigger_windows(time_window)))
                threshold_current = threshold_current + 10
                project.feb.configure_10b_dac(strip,threshold_current)
            threshold_min = threshold_current - 20
            ###Threashold max
            while(project.get_trigger_windows(time_window) != 0):
                print("nombre données : {}" .format(project.get_trigger_windows(time_window)))
                threshold_current = threshold_current + 10
                project.feb.configure_10b_dac(strip,threshold_current)
            threshold_max = threshold_current + 20

            print("Threshold min. {} max. {}".format(threshold_min,threshold_max))
            threshold_current = threshold_min
            cpt_zero = 0
            scurve = []
            project.feb.configure_10b_dac(strip,threshold_current)
            while(threshold_current<threshold_max+1):
                percent= int(  ((threshold_current-threshold_min)/(threshold_max-threshold_min) )*100)
                print("processing : {}%".format(percent),end="\r")
                r=[]
                for i in range(20):
                    r.append(project.get_trigger_windows(time_window))
                trigger_mean_value = int(mean(r))
                if trigger_mean_value==0:
                    cpt_zero = 0
                    scurve.append((threshold_current,trigger_mean_value))
                    threshold_current = threshold_current + 1
                elif trigger_mean_value!=0:
                    scurve.append((threshold_current,trigger_mean_value))
                    threshold_current = threshold_current + 1
                    cpt_zero = 0
                project.feb.configure_10b_dac(strip,threshold_current)
                time.sleep(0.5)
            if first:
                with open("scurve.raw", "w") as outfile:
                    outfile.write("#{},{},asic_{},ch_{},tdc_{}\n".format(strip,project.feb.lookuptable_strips[strip][0],project.feb.lookuptable_strips[strip][1],project.feb.lookuptable_strips[strip][2],project.feb.lookuptable_strips[strip][3]))
                    for x in scurve:
                        outfile.write((str(x[0]) + "," +str(x[1]) + "\n"))
                        first=False
            else:
                with open("scurve.raw", "a") as outfile:
                    outfile.write("#{},{},asic_{},ch_{},tdc_{}\n".format(strip,project.feb.lookuptable_strips[strip][0],project.feb.lookuptable_strips[strip][1],project.feb.lookuptable_strips[strip][2],project.feb.lookuptable_strips[strip][3]))
                    for x in scurve:
                        outfile.write((str(x[0]) + "," +str(x[1]) + "\n"))

    def pedestal_search_equalized(self,strip_ref,first, all_asic = False):
        
        fpga = self.feb.lookuptable_strips[strip_ref[0]][project.feb.index_lut_fpga]
        asic = self.feb.lookuptable_strips[strip_ref[0]][project.feb.index_lut_asic]
        all_strips = [x for x in range(0,31,2)]
        ### Active all ASIC channels
        if all_asic == True:
            project.feb.asic.init(False)
            self.feb.asic.disable_channel(fpga,asic,all_strips)
            self.feb.asic.enable_channel(fpga,asic,all_strips)
            self.feb.asic.configure(fpga,"all")
            print("ASIC ", asic,"of FPGA",fpga," CHANNEL enable: {}".format(project.feb.asic.get_enable_channel(fpga,asic)))
            
        for strip in strip_ref:
            print("#############################################")
            print("#### ",strip)
                        
            ########### INIT config. chan / chan #####################################################
            if all_asic == False:
                self.feb.init_configuration([strip],False) #enable asic channel (mask) and TDC channel
            print("Find out Threshold range...")
            threshold_min = 300
            threashiold_current = threshold_min
            project.feb.configure_10b_dac(strip,threashiold_current)
        
            while(project.get_trigger_windows(time_window)>1800):
                print("nombre données : {}" .format(project.get_trigger_windows(time_window)))
                threshold_current = threshold_current + 10
                project.feb.configure_10b_dac(strip,threshold_current)
            threshold_min = threshold_current - 20
            ###Threashold max
            while(project.get_trigger_windows(time_window) != 0):
                print("nombre données : {}" .format(project.get_trigger_windows(time_window)))
                threshold_current = threshold_current + 10
                project.feb.configure_10b_dac(strip,threshold_current)
            threshold_max = threshold_current + 20

            print("Threshold min. {} max. {}".format(threshold_min,threshold_max))
            threshold_current = threshold_min
            cpt_zero = 0
            scurve = []
            project.feb.configure_10b_dac(strip,threshold_current)
            while(threshold_current<threshold_max+1):
                percent= int(  ((threshold_current-threshold_min)/(threshold_max-threshold_min) )*100)
                print("processing : {}%".format(percent),end="\r")
                r=[]

                ### mean of trigger threashold values
                for i in range(20):
                    r.append(project.get_trigger_windows(time_window))
                trigger_mean_value = int(mean(r))
                if trigger_mean_value==0:
                    cpt_zero = 0
                    scurve.append((threshold_current,trigger_mean_value))
                    threshold_current = threshold_current + 1
                elif trigger_mean_value!=0:
                    scurve.append((threshold_current,trigger_mean_value))
                    threshold_current = threshold_current + 1
                    cpt_zero = 0
                project.feb.configure_10b_dac(strip,threshold_current)
                time.sleep(0.5)
            if first:
                with open("scurve.raw", "w") as outfile:
                    outfile.write("#{},{},asic_{},ch_{},tdc_{}\n".format(strip,project.feb.lookuptable_strips[strip][0],project.feb.lookuptable_strips[strip][1],project.feb.lookuptable_strips[strip][2],project.feb.lookuptable_strips[strip][3]))
                    for x in scurve:
                        outfile.write((str(x[0]) + "," +str(x[1]) + "\n"))
                        first=False
            else:
                with open("scurve.raw", "a") as outfile:
                    outfile.write("#{},{},asic_{},ch_{},tdc_{}\n".format(strip,project.feb.lookuptable_strips[strip][0],project.feb.lookuptable_strips[strip][1],project.feb.lookuptable_strips[strip][2],project.feb.lookuptable_strips[strip][3]))
                    for x in scurve:
                        outfile.write((str(x[0]) + "," +str(x[1]) + "\n"))