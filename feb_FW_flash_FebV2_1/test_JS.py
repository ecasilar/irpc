from tools_box import *
import numpy as np
import sys
#from test_class import test_class
import struct
import time
from test_class import *

TMS_data = [0,0,0,0]
r= bf(TMS_data[0])
test = 0b10011101
file = "JTAG_bin_files/JTAG_TDI_LEDs.bin"
### !!!!!!! UNCOMMENT WHEN FC7 is CONNECTED !!!!!!! ###
testbench = test_class(False)                      ###
#######################################################

#===========JTAG COMMAND TAP controller ============#
Reset           = 0b111110
idle_shiftDR    = 0b100
idle_shiftIR    = 0b1100
exitIR_shiftDR  = 0b11100
#===================================================#

# testbench.init()

#===================================================================================================================================
# test read binary file

# TDI_buf = 0x4464785458
# f = open(file,"rb")
# a = np.fromfile(f,dtype=np.uint8)
# print(len(a))

# for i in range(len(a)):
    # print(a[i])

# pourcent = (rd_cnt/len(a))*100
# sys.stdout.write("Download lecture du fichier à: %d%%   \r" % (pourcent) )
# sys.stdout.flush()
#===================================================================================================================================

#================================================================================#
                        ###########################
                        ####  TOOLS FUNCTIONS  ####
                        #####                 #####

def dump_config_from_file(file):

    i=0 # compteur lecture fichier
    with open(file,'rb') as f:
        buf32   = 0
        buf128  = []
        byte    = f.read(1)
        a       = np.fromfile(f,dtype=np.uint8) # chaque octects => LSB First
        j       = 0 # compteur for 32 bits integer generation
        t1= time.time()
        g = 0
        #while a[i] != None :
        #for i in range(len(a)): # on parcours le fichier
        while i < len(a):
            pourcent = (i/len(a))*100
            sys.stdout.write("Download lecture du fichier à: {} %   \r".format(pourcent))
            sys.stdout.flush()
            buf32 = (a[i+3].item() << 24) + (a[i+2].item() << 16) + (a[i+1].item() << 8) + a[i].item() # item() => conversion np.uint(8) en int
            #buf32 = buf32 + (a[i] << (j*8))
            #print("buf32 = {} | a[0] = {}, a[1] = {}, a[2] = {}, a[3] = {}".format(buf32,hex(a[i]),hex(a[i+1]),hex(a[i+2]),hex(a[i+3])))
            buf128.append(buf32)
            j = 0

            ##### écrit tdi
            if (len(buf128) == 4 ) :
                ## Debug
                #print("buf 128 : {} | a[0] = {}, a[1] = {}, a[2] = {}, a[3] = {} |i= {} ".format(buf128,hex(a[i]),hex(a[i+1]),hex(a[i+2]),hex(a[i+3]),i))
                #print("buf128 _> hex : 3 : {} | 2 : {} | 1 : {} | 0 : {}".format(hex(buf128[0]), hex(buf128[1]), hex(buf128[2]),hex(buf128[3])))

                ### DATA TYPE ### 
                # print(type(buf128))
                # print(type(buf32))
                # print(type(a[i]))
                # print(type(a[i].item()))
                #################

                ### START WRITING SCA BUFFERS ###
                testbench.feb.feb_ctrl_and_status.sca.write_buffer_length(0)
                testbench.feb.feb_ctrl_and_status.sca.write_TDO_buf(buf128) # drive TD0 high while moving to shiftDR
                testbench.feb.feb_ctrl_and_status.sca.write_TMS_buf([0,0,0,0]) # last bit need TMS active to exitIR -> UpdateIR -> Select_DR -> captureDR -> shiftDR 
                testbench.feb.feb_ctrl_and_status.sca.start_JTAG_transmission()
                ################

                buf128 = []
            buf32=0
            i+=4

    t2 = time.time()
    print("temps total JTAG programmation : ", (t2-t1))                
    f.close()



def reverse_bit_order(value , nb_bit = 0):
    reverse = sum(1<<(nb_bit-1-i) for i in range(nb_bit) if value >>i&1)
    #print("initial value : {} \n reversed number : {} , bit length = {} ".format(bin(value),bin(reverse),nb_bit))
    return reverse

def set_TMS_commande(cmd = 0b11111, size = 5):  # ! cmd < 32 bits
    cmd = reverse_bit_order(cmd, size)
    testbench.feb.feb_ctrl_and_status.sca.write_buffer_length(size) 
    testbench.feb.feb_ctrl_and_status.sca.write_TDO_buf([0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF])
    testbench.feb.feb_ctrl_and_status.sca.write_TMS_buf([cmd,0,0,0])

def set_JTAG_FREQ(value_Hz = 20000000):
    value = ((2*((10**7))//value_Hz))-1
    data = testbench.feb.feb_ctrl_and_status.sca.set_JTAG_frequency(value)
    data = testbench.feb.feb_ctrl_and_status.sca.get_JTAG_frequency()
    print("\n FREQ =  {} Hz \n".format((2*((10**7)/(data+1)))))

def set_JTAG_CONF(basic_cfg = 0x0000):
    testbench.feb.feb_ctrl_and_status.sca.set_JTAG_Ctrl(data)
    print("\nControl reg =  ",hex(testbench.feb.feb_ctrl_and_status.sca.get_JTAG_Ctrl()))
    print("-----------------------------\n")

    ##########################################
def configure_JTAG_ctrl(basic_cfg=0x0000, JTAG_freq = 0):
    #basic_cfc=0b0000000000000000  #[length] [] [busy] [RxEdge] [TxEdge] [MSB/LSB (lsb?)] [] [INVTCK] [] []
    set_JTAG_FREQ(JTAG_freq)
    set_JTAG_CONF(basic_cfg)


                        ########          #########
                        ######## END BLOC #########
                        ###########################
#================================================================================#

#================================================================================#
                        ###########################
                        #### JTAG CHAIN CONFIG ####
                        #####                 #####

def SetupChain(self, action = "PROGRAM"):
# 0. Reset -> idle
    print("\n ######### SETUP CHAIN ######### \n")
    reset_JS()
# 1. Move to shift IR
    mv_idle_shiftIR()

# 2. Bypass F2,F3 and program F1
    # at the end of IR => UpdateIR and go to shift_DR
    testbench.feb.feb_ctrl_and_status.sca.write_buffer_length(16)
    # CMD : 3FF = F1 0010 : PROGRAM (ISP/ICR) F2 1111 (BYPASS) F3 1111 (BYPASS)
    testbench.feb.feb_ctrl_and_status.sca.write_TDO_buf([0x000032FF,0,0,0]) # drive TD0 high while moving to shiftDR
    testbench.feb.feb_ctrl_and_status.sca.write_TMS_buf([0x00003800,0,0,0]) # last bit need TMS active to exitIR -> UpdateIR -> Select_DR -> captureDR -> shiftDR 
    testbench.feb.feb_ctrl_and_status.sca.start_JTAG_transmission()


def JTAG_configuration(file):
    pourcent = 0
    # 0. set Frequency, Edge detection, etc.
    #testbench.feb.feb_ctrl_and_status.sca.configure_JTAG_ctrl(freq)
    configure_JTAG_ctrl(basic_cfg = 0, JTAG_freq = 20000000)
    # 1. Load program instruction -> SetupChain : setup JTAG chain for each devices of the chain [BYPASS FPGAs 2,3 and program 1 ] and move to shiftDR
    testbench.feb.feb_ctrl_and_status.sca.SetupChain()
    # 2. In ShiftDR : Drive TDO High (Tinit) Init time times = 17408 ?!! ==> 136 * 128 bits
    # for i in range(136):
    #     pourcent = (i/136)*100
    #     sys.stdout.write("MAX JTAG_INIT_CLOCK: %d%%   \r" % (pourcent) )
    #     testbench.feb.feb_ctrl_and_status.sca.write_buffer_length(0)    
    #     testbench.feb.feb_ctrl_and_status.sca.write_TDO_buf([0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF])
    #     testbench.feb.feb_ctrl_and_status.sca.write_TMS_buf([0,0,0,0])
    #     testbench.feb.feb_ctrl_and_status.sca.start_JTAG_transmission()

    # 3. Dump configuration
    dump_config_from_file(file)
    # 4. Dump AKL (V1.7)

    # 5. Add aditionnal (128 Jrunner / 134 : cycloneV datasheet) TCK at the end of TDI to perform device initialisation
    ones = 0xFFFFFFFF
    testbench.feb.feb_ctrl_and_status.sca.write_buffer_length(0)  
    testbench.feb.feb_ctrl_and_status.sca.write_TDO_buf(ones,ones,ones,ones)
    testbench.feb.feb_ctrl_and_status.sca.write_TMS_buf([0,0,0,0])
    ## For cyclone V ? 134-128 = 6 additional bits
    testbench.feb.feb_ctrl_and_status.sca.write_buffer_length(6)  
    testbench.feb.feb_ctrl_and_status.sca.write_TDO_buf([0x3F,0,0,0])
    testbench.feb.feb_ctrl_and_status.sca.write_TMS_buf([0,0,0,0])

    # 6. updateDR
    testbench.feb.feb_ctrl_and_status.sca.write_buffer_length(2)    
    testbench.feb.feb_ctrl_and_status.sca.write_TDO_buf([0,0,0,0])
    testbench.feb.feb_ctrl_and_status.sca.write_TMS_buf([0b11,0,0,0])
    testbench.feb.feb_ctrl_and_status.sca.start_JTAG_transmission()

    # 7. Move to runidle
    reset_JS()
    set_TMS_commande(Reset,6)
    testbench.feb.feb_ctrl_and_status.sca.start_JTAG_transmission()



                        ########          #########
                        ######## END BLOC #########
                        ###########################
#================================================================================#

#================================================================================#
                        ###########################
                        ##### JTAG CHAIN TEST #####
                        #####                 #####
def reset_JS():
    # go to reset state (that loads IDCODE into IR of all the devices)
    set_TMS_commande(Reset,6)
    testbench.feb.feb_ctrl_and_status.sca.start_JTAG_transmission()

def mv_idle_shiftIR():
    # Go to shiftDR from reset => update and read data register IDCode
    set_TMS_commande(idle_shiftDR,3)
    testbench.feb.feb_ctrl_and_status.sca.start_JTAG_transmission()


def print_IDcode(): ### Valid : 9:18 20 oct on 100 loop ###
    # go to reset state (that loads IDCODE into IR of all the devices)
    print("\n ######### ID CODE ######### \n")
    set_TMS_commande(Reset,6)
    testbench.feb.feb_ctrl_and_status.sca.start_JTAG_transmission()

    # Go to shiftDR from reset => update and read data register IDCode
    set_TMS_commande(idle_shiftDR,3)
    testbench.feb.feb_ctrl_and_status.sca.start_JTAG_transmission()

    # push 32 bits to read IDcode
    testbench.feb.feb_ctrl_and_status.sca.write_buffer_length(98)
    testbench.feb.feb_ctrl_and_status.sca.write_TDO_buf([0,0,0,0])
    testbench.feb.feb_ctrl_and_status.sca.write_TMS_buf([0,0,0x80000000,0x3])
    testbench.feb.feb_ctrl_and_status.sca.start_JTAG_transmission()
    # Read
    r = testbench.feb.feb_ctrl_and_status.sca.read_TDI0_JTAG() # lecture : from MSB to LSB [10..00] => [00..01]
    r1 = testbench.feb.feb_ctrl_and_status.sca.read_TDI1_JTAG()
    r2 = testbench.feb.feb_ctrl_and_status.sca.read_TDI2_JTAG()
    r3 = testbench.feb.feb_ctrl_and_status.sca.read_TDI3_JTAG()
    print("ID_CODE = {}, {}, {}, {} \n".format(hex(r),hex(r1),hex(r2),hex(r3)))

def Bypass_all():
    print("\n ######### BYPASS ALL CHAIN ######### \n")
    # Reset JSM and go to IDLE
    set_TMS_commande(Reset,6)
    testbench.feb.feb_ctrl_and_status.sca.start_JTAG_transmission()
    # Go to shiftIR from idle
    set_TMS_commande(idle_shiftIR,4) 
    testbench.feb.feb_ctrl_and_status.sca.start_JTAG_transmission()

    # Send 1 to make all devices in bypass mode
    # at the end of IR => UpdateIR and go to shift_DR
    testbench.feb.feb_ctrl_and_status.sca.write_buffer_length(16)
    testbench.feb.feb_ctrl_and_status.sca.write_TDO_buf([0x000032FF,0,0,0])
    testbench.feb.feb_ctrl_and_status.sca.write_TMS_buf([0x00003800,0,0,0]) # last bit need TMS active to exit shift_IR
    testbench.feb.feb_ctrl_and_status.sca.start_JTAG_transmission()
    # Send static pattern
    testbench.feb.feb_ctrl_and_status.sca.write_buffer_length(96)
    testbench.feb.feb_ctrl_and_status.sca.write_TDO_buf([0xABABABAB,0xABABABAB,0xABABABAB,0])
    testbench.feb.feb_ctrl_and_status.sca.write_TMS_buf([0,0,0,0]) # leave Shift-DR at then end of transmition
    testbench.feb.feb_ctrl_and_status.sca.start_JTAG_transmission()
    # Read Data register (static pattern delay by number of ICs bypassed)
    r0 = testbench.feb.feb_ctrl_and_status.sca.read_TDI0_JTAG()
    r1 = testbench.feb.feb_ctrl_and_status.sca.read_TDI1_JTAG()
    r2 = testbench.feb.feb_ctrl_and_status.sca.read_TDI2_JTAG()
    r3 = testbench.feb.feb_ctrl_and_status.sca.read_TDI3_JTAG()
    print("TDO regiter = {},{},{},{} \n".format(hex(r0),hex(r1),hex(r2),hex(r3)))


                        ########          #########
                        ######## END BLOC #########
                        ###########################
#================================================================================#

#===================================================================================================================================
#======================================================= MAIN CODE =================================================================
#===================================================================================================================================
###_______________________________________________________###
# testbench.feb.feb_ctrl_and_status.deconfigure_FPGA()
# testbench.feb.feb_ctrl_and_status.configure_FPGA()
#testbench.feb.feb_ctrl_and_status.reset_FPGA()

# print(testbench.feb.feb_ctrl_and_status.get_fault("2V"))
# print(testbench.feb.feb_ctrl_and_status.get_fault("4V"))
###_______________________________________________________###


###_______________________________________________________###
## TEST SCA JTAG CTRL REG
data = 0x0000
testbench.feb.feb_ctrl_and_status.sca.set_JTAG_Ctrl(data)
print("\nControl reg = ",hex(testbench.feb.feb_ctrl_and_status.sca.get_JTAG_Ctrl()))


## Disable TDC and reset JTAG SCA
testbench.DisableTDC("all")
## Need when SCA return errors
#testbench.feb.feb_ctrl_and_status.sca.reset_JTAG()

## Set/Readback JTAG_freq  !!! 10MHz - 5MHz ==> stable 
set_JTAG_FREQ(10000000)
###_______________________________________________________###


#####################################################################################
# Après un restart complet de la carte : pas de problème pour IDcode et NbDevices.
# Après un certain temps : retour faux pour NbDevices, puis faux pour IDcode.
# NbDevice => mauvais état JSTap?
#####################################################################################

print("######################")
# dump_config_from_file(file)



### Test IDCODE & BYPASS cmd
#for i in range(3):
print_IDcode()

#for i in range(3):
Bypass_all()
#############################


## configure FPGA [TEST] 
#JTAG_configuration(file)



# testbench.feb.feb_ctrl_and_status.sca.write_buffer_length(4)
# cmd1=0b11111
# testbench.feb.feb_ctrl_and_status.sca.write_TD0_buf([0,0,0,0])
# testbench.feb.feb_ctrl_and_status.sca.write_TMS_buf([cmd1,0,0,0]) 
# testbench.feb.feb_ctrl_and_status.sca.start_JTAG_transmission()
#===================================================================================================================================
#===================================================================================================================================
#===================================================================================================================================


