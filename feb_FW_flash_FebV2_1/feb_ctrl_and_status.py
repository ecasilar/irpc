import time
import sys
import os
import types
from tools_box import *
from feb_sca import *


###---------------------------------------------###
#     This class control and status FEB reg.      #
###---------------------------------------------###
class feb_ctrl_and_status:

    def __init__(self,fc7_registers,verbose):
        self.verbose = verbose
        self.sca = feb_sca(fc7_registers,verbose)



      	# value,channel_adc,ratio,unit,underlimit,upperlimit
        self.powersupplies_value = {
        "RSSI"                : [0.0,0,1,"V",0,1],
        "4V_IN"               : [0.0,21,0.2,"V",3.8,4.2],
        "4V_IN_CURRENT"       : [0.0,13,0.2,"A",0,4],
        "2V5_SAFE"            : [0.0,5,0.33,"V",2.48,2.52],
        "3V3_VCCIO"           : [0.0,3,0.2,"V",3.28,3.34],
        "2V5_VCCIO"           : [0.0,2,0.33,"V",2.48,2.52],
        "VH_VCCIO_LEFT"       : [0.0,17,0.33,"V",1.40,1.55],
        "VH_VCCIO_MIDDLE"     : [0.0,18,0.33,"V",1.40,1.55],
        "VH_VCCIO_RIGHT"      : [0.0,19,0.33,"V",1.40,1.55],
        "2V_IN"               : [0.0,20,0.33,"V",1.8,2.2],
        "2V_IN_CURRENT"       : [0.0,12,0.1,"A",0,7], 
        "1V5_SAFE"            : [0.0,4,0.5,"V",1.48,1.52],
        "1V5_VCCIO"           : [0.0,1,0.5,"V",1.48,1.52],
        "1V1_CORE_LEFT"       : [0.0,6,0.5,"V",1.08,1.12],
        "1V1_CORE_MIDDLE"     : [0.0,7,0.5,"V",1.08,1.12],
        "1V1_CORE_RIGHT"      : [0.0,8,0.5,"V",1.08,1.12],
        "1V1_VCCE_LEFT"       : [0.0,9,0.5,"V",1.08,1.12],
        "1V1_VCCE_MIDDLE"     : [0.0,10,0.5,"V",1.08,1.12],
        "1V1_VCCE_RIGHT"      : [0.0,11,0.5,"V",1.08,1.12],
        "CURRENT_CORE_LEFT"   : [0.0,14,0.2,"A",0,3],
        "CURRENT_CORE_MIDDLE" : [0.0,15,0.2,"A",0,3],
        "CURRENT_CORE_RIGHT"  : [0.0,16,0.2,"A",0,3]}

        self.temperature_value = {
        "LM75_SENSOR1"        : [0.0,0x48,"°C",10,50],
        "LM75_SENSOR2"        : [0.0,0x49,"°C",10,50],
        "LM75_SENSOR3"        : [0.0,0x4A,"°C",10,50],
        "LM75_SENSOR4"        : [0.0,0x4B,"°C",10,50],
        "LM75_SENSOR5"        : [0.0,0x4C,"°C",10,50]}

    def init(self):
        
        self.sca.init()
        self.power_off_FPGA()
        self.shutdown_2v()
        self.shutdown_4v()
        self.power_up_2v()
        self.power_up_4v()
        self.power_on_FPGA()
        self.configure_FPGA()
        self.reset_FPGA()
        if self.verbose: self.print()
  

    ######### Misc functions ##############

    def sca_reset_cycle(self):
        #Reset high
        self.sca.set_GPIO(self.sca.GPIO_direction["SOFT_RESET_FPGA"][1],1)
        time.sleep(0.3)
        #Reset low
        self.sca.set_GPIO(self.sca.GPIO_direction["SOFT_RESET_FPGA"][1],0)


    def sca_nconfig_cycle(self):
        #Keep reset high
        self.sca.set_GPIO(self.sca.GPIO_direction["SOFT_RESET_FPGA"][1],1)
        #NCONFIG at '0'
        self.sca.set_GPIO(self.sca.GPIO_direction["NCONFIG_MIDDLE"][1],0)
        self.sca.set_GPIO(self.sca.GPIO_direction["NCONFIG_LEFT"][1],0)
        self.sca.set_GPIO(self.sca.GPIO_direction["NCONFIG_RIGHT"][1],0)
        #NCONFIG at '1'
        self.sca.set_GPIO(self.sca.GPIO_direction["NCONFIG_MIDDLE"][1],1)
        self.sca.set_GPIO(self.sca.GPIO_direction["NCONFIG_LEFT"][1],1)
        self.sca.set_GPIO(self.sca.GPIO_direction["NCONFIG_RIGHT"][1],1)
        #Perform reset cycle
        self.sca_reset_cycle()
 ####################################################################

    def ready(self):
        return self.sca.check_config()

    def get_powersupplies(self):
        nb=len(self.powersupplies_value)
        i=0
        for key in self.powersupplies_value.keys():
            self.powersupplies_value[key][0] = round((self.sca.read_adc(self.powersupplies_value[key][1]))/(pow(2,12)*self.powersupplies_value[key][2]),2)
            i = i + 1
            if self.verbose:
                print("Read FEB voltage and current : {}%".format(round((i/nb)*100),0),end="\r")
                sys.stdout.flush()
        if self.verbose==True: print("")
        return self.powersupplies_value

    def check_powersupplies(self):
        i=0
        for key,value in self.powersupplies_value.items():
            if (value[0]>=value[4] and value[0]<=value[5]):
                pass
            else:
                print((bcolors.FAIL + "Check powersupplies {} : {}{} [WARNING]"+ bcolors.ENDC).format(key,value[0],value[2]))
                i = i + 1
        if i==0: print(("Check powersupplies" + bcolors.GREEN + " [OK]" + bcolors.ENDC))

    def get_temperature(self):
        nb=len(self.temperature_value)
        i=0
        for key in self.temperature_value.keys():
            self.temperature_value[key][0] = (self.sca.read_i2c(1,self.temperature_value[key][1],0x00)>>23)/2
            i = i + 1
            if self.verbose==True:
                print("Read Temperature Sensors : {}%".format( round((i/nb)*100),0) ,end="\r")
                sys.stdout.flush()
        if self.verbose==True: print("")
        return self.temperature_value

    def check_temperature(self):
        i=0
        for key,value in self.temperature_value.items():
            if (value[0]>=value[3] and value[0]<=value[4]):
                pass
            else:
                print((bcolors.FAIL + "Check temperature sensors {} : {}{} [WARNING] "+ bcolors.ENDC).format(key,value[0],value[2]))
                i=i+1
        if i==0: print(("Check temperature sensors" + bcolors.GREEN + " [OK]" + bcolors.ENDC))

    def power_on_FPGA(self):
        self.sca.set_GPIO(self.sca.GPIO_direction["ENABLE_POWER_FPGA"][1],1)
        print(("Power_supplies FPGA ON"+ bcolors.GREEN +" [OK]" + bcolors.ENDC))

    def power_off_FPGA(self):
        self.sca.set_GPIO(self.sca.GPIO_direction["ENABLE_POWER_FPGA"][1],0)
        print((bcolors.FAIL + "Power_supplies FPGA OFF !" + bcolors.ENDC))

    def shutdown_2v(self):
        self.sca.set_GPIO(self.sca.GPIO_direction["SHDN_2V"][1],0)
        print((bcolors.FAIL+ "Shutdown 2V !" + bcolors.ENDC))

    def power_up_2v(self):
        self.sca.set_GPIO(self.sca.GPIO_direction["SHDN_2V"][1],1)
        print(("Powerup 2V"+bcolors.GREEN+" [OK]" + bcolors.ENDC))

    def shutdown_4v(self):
        self.sca.set_GPIO(self.sca.GPIO_direction["SHDN_4V"][1],0)
        print((bcolors.FAIL + "Shutdown 4V !" + bcolors.ENDC))

    def power_up_4v(self):
        self.sca.set_GPIO(self.sca.GPIO_direction["SHDN_4V"][1],1)
        print(("Powerup 4V"+ bcolors.GREEN+" [OK]" + bcolors.ENDC))

    def reset_FPGA(self):
        print((bcolors.WARNING + "Reset FPGA")	+ bcolors.ENDC)
        self.sca.set_GPIO(self.sca.GPIO_direction["SOFT_RESET_FPGA"][1],1)
        time.sleep(0.7)
        self.sca.set_GPIO(self.sca.GPIO_direction["SOFT_RESET_FPGA"][1],0)

    def configure_FPGA(self):
        self.sca.set_GPIO(self.sca.GPIO_direction["SOFT_RESET_FPGA"][1],1)
        self.sca.set_GPIO(self.sca.GPIO_direction["NCONFIG_MIDDLE"][1],1)
        print(("FPGA middle configured"+bcolors.GREEN+" [OK]" + bcolors.ENDC))
        time.sleep(0.9)
        self.sca.set_GPIO(self.sca.GPIO_direction["NCONFIG_LEFT"][1],1)
        print(("FPGA left configured"+bcolors.GREEN+" [OK]" + bcolors.ENDC))
        time.sleep(0.9)
        self.sca.set_GPIO(self.sca.GPIO_direction["NCONFIG_RIGHT"][1],1)
        print(("FPGA right configured"+bcolors.GREEN+" [OK]" + bcolors.ENDC))
        time.sleep(0.9)

    def deconfigure_FPGA(self):
        self.sca.set_GPIO(self.sca.GPIO_direction["NCONFIG_MIDDLE"][1],0)
        print((bcolors.FAIL + "FPGA middle deconfigured" + bcolors.ENDC))
        self.sca.set_GPIO(self.sca.GPIO_direction["NCONFIG_LEFT"][1],0)
        print((bcolors.FAIL + "FPGA left deconfigured" + bcolors.ENDC))
        self.sca.set_GPIO(self.sca.GPIO_direction["NCONFIG_RIGHT"][1],0)
        print((bcolors.FAIL + "FPGA right deconfigured" + bcolors.ENDC))

    def get_CRC_error(self,value):
        if value=="LEFT":
            return(int( bf(self.sca.get_GPIO_R())[self.sca.GPIO_direction["CRC_ERROR_LEFT"][1]]))
        elif value=="MIDDLE":
            return(int( bf(self.sca.get_GPIO_R())[self.sca.GPIO_direction["CRC_ERROR_MIDDLE"][1]]))
        elif value=="RIGHT":
            return(int( bf(self.sca.get_GPIO_R())[self.sca.GPIO_direction["CRC_ERROR_RIGHT"][1]]))
        else:
            print(bcolors.FAIL + "error CRC ERROR parameters" + bcolors.ENDC)

    def get_init_done(self,value):
        if value=="LEFT":
            return(int( bf(self.sca.get_GPIO_R())[self.sca.GPIO_direction["INIT_DONE_LEFT"][1]]))
        elif value=="MIDDLE":
            return(int( bf(self.sca.get_GPIO_R())[self.sca.GPIO_direction["INIT_DONE_MIDDLE"][1]]))
        elif value=="RIGHT":
            return(int( bf(self.sca.get_GPIO_R())[self.sca.GPIO_direction["INIT_DONE_RIGHT"][1]]))
        else:
            print(bcolors.FAIL + "error INIT_DONE parameters" + bcolors.ENDC)

    def get_fault(self,value):
        if value=="2V":
            return(int( bf(self.sca.get_GPIO_R())[self.sca.GPIO_direction["FAULT_2V"][1]]))
        elif value=="4V":
            return(int( bf(self.sca.get_GPIO_R())[self.sca.GPIO_direction["FAULT_4V"][1]]))
        else:
            print(bcolors.FAIL + "error FAULT parameters" + bcolors.ENDC)

    def get_temperature_alert(self,value):
        if value=="LEFT":
            return(int( bf(self.sca.get_GPIO_R())[self.sca.GPIO_direction["T_ALTERT_FPGA_LEFT"][1]]))
        elif value=="MIDDLE":
            return(int( bf(self.sca.get_GPIO_R())[self.sca.GPIO_direction["T_ALTERT_FPGA_MIDDLE"][1]]))
        elif value=="RIGHT":
            return(int( bf(self.sca.get_GPIO_R())[self.sca.GPIO_direction["T_ALTERT_FPGA_RIGHT"][1]]))
        elif value=="2V":
            return(int( bf(self.sca.get_GPIO_R())[self.sca.GPIO_direction["T_ALTERT_2V"][1]]))
        elif value=="4V":
            return(int( bf(self.sca.get_GPIO_R())[self.sca.GPIO_direction["T_ALTERT_4V"][1]]))
        else:
            print(bcolors.FAIL + "error Temperature alert parameters" + bcolors.ENDC)

 ## info
    def print(self):
        self.get_powersupplies()
        self.get_temperature()
        for key,value in self.powersupplies_value.items():
            time.sleep(0.1)
            if (value[0]>=value[4] and value[0]<=value[5]):
                print(("{} :" + bcolors.GREEN + " {}{} [OK]" + bcolors.ENDC).format(key,value[0],value[3]))
            else:
                print((bcolors.FAIL + "{} : {}{} [WARNING]" + bcolors.ENDC).format(key,value[0],value[3]))

        for key,value in self.temperature_value.items():
            time.sleep(0.1)
            if (value[0]>=value[3] and value[0]<=value[4]):
                print(("{} :" + bcolors.GREEN + " {}{} [OK]" + bcolors.ENDC).format(key,value[0],value[2]))
            else:
                print((bcolors.FAIL + "{} : {}{} [WARNING]" + bcolors.ENDC).format(key,value[0],value[2]))

        if self.get_CRC_error("LEFT")==0:
            print("CRC_ERROR_LEFT      : 0" + bcolors.GREEN + " [OK]" + bcolors.ENDC)
        else:
            print("CRC_ERROR_LEFT      : 1" + bcolors.FAIL + " [ERROR]" + bcolors.ENDC)
        if self.get_CRC_error("MIDDLE")==0:
            print("CRC_ERROR_MIDDLE    : 0" + bcolors.GREEN + " [OK]" + bcolors.ENDC)
        else:
            print("CRC_ERROR_MIDDLE    : 1" + bcolors.FAIL + " [ERROR]" + bcolors.ENDC)
        if self.get_CRC_error("RIGHT")==0:
            print("CRC_ERROR_RIGHT     : 0" + bcolors.GREEN + " [OK]" + bcolors.ENDC)
        else:
            print("CRC_ERROR_RIGHT     : 1" + bcolors.FAIL + "[ERROR]" + bcolors.ENDC)
        if self.get_init_done("LEFT")==1:
            print("INIT_DONE_LEFT      : 0" + bcolors.GREEN + " [OK]" + bcolors.ENDC)
        else:
            print("INIT_DONE_LEFT      : 1" + bcolors.FAIL + " [ERROR]" + bcolors.ENDC)
        if self.get_init_done("RIGHT")==1:
            print("INIT_DONE_RIGHT     : 0" + bcolors.GREEN + " [OK]" + bcolors.ENDC)
        else:
            print("INIT_DONE_RIGHT     : 1" + bcolors.FAIL + " [ERROR]" + bcolors.ENDC)
        if self.get_init_done("MIDDLE")==1:
            print("INIT_DONE_MIDDLE    : 0" + bcolors.GREEN + " [OK]" + bcolors.ENDC)
        else:
            print("INIT_DONE_MIDDLE    : 1" + bcolors.FAIL + " [ERROR]" + bcolors.ENDC)
        if self.get_fault("2V")==1:
            print("FAULT_2V#           : 1" + bcolors.GREEN + " [OK]" + bcolors.ENDC)
        else:
            print("FAULT_2V#           : 0" + bcolors.FAIL + " [ERROR]" + bcolors.ENDC)
        if self.get_fault("4V")==1:
            print("FAULT_4V#           : 1" + bcolors.GREEN + " [OK]" + bcolors.ENDC)
        else:
            print("FAULT_4V#           : 0" + bcolors.FAIL + " [ERROR]" + bcolors.ENDC)
        if self.get_temperature_alert("4V")==1:
            print("TEMP. ALERT 4V#     : 1" + bcolors.GREEN + " [OK]" + bcolors.ENDC)
        else:
            print("TEMP. ALERT 4V#     : 0" + bcolors.FAIL + " [ERROR]" + bcolors.ENDC)
        if self.get_temperature_alert("2V")==1:
            print("TEMP. ALERT 2V#     : 1" + bcolors.GREEN + " [OK]" + bcolors.ENDC)
        else:
            print("TEMP. ALERT 2V#     : 0" + bcolors.FAIL + " [ERROR]" + bcolors.ENDC)
        if self.get_temperature_alert("LEFT")==1:
            print("TEMP. ALERT LEFT#   : 1" + bcolors.GREEN + " [OK]" + bcolors.ENDC)
        else:
            print("TEMP. ALERT LEFT#   : 0" + bcolors.FAIL + " [ERROR]" + bcolors.ENDC)
        if self.get_temperature_alert("4V")==1:
            print("TEMP. ALERT MIDDLE# : 1" + bcolors.GREEN + " [OK]" + bcolors.ENDC)
        else:
            print("TEMP. ALERT MIDDLE# : 0" + bcolors.FAIL + " [ERROR]" + bcolors.ENDC)
        if self.get_temperature_alert("RIGHT")==1:
            print("TEMP. ALERT RIGHT#  : 1" + bcolors.GREEN + " [OK]" + bcolors.ENDC)
        else:
            print("TEMP. ALERT RIGHT#  : 0" + bcolors.FAIL + " [ERROR]" + bcolors.ENDC)
