#import uhal
import time
import sys
import os
import types
from tools_box import *
from feb_fpga import *

###---------------------------------------------###
#     This class control and status FPGAs  TDC    #
###---------------------------------------------###
class feb_tdc:

    def __init__(self,feb_fpga,fpga_name,verbose):
        self.verbose = verbose
        self.feb_fpga = feb_fpga
        self.fpga_name = fpga_name

######################################################################################
    def init(self,verbose):
        if verbose: print("Initialize all FPGAs TDCs (all disabled)...")
        for fpga in self.fpga_name:
            self.clear_enable(fpga)
            self.clear_cmd_valid(fpga)
            self.channel_disable(fpga,"all")

######################################################################################
    def set_enable(self,fpga_ref):
        self.feb_fpga.write(fpga_ref,"TDC_ENABLE",1)

    def clear_enable(self,fpga_ref):
        self.feb_fpga.write(fpga_ref,"TDC_ENABLE",0)

    def get_enable_status(self,fpga_ref):
        print(self.feb_fpga.read(fpga_ref,"TDC_ENABLE",1))
        if self.feb_fpga.read(fpga_ref,"TDC_ENABLE",1)==1:
            return "ENABLE"
        else:
            return "DISABLE"

    ##############################################################################

    def set_cmd_valid(self,fpga_ref):
        self.feb_fpga.write(fpga_ref,"TDC_CMD_VALID",1)

    def clear_cmd_valid(self,fpga_ref):
        self.feb_fpga.write(fpga_ref,"TDC_CMD_VALID",0)

    def get_cmd_valid_status(self,fpga_ref):
        if self.feb_fpga.read(fpga_ref,"TDC_CMD_VALID",1)==1:
            return "ENABLE"
        else:
            return "DISABLE"

    ##############################################################################

    def channel_enable(self,fpga_ref,channel):
        self.channel_ctrl(fpga_ref,channel,1)

    def channel_disable(self,fpga_ref,channel):
        self.channel_ctrl(fpga_ref,channel,0)

    def get_channel_status(self,fpga_ref):
        r0=bf(self.feb_fpga.read(fpga_ref,"TDC_ENABLE_CH0_15",1))
        r1=bf(self.feb_fpga.read(fpga_ref,"TDC_ENABLE_CH16_31",1))
        r2=bf(self.feb_fpga.read(fpga_ref,"TDC_ENABLE_CH32_33",1))
        r = []
        for i in range(16):
            r.append(int(r0[i]))
        for i in range(16):
            r.append(int(r1[i]))
        for i in range(2):
            r.append(int(r2[i]))
        return [int(r0),int(r1),int(r2[0:2]),r]

    def channel_ctrl(self,fpga_ref,channel,value):
        r=self.get_channel_status(fpga_ref)
        r0 = bf(r[0])
        r1 = bf(r[1])
        r2 = bf(r[2])

        if isinstance(channel,list):
            for ch in channel:
                if ch<16:
                    r0[ch]=value
                elif ch>15 and ch<32:
                    r1[ch-16]=value
                elif ch>31 and ch<34:
                    r2[ch-32]=value
                else:
                    raise(ValueError("Invalid TDC channel list"))
        elif isinstance(channel,str):
            if channel=="all" and value==1:
                r0[0:16]=0xFFFF
                r1[0:16]=0xFFFF
                r2[0:16]=0x0003

            elif channel=="all" and value==0:
                r0[0:16]=0x0000
                r1[0:16]=0x0000
                r2[0:16]=0x0000

        else:
            raise(ValueError("TDC channel must be a list"))

        self.feb_fpga.write(fpga_ref,"TDC_ENABLE_CH0_15",int(r0))
        self.feb_fpga.write(fpga_ref,"TDC_ENABLE_CH16_31",int(r1))
        self.feb_fpga.write(fpga_ref,"TDC_ENABLE_CH32_33",int(r2))

    def set_tdc_injection_mode(self, fpga_ref, value):
        self.feb_fpga.write(fpga_ref,"TDC_INJECTION_MODE", int(value))

    def get_tdc_injection_mode(self, fpga_ref):
        return self.feb_fpga.read(fpga_ref,"TDC_INJECTION_MODE",1)

    def print(self,fpga_ref):
        r=self.get_channel_status(fpga_ref)[3]
        tdc_channel_list = []
        for index,value in enumerate(r):
            if value==1:
                tdc_channel_list.append(index)
        print("**********************")
        print("*** TDC PARAMETERS ***")
        print("**********************")
        print("FPGA                 : {}".format(fpga_ref))
        print("TDC_ENABLE           : {}".format(self.get_enable_status(fpga_ref)))
        print("TDC_CMD_VALID        : {}".format(self.get_cmd_valid_status(fpga_ref)))
        print("TDC_CHANNEL_ENABLE   : {}".format(tdc_channel_list))



if __name__ == '__main__':
    obj = feb_tdc(False,False)
    print(obj.lookup_strip(["all"]))
    print(obj.lookup_strip(["strip_0","strip_17","strip_40","strip_20","strip_42","strip_32"]))
