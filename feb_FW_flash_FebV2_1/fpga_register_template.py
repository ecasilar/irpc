import time
import sys
import os
import types
from tools_box import *

class feb_fpga_register_template:

    def __init__(self,name,address,mode,length,size,value):
        self.name = name
        self.address = address
        self.mode = mode
        self.length = length
        self.size = size
        self.value = value
        
    def get_value(self):
        return self.value

    def set_value(self,value):
        self.value = value

    def get_value_bit(self,bit_start,bit_end):
        if ((bit_start>=0 and bit_start<self.length) and (bit_end>=0 and bit_end<self.length) and bit_end>=bit_start) :
            return int(bf(self.value)[bit_start:(bit_end+1)])
        else:
            raise(ValueError("bit number invalid"))

    def set_value_bit(self,bit_start,bit_end,value):
        if ((bit_start>=0 and bit_start<self.length) and (bit_end>=0 and bit_end<self.length) and bit_end>=bit_start) :
            r = self.value
            r[bit_start:(bit_end+1)] = value
            self.value = int(r)
        else:
            raise(ValueError("bit number invalid"))



if __name__ == '__main__':
    reg1 = feb_fpga_register_template("GENERAL",0x0000,"rw",16,1,0x4455)
    print(hex(reg1.get_value()))
    print(hex(reg1.get_value_bit(12,15)))

