import uhal
import time
import sys
import os
import types
from fc7 import *
from fc7_ipbus import *
from tools_box import *

class fc7_registers:

    def __init__(self,ipbus,verbose):
        self.verbose = verbose
        self.ipbus_device = ipbus
        self.ipbus_device.connect()

  #######################################################################
  ## FC7 SYSTEM REGITERS
  #######################################################################
  # READ REGISTERS
  #######################################################################
    def get_boardID(self):
        r = bf(self.ipbus_device.ReadReg("BOARD_ID"))
        return chr(r[24:32]) + chr(r[16:24]) + chr(r[8:16]) + chr(r[0:8])
    def get_revID(self):
        r = bf(self.ipbus_device.ReadReg("REV_ID"))
        return chr(r[24:32]) + chr(r[16:24]) + chr(r[8:16]) + chr(r[0:8])
    def get_version(self):
        r = bf(self.ipbus_device.ReadReg("VERSION"))
        return str(int(r[28:32])) + "." + str(int(r[24:28])) + "." + str(int(r[16:24])) + " // " + str(int(r[0:5])) + "/" + str(int(r[5:9])) + "/20" + str(int(r[9:16]))
    def get_xpoint(self):
        return self.ipbus_device.ReadReg("XPOINT")
    def get_cdce_status(self):
        return self.ipbus_device.ReadReg("CDCE_STATUS")
    def get_cdce_spi_rx_data(self):
        return self.ipbus_device.ReadReg("CDCE_SPI_RXDATA")
    def get_cdce_spi_tx_data(self):
        return self.ipbus_device.ReadReg("CDCE_SPI_TXDATA")
    def get_cdce_spi_command(self):
        return self.ipbus_device.ReadReg("CDCE_SPI_COMMAND")
    def get_i2c_settings(self):
        return self.ipbus_device.ReadReg("I2C_SETTINGS")
    def get_i2c_command(self):
        return self.ipbus_device.ReadReg("I2C_COMMAND")
    def get_i2c_reply(self):
        return self.ipbus_device.ReadReg("I2C_REPLY")
    def get_FMC_settings(self):
        return self.ipbus_device.ReadReg("FMC_REG")
    def get_MAC_settings(self):
        return self.ipbus_device.ReadReg("MAC_REG")
    def get_EUI_1_settings(self):
        return self.ipbus_device.ReadReg("EUI_1")
    def get_EUI_2_settings(self):
        return self.ipbus_device.ReadReg("EUI_2")
    def get_PCA8574_status(self):
        return self.ipbus_device.ReadReg("STATUS_PCA8574")
    def get_eeprom_rw(self):
        return self.ipbus_device.ReadReg("EEPROM_RW")

    #######################################################################
    ## FC7 SYSTEM REGITERS
    #######################################################################
    # WRITE REGISTERS
    #######################################################################
    def set_xpoint(self,value):
        return self.ipbus_device.WriteReg("XPOINT",value)
    def set_cdce_status(self,value):
        return self.ipbus_device.WriteReg("CDCE_STATUS",value)
    def set_cdce_spi_tx_data(self,value):
        return self.ipbus_device.WriteReg("CDCE_SPI_TXDATA",value)
    def set_cdce_spi_command(self,value):
        return self.ipbus_device.WriteReg("CDCE_SPI_COMMAND",value)
    def set_i2c_settings(self,value):
        return self.ipbus_device.WriteReg("I2C_SETTINGS",value)
    def set_i2c_command(self,value):
        return self.ipbus_device.WriteReg("I2C_COMMAND",value)
    def set_FMC_settings(self,value):
        return self.ipbus_device.WriteReg("FMC_REG",value)
    def set_eeprom_rw(self,value):
        return self.ipbus_device.WriteReg("EEPROM_RW",value)

  #######################################################################
  ## FC7 USER REGITERS
  #######################################################################
  # READ REGISTERS
  #######################################################################
    def get_general_register(self):
        return self.ipbus_device.ReadReg("GENERAL")
    
    ## Fifos
    #########
    def get_fifos_ctrl(self):
        return self.ipbus_device.ReadReg("FIFOs_ctrl")
    def get_fifos_status(self):
        return self.ipbus_device.ReadReg("FIFOs_status")
    def get_rx_fifo_SC_block(self,nb_words):
        return self.ipbus_device.ReadBlock("USER_OUTPUT_FIFO_SC",nb_words)
    def get_rx_fifo_SC_single(self):
        return self.ipbus_device.ReadReg("USER_OUTPUT_FIFO_SC")
    def get_rx_fifo_TDC_block(self,nb_words):
        return self.ipbus_device.ReadBlock("USER_OUTPUT_FIFO_TDC",nb_words)
    def get_rx_fifo_TDC_single(self):
        return self.ipbus_device.ReadReg("USER_OUTPUT_FIFO_TDC")
    def get_fifo_frame_nb_words(self):
        return self.ipbus_device.ReadReg("FIFO_Frame_Nb_Words")
    
    ## GBT
    #########
    def get_config_gbt_fpga(self):
        return self.ipbus_device.ReadReg("CONFIG_GBT_FC7")
    def get_status_gbt_fpga(self):
        return self.ipbus_device.ReadReg("STATUS_GBT_FC7")
    
    ## SCA
    #########
    def get_sca_config(self):
        reg1= self.ipbus_device.ReadReg("SCA_configs_1")
        reg2= self.ipbus_device.ReadReg("SCA_configs_2")
        reg3= self.ipbus_device.ReadReg("SCA_configs_3")
        return [reg1,reg2,reg3]
    def get_sca_status(self):
        reg1= self.ipbus_device.ReadReg("SCA_status_1")
        reg2= self.ipbus_device.ReadReg("SCA_status_2")
        reg3= self.ipbus_device.ReadReg("SCA_status_3")
        return [reg1,reg2,reg3]
    def get_sca_pulse(self):
      return self.ipbus_device.ReadReg("SCA_pulse_sigs")
  
    ## IC
    #########
    def get_ic_pulse(self):
        return self.ipbus_device.ReadReg("IC_pulse_sigs")
    def get_ic_status(self):
        return self.ipbus_device.ReadReg("IC_status")
    def get_ic_config(self):
        reg1 = self.ipbus_device.ReadReg("IC_configs_1")
        reg2 = self.ipbus_device.ReadReg("IC_configs_2")
        return [reg1,reg2]
    
    ## MODE reception
    #########
    def get_time_window_size(self):
        return self.ipbus_device.ReadReg("Time_Window_Size")
    def get_circular_buffer_size(self):
        return self.ipbus_device.ReadReg("Circular_Buffer_Size")
    def get_counter_trigger(self):
        return self.ipbus_device.ReadReg("COUNTER_TRIGGER")
    
    ## MODE transmission
    #########
    def get_FC_transmission(self):
        return self.ipbus_device.ReadReg("FC_transmission")
        
    def get_BC0_Resync_period(self):
        return self.ipbus_device.ReadReg("FC_BC0_Resync_period")
    
    ## BC0 cosmic transmission config
    #########
    def get_BC0_cosmic_config(self):
        return self.ipbus_device.ReadReg("BC0_cosmic_config")

    def get_BC0_cosmic_stages(self):
        return self.ipbus_device.ReadReg("BC0_state_cosmic")

    ## Trigger_fmc l12
    #########
    def get_trigger_out_fmc(self):
        return self.ipbus_device.ReadReg("trigger_out_fmc")

    

  #######################################################################
  ## FC7 USER REGITERS
  #######################################################################
  # WRITE REGISTERS
    def set_general_register(self,value):
      	self.ipbus_device.WriteReg("GENERAL",value)
          
    ## fifos
    ########
    def set_fifos_ctrl(self,value):
        self.ipbus_device.WriteReg("FIFOs_ctrl",value)
    def set_tx_fifo_SC_single(self,value):
        return self.ipbus_device.WriteReg("USER_INPUT_FIFO_SC",value)
    def set_tx_fifo_SC_block(self,value):
        return self.ipbus_device.WriteBlock("USER_INPUT_FIFO_SC",value)

    ## GBT
    ########
    def set_config_gbt_fpga(self,value):
        self.ipbus_device.WriteReg("CONFIG_GBT_FC7",value)

    ## SCA
    ########
    def set_sca_config(self,reg):
        self.ipbus_device.WriteReg("SCA_configs_1",reg[0])
        self.ipbus_device.WriteReg("SCA_configs_2",reg[1])
        self.ipbus_device.WriteReg("SCA_configs_3",reg[2])
    def set_sca_pulse(self,value):
        self.ipbus_device.WriteReg("SCA_pulse_sigs",value)

    ## IC
    ########
    def set_ic_pulse(self,value):
        self.ipbus_device.WriteReg("IC_pulse_sigs",value)
    def set_ic_config(self,reg):
        reg1 = self.ipbus_device.ReadReg("IC_configs_1",reg[0])
        reg2 = self.ipbus_device.ReadReg("IC_configs_2",reg[1])
        return [reg1,reg2]

    ## MODE
    ########
    def set_time_window_size(self,value):
        return self.ipbus_device.WriteReg("Time_Window_Size",value)
    def set_circular_buffer_size(self,value):
        r=bf(self.get_circular_buffer_size())
        r[0:17]=bf(value)[0:17]
        return self.ipbus_device.WriteReg("Circular_Buffer_Size",int(r))
    def set_BC0_period(self, value):
        r=bf(self.get_BC0_Resync_period())
        r[0:15]=bf(value)[0:15]
        return self.ipbus_device.WriteReg("FC_BC0_Resync_period", int(r))

    def set_resync_period(self, value):
        r=bf(self.get_BC0_Resync_period())
        r[15:30]=bf(value)[0:15]
        return self.ipbus_device.WriteReg("FC_BC0_Resync_period", int(r))
    def set_FC_transmission(self, value):
        self.ipbus_device.WriteReg("FC_transmission",value)
    

    ## BC0 cosmic transmission config
    #########
    def set_BC0_cosmic_config(self, value):
        return self.ipbus_device.WriteReg("BC0_cosmic_config",value)

    def set_BC0_cosmic_stages(self, value):
        return self.ipbus_device.WriteReg("BC0_state_cosmic", value)




    ## BC0 cosmics tests
    def set_BC0_reset_acq_val(self,value): # reset BC0 cosmic transmission when counter reach this value. [By default, the counter reach the BC0 period value [89 us] this value can be set at a pourcent of BC0 period]
        r=bf(self.get_BC0_cosmic_config())
        r[0:15]=bf(value)[0:15]
        return self.ipbus_device.WriteReg("BC0_cosmic_config",int(r))

    def set_Delay_resync_afterTrigger(self, value): # send Resync by Fast control, this is the waiting time after the Resync sig is sent.
        r=bf(self.get_BC0_cosmic_config())
        r[15:31]=bf(value)[0:16]
        return self.ipbus_device.WriteReg("BC0_cosmic_config",int(r))

    def set_BC0_N_orbit(self, value):
        r=bf(self.get_FC_transmission())
        r[21:29] = bf(value)[0:7]
        return self.ipbus_device.WriteReg("FC_transmission",int(r))

    def set_start_orbit(self, value): 
        r=bf(self.get_BC0_cosmic_config())
        r[31]=bf(value)[0]
        return self.ipbus_device.WriteReg("BC0_cosmic_config",int(r))

    def set_BC0_cosmic_stage1(self, value): 
        r=bf(self.get_BC0_cosmic_stages())
        r[0:7]=bf(value)[0:7]
        return self.ipbus_device.WriteReg("BC0_state_cosmic",int(r))
    def set_BC0_cosmic_stage2(self, value): 
        r=bf(self.get_BC0_cosmic_stages())
        r[8:15]=bf(value)[0:7]
        return self.ipbus_device.WriteReg("BC0_state_cosmic",int(r))
    def set_BC0_cosmic_stage3(self, value): 
        r=bf(self.get_BC0_cosmic_stages())
        r[16:23]=bf(value)[0:7]
        return self.ipbus_device.WriteReg("BC0_state_cosmic",int(r))
    def set_BC0_cosmic_stage4(self, value):
        r=bf(self.get_BC0_cosmic_stages())
        r[24:31]=bf(value)[0:7]
        return self.ipbus_device.WriteReg("BC0_state_cosmic",int(r))


    ## Trigger_fmc l12
    def set_trigger_out_fmc(self,value):
        return self.ipbus_device.WriteReg("trigger_out_fmc", value)
    def set_trigger_out_fmc_2(self,value):
        return self.ipbus_device.WriteReg("trigger_out_fmc_2", value)

        

  #######################################################################
  ## FC7 BOARD FUNCTIONS
  #######################################################################

    def init(self):
  	     if self.verbose : self.info()

    def info(self):
        print("################################################")
        print("Board ID : {}".format(self.get_boardID()))
        print("Rev ID   : {}".format(self.get_revID()))
        print("Version  : {}".format(self.get_version()))



if __name__ == '__main__':
    Obj = fc7(True)
    Obj.fpga_registers.get_fifos_status()
    Obj.fpga_registers.get_rx_fifo_TDC_single()