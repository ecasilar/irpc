import time
import sys
import os
import types
from tools_box import *
from feb_fpga import *
from feb_tdc import *
from feb_asics import *
from feb_ctrl_and_status import *


###---------------------------------------------###
# This class is the main class of FEB board       #
###---------------------------------------------###

class feb_v2_cycloneV_r1:

    def __init__(self,fc7,verbose):
        self.verbose = verbose
        self.fc7 = fc7
        self.feb_ctrl_and_status =feb_ctrl_and_status(self.fc7.fpga_registers,verbose)

        self.fpga_name = ["fpga_0","fpga_1","fpga_2"]
        self.fpga_list = {
        self.fpga_name[0] : 1,
        self.fpga_name[1] : 2,
        self.fpga_name[2] : 4,
        "all" : 7,
        }

        self.fpga = feb_fpga(fc7,self.fpga_list)
        self.tdc = feb_tdc(self.fpga,self.fpga_name,verbose)

        self.asic_list = ["top","bottom"]
        self.asic = feb_asics(self.fpga,self.fpga_name,verbose)

        self.index_lut_fpga =0
        self.index_lut_asic =1
        self.index_lut_asic_channel =2
        self.index_lut_tdc_channel =3
        
        self.lookuptable_strips = {
        "r_strip_0"     : ["fpga_0","bottom",0,16],
        "strip_0"       : ["fpga_0","bottom",2,17],
        "r_strip_1"     : ["fpga_0","bottom",4,18],
        "strip_1"       : ["fpga_0","bottom",6,19],
        "r_strip_2"     : ["fpga_0","bottom",8,20],
        "strip_2"       : ["fpga_0","bottom",10,21],
        "r_strip_3"     : ["fpga_0","bottom",12,22],
        "strip_3"       : ["fpga_0","bottom",14,23],
        "r_strip_4"     : ["fpga_0","bottom",16,24],
        "strip_4"       : ["fpga_0","bottom",18,25],
        "r_strip_5"     : ["fpga_0","bottom",20,26],
        "strip_5"       : ["fpga_0","bottom",22,27],
        "r_strip_6"     : ["fpga_0","bottom",24,28],
        "strip_6"       : ["fpga_0","bottom",26,29],
        "r_strip_7"     : ["fpga_0","bottom",28,30],
        "strip_7"       : ["fpga_0","bottom",30,31],
        "r_strip_8"     : ["fpga_0","top",30,15],
        "strip_8"       : ["fpga_0","top",28,14],
        "r_strip_9"     : ["fpga_0","top",26,13],
        "strip_9"       : ["fpga_0","top",24,12],
        "r_strip_10"    : ["fpga_0","top",22,11],
        "strip_10"      : ["fpga_0","top",20,10],
        "r_strip_11"    : ["fpga_0","top",18,9],
        "strip_11"      : ["fpga_0","top",16,8],
        "r_strip_12"    : ["fpga_0","top",14,7],
        "strip_12"      : ["fpga_0","top",12,6],
        "r_strip_13"    : ["fpga_0","top",10,5],
        "strip_13"      : ["fpga_0","top",8,4],
        "r_strip_14"    : ["fpga_0","top",6,3],
        "strip_14"      : ["fpga_0","top",4,2],
        "r_strip_15"    : ["fpga_0","top",2,1],
        "strip_15"      : ["fpga_0","top",0,0],

        "r_strip_16"    : ["fpga_1","bottom",0,16],
        "strip_16"      : ["fpga_1","bottom",2,17],
        "r_strip_17"    : ["fpga_1","bottom",4,18],
        "strip_17"      : ["fpga_1","bottom",6,19],
        "r_strip_18"    : ["fpga_1","bottom",8,20],
        "strip_18"      : ["fpga_1","bottom",10,21],
        "r_strip_19"    : ["fpga_1","bottom",12,22],
        "strip_19"      : ["fpga_1","bottom",14,23],
        "r_strip_20"    : ["fpga_1","bottom",16,24],
        "strip_20"      : ["fpga_1","bottom",18,25],
        "r_strip_21"    : ["fpga_1","bottom",20,26],
        "strip_21"      : ["fpga_1","bottom",22,27],
        "r_strip_22"    : ["fpga_1","bottom",24,28],
        "strip_22"      : ["fpga_1","bottom",26,29],
        "r_strip_23"    : ["fpga_1","bottom",28,30],
        "strip_23"      : ["fpga_1","bottom",30,31],
        "r_strip_24"    : ["fpga_1","top",30,15],
        "strip_24"      : ["fpga_1","top",28,14],
        "r_strip_25"    : ["fpga_1","top",26,13],
        "strip_25"      : ["fpga_1","top",24,12],
        "r_strip_26"    : ["fpga_1","top",22,11],
        "strip_26"      : ["fpga_1","top",20,10],
        "r_strip_27"    : ["fpga_1","top",18,9],
        "strip_27"      : ["fpga_1","top",16,8],
        "r_strip_28"    : ["fpga_1","top",14,7],
        "strip_28"      : ["fpga_1","top",12,6],
        "r_strip_29"    : ["fpga_1","top",10,5],
        "strip_29"      : ["fpga_1","top",8,4],
        "r_strip_30"    : ["fpga_1","top",6,3],
        "strip_30"      : ["fpga_1","top",4,2],
        "r_strip_31"    : ["fpga_1","top",2,1],
        "strip_31"      : ["fpga_1","top",0,0],

        "r_strip_32"    : ["fpga_2","bottom",0,16],
        "strip_32"      : ["fpga_2","bottom",2,17],
        "r_strip_33"    : ["fpga_2","bottom",4,18],
        "strip_33"      : ["fpga_2","bottom",6,19],
        "r_strip_34"    : ["fpga_2","bottom",8,20],
        "strip_34"      : ["fpga_2","bottom",10,21],
        "r_strip_35"    : ["fpga_2","bottom",12,22],
        "strip_35"      : ["fpga_2","bottom",14,23],
        "r_strip_36"    : ["fpga_2","bottom",16,24],
        "strip_36"      : ["fpga_2","bottom",18,25],
        "r_strip_37"    : ["fpga_2","bottom",20,26],
        "strip_37"      : ["fpga_2","bottom",22,27],
        "r_strip_38"    : ["fpga_2","bottom",24,28],
        "strip_38"      : ["fpga_2","bottom",26,29],
        "r_strip_39"    : ["fpga_2","bottom",28,30],
        "strip_39"      : ["fpga_2","bottom",30,31],
        "r_strip_40"    : ["fpga_2","top",30,15],
        "strip_40"      : ["fpga_2","top",28,14],
        "r_strip_41"    : ["fpga_2","top",26,13],
        "strip_41"      : ["fpga_2","top",24,12],
        "r_strip_42"    : ["fpga_2","top",22,11],
        "strip_42"      : ["fpga_2","top",20,10],
        "r_strip_43"    : ["fpga_2","top",18,9],
        "strip_43"      : ["fpga_2","top",16,8],
        "r_strip_44"    : ["fpga_2","top",14,7],
        "strip_44"      : ["fpga_2","top",12,6],
        "r_strip_45"    : ["fpga_2","top",10,5],
        "strip_45"      : ["fpga_2","top",8,4],
        "r_strip_46"    : ["fpga_2","top",6,3],
        "strip_46"      : ["fpga_2","top",4,2],
        "r_strip_47"    : ["fpga_2","top",2,1],
        "strip_47"      : ["fpga_2","top",0,0]
        }

    def init(self):
        if self.feb_ctrl_and_status.ready():
            print("FEB already configured : reloading SCA configuration...")
            self.feb_ctrl_and_status.init()
        else:
            print("FEB not configured : loading SCA configuration...")
            self.feb_ctrl_and_status.init()

    def reset(self):
        self.feb_ctrl_and_status.reset_FPGA()

    def init_configuration(self,strip_list,verbose):
        self.tdc.init(verbose)
        self.asic.init(verbose)
        enable_channel_tdc_asic_list ={
        self.fpga_name[0] : [[],[],[]],
        self.fpga_name[1] : [[],[],[]],
        self.fpga_name[2] : [[],[],[]]
        }
        # generate data for config TDC ans ASICS
        for strip in strip_list:
            if strip in self.lookuptable_strips.keys():
                if self.lookuptable_strips[strip][self.index_lut_asic]=="top":
                    enable_channel_tdc_asic_list[self.lookuptable_strips[strip][self.index_lut_fpga]][0].append(self.lookuptable_strips[strip][self.index_lut_asic_channel])
                elif self.lookuptable_strips[strip][self.index_lut_asic]=="bottom":
                    enable_channel_tdc_asic_list[self.lookuptable_strips[strip][self.index_lut_fpga]][1].append(self.lookuptable_strips[strip][self.index_lut_asic_channel])

                enable_channel_tdc_asic_list[self.lookuptable_strips[strip][self.index_lut_fpga]][2].append(self.lookuptable_strips[strip][self.index_lut_tdc_channel])
            else:
                raise(ValueError("Strip name invalid"))

        #Configure TDCs
        for key,param in enable_channel_tdc_asic_list.items():
            if param[2]!=[]:
                self.tdc.clear_cmd_valid(key)
                self.tdc.set_enable(key)
                self.tdc.set_cmd_valid(key)
                self.tdc.channel_enable(key,param[2])

            if verbose : self.tdc.print(key)

        #Configure ASICS
        for key in enable_channel_tdc_asic_list.keys():

            if enable_channel_tdc_asic_list[key][0]!=[]:
                self.asic.enable_channel(key,"top",enable_channel_tdc_asic_list[key][0])
                self.asic.configure(key,"top")
                self.asic.start_nor32raz_fsm(key,"top")
                if verbose : self.asic.print(key,"top")

            if enable_channel_tdc_asic_list[key][1]!=[]:
                self.asic.enable_channel(key,"bottom",enable_channel_tdc_asic_list[key][1])
                self.asic.configure(key,"bottom")
                self.asic.start_nor32raz_fsm(key,"bottom")
                if verbose : self.asic.print(key,"bottom")

    def configure_10b_dac(self,strip,value):
        if strip in self.lookuptable_strips.keys():
            fpga_ref = self.lookuptable_strips[strip][self.index_lut_fpga]
            asic_ref = self.lookuptable_strips[strip][self.index_lut_asic]
        else:
            raise(ValueError("Strip name invalid"))
        self.asic.set_10b_dac(fpga_ref,asic_ref,value)
        self.asic.configure(fpga_ref,asic_ref)
    
    def configure_6b_dac(self,strip,value):
        if strip in self.lookuptable_strips.keys():
            fpga_ref = self.lookuptable_strips[strip][self.index_lut_fpga]
            asic_ref = self.lookuptable_strips[strip][self.index_lut_asic]
            
            channel_ref = self.lookuptable_strips[strip][self.index_lut_asic_channel]

            
            self.asic.set_6b_dac(fpga_ref,asic_ref,channel_ref,value)
            self.asic.configure(fpga_ref,asic_ref)
        else:
            raise(ValueError("Strip name invalid"))

