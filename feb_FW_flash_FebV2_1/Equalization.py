#Make this code Python 2 and 3 compatible
from __future__ import print_function

import uhal
import time
import sys
import os
import types
import cProfile

from fc7 import *
from feb_v2_cycloneV_r1 import *

from feb_ic import *
from feb_sca import *

import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt

#Structure which memorize an unique runID and the figID for plot saving
FIG_ID=[int(time.time()*1000),0]

#Constant definitions
MID   = int("010",2) #2
LEFT  = int("001",2) #1
RIGHT = int("100",2) #4

def receive_SC_frame():
  SC_frames = []
  r = project.fpga_registers.get_rx_fifo_SC_block(project.get_nb_word_in_fifo_SC())
  for i in range(0,len(r),4):
    SC_frames.append(( (r[i]<<96) + (r[i+1]<<64) +(r[i+2]<<32) +r[i+3])>>16 )
    #print(hex(SC_frames[-1]))
  return UL.decode_SC_frame(SC_frames)

def send_SC_frame(target, rw, address, data):
  project.fpga_registers.set_tx_fifo_SC_block(DL.sc_frame_format_32bits(DL.sc_frame_request(target,rw,address,data)))

def test_com_SCA(sca_reg):
  print("-"*80)
  print("SCA EC communication test")

  #######Reset GPIO values
  sca_reg.set_GPIO(sca_reg.GPIO_direction["SOFT_RESET_FPGA"][1],0)
  sca_reg.set_GPIO(sca_reg.GPIO_direction["ENABLE_POWER_FPGA"][1],0)
  sca_reg.set_GPIO(sca_reg.GPIO_direction["SHDN_2V"][1],0)
  sca_reg.set_GPIO(sca_reg.GPIO_direction["SHDN_4V"][1],0)
  sca_reg.set_GPIO(sca_reg.GPIO_direction["NCONFIG_MIDDLE"][1],0)
  sca_reg.set_GPIO(sca_reg.GPIO_direction["NCONFIG_LEFT"][1],0)
  sca_reg.set_GPIO(sca_reg.GPIO_direction["NCONFIG_RIGHT"][1],0)

  ##### INITIALIZIATION

  #This function perform all the needed configurations of the SCA
  #Set the values for ctrl registers B,C and D
  #Set the correct configuration for the 2 I2C buses managed by the SCA
  #Configure the GPIOs directions
  sca_reg.set_CRB(sca_reg.CRB_value)
  sca_reg.set_CRC(sca_reg.CRC_value)
  sca_reg.set_CRD(sca_reg.CRD_value)
  sca_reg.i2c_configure(0,"100kHz",2,"CMOS")
  sca_reg.i2c_configure(1,"100kHz",2,"CMOS")
  sca_reg.gpio_configure()
  print("SCA configuration OK")

  print("Read back register values")
  print(hex(sca_reg.get_CRB()))
  print(hex(sca_reg.get_CRC()))
  print(hex(sca_reg.get_CRD()))
  print(hex(sca_reg.read_i2c_ctrl_register(0)))
  print(hex(sca_reg.read_i2c_ctrl_register(1)))
  print("0x"+hex(sca_reg.read_GPIO_direction())[2:].rjust(8,'0'))

  ###### POWER ENABLE

  #Check init_done
  print(bin(sca_reg.get_GPIO_R())[2:].rjust(32,'0'))

  #PowerON
  sca_reg.set_GPIO(sca_reg.GPIO_direction["SHDN_2V"][1],1)
  sca_reg.set_GPIO(sca_reg.GPIO_direction["SHDN_4V"][1],1)
  sca_reg.set_GPIO(sca_reg.GPIO_direction["ENABLE_POWER_FPGA"][1],1)
  #"""
  #sys.exit()
  #Configure FPGA
  sca_reg.set_GPIO(sca_reg.GPIO_direction["SOFT_RESET_FPGA"][1],1)
  sca_reg.set_GPIO(sca_reg.GPIO_direction["NCONFIG_MIDDLE"][1],1)
  sca_reg.set_GPIO(sca_reg.GPIO_direction["NCONFIG_LEFT"][1],1)
  sca_reg.set_GPIO(sca_reg.GPIO_direction["NCONFIG_RIGHT"][1],1)
  time.sleep(0.5)
  #"""

  #Check init_done
  print(bin(sca_reg.get_GPIO_R())[2:].rjust(32,'0'))

  #"""
  #Deassert FPGA Reset
  sca_reg.set_GPIO(sca_reg.GPIO_direction["SOFT_RESET_FPGA"][1],0)
  #"""

def sc_read(fpga, addr, nWords, bVerbose=False):
  RD = 0
  if bVerbose:
    print("Read",nWords,"word from addr","0x"+hex(addr)[2:].rjust(4,'0'))
  send_SC_frame(fpga, RD, address=addr, data=nWords)
  ReceivedSC = receive_SC_frame()
  llSC=[]
  llSC.append(["0x"+hex(item)[2:].rjust(4,'0') for item in ReceivedSC['fpga_0']])
  llSC.append(["0x"+hex(item)[2:].rjust(4,'0') for item in ReceivedSC['fpga_1']])
  llSC.append(["0x"+hex(item)[2:].rjust(4,'0') for item in ReceivedSC['fpga_2']])

  if bVerbose:
    print("FPGA 0".rjust(15,' ')+'|'+"FPGA 1".rjust(16,' ')+'|'+"FPGA 2".rjust(16,' ')+'|')
    for i in range(max([len(item) for item in llSC])):
      for fpgaID in range(3):
        if i < len(llSC[fpgaID]):
          print(llSC[fpgaID][i].rjust(15,' ')+'|',end='')
        else:
          print(" "*15+'|',end='')
      print()
  return llSC

def sc_write(fpga, addr, data):
  WR=1
  send_SC_frame(fpga, WR, address=addr, data=data)

def tdc_read():
  ReceivedData = UL.receive_TDC_frame()
  llData=[]
  for sFpga in ['fpga_0', 'fpga_1', 'fpga_2']:
    llData.append([[int(item[0]),float(item[1])*2.5/256.0] for item in ReceivedData[sFpga]])

  print("Received frame from different FPGAs:",len(llData[0]),len(llData[1]),len(llData[2]))
  for fpgaID in range(3):
    print("Fpga",fpgaID,"data:")
    print('\n'.join(["Chan"+str(item[0])+" "+str(item[1]) for item in llData[fpgaID]]))
  return llData

def EnableTDC():
  sc_write(LEFT+MID+RIGHT,addr=0x0300, data=[1]) #TDC module enable
  sc_write(LEFT+MID+RIGHT,addr=0x0308, data=[0]) #standard mode
  sc_write(LEFT+MID+RIGHT,addr=0x0305, data=[0xFFFF,0xFFFF,3]) #meas enable for All TDC channels only
  sc_write(LEFT+MID+RIGHT,addr=0x0301, data=[0]) #CMD valid = 0
  sc_write(LEFT+MID+RIGHT,addr=0x0301, data=[1]) #CMD valid = 1
  sc_write(LEFT+MID+RIGHT,addr=0x0301, data=[0]) #CMD valid = 0
  time.sleep(0.005)

def DisableTDC():
  sc_write(LEFT+MID+RIGHT,addr=0x0300, data=[0]) #TDC module disable
  #sc_write(LEFT+MID+RIGHT,addr=0x0305, data=[0,0,0]) #meas enable for All TDC channels only

def LaunchDataCounter(nCycles=0x00100000):
  sc_write(LEFT+MID+RIGHT, addr=0x030A, data=[nCycles&0xFFFF, nCycles>>16]) #Data counter time window size= 0x00100000 = 10.486ms
  sc_write(LEFT+MID+RIGHT, addr=0x030C, data=[0]) #Ensure data counter disabled
  sc_write(LEFT+MID+RIGHT, addr=0x030C, data=[1]) #Data counter start
  sc_write(LEFT+MID+RIGHT, addr=0x030C, data=[0]) #Ensure data counter disabled

def ReadDataCounter(bVerbose=False):
  if bVerbose:
    sc_read(LEFT+MID+RIGHT, addr=0x0316, nWords=1,bVerbose=True)    #Check data counter valid (timer has reach the required value)
  llRaw=sc_read(LEFT+MID+RIGHT, addr=0x0317, nWords=34*2, bVerbose=bVerbose) #Read data counter for BC0 and Resync channels
  llCounter=[]
  for fpgaID in range(len(llRaw)):
    llCounter.append([])
    for i in range(0, len(llRaw[fpgaID]),2):
      llCounter[-1].append((int(llRaw[fpgaID][i+1],16)<<16)+int(llRaw[fpgaID][i],16))
  return llCounter

def ResetRegFc7():
  GenRegVal = project.fpga_registers.ipbus_device.ReadReg("GENERAL")
  project.fpga_registers.ipbus_device.WriteReg("GENERAL", GenRegVal + (1<<30))
  project.fpga_registers.ipbus_device.WriteReg("GENERAL", GenRegVal)

def test_com_FPGA():
  #Ensure fifos are flushed
  project.fifos_clear()
  print("----------------------- test FPGA COM ----------------------------")
  #Read FPGA ID of the 3 FPGAs
  sc_read(LEFT+MID+RIGHT, 0x0010, 1, bVerbose=True)

def EnableTrigExt():
  sc_write(LEFT+MID+RIGHT, 0x0103, [1])
  sc_write(LEFT+MID+RIGHT, 0x0203, [1])

def DisableTrigExt():
  sc_write(LEFT+MID+RIGHT, 0x0103, [0])
  sc_write(LEFT+MID+RIGHT, 0x0203, [0])

def EnableAutoResetSM():
  sc_write(LEFT+MID+RIGHT, 0x0104, [1, int((100-15)/5), 1, 3])
  sc_write(LEFT+MID+RIGHT, 0x0204, [1, int((100-15)/5), 1, 3])

def DisableAutoResetSM():
  sc_write(LEFT+MID+RIGHT, 0x0104, [0])
  sc_write(LEFT+MID+RIGHT, 0x0204, [0])

def test_TDC():
  #Ensure fifos are flushed
  project.stop_acquisition()
  project.fifos_clear()
  print("----------------------TDC test------------------------------------")

  project.set_acquisition_mode("TDC_Count")
  project.set_nb_frame_acquisition(10)

  project.set_transmission_mode(0)
  #project.gen_BC0(500) # (+25)
  #project.gen_BC0_resync_synchronous(100)
  project.fpga_registers.ipbus_device.WriteReg("FC_BC0_Resync_period", 0)

  #Enable BC0 gen
  project.set_enable_BC0(1)
  #project.set_enable_resync(1)

  #"""
  #Enable recording in circular buffer
  project.start_acquisition()

  EnableTDC()

  time.sleep(0.2)
  
  LaunchDataCounter()
  
  time.sleep(0.2)

  #disableBC0gen
  project.reset_BC0_resync_gen()

  #Disable recording in circular buffer
  project.stop_acquisition()

  #read tdc data FIFO
  llData=tdc_read()

  DisableTDC() #(to stop spamming frames)

  llCounter=ReadDataCounter(bVerbose=True)

  """
  plt.figure(figsize=[12.8,9.6])
  for fpgaID in range(3):
    plt.plot(llCounter[fpgaID])
  plt.show()
  #"""

def PetirocChannelEnable(lsFpga=["fpga_1"], lsAsic=["top"], lbChannelEnable=[True]*16):
  lnChannel=[]
  for i in range(16):
    if lbChannelEnable[i]:
      lnChannel.append(i*2)

  #Temp
  #lnChannel=range(32)

  for fpgaID in lsFpga:
    for asicID in lsAsic:
      feb.asic.enable_channel(fpgaID, asicID, lnChannel)

def Dac10bConfig(nDacVal=400, lsFpga=["fpga_1"], lsAsic=["top"]):
  for fpgaID in lsFpga:
    for asicID in lsAsic:
      feb.asic.set_10b_dac(fpgaID, asicID, nDacVal)

def Dac6bConfig(lnDacVal=[32]*16, lsFpga=["fpga_1"], lsAsic=["top"]):
  for fpgaID in lsFpga:
    for asicID in lsAsic:
      for i in range(16):
        feb.asic.set_6b_dac(fpgaID,asicID, i*2, lnDacVal[i])
        feb.asic.set_6b_dac(fpgaID,asicID, i*2+1, 0) #Unused (odd) channels

def DisableAllChannels():
  lsAllFpgas=["fpga_0", "fpga_1", "fpga_2"]
  lsAllAsics=["top", "bottom"]
  for fpgaID in lsAllFpgas:
    for asicID in lsAllAsics:
      feb.asic.disable_channel(fpgaID, asicID, range(32))

def LatchingConfig(sConfig="asic"):

  #Enable/DisableFpga latching
  nFPGAConf= 3 if sConfig in ["fpga"] else 2 #FPGA Nor32 is always used
  sc_write(LEFT+MID+RIGHT, 0x0108, [nFPGAConf])
  sc_write(LEFT+MID+RIGHT, 0x0208, [nFPGAConf])

  #Enable/Disable asic latching
  lsAllFpgas=["fpga_0", "fpga_1", "fpga_2"]
  lsAllAsics=["top", "bottom"]
  for fpgaID in lsAllFpgas:
    for asicID in lsAllAsics:
      if sConfig in ["fpga", "no"]:
        feb.asic.disableLatch(fpgaID, asicID)
      else:
        feb.asic.enableLatch(fpgaID, asicID)

def ApplyConfig(lsFpga=["fpga_0", "fpga_1", "fpga_2"]):
  for fpgaID in lsFpga:
    #Gen config registers
    ROC_top_config = feb.asic.asic[fpgaID][feb.asic.asic_top_index].gen_config()
    ROC_bot_config = feb.asic.asic[fpgaID][feb.asic.asic_bottom_index].gen_config()

    #Send config registers to FEB FPGAs
    if   fpgaID=="fpga_0": nTargetFPGA = LEFT
    elif fpgaID=="fpga_1": nTargetFPGA = MID
    elif fpgaID=="fpga_2": nTargetFPGA = RIGHT
    else: sys.exit("ERROR : UNKNOWN FPGA ID")

    sc_write(nTargetFPGA, 0x0117, ROC_top_config)
    sc_write(nTargetFPGA, 0x0217, ROC_bot_config)

  nTargetFPGA=0
  if "fpga_0" in lsFpga: nTargetFPGA += LEFT
  if "fpga_1" in lsFpga: nTargetFPGA += MID
  if "fpga_2" in lsFpga: nTargetFPGA += RIGHT

  #PETIROC load cycle
  sc_write(nTargetFPGA, 0x0100, [0]) #ROC TOP LOAD = 0
  sc_write(nTargetFPGA, 0x0200, [0]) #ROC BOT LOAD = 0
  sc_write(nTargetFPGA, 0x0100, [1]) #ROC TOP LOAD = 1
  sc_write(nTargetFPGA, 0x0200, [1]) #ROC BOT LOAD = 1
  sc_write(nTargetFPGA, 0x0100, [0]) #ROC TOP LOAD = 0
  sc_write(nTargetFPGA, 0x0200, [0]) #ROC BOT LOAD = 0

  time.sleep(0.2)

def loadPETIROCconfig(dac_10b):
  DisableTrigExt()
  #Reg dict creation
  feb.asic.init(False)

  lsAllFpgas=["fpga_0", "fpga_1", "fpga_2"]
  lsAllAsics=["top", "bottom"]

  PetirocChannelEnable(lsAllFpgas, lsAllAsics, [True]*16)

  Dac10bConfig(400, lsAllFpga, lsAllAsics)

  Dac6bConfig([32]*16, lsAllFpga, lsAllAsics)


  #configure petiroc
  for fpgaID in ["fpga_0", "fpga_1", "fpga_2"]:
    feb.asic.configure(fpgaID,"all")

  #Set autoreset sm
  EnableAutoResetSM()

  #EnableTrigExt()
  DisableTrigExt()
  
  
def CountROCData(fTime=0.100, bVerbose=False):
  """ Use the FEB FPGAs internal counters to count TDC data for each channel during fTime (counting time in seconds) """
  EnableTDC()
  LaunchDataCounter(nCycles=int(fTime*(10**8))) # nCycles*10ns
  time.sleep(fTime) #Must wait at least the required time
  DisableTDC()
  llCounter=ReadDataCounter(bVerbose=bVerbose)
  return llCounter

def test_PETIROC():
  #Ensure fifos are flushed
  project.stop_acquisition()
  project.fifos_clear()
  print("----------------------PETIROC Test------------------------------------")

  loadPETIROCconfig(dac_10b=500)

  llCounter = CountROCData()

  #"""
  plt.figure(figsize=[12.8,9.6])
  for fpgaIndex in range(3):
    plt.plot(llCounter[fpgaIndex])
  plt.show()
  #"""

def saveDac6bConfig(sFpga, sAsic, nTarget, lnDac6bVal):
  with open("./dac6bConfig/dac6b_"+sFpga+"_"+sAsic+".txt","w") as outFile:
    outFile.write("dac10b Target ="+str(nTarget)+"\n")
    for chanIndex in range(16):
      outFile.write("chan"+str(chanIndex)+"="+str(lnDac6bVal[chanIndex])+"\n")

def loadDac6bConfig(sFpga, sAsic):
  lnDac6bVal=[]
  with open("./dac6bConfig/dac6b_"+sFpga+"_"+sAsic+".txt","r") as inFile:
    bFirstLine=True
    for line in inFile:
      lsLine = line.split("=")
      if bFirstLine: #Skip the first line of the config file
        bFirstLine=False
      else:
        lnDac6bVal.append(int(lsLine[1][:-1]))

  Dac6bConfig(lsFpga=[sFpga], lsAsic=[sAsic],lnDacVal=lnDac6bVal)
  ApplyConfig(lsFpga=[sFpga])

def PetirocEqualization(lsFpga=["fpga_1"], lsAsic=["top"], bPlot=False, lbChannelEnable=[True]*16, sLatchConfig="asic"):
  print("\nFPGA : {}".format(lsFpga))
  print("Asic : {}\n".format(lsAsic))
  #Ensure fifos are flushed
  project.stop_acquisition()
  project.fifos_clear()

  #Initialize all Petiroc
  feb.asic.rstb("all","all")
  feb.asic.sr_rst("all","all")
  EnableAutoResetSM() #Warning, SM is disabled by initialization!!!!
  if sLatchConfig=="no":
    DisableAutoResetSM()
  
  LatchingConfig(sLatchConfig)

  DisableAllChannels()
  lnDac6bVal=[32]*16
  PetirocChannelEnable(lsFpga=lsFpga, lsAsic=lsAsic, lbChannelEnable=lbChannelEnable)

  #Starting range (can be useful to perform a coarse sweep to tune it)
  lDac10bVal=range(250,600,1)

  for iteration in range(2):

    print("Iteration",iteration)

    Dac6bConfig(lsFpga=lsFpga, lsAsic=lsAsic,lnDacVal=lnDac6bVal)
    ApplyConfig(lsFpga=lsFpga)

    lllCounter = sweepDac10b(lDac10bVal=lDac10bVal, lsFpga=lsFpga, lsAsic=lsAsic, bPlot=bPlot)

    #TODO rework this (currently it can take only 1 asic at a time)
    if "fpga_0" in lsFpga:
      fpgaIndex=0
    elif "fpga_2" in lsFpga:
      fpgaIndex=2
    else:
      fpgaIndex=1

    llChannelCurves=[[lllCounter[dacValIndex][fpgaIndex][tdcChannelIndex] for dacValIndex in range(len(lDac10bVal))] for tdcChannelIndex in (range(16,32) if "bottom" in lsAsic else range(16))]

    lnTargetIndex=[]
    lnMaxMinIndex=[]
    #For each channel data
    for chanIndex in range(16):
      if lbChannelEnable[chanIndex]:
        #Get the min and max value of channel data
        maxCountVal=max(llChannelCurves[chanIndex])-1  #-1 to manage the test misalignment
        minCountVal=min(llChannelCurves[chanIndex])
        bMaxReached=False
        #Get the last max from the beginning
        for valIndex in range(len(lDac10bVal)):
          if llChannelCurves[chanIndex][valIndex] >= maxCountVal:
            lastMaxIndex = valIndex
            bMaxReached = True
          else:
            if bMaxReached == True:
              break
        #Get the last min from the end
        for valIndex in range(len(lDac10bVal))[::-1]:
          if llChannelCurves[chanIndex][valIndex] == 0:
            firstMinIndex = valIndex
          else:
            break

        #Record transition start and stop index and process the mean
        lnMaxMinIndex.append([lastMaxIndex, firstMinIndex]) # correct
        lnTargetIndex.append(int((firstMinIndex+lastMaxIndex)/2)) #Define the target as the transition mean
        #lnTargetIndex.append(firstMinIndex)#Define the target as firstMin
        print("Chan",chanIndex," Transition between",lDac10bVal[lastMaxIndex],"and",lDac10bVal[firstMinIndex])

      else:
        #If disabled channel, create dummy data to keep the same indexing
        lnMaxMinIndex.append([1000,0])
        lnTargetIndex.append(0)

    #Get the target list of enabled channels only
    lnTargetOfEnabled=[]
    for chanIndex in range(16):
      if lbChannelEnable[chanIndex]:
        lnTargetOfEnabled.append(lnTargetIndex[chanIndex])

    #Process the target (mean of channel targets)
    nTargetIndex = int(round(float(sum(lnTargetOfEnabled))/len(lnTargetOfEnabled)))
    print("Target=",lDac10bVal[nTargetIndex])

    #Process the requested correction
    for chanIndex in range(16):
      nRequestedCorrection = -(lnTargetIndex[chanIndex]-nTargetIndex)
      lnDac6bVal[chanIndex] += nRequestedCorrection
      #Disabled channels dac value stays at 32
      if lbChannelEnable[chanIndex]==False:
        lnDac6bVal[chanIndex]=32

    print("Target dac6b values")
    for chanIndex in range(16):
      print("Chan",chanIndex,":",lnDac6bVal[chanIndex])

    #If a desired value is more than max, every other values are decremented
    if max(lnDac6bVal) > 63:
      for chanIndex in range(16):
        lnDac6bVal[chanIndex]-=1

    #If a desired value is less than min, every other values are incremented
    if min(lnDac6bVal) < 0:
      for chanIndex in range(16):
        lnDac6bVal[chanIndex]+=1

    #Cut values under the min and above the max
    for chanIndex in range(16):
      lnDac6bVal[chanIndex] = max(min(lnDac6bVal[chanIndex],63),0)

    print("New dac6b values")
    for chanIndex in range(16):
      print("Chan",chanIndex,":",lnDac6bVal[chanIndex])

    #Process new sweep window (from the last transitions boundaries)
    newSweepStart=lDac10bVal[min([item[0] for item in lnMaxMinIndex])]-20
    newSweepStop=lDac10bVal[max([item[1] for item in lnMaxMinIndex])]+20
    lDac10bVal=range(newSweepStart, newSweepStop+1, 1)

  #Save processed config in a file
  saveDac6bConfig(sFpga=lsFpga[0], sAsic=lsAsic[0], nTarget=lDac10bVal[nTargetIndex], lnDac6bVal=lnDac6bVal)

  #Apply last correction
  Dac6bConfig(lsFpga=lsFpga, lsAsic=lsAsic,lnDacVal=lnDac6bVal)

  #Apply a correct (max+10 is OK ?) dac10b value
  Dac10bConfig(newSweepStop, lsFpga, lsAsic)
  ApplyConfig(lsFpga=lsFpga)

  return lDac10bVal[nTargetIndex]
  #plt.show()

def sweepDac10b(lDac10bVal=range(350,480,1), lsFpga=["fpga_1"], lsAsic=["top"], bPlot=False):
  """ This function perform a sweep of the 10bDac value and return the data counter values """
  print("---- Dac10b sweep : ",len(lDac10bVal),"values between", min(lDac10bVal),"and", max(lDac10bVal),"----")

  lllCounter=[]

  for dac_10b in lDac10bVal:
    #Overload DAC10bits config
    Dac10bConfig(dac_10b, lsFpga, lsAsic)
    ApplyConfig(lsFpga=lsFpga)
    lllCounter.append(CountROCData())

  if bPlot:
    plt.figure(figsize=[12.8,9.6])
    for fpgaID in range(3):
      for channelID in range(34):
        #Only non-null channels are added to the plot
        if max([lllCounter[configIndex][fpgaID][channelID] for configIndex in range(len(lDac10bVal))])!=0:
          plt.plot(lDac10bVal, [lllCounter[configIndex][fpgaID][channelID] for configIndex in range(len(lDac10bVal))], label="FPGA"+str(fpgaID)+" Chan"+str(channelID))
    plt.legend(loc="upper right")
    plt.ylim(-1,1001)
    plt.savefig("./Plots/"+str(FIG_ID[0])+"_"+str(FIG_ID[1]))
    FIG_ID[1]+=1 #Increment the figNumber
    #plt.show()

  return lllCounter

def InjectionTest():
  #Ensure fifos are flushed
  project.stop_acquisition()
  project.fifos_clear()

  project.set_acquisition_mode("Circular_Buffer_Internal")
  project.set_circular_buffer_size(500)

  project.set_transmission_mode(1)
  project.start_acquisition()

  EnableAutoResetSM()

  EnableTDC()

  fTime= 1.000 #count time in second
  LaunchDataCounter(nCycles=int(fTime*(10**8))) # nCycles*10ns

  TriggerPeriod=32000 #=>32000*25ns = 800us
  periodRegVal = TriggerPeriod<<14

  #Set period of periodic trigger
  project.fpga_registers.ipbus_device.WriteReg("TRIGGER_OUT_FMC", periodRegVal)
  project.fpga_registers.ipbus_device.WriteReg("TRIGGER_OUT_FMC", periodRegVal)
  #Enable periodic trigger
  project.fpga_registers.ipbus_device.WriteReg("TRIGGER_OUT_FMC", periodRegVal + 1)

  #Disable periodic trigger
  project.fpga_registers.ipbus_device.WriteReg("TRIGGER_OUT_FMC", periodRegVal)

  DisableTDC()

  #Dump buffer into output ipbus data FIFO
  project.en_trigger_TDC_buffer()
  project.dis_trigger_TDC_buffer()

  #Disable recording in circular buffer
  project.stop_acquisition()
  project.set_transmission_mode(0)

  llCounter=ReadDataCounter(bVerbose=True)

  #read tdc data FIFO
  llData=tdc_read()

def TrigExtTest():
  #Ensure fifos are flushed
  project.stop_acquisition()
  project.fifos_clear()

  project.set_acquisition_mode("Circular_Buffer_Internal")
  project.set_circular_buffer_size(500)

  project.start_acquisition()

  EnableTDC()

  #Make sure ROC triggers are reset
  EnableAutoResetSM()
  DisableAutoResetSM()

  fTime= 1.000 #count time in second
  LaunchDataCounter(nCycles=int(fTime*(10**8))) # nCycles*10ns

  for i in range(20):
    #TrigExt => 1 => 0
    EnableTrigExt()
    DisableTrigExt()
    EnableAutoResetSM()
    DisableAutoResetSM()

  DisableTDC()

  EnableAutoResetSM()

  llCounter=ReadDataCounter(bVerbose=True)

  #Dump buffer into output ipbus data FIFO
  project.en_trigger_TDC_buffer()
  project.dis_trigger_TDC_buffer()

  #Disable recording in circular buffer
  project.stop_acquisition()

  #read tdc data FIFO
  llData=tdc_read()
  print("Number of tdc data received:", len(llData[1]))

  #Reorganize tdc data per channel
  llChannelData=[]
  for chanIndex in range(16):
    llChannelData.append([])
  for data in llData[1]:
    llChannelData[data[0]].append(data[1])

  #Timestamp offset correction (based on channel0)
  for dataIndex in range(len(llChannelData[0])):
    for chanIndex in range(1,16):
      llChannelData[chanIndex][dataIndex]-=llChannelData[0][dataIndex]
    llChannelData[0][dataIndex]=0.0

  plt.figure(figsize=[12.8,9.6])
  for channelData in llChannelData:
    plt.plot(channelData)
  plt.show()


def dryRun():
  #"""
  #Ensure fifos are flushed
  project.stop_acquisition()
  project.fifos_clear()
  print("----------------------TDC test------------------------------------")
  #"""
  project.set_acquisition_mode("TDC_Count")
  project.set_nb_frame_acquisition(10)

  for i in range(100):
    #Enable recording in circular buffer
    project.start_acquisition()
    time.sleep(0.5)
    #read tdc data FIFO
    llData=tdc_read()
    #Disable recording in circular buffer
    project.stop_acquisition()


def InitFc7():
  ResetRegFc7()
  project.init()
  #Test EC communication with the SCA ASIC
  sca_reg = feb_sca(project.fpga_registers, True)
  test_com_SCA(sca_reg)
  sys.exit()
  


########################## TESTS START HERE ######################################

if __name__ == "__main__":

  project = fc7(False)
  DL, UL = project.downlink, project.uplink
  feb = feb_v2_cycloneV_r1(project, False)

  #This function has to be run once for the correct FC7 initialization
  #InitFc7()

  DisableTDC()
  #project.set_transmission_mode(0)

  #Test communication with the 3 FEB FPGAs
  test_com_FPGA()

  """
  #Old code for time profiling (runtime optimization)
  t1=time.time()
  cProfile.run('PetirocEqualization()')
  print("Time=",time.time()-t1)
  sys.exit()
  """

  """
  #Perform equalization for the 6 asics
  for sFpga in ["fpga_0", "fpga_1", "fpga_2"]:
    for sAsic in ["top", "bottom"]:
      PetirocEqualization(lsFpga=[sFpga], lsAsic=[sAsic], bPlot=True, sLatchConfig="asic")
  """

  #List of enable channels
  lnChannelEnable=range(16)#[2,3,4,5,9,10,11,12,13,14]#[2,4,6,8,10,12]#+[1,3,5,7,9,11,13,15]

  # lbChannelEnable = [True if n in lnChannelEnable else False for n in lnChannelEnable]
  # target = PetirocEqualization(lsFpga=["fpga_2"], lsAsic=["bottom"], bPlot=False,lbChannelEnable=lbChannelEnable, sLatchConfig="asic")
  # target = PetirocEqualization(lsFpga=["fpga_2"], lsAsic=["top"], bPlot=False,lbChannelEnable=lbChannelEnable, sLatchConfig="asic")



  #Create a binary list of enable channels
  lbChannelEnable = [True if n in lnChannelEnable else False for n in lnChannelEnable]
  summary = []
  for fpga in ["fpga_0","fpga_1","fpga_2"]:
  #for fpga in ["fpga_1"]:
  #   for asic in ["top", "bottom"]:
    target = PetirocEqualization(lsFpga=[fpga], lsAsic=["bottom"], bPlot=False,lbChannelEnable=lbChannelEnable, sLatchConfig="asic")
    summary.append(target)
    target = PetirocEqualization(lsFpga=[fpga], lsAsic=["top"], bPlot=False,lbChannelEnable=lbChannelEnable, sLatchConfig="asic")
    summary.append(target)

  print(summary)

    #plt.show()

# à faire pour 1 channel enabled
#  DisableAllChannels()
#  PetirocChannelEnable(lsFpga=["fpga_2"], lsAsic=["top"], lbChannelEnable=[False]*15+[True])

  #sys.exit()

  # for i in range(100):
  #   #loadDac6bConfig(sFpga="fpga_1", sAsic="top")
    
  #   DisableAutoResetSM()
  #   LatchingConfig("no")

  #   sweepDac10b(lDac10bVal=range(350,500,1), lsFpga=["fpga_1"], lsAsic=["top"], bPlot=True)
  #   """

  #   """
  #   #EnableAutoResetSM()
  #   #LatchingConfig("asic")
  #   #feb.asic.disableNor32("fpga_2",'top')
  #   PetirocChannelEnable(lsFpga=["fpga_2"], lsAsic=["top"], lbChannelEnable=[True]*16)

  #   sweepDac10b(lDac10bVal=range(450,600,1), lsFpga=["fpga_2"], lsAsic=["top"], bPlot=True)

  #   plt.show()

  #   DisableAllChannels()
  #   PetirocChannelEnable(lsFpga=["fpga_2"], lsAsic=["top"], lbChannelEnable=[True]*15+[False])

  #   sweepDac10b(lDac10bVal=range(450,600,1), lsFpga=["fpga_2"], lsAsic=["top"], bPlot=True)

  #   #feb.asic.enableNor32('fpga_2','top')
  #   #sweepDac10b(lDac10bVal=range(350,600,1), lsFpga=["fpga_2"], lsAsic=["top"], bPlot=True)

  #   plt.show()
  # #TrigExtTest()

  # #InjectionTest()

  # plt.show()

  # sys.exit()


  # #DisableAutoResetSM()
  # #LatchingConfig("no")
  # #plt.show()
  # #sys.exit()

  # TriggerPeriod=4000 #=>4000*25ns = 100000ns = 100us => 10kHz
  # periodRegVal = TriggerPeriod<<14

  # project.set_transmission_mode(1)
  # #Set period of periodic trigger
  # project.fpga_registers.ipbus_device.WriteReg("TRIGGER_OUT_FMC", periodRegVal)
  # #Enable periodic trigger
  # project.fpga_registers.ipbus_device.WriteReg("TRIGGER_OUT_FMC", periodRegVal + 1)

  # sweepDac10b(lDac10bVal=range(425,500,1), lsFpga=["fpga_1"], lsAsic=["top"], bPlot=True)

  # #Disable periodic trigger
  # project.fpga_registers.ipbus_device.WriteReg("TRIGGER_OUT_FMC", periodRegVal)
  # project.set_transmission_mode(0)

  # #Reconfigure to set ASICs and FPGAs in standard mode
  # LatchingConfig("asic")
  # ApplyConfig()

  # plt.show()
