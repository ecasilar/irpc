# -*- coding: utf-8 -*-
"""
Created on Mon May 10 11:26:51 2021

@author: luciol
"""
import uhal
import time
import signal
import sys
import os
import types
import argparse
import json
import shutil
import threading
import time
from datetime import datetime
#from test_class import * as tc

from test_class import test_class
from tools_box import *
from fc7 import *
from feb_v2_cycloneV_r1 import *
from feb_ctrl_and_status import *
from testbench_calibrate_asic import *
from feb_fpga_registers import *
from Equalization import *
from Parsers_irradiation_tests import *
from statistics import mean
import matplotlib.pyplot as plt

##########################################
### TESTBENCH FILE for radiation tests ###
##########################################

class test(test_class): 
    
    def __init__(self,verbose):
        self.verification = {"GBTx"  : 0,
                             "SCA"   : 0,
                             "FPGas" : 0,
                             "Asics" : 0}
        
               
        super().__init__(verbose)
        


if __name__ == '__main__':

    #Constant definitions FPGA int
    MID   = int("010",2) #2
    LEFT  = int("001",2) #1
    RIGHT = int("100",2) #4

    project = fc7(False)
    DL, UL = project.downlink, project.uplink
    feb = feb_v2_cycloneV_r1(project, False)

    project.stop_acquisition()
    project.fifos_clear()

    sca_reg=feb_sca(project.fpga_registers, True)

    print("\n----------------------- test FPGA COM ----------------------------\n")
    #Read FPGA ID of the 3 FPGAs
    project.sc_read(LEFT+MID+RIGHT, 0x0010, 3, bVerbose=True)
    print("\n------------------------------------------------------------------\n")


#     #At this point we consider the FC7/GBT FPGAs are ready
#     #And the SCA is initialized and configured

#     feb.feb_ctrl_and_status.sca_nconfig_cycle()

#     feb.feb_ctrl_and_status.sca_reset_cycle()

#     #We are normally on the Golden Firmware (Flash addr 0)
    
#     feb.feb_ctrl_and_status.test_com_FPGA()


#     #Set the Remote update controller registers to shift to the application firmware (flash byte addr 0x800000)
#     feb.feb_ctrl_and_status.RemoteUpdateControllerWrite(LEFT+MID+RIGHT, 3, 0x800000) #Firmware flash byte address
#     feb.feb_ctrl_and_status.RemoteUpdateControllerWrite(LEFT+MID+RIGHT, 4, 0x1) #This reg needs to be at '1' before switching to another firmware

#     #Check the remote update controller register values
#     print(feb.feb_ctrl_and_status.RemoteUpdateControllerRead(LEFT+MID+RIGHT, 3))
#     print(feb.feb_ctrl_and_status.RemoteUpdateControllerRead(LEFT+MID+RIGHT, 4))

#     #Launch reconfiguration !!!!
#     feb.feb_ctrl_and_status.RemoteUpdateControllerWrite(LEFT+RIGHT, 6, 0x1) #External FPGAs first
#     feb.feb_ctrl_and_status.RemoteUpdateControllerWrite(MID, 6, 0x1) #Middle FPGA

#     feb.feb_ctrl_and_status.sca_reset_cycle()

#     #At this moment we are supposed to be on the application firmware
#     #If the flash mem is corrupted, we are back to the golden firmware
#     feb.feb_ctrl_and_status.test_com_FPGA()

#     #time.sleep(1.0)
#     #FlashEraseSector(MID, 128)

#     sys.exit()
"""
    # # CONSTANTS
    lsAllFpgas=["fpga_0", "fpga_1", "fpga_2"]
    lsAllAsics=["top", "bottom"]

    # DATA
    data_sca = []
    data_fpgas = []



    # # INIT Test_class
    testbench = test(False)

    # Apply default configuration of asic
    testbench.ApplyDefaultConfig(lsAllFpgas)

    # Reset Bitflip counter
    testbench.feb.asic.reset_bitflip_counter("fpga_1","top")
    testbench.feb.asic.get_bitslip_counter("fpga_1","top")

    # Set petiROC 2C type registers 
    testbench.feb.asic.set_PetiROC_type("fpga_1","top", stype="2C")
    print("register PETIROC_TYPE value : {}\n".format(testbench.feb.fpga.read("fpga_1","PETIROC_TYPE_SELECT_TOP",1)))

    # Read asic config once
    print(testbench.feb.asic.read_config("fpga_1","top"))
    bitflip_n = testbench.feb.asic.get_bitslip_counter("fpga_1","top")
    print("Bitflip counter value = {}".format(bitflip_n))

    # Configure delay_reset_trigger
    testbench.feb.asic.asic["fpga_1"]["top"].enable_delay_reset_trigger(1)
    testbench.feb.asic.configure("fpga_1","top")
    bitflip_n = testbench.feb.asic.get_bitslip_counter("fpga_1","top")
    print("Bitflip counter value after configuration= {}".format(bitflip_n))
    
    # rst bitflip counter
    testbench.feb.asic.reset_bitflip_counter("fpga_1","top")

    # Read back asic configuration
    print("---------------------------")
    print("READBACK Configuration : \n")
    print(testbench.feb.asic.read_config("fpga_1","top"))
    print(testbench.feb.asic.read_previous_config("fpga_1","top"))
    print("---------------------------")

    # enable all channels and dac 10bit a 300
    testbench.EnableTDC("fpga_1", [x for x in range(34)])
    testbench.feb.asic.asic["fpga_1"]["top"].set_10b_dac_T(300)
    testbench.feb.asic.configure("fpga_1","top")



    print("===================\n")
    FEB_ID= input('Enter the FEB ID : ')

    with open('FEB_test&start/FEB_ID_{}.txt'.format(FEB_ID), 'w') as f:
        print('Filename:',"FEB_{}\n".format(FEB_ID),file=f)  # Python 3.x

    #############################################################################################################################
    ################################################# Start validation of process ###############################################
    #############################################################################################################################
    
    print("===================\n")
    a = input("Initialize the setup ? (y/n)\n")
    if a == "y":
        testbench.init()

        print("-------------- Setup initialized ----------------\n")


    print("===================\n")
    a = input("test SCA ?(y/n)\n")

    if a == "y":

        print("-------------- SCA TEST ----------------\n")

        if testbench.feb.feb_ctrl_and_status.sca.check_config() == True : 
            print("\n----- SCA config [OK] -----\n")
        print("\n----- GPIOs configuration : \n")
        testbench.read_GPIOs_SCA()

        ## SCA ID
        id_sca = testbench.feb.feb_ctrl_and_status.sca.get_id()
        print("\n----- SCA ID : {} ------\n".format(id_sca))
        ## decode SCA inputs in one SlowControl request and ctrl reconfig or restart
        testbench.decode_ctrl_SCA_status()
        ## Read SCA SEU counter
        testbench.read_SCA_SEU_counter()
        ## FEB Temperature check
        testbench.read_temperature()
        ## FEB Power_supplies
        testbench.read_powersupplies()

        data_sca.append({    "SCA_ID"     : id_sca,
                             "CRC_errors" : testbench.CRC_ERRORS,
                             "FAULTs": testbench.FAULTS,
                             "SCA_SEU": testbench.SEU_counter,
                             "Temperatures": testbench.temp_sensor_values,
                             "Power_supplies":testbench.power_supplies_val })  
        ####################
        ## WRITE LOG FILE ##
        with open('FEB_test&start/FEB_ID_{}.txt'.format(FEB_ID), 'a') as f:
            print("################# SCA TEST ################## \n", file = f)
            for i,j in data_sca[0].items():
                if i == "Power_supplies": 
                    print("______POWER SUPLLIES______ : \n",file=f)
                    for k,l in j.items():
                        print("{} : {} \n".format(k,l), file = f)
                else:
                    print("{} : {} \n".format(i,j), file = f)
            print("############################################# \n", file = f)
         ####################

        testbench.feb.feb_ctrl_and_status.print()

        
        print("-------------- SCA TEST END ----------------\n")



    print("===================\n")
    

    
    #############################################################################################################################
    ################################################# Configuration of FPGAs ####################################################
    #############################################################################################################################

    a = input("Configure FPGAs ?(y/n)\n")

    if a == "y":
        print("----------------------- Reconfiguration of FPGAs ---------------------------\n")
        testbench.feb.feb_ctrl_and_status.deconfigure_FPGA()
        testbench.feb.feb_ctrl_and_status.configure_FPGA()
        testbench.feb.feb_ctrl_and_status.reset_FPGA()
        print("FPGAs configured ! \n")
    
    print("===================\n")
    

    #############################################################################################################################
    ################################################# FPGAs Read/Write test #####################################################
    #############################################################################################################################
    a = input("Test FPGAs register Rd/Wr ?(y/n)")

    if a == "y":
    

        #FPGA register test
        print("----------------------- test 1 FPGA ID COM ----------------------------\n")
        #Read FPGA ID of the 3 FPGAs
        testbench.sc_read(7, 0x0010, 1, bVerbose=True)

        print("------------------------- test 2 FPGA ID COM ------------------------\n")

        ####################
        ## WRITE LOG FILE ##
        with open('FEB_test&start/FEB_ID_{}.txt'.format(FEB_ID), 'a') as f:
            print("\n################# FPGAs TEST ################## \n", file = f)
            print("\n______READ/WRITE TEST ON FPGAS_______", file=f)
            print("FPGAs IDs :{}".format(testbench.sc_read(7, 0x0010, 1, bVerbose=True)), file = f)
            print("\n-----------------FPGA write test on register :TDC_ENABLE_CH0_15 ---------------------", file = f)
            print("fpga_0 : write 0xffff ", file = f)
            testbench.feb.fpga.write("fpga_0","TDC_ENABLE_CH0_15",0xffff)
            print(testbench.feb.fpga.read("fpga_0","TDC_ENABLE_CH0_15",1), file = f)

            print("fpga_1 : write 0xffff ", file = f)
            testbench.feb.fpga.write("fpga_1","TDC_ENABLE_CH0_15",0xffff)
            print(testbench.feb.fpga.read("fpga_1","TDC_ENABLE_CH0_15",1), file = f)

            print("fpga_2 : write 0xffff  ", file = f)
            testbench.feb.fpga.write("fpga_2","TDC_ENABLE_CH0_15",0xffff)
            print(testbench.feb.fpga.read("fpga_2","TDC_ENABLE_CH0_15",1), file = f)
            print("############################################# \n", file = f)
        ####################

    print("===================\n")
    

    #############################################################################################################################
    ################################################# FAsic config. BITFLIP COUNT ###############################################
    #############################################################################################################################
    a = input("bitflip test ?(y/n)\n")

    if a == "y":

        print("----------------- bitflip count after asic configuration ----------------------")
        print("\n ==> loading configuration . . . \n")
        #testbench.load_feb_config(lsFpga, asicID)
        testbench.ApplyDefaultConfig()

        #Reset SEU counter
        print("reset bitflip counter . . . ")
        testbench.reset_bitflip_counter(lsfpga=lsAllFpgas, asic="all")

        print("tst 1 : Read bitflip counter . . . \n")
        bitflip_count_1 = testbench.read_bitflip_counter(lsfpga=lsAllFpgas, asic="all")
        print(" BITFLIP COUNTERs = {}".format(bitflip_count_1))
        ## Load configuration 1st time to set default 

        #Reset SEU counter
        print("reset bitflip counter . . . ")
        testbench.reset_bitflip_counter(lsfpga=lsAllFpgas, asic="all")

        ## Load configuration 2nd time to check bitflip
        print("\n ==> loading configuration . . . \n")
        testbench.ApplyDefaultConfig()
        #testbench.load_feb_config(lsFpga, asicID)

        print("tst 2 : Read bitflip counter . . . \n")
        bitflip_count_2 = testbench.read_bitflip_counter(lsfpga=lsAllFpgas, asic="all") # TOP : [fpga_0_1, fpga_1_1, fpga_2_1] [fpga_0_1, fpga_1_1, fpga_2_1]                
        print("BITFLIP COUNTERs = {}".format(bitflip_count_2))
        print("---------------------------------------------------------------------\n")
   
        ####################
        ## WRITE LOG FILE ##
        with open('FEB_test&start/FEB_ID_{}.txt'.format(FEB_ID), 'a') as f:
            print("\n################# ASICs TEST ################## \n", file = f)
            print("\n______BITFLIP COUNT TEST_______", file=f)
            print(" bitflip_count : test 1 {} \n test 2 {}".format(bitflip_count_1,bitflip_count_2), file = f)
            print("############################################# \n", file = f)
        ####################
    print("===================\n")


    #############################################################################################################################
    ################################################# PetiROC EQUALIZATION ######################################################
    #############################################################################################################################

    a = input("Equalizitation of PetiROCs ?(y/n)\n")
    if a == "y":
        #List of enable channels
        lnChannelEnable=range(16)#[2,3,4,5,9,10,11,12,13,14]#[2,4,6,8,10,12]#+[1,3,5,7,9,11,13,15]

        #Create a binary list of enable channels
        lbChannelEnable = [True if n in lnChannelEnable else False for n in lnChannelEnable]
        summary = []

        for fpga in ["fpga_0","fpga_1","fpga_2"]:
            print(fpga)
            target = testbench.PetirocEqualization(lsFpga=[fpga], lsAsic=["bottom"], bPlot=False,lbChannelEnable=lbChannelEnable, sLatchConfig="asic")
            summary.append(target)
            target = testbench.PetirocEqualization(lsFpga=[fpga], lsAsic=["top"], bPlot=False,lbChannelEnable=lbChannelEnable, sLatchConfig="asic")
            summary.append(target)
            print(summary)
            ####################
            ## WRITE LOG FILE ##
            with open('FEB_test&start/FEB_ID_{}.txt'.format(FEB_ID), 'a') as f:
                for i in range(len(summary)) :
                    if 0<i<2 : 
                        id_=0
                    if 2<i<4 : 
                        id_=1
                    if 4<i<6 : 
                        id_=2
                    if i%2 == 0 : 
                        asic = "bottom"
                    if i%2 != 0 : 
                        asic = "top"
                    print(" 10b DAC value :  FPGA_{}, asic_{} : 10bit DAC = {}".format(id_,asic,summary[i]), file = f)
            ####################


    testbench.ApplyDefaultConfig(lsFpga = lsAllFpgas)
    a = input("Sweep DAC 10 bits of PetiROCs ?(y/n)\n")
    if a == "y":

        testbench.Scurve_process(lsAllFpgas, lsAllAsics)

"""

#================================================================================#
#================================================================================#
#================================================================================#
#================================================================================#
#================================================================================#
#================================================================================#
#================================================================================#
#================================================================================#
#================================================================================#
#================================================================================#
