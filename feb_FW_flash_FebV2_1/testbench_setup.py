import uhal
import time
import sys
import os
import types
import argparse
from fc7 import *
from tools_box import *
from feb_v2_cycloneV_r1 import *
from EPCQ_prog import *


### Initialize FC7 and FEB cards.

class testbench_setup:

    def __init__(self,fc7_verbose,feb_verbose):
        self.fc7 = fc7(fc7_verbose)
        self.feb = feb_v2_cycloneV_r1(self.fc7,feb_verbose)

    def init(self):
        self.fc7.init()
        self.feb.init()
        ### Remote update FW ###
        a = input("\n You are in GoldenImage FW : SWITCH FIRMWARE TO USER APP ? (y/n) ")
        if a == "y":
            self.feb.feb_ctrl_and_status.sca_nconfig_cycle()
            self.feb.feb_ctrl_and_status.sca_reset_cycle()
            self.fc7.switch_to_app()
            self.feb.feb_ctrl_and_status.sca_reset_cycle()
            self.fc7.test_com_FPGA()
            print(self.fc7.RemoteUpdateControllerRead(0x7, 4), "0 for Golden / 1 for App FW") # =='1' if success
        else : 
            self.fc7.test_com_FPGA()
            print(self.fc7.RemoteUpdateControllerRead(0x7, 4), "0 for Golden / 1 for App FW") # =='1' if success
            print("\n!!! You are in GoldenImage FW, you need to write a new FW into FLASH !!! ")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-vfc7", "--fc7_verbose", help="increase output verbosity",action="store_true")
    parser.add_argument("-vfeb", "--feb_verbose", help="increase output verbosity",action="store_true")
    args = parser.parse_args()
    clear = lambda: os.system('clear')
    clear()
    if args.fc7_verbose:
        print("verbosity fc7 turned on")
    if args.feb_verbose:
        print("verbosity feb turned on")

    testbench = testbench_setup(args.fc7_verbose,args.feb_verbose)
    testbench.init()


    
