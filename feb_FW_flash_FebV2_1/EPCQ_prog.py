import uhal
import time
import sys
import os
import types
import cProfile
from tools_box import *
from fc7 import *
from feb_v2_cycloneV_r1 import *

from feb_ic import *
from feb_sca import *
import numpy as np
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt


#|||||||||||||||
# TOP COMMENT ||
#|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
##############################################################################################################################
## This file is used to update the application firmware / rewrite and reprogram FLASH of FPGAs                              ||
## Firmware to load into the flash are listed in "FLASH_FW/" folder, those are .rpd file. [working FW : 1.5 +]              ||
##                                                                                                                          ||
## The start address of App FW is always 0x800000.                                                                          ||
## The stop address of App FW depend on the version, please check into "FLASH_FW/<FW_version>.map" to know the stop address.|| 
##                                                                                                                          || 
## Main functions used to reprogram the flash are described l.444.                                                          || 
## jump_to_GoldenImg() : change firmware to Golden FW                                                                       || 
## jump_to_app(): change firmware to App FW                                                                                 || 
## write_flash_from_RPD_file() : write Flash programming file into flash EPCQ128A                                           || 
##############################################################################################################################
#|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


#### Constant definitions FPGA int ####

MID   = int("010",2) #2
LEFT  = int("001",2) #1
RIGHT = int("100",2) #4
SFL_reg = {    "FL_CONTROL"           : 0x0,
               "FL_SPI_CLOCK_BR"      : 0x1,
               "FL_CS_DELAY"          : 0x2,
               "FL_READ_CAPTURING"    : 0x3,
               "FL_OPERATION_SETTINGS": 0x4,
               "FL_READ_IR"           : 0x5, # dual input fast read
               "FL_WRITE_IR"          : 0x6,
               "FL_CMD_SETTINGS"      : 0x7,
               "FL_CMD_CTRL"          : 0x8,
               "FL_CMD_ADDR"          : 0x9,
               "FL_CMD_WR_DATA0"      : 0xA,
               "FL_CMD_WR_DATA1"      : 0xB,
               "FL_CMD_RD_DATA0"      : 0xC,
               "FL_CMD_RD_DATA1"      : 0xD}


#### FPGA READ/WRITE FPGAs registers ####
def sc_read(fpga, addr, nWords, bVerbose=False):
    RD = 0
    if bVerbose:
        print("Read",nWords,"word from addr","0x"+hex(addr)[2:].rjust(4,'0'))
    send_SC_frame(fpga, RD, address=addr, data=nWords)
    ReceivedSC = receive_SC_frame()
    llSC=[]
    llSC.append(["0x"+hex(item)[2:].rjust(4,'0') for item in ReceivedSC['fpga_0']])
    llSC.append(["0x"+hex(item)[2:].rjust(4,'0') for item in ReceivedSC['fpga_1']])
    llSC.append(["0x"+hex(item)[2:].rjust(4,'0') for item in ReceivedSC['fpga_2']])

    if bVerbose:
        print("FPGA 0".rjust(15,' ')+'|'+"FPGA 1".rjust(16,' ')+'|'+"FPGA 2".rjust(16,' ')+'|')
        for i in range(max([len(item) for item in llSC])):
            for fpgaID in range(3):
                if i < len(llSC[fpgaID]):
                    print(llSC[fpgaID][i].rjust(15,' ')+'|',end='')
                else:
                    print(" "*15+'|',end='')
                    print()
    return llSC

def sc_write(fpga, addr, data):
    WR=1
    send_SC_frame(fpga, WR, address=addr, data=data)

def test_com_FPGA(FPGA = 0x7):
    project.fifos_clear()
    print("\n----------------------- test FPGA COM ----------------------------\n")
    #Read FPGA ID of the 3 FPGAs + version FW
    project.sc_read(FPGA, 0x0010, 3, bVerbose=False)
    print("\n------------------------------------------------------------------\n")

def send_SC_frame(target, rw, address, data):
    project.fpga_registers.set_tx_fifo_SC_block(DL.sc_frame_format_32bits(DL.sc_frame_request(target,rw,address,data)))

def receive_SC_frame():
  SC_frames = []
  r = project.fpga_registers.get_rx_fifo_SC_block(project.get_nb_word_in_fifo_SC())
  for i in range(0,len(r),4):
    SC_frames.append(( (r[i]<<96) + (r[i+1]<<64) +(r[i+2]<<32) +r[i+3])>>16 )
    #print(hex(SC_frames[-1]))
  return UL.decode_SC_frame(SC_frames)

#### Basic communication functions ####
def flash_csr_read(FPGA, ADDR):
  sc_write(FPGA, 0x2602, [ADDR, 0, 1])
  llsVal = sc_read(FPGA, 0x2681, 2, bVerbose=False)
  return [hex((int(val[0],16)<<16)+int(val[1],16)) if len(val)==2 else None for val in llsVal]

def flash_csr_write(FPGA, ADDR, DATA):
  sc_write(FPGA, 0x2600, [(DATA>>16), (DATA & 0xFFFF), ADDR, 1])

# def flash_mem_read(FPGA, ADDR):
#   #Constant size burst transaction to the FEB, Flash read procedure doesn't manage bursts
#   sc_write(FPGA, 0x2605, [1, 0xF] + [0]*64 + [(ADDR>>16), (ADDR & 0xFFFF), 0, 1])
#   llsVal = sc_read(FPGA, 0x2684, 2, bVerbose=False)
#   return [hex((int(val[0],16)<<16)+int(val[1],16)) if len(val)==2 else None for val in llsVal]

def flash_mem_read(FPGA, ADDR, burst_len=1):
  if burst_len > 32 or burst_len == 0:
    sys.exit("ERROR : ",burst_len," IS NOT A VALID FLASH RD_BURST LEN (must be in the range ]0:32])")
  sc_write(FPGA, 0x2605, [burst_len, 0xF] + [0]*64 + [(ADDR>>16), (ADDR & 0xFFFF), 0, 1])
  time.sleep(0.05)
  llsVal = sc_read(FPGA, 0x2684, 2*burst_len, bVerbose=False)
  return [[((int(val[2*i],16)<<16)+int(val[2*i+1],16))for i in range(burst_len)] if len(val)==burst_len*2 else None for val in llsVal]

def flash_mem_write(FPGA, ADDR, lDATA):
  if (ADDR & 0x3FFFFF) < 0x200000:
    sys.exit("ERROR : FORBIDDEN TO WRITE IN THE FIRST HALF OF THE FLASH MEMORY")
  burst_len = len(lDATA)
  if burst_len > 32 or burst_len == 0:
    sys.exit("ERROR : ",burst_len," IS NOT A VALID FLASH WR_BURST LEN (must be in the range ]0:32])")

  #Create the 64 16bits registers
  l64RegsData = [0]*64
  for i in range(burst_len):
    l64RegsData[2*i]   = lDATA[i] >> 16     #16 MSbits of the word
    l64RegsData[2*i+1] = lDATA[i] & 0xFFFF  #16 LSbits of the word

  #Constant size burst transaction to the FEB
  sc_write(FPGA, 0x2605, [burst_len, 0xF] + l64RegsData + [(ADDR>>16), (ADDR & 0xFFFF), 1])

  time.sleep(0.001)


#### Higher level flash operations ####
def FlashReadID(FPGA):
  flash_csr_write(FPGA, SFL_reg["FL_CMD_SETTINGS"], 0x489F)
  flash_csr_write(FPGA, SFL_reg["FL_CMD_CTRL"], 0x01)
  print("FLASH ID : ", flash_csr_read(FPGA, SFL_reg["FL_CMD_CTRL"]))

def FlashEraseBulk(FPGA):
  """ Puts 0xFF everywhere in the flash """ # Need to unprotect all Flash
  sys.exit("ERROR: FORBIDDEN FUNCTION IF PROTECTED SECTORS")
  #Write enable
  flash_csr_write(FPGA, SFL_reg["FL_CMD_SETTINGS"], 0x6)
  flash_csr_write(FPGA, SFL_reg["FL_CMD_CTRL"], 0x01)
  # #Write status
  flash_csr_write(FPGA, SFL_reg["FL_CMD_WR_DATA0"], 0)
  flash_csr_write(FPGA, SFL_reg["FL_CMD_CTRL"], 0x01)
  #Write enable
  flash_csr_write(FPGA, SFL_reg["FL_CMD_SETTINGS"], 0x6)
  flash_csr_write(FPGA, SFL_reg["FL_CMD_CTRL"], 0x01)
  #Erase bulk
  flash_csr_write(FPGA, SFL_reg["FL_CMD_SETTINGS"] ,0x3C7)
  flash_csr_write(FPGA, SFL_reg["FL_CMD_CTRL"], 0x01)
  time.sleep(2)

def FlashEraseSector(FPGA, nSector):
  """ Puts 0xFF everywhere in a sector of the flash """
  if nSector < 128: # protection of Golden Firmware part
    sys.exit("ERROR: FORBIDDEN TO ERASE THE FIRST 128 SECTORS OF THE FLASH MEMORY")  
  #Write enable
  flash_csr_write(FPGA, SFL_reg["FL_CMD_SETTINGS"], 0x6)
  flash_csr_write(FPGA, SFL_reg["FL_CMD_CTRL"], 0x01)
  time.sleep(0.05)
  #Erase sector
  flash_csr_write(FPGA, SFL_reg["FL_CMD_ADDR"], (nSector<<16))
  flash_csr_write(FPGA, SFL_reg["FL_CMD_SETTINGS"] ,0x3D8)
  flash_csr_write(FPGA, SFL_reg["FL_CMD_CTRL"], 0x01)
  #print("Status = ",FlashReadStatus(FPGA))
  time.sleep(0.4) # !! important

def FlashProtectAllSector(FPGA):
  #Write enable
  flash_csr_write(FPGA, SFL_reg["FL_CMD_SETTINGS"], 0x6)
  flash_csr_write(FPGA, SFL_reg["FL_CMD_CTRL"], 0x01)
  time.sleep(0.2)
  #Write status
  flash_csr_write(FPGA, SFL_reg["FL_CMD_SETTINGS"], 0x1001)
  flash_csr_write(FPGA, SFL_reg["FL_CMD_WR_DATA0"], 0x3c) # protect all sectors
  flash_csr_write(FPGA, SFL_reg["FL_CMD_CTRL"], 0x01)
  time.sleep(0.3) # !!! wait

def FlashProtectSector0to127(FPGA):
  #Write enable
  flash_csr_write(FPGA, SFL_reg["FL_CMD_SETTINGS"], 0x6)
  flash_csr_write(FPGA, SFL_reg["FL_CMD_CTRL"], 0x01)

  #Write status
  flash_csr_write(FPGA, SFL_reg["FL_CMD_SETTINGS"], 0x1001)
  flash_csr_write(FPGA, SFL_reg["FL_CMD_WR_DATA0"], 0x38) # protect sectors 0 to 127 unprotect 128 - 255
  flash_csr_write(FPGA, SFL_reg["FL_CMD_CTRL"], 0x01)
  time.sleep(0.1) # !!! wait 15 ms after write status operation

def FlashUnprotectAllSector(FPGA):
  #Write enable
  flash_csr_write(FPGA, SFL_reg["FL_CMD_SETTINGS"], 0x6)
  flash_csr_write(FPGA, SFL_reg["FL_CMD_CTRL"], 0x01)
  time.sleep(0.2)
  #Write status protection
  flash_csr_write(FPGA, SFL_reg["FL_CMD_SETTINGS"], 0x1001)
  flash_csr_write(FPGA, SFL_reg["FL_CMD_WR_DATA0"], 0x20)
  flash_csr_write(FPGA, SFL_reg["FL_CMD_CTRL"], 0x01)
  time.sleep(0.2)

def FlashReadStatus(FPGA):
  # Read Status
  flash_csr_write(FPGA, SFL_reg["FL_CMD_SETTINGS"], 0x00001005)
  flash_csr_write(FPGA, SFL_reg["FL_CMD_CTRL"], 0x01)
  return flash_csr_read(FPGA, SFL_reg["FL_CMD_RD_DATA0"])

def FlashReadback(FPGA, ADDR, size):
  #flash_csr_write(FPGA, 7, 0x4803) #Read_bytes
  #flash_csr_write(FPGA, SFL_reg["FL_CMD_SETTINGS"], 0x00801003)#Read_bytes
  ### Extended mode ? ###
  # flash_csr_write(FPGA, 4, 0x0) # set operation protocol
  # flash_csr_write(FPGA, 0, 0x1) # set control reg.
  # flash_csr_write(FPGA, 5, 0x3) # set IR
  #######################

  #flash_csr_write(FPGA, SFL_reg["FL_CMD_CTRL"], 0x01) # start Read_bytes

  BASE_ADDR = ADDR
  addr = BASE_ADDR
  lData=[[],[],[]]
  while addr < BASE_ADDR+size:
    if addr+32 < BASE_ADDR+size:
      l = flash_mem_read(FPGA, addr, 32)
    else:
      l =flash_mem_read(FPGA, addr,BASE_ADDR+size-addr)

    for i in range(3):
      lData[i]+= l[i]

    addr+=32

  return lData

def FlashWrite(FPGA, start_ADDR, lDATA): # !!! First erase sector before write it
  #Write enable
  # flash_csr_write(FPGA, SFL_reg["FL_CMD_SETTINGS"], 0x6)
  # flash_csr_write(FPGA, SFL_reg["FL_CMD_CTRL"], 0x01)
  # #Write_bytes
  # flash_csr_write(FPGA, SFL_reg["FL_CMD_SETTINGS"], 0x0003F002)
  # flash_csr_write(FPGA,  SFL_reg["FL_CMD_CTRL"], 0x01) # start Read_bytes
  flash_mem_write(FPGA, start_ADDR, lDATA)

def compareFlash(start = 0x800000, stop = 0xD54FD6):
      i = start
      err = []
      cnt = 0
      pourcent = 0
      for i in range(start//4, stop//4):
        ## % Progression erase sector
        pourcent = (i/(stop))*100
        sys.stdout.write("Compare FLash FPGAs LEFT-MID-RIGHT {}%   \r".format(pourcent))
        sys.stdout.flush()
        x = FlashRead(LEFT+MID+RIGHT, i)

        if x[0][0] != x[1][0] or x[0][1] != x[1][1] or x[2][0] != x[0][0] or x[2][1] != x[0][1]:
          cnt = cnt+1
          print(i)
          print("Flash reg LEFT-MID-RIGHT: ",x)
          err.append([x[0],x[1],x[2],hex(i<<2)])
      print("ERROR cnt = ", cnt,"\n", "ERRORS : ", err[0:50]) # 
      
def compareFlashSector(sector = 128):
      i = 0
      err = []
      cnt = 0
      pourcent = 0
      for i in range(4096): # bytes/sectors/4 => 32 bits regs
        ## % Progression
        pourcent = (i/(4096))*100
        sys.stdout.write("Compare FLash FPGAs LEFT-RIGHT {}%   \r".format(pourcent))
        sys.stdout.flush()
        x = FlashRead(LEFT+MID+RIGHT, 0x200000+(0x4000*(sector-128))+i)

        if x[0][0] != x[1][0] or x[0][1] != x[1][1] or x[2][0] != x[0][0] or x[2][1] != x[0][1]:
          cnt = cnt+1
          print(i)
          print("Flash reg LEFT-MID-RIGHT: ",x)
          err.append([x[0],x[1],x[2],hex(i<<2)])
      print("ERROR cnt = ", cnt,"\n", "ERRORS : ", err[0:50])
      return err     

def compareFlashToRPD(file_name = "output_file_auto_1.8.rpd",start = 0x800000, stop = 0x810000):
    i = start
    current_addr = start
    wordlist = []
    error_cnt = [0,0,0]
    print("\n Check Flash before rewrite it \n")
    with open("Flash_FW/"+file_name, "rb") as file:

      #file.read(start) # start pointer
      while  start<= i < stop:
        pourcent = ((i-start)/(stop-start))*100
        sys.stdout.write("Comparaison {}% \r".format(pourcent))
        sys.stdout.flush()
        # Read binary file 4*bits
        a1 = ord(file.read(1))
        a2 = ord(file.read(1))
        a3 = ord(file.read(1))
        a4 = ord(file.read(1))
        # Reverse bit order
        x1 = reverse_bit_order(a1,8)
        x2 = reverse_bit_order(a2,8)
        x3 = reverse_bit_order(a3,8)
        x4 = reverse_bit_order(a4,8)
        # LSB first
        buf32 = (x4 << 24) + (x3 << 16) + (x2 << 8) + x1
        wordlist.append(buf32)
        
        if len(wordlist)==32:
          flash_buf = flash_mem_read(LEFT+MID+RIGHT,(current_addr >>2),32) # [[16b1,16b2], [16b1,16b2],[16b1,16b2]]
          #print(flash_buf)
          #sys.exit()
          current_addr += 32*4
          #print(buf32, flash_buf[0])
          for j in range(0,3):
            for k in range(len(wordlist)):
              #print(flash_buf[j][0])
              #print(wordlist[k], flash_buf[j][k])
              if wordlist[k] != flash_buf[j][k]:

                  error_cnt[j] +=1

                  #print(wordlist, flash_buf, i, current_addr)


                  #sys.exit("Flash corrupted : reload GoldenIMAGE before update the firmware !")
              else :
                  continue
          wordlist = []

        else:
          i+= 4
    file.close()
    if error_cnt != 0:
      print("\nFlash is corrupted from ",start," to ",stop, "eror count", error_cnt)
    else:
      print("\nno errors", error_cnt)
        

#### Remote update controller functions ####
def RemoteUpdateControllerWrite(FPGA, ADDR, DATA):
  sc_write(FPGA, 0x2700, [(DATA>>16), (DATA & 0xFFFF), ADDR, 1])

def RemoteUpdateControllerRead(FPGA, ADDR):
  sc_write(FPGA,0x2702,[ADDR, 0, 1])
  llsVal = sc_read(FPGA, 0x2711, 2, bVerbose=False)
  return [hex((int(val[0],16)<<16)+int(val[1],16)) if len(val)==2 else None for val in llsVal]

def Read_CRC_errors_RU(FPGA):
    time.sleep(0.1)
    r = RemoteUpdateControllerRead(FPGA, 0)
    time.sleep(0.1)
    print("\n CRC ERROR: ",int(r[0],16) & 1,"\n")

def Read_FW_mode(FPGA):
  print(RemoteUpdateControllerRead(FPGA, 4), "0 for Golden / 1 for App FW")

#### Misc functions ####
def sca_reset_cycle():
  #Reset high
  sca.set_GPIO(sca.GPIO_direction["SOFT_RESET_FPGA"][1],1)
  time.sleep(1.0)
  #Reset low
  sca.set_GPIO(sca.GPIO_direction["SOFT_RESET_FPGA"][1],0)

def sca_nconfig_cycle():
  #Keep reset high
  sca.set_GPIO(sca.GPIO_direction["SOFT_RESET_FPGA"][1],1)
  #NCONFIG at '0'
  sca.set_GPIO(sca.GPIO_direction["NCONFIG_MIDDLE"][1],0)
  sca.set_GPIO(sca.GPIO_direction["NCONFIG_LEFT"][1],0)
  sca.set_GPIO(sca.GPIO_direction["NCONFIG_RIGHT"][1],0)
  #NCONFIG at '1'
  sca.set_GPIO(sca.GPIO_direction["NCONFIG_MIDDLE"][1],1)
  sca.set_GPIO(sca.GPIO_direction["NCONFIG_LEFT"][1],1)
  sca.set_GPIO(sca.GPIO_direction["NCONFIG_RIGHT"][1],1)
  #Perform reset cycle
  sca_reset_cycle()

def reverse_bit_order(n, nb_bits = 8):
  # reverse bit order of int
  result = 0
  for i in range(nb_bits):
    result <<= 1
    result |= n&1
    n>>=1
  return result

def read_RPD_file(start_addr,nbr_data, file_name ="output_file_auto_1.8.rpd"):
  with open("Flash_FW/"+file_name, "rb") as file:
    wordlist=[]
    err_cnt = 0
    err_buf=[]
    i_err = []
    err_y=[]

    print("file size =", os.path.getsize("Flash_FW/"+file_name))
    print("pages = ", os.path.getsize("Flash_FW/"+file_name)/256)
    print("Balanceed bytes = ", os.path.getsize("Flash_FW/"+file_name)%256)
    file.read(start_addr) # start pointer
    pourcent  = 0
    i = start_addr
    cnt = 0
    while  start_addr<= i < ((start_addr+nbr_data)):
      # Read binary file 4*bits
      a1 = ord(file.read(1))
      a2 = ord(file.read(1))
      a3 = ord(file.read(1))
      a4 = ord(file.read(1))
      # Reverse bit order
      x1 = reverse_bit_order(a1,8)
      x2 = reverse_bit_order(a2,8)
      x3 = reverse_bit_order(a3,8)
      x4 = reverse_bit_order(a4,8)
      # LSB first
      buf32 = (x4 << 24) + (x3 << 16) + (x2 << 8) + x1
      print(hex(buf32))
      start_addr +=1
      i+= 4
      cnt +=1
  file.close()


#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
####################################################################### MAIN FUNCTIONS ############################################################################
## jump_to_GoldenImg() : change firmware to Golden FW
## jump_to_app(): change firmware to App FW
## write_flash_from_RPD_file(FPGA, file_name, end_addr_file) : write Flash programming file into flash EPCQ128A
##      INPUTS : -> FPGA            : select FPGA to flash, ex to FLASH FPGA_LEFT and MIDDLE write : LEFT+MID
##               -> file_name       : The filename of the .rpd file, the file must be in the folder "FLASH_FW/"
##               -> end_addr_file   : The end address of the file, correspond to the end of the write processus, this addresse must change beetween differents FW.
###################################################################################################################################################################
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||  

def jump_to_GoldenImg():
  print("\nINITAL FW : \n")
  test_com_FPGA()
  print("_______________ \n")
  print(RemoteUpdateControllerRead(LEFT+MID+RIGHT, 4), "0 for Golden / 1 for App FW\n")
  print("_______________ \n")

  #Set the Remote update controller registers to shift to the application firmware (flash byte addr 0x800000)
  RemoteUpdateControllerWrite(LEFT+MID+RIGHT, 3, 0x0) #Firmware flash byte address
  RemoteUpdateControllerWrite(LEFT+MID+RIGHT, 4, 0x0) #This reg needs to be at '1' before switching to another firmware

  #Check the remote update controller register values
  # print(RemoteUpdateControllerRead(self.LEFT+self.MID+self.RIGHT, 3))
  # print(RemoteUpdateControllerRead(self.LEFT+self.MID+self.RIGHT, 4))
  time.sleep(0.1)
  #Launch reconfiguration !!!!
  RemoteUpdateControllerWrite(LEFT+RIGHT, 6, 0x1) #External FPGAs first
  RemoteUpdateControllerWrite(MID, 6, 0x1) #Middle FPGA
  sca_reset_cycle()

  #At this moment we are supposed to be on the application firmware
  #If the flash mem is corrupted, we are back to the golden firmware
  test_com_FPGA()

  print("FINAL FW : \n")
  print("_______________ \n")
  #If the RemoteUpdateController is present in the application firmware, check that firmware switch succeeds
  print(RemoteUpdateControllerRead(LEFT+MID+RIGHT, 4), "0 for Golden / 1 for App FW") # =='1' if success
  print("_______________ \n") # Load APP Firmware into FPGAs

def jump_to_app():
  #At this point we consider the FC7/GBT FPGAs are ready
  #And the SCA is initialized and configured
  
  #We are normally on the Golden Firmware (Flash addr 0)
  print("\nINITAL FW : \n")
  test_com_FPGA()
  print("\n_______________ \n")
  print(RemoteUpdateControllerRead(LEFT+MID+RIGHT, 4), "0 for Golden / 1 for App FW")
  print("\n_______________ \n")
  #Set the Remote update controller registers to shift to the application firmware (flash byte addr 0x800000)
  RemoteUpdateControllerWrite(LEFT+MID+RIGHT, 3, 0x800000) #Firmware flash byte address
  RemoteUpdateControllerWrite(LEFT+MID+RIGHT, 4, 0x1) #This reg needs to be at '1' before switching to another firmware

  
  
  #Launch reconfiguration !!!!
#  sca.set_GPIO(sca.GPIO_direction["SOFT_RESET_FPGA"][1],1)
  time.sleep(1)
  RemoteUpdateControllerWrite(LEFT, 6, 0x1) #External FPGAs first
  time.sleep(1)
  RemoteUpdateControllerWrite(RIGHT, 6, 0x1) #External FPGAs first
  time.sleep(1)
  RemoteUpdateControllerWrite(MID, 6, 0x1) #Middle FPGA
  time.sleep(1)
  sca_reset_cycle()
  #  time.sleep(1)
  #  print("After sca reset")


#project.gbt_fpga.init()
#  sca.init()
#  feb.feb_ctrl_and_status.power_off_FPGA()
#  feb.feb_ctrl_and_status.shutdown_2v()
#        feb.feb_ctrl_and_status.shutdown_4v()
#        feb.feb_ctrl_and_status.power_up_2v()
#    feb.feb_ctrl_and_status.power_up_4v()
#  feb.feb_ctrl_and_status.power_on_FPGA()
#  feb.feb_ctrl_and_status.configure_FPGA()
#  feb.feb_ctrl_and_status.reset_FPGA()
#  time.sleep(1.0)
  #Reset low
  #sca.set_GPIO(sca.GPIO_direction["SOFT_RESET_FPGA"][1],1)
  #time.sleep(1.0)
  #sca.set_GPIO(sca.GPIO_direction["SOFT_RESET_FPGA"][1],0)

  #At this moment we are supposed to be on the application firmware
  #If the flash mem is corrupted, we are back to the golden firmware
  test_com_FPGA()
  print("\nFINAL FW : \n")
  #If the RemoteUpdateController is present in the application firmware, check that firmware switch succeeds
  print("\n_______________ \n")
  print(RemoteUpdateControllerRead(LEFT+MID+RIGHT, 4), "0 for Golden / 1 for App FW") # =='1' if success
  print("\n_______________ \n")# Load GOLDEN Firmware into FPGAs
  return RemoteUpdateControllerRead(LEFT+MID+RIGHT, 4) # add return for tests

def write_flash_from_RPD_file(FPGA, file_name ="output_file_auto_1.8.rpd", end_addr_file = 0xD64709):
  with open("Flash_FW/"+file_name, "rb") as file:

    print("\n*** START FLASH PROGRAMMATION FROM RPD FILE ***\n")
    start_addr_file= 0x800000   # should bealways this value
    pourcent  = 0               
    i         = start_addr_file # file indice
    j         = 0               # sector indice
    wordlist = []               # list de 32 mots de 32 bits

    file.read(start_addr_file)  # Pointer to file addr
    start_addr_FPGA = 0x200000  # start addr of FPGAs SFL

    # protect sector 0-127 of EPCQ128A for all FPGAs
    print("protect sector 0 to 127 of the FLASH\n")
    FlashProtectSector0to127(LEFT+MID+RIGHT)

    # erase App FW sector (before write)
    for j in range(128):
      FlashEraseSector(FPGA, j+128)
      ## % Progression erase sector
      pourcent = (j/127)*100
      sys.stdout.write("Erase sectors {}% \r".format(pourcent))
      sys.stdout.flush()
    pourcent = 0

    # Read file bytes and construct vector to write [32 words of 32 bits]
    while start_addr_file<= i <= end_addr_file-3:
      ## % Progression programmation
      pourcent = ((i-0x800000)/((end_addr_file-3)-0x800000))*100
      sys.stdout.write("Programmation FLASH: {} %   \r".format(pourcent))
      sys.stdout.flush()

      if len(wordlist)==32: # If vector is full
        FlashWrite(FPGA,start_addr_FPGA,wordlist)
        # 32 words burst ==> addr = addr + 32
        start_addr_FPGA += 32
        wordlist = []

      else: # reverse bytes order and create 32 bits buffer
        # Read binary file
        a1 = ord(file.read(1))
        a2 = ord(file.read(1))
        a3 = ord(file.read(1))
        a4 = ord(file.read(1))
        # Reverse bit order
        x1 = reverse_bit_order(a1,8)
        x2 = reverse_bit_order(a2,8)
        x3 = reverse_bit_order(a3,8)
        x4 = reverse_bit_order(a4,8)
        # LSB first build 32 bit vector
        buf32 = (x4 << 24) + (x3 << 16) + (x2 << 8) + x1
        wordlist.append(buf32)
        buf32 = 0
        i+= 4      # increment 32 bits (4 octets)

  file.close()


########################## TESTS and MAIN CODE START HERE ###################################

if __name__ == "__main__":

  # INIT
  project = fc7(False)
  DL, UL = project.downlink, project.uplink
  feb = feb_v2_cycloneV_r1(project, False)

  project.stop_acquisition()
  project.fifos_clear()

  sca=feb_sca(project.fpga_registers, True)

  FPGA_underTest = LEFT+MID+RIGHT
  App_FW_baseAddr=0x00800000
  FlashProtectSector0to127(LEFT+MID+RIGHT)
  #compareFlashToRPD()

  write_flash_from_RPD_file(FPGA_underTest, file_name ="output_file_auto_1.8.rpd", end_addr_file = 0xD64709)    # Rewrite Flash / end_addr_file : end address of app FW : see page_1 END ADDR
  for i in range(1):
      
      sca_nconfig_cycle()
      time.sleep(2)
      jump_to_app()
      time.sleep(2)
      Read_FW_mode(FPGA_underTest)
  
#  Read_FW_mode(FPGA_underTest)
  # status of FW loaded
#  Read_FW_mode(FPGA_underTest)
  

  ## Exemple : reprogram FPGA LEFT
  #---------------------------------------------------------------

 ### sca_nconfig_cycle()
# reprogram FPGAs from Flash
 ### jump_to_app()                                                                                                 # status of FW loaded


#---------------------------------------------------------------
  
  
  
  
  # # ##========================================================================##
  # # ############################ TEST VALIDATION ###############################
  # # ## - FLASH PROGRAMMATION TEST OVER 50 reprogrammation : STATUS : Validated 
  # # ##========================================================================##
  # x = []
  # i= 0
  # print("\nREPROGRAM FLASH\n")
  # for i in range(50):
  #   t1 = time.time()
  #   write_flash_from_RPD_file(LEFT+MID+RIGHT) # Rewrite Flash
  #   t2 = time.time()
  #   temps = t2-t1

  #   print("Time for flash reprogrammation is : ", temps,"sec.")
  #   sca_nconfig_cycle() # reprogram
  #   x.append([jump_to_app(), temps]) # FW 1.8

  # j=0
  # for j in range(len(x)): 
  #   print(x[i])
  # # ##========================================================================##




  # ##========================================================================##
  # ############################ TEST VALIDATION ###############################
  # ## - Uncomment this bloc to test Flash reprogrammation
  # ##========================================================================##

  # FlashEraseSector(FPGA_underTest, 128)

  # l=FlashReadback(FPGA_underTest, (App_FW_baseAddr>>2), size = 10) # read  MEM
  
  # for fpga in l:
    # print(fpga)
    # print("_"*50)

  # #FlashUnprotectAllSector(LEFT)

  # # Reconfigure FPGAs
  # sca_nconfig_cycle()
  # time.sleep(1)

  # ## Jump to App Firmware ##
  # ## - Not possible if MEM corrupted
  # ##------------------------------------
  # jump_to_app() # FW 1.8
  # print("\n============================== \n")
  # ##-- Jump to GoldenFW to reprogram MEM 
  # jump_to_GoldenImg()
  # print("\n============================== \n")

  
  # time.sleep(1)

  # #sys.exit()

  # print("REPROGRAM FLASH \n")
  # #FlashUnprotectAllSector(FPGA_underTest)
  # write_flash_from_RPD_file(FPGA_underTest) # Rewrite Flash
  # sca_nconfig_cycle() # reprogram
  # time.sleep(1)
  # #FlashReadback(FPGA_underTest, (0x00D34709>>2)-100, size = 20) # read  MEM


  # ## Jump to App Firmware ##
  # ## - Possible after Rewriting
  # ##------------------------------------
  # jump_to_app() # FW 1.8
  # print("\n==============================\n")
  # jump_to_GoldenImg()
  # print("\n==============================\n")
  # jump_to_app() 
  # ##-------------------------------------
  # ##========================================================================##







  sys.exit()
