import time
import sys
import os
import types
from tools_box import *
from feb_fpga_registers import *


###---------------------------------------------###
#     This class instanciate FEB FPGAs            #
###---------------------------------------------###
class feb_fpga:

    def __init__(self,fc7,fpga_list):
        self.fc7 = fc7
        self.fpga_list = fpga_list

        self.fpgas = {}
        for key,id in self.fpga_list.items():
            self.fpgas[key] = feb_fpga_registers(id)


 #######################################################
 ## FPGA REGISTER ACCESS
 #######################################################
    def write(self,fpga,reg,data):
        fpga_id = self.fpgas[fpga].fpga_id

        ## test data type
        if isinstance(data,list):
            for x in data:
                if not(isinstance(x,int)):
                    raise(TypeError("write_fpga_register function : data type incorrect !"))
                wdata = data
        elif isinstance(data,int):
            wdata = [data]
        else:
            raise(TypeError("write_fpga_register function : data type incorrect !"))

        ## test address type
        if isinstance(reg,str):
            if reg in self.fpgas[fpga].fpga_register.keys():
                if self.fpgas[fpga].get_mode(reg)=="rw" or self.fpgas[fpga].get_mode(reg)=="wo":
                    address = self.fpgas[fpga].get_address(reg)
                else:
                    raise(ValueError("write_fpga_register function : mode access incorrect !"))
            else:
                raise(ValueError("write_fpga_register function : address name unknown !"))

        elif isinstance(reg,int):
            if self.fpgas[fpga].get_mode(reg)=="rw" or self.fpgas[fpga].get_mode(reg)=="wo":
                address = self.fpgas[fpga].get_address(reg)
            else:
                raise(ValueError("write_fpga_register function : mode access incorrect !"))

        else:
            raise(TypeError("write_fpga_register function : address type incorrect !"))

        # Send request to fc7
        self.fc7.downlink.send_SC_frame(fpga_id,1,address,wdata)



    def read(self,fpga,reg,nb_data):
        fpga_id = self.fpgas[fpga].fpga_id
        ## test address type
        if isinstance(reg,str):
            if reg in self.fpgas[fpga].fpga_register.keys():
                if self.fpgas[fpga].get_mode(reg)=="rw" or self.fpgas[fpga].get_mode(reg)=="ro":
                    address = self.fpgas[fpga].get_address(reg)
                    nb =  self.fpgas[fpga].get_size(reg)
                else:
                    raise(ValueError("write_fpga_register function : mode access incorrect !"))
            else:
                raise(ValueError("write_fpga_register function : address name unknown !"))

        elif isinstance(reg,int):
            if self.fpgas[fpga].get_mode(reg)=="rw" or self.fpgas[fpga].get_mode(reg)=="ro":
                address = self.fpgas[fpga].get_address(reg)
                nb = nb_data
            else:
                raise(ValueError("write_fpga_register function : mode access incorrect !"))

        else:
            raise(TypeError("write_fpga_register function : address type incorrect !"))

        # Send request to fc7
        self.fc7.downlink.send_SC_frame(fpga_id,0,address,nb)
        time.sleep(0.1)
        # Read data from fc7
        r =self.fc7.uplink.receive_SC_frame()
        if fpga=="all":
            return r
        else:
            if nb_data==1:
                if r[fpga]==[]:
                    return -1
                else:
                    return r[fpga][0]
            else:
                return r[fpga]

if __name__ == '__main__':
    feb = feb_v2_cycloneV_r1(None,None,None)
    proj = feb_fpgas(None, feb)
    proj.write("fpga_0","GENERAL",0x44)
