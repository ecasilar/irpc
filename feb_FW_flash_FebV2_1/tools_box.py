#!/usr/bin/env python
# -*- coding: utf-8 -*-

class bcolors:
    HEADER = '\033[95m'
    RED   = "\033[1;31m"  
    BLUE  = "\033[1;34m"
    CYAN  = "\033[1;36m"
    GREEN = "\033[1;32m"
    WARNING = '\033[1;93m'
    FAIL = "\033[1;31m" 
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class bf(object):

    """
    The bitfield manipulation class that enables individual bits in a int
    or long to be manipulated as a list (via access and slicing)
    """

    def __init__(self, value=0):
        """constructor"""

        self._d = value

    def __getitem__(self, index):
        """Get an individual bit."""

        if isinstance(index, slice):
            return self.__getslice__(index.start, index.stop)

        return (self._d >> index) & 1

    def __setitem__(self, index, value):
        """Set an individual bit."""

        if isinstance(index, slice):
            self.__setslice__(index.start, index.stop, value)
            return
        value = (value & 1) << index
        mask = 1 << index
        self._d = (self._d & ~mask) | value

        
    def __getslice__(self, start, end):
        """Get the value of a range of bits."""

        mask = 2 ** (end - start) - 1

        return (self._d >> start) & mask

    def __setslice__(self, start, end, value):
        """Set a range of bits, using the slice syntax: num[2:3]=1."""

        mask = 2 ** (end - start) - 1
        value = (value & mask) << start
        mask = mask << start
        self._d = (self._d & ~mask) | value

        return (self._d >> start) & mask

    def __int__(self):
        """Retrieve underlying numeric representation."""

        return self._d


def bits_modify(value, bits_range, bits_value):
    """Modifie la valeur des bits désirés et retourne la nouvelle valeur.

    >>> bits_modify(123, (0,0), 0)
    122L

    """

    v = bf(value)
    v[bits_range[0]:bits_range[1] + 1] = bits_value

    return int(v)


def bits_read(value, bits_range):
    """Retourne la valeur correspondante aux bits séléctionnés.

    >>> bits_read(123, (0,0))
    1

    """

    v_bits = bf(value)

    return int(v_bits[bits_range[0]:bits_range[1] + 1])


if __name__ == '__main__':
    import doctest
    doctest.testmod()
    bf(5)[0:3]
