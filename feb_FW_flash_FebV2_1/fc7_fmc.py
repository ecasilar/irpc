import uhal
import time
import sys
import os
import types
from fc7_registers import *
from tools_box import *

###---------------------------------------------###
#     This class instanciate FMC Ctrl and Status  #
###---------------------------------------------###

class fc7_fmc:

  def __init__(self,fc7_registers,verbose):
  	self.verbose = verbose
  	self.fc7_registers = fc7_registers
  	self.config = self.fc7_registers.get_FMC_settings()


 ## Get function
  def read_status_register(self):
  	self.config = self.fc7_registers.get_FMC_settings()
  	return self.config

  def get_slot_l12_pwr_en(self):
  	return int(bf(self.config)[0])

  def get_slot_l8_pwr_en(self):
  	return int(bf(self.config)[1])

  def get_slot_l12_pg_m2c(self):
  	return int(bf(self.config)[16])

  def get_slot_l12_prst_l(self):
  	return int(bf(self.config)[17])

  def get_slot_l8_pg_m2c(self):
  	return int(bf(self.config)[18])

  def get_slot_l8_prst_l(self):
  	return int(bf(self.config)[19])


## Set function

  def write_config_register(self):
  	self.fc7_registers.set_FMC_settings(self.config)

  def set_slot_l12_pwr_en(self,value):
  	r= bf(self.config)
  	r[0] = bf(value)[0]
  	self.config = int(r)

  def set_slot_l8_pwr_en(self,value):
  	r= bf(self.config)
  	r[1] = bf(value)[0]
  	self.config = int(r)

  ##  high level function

  def slot1_power_on(self):
  	self.set_slot_l12_pwr_en(1)
  	self.write_config_register()

  def slot1_power_off(self):
  	self.set_slot_l12_pwr_en(0)
  	self.write_config_register()

  def slot2_power_on(self):
  	self.set_slot_l8_pwr_en(1)
  	self.write_config_register()

  def slot2_power_off(self):
  	self.set_slot_l8_pwr_en(0)
  	self.write_config_register()

  def init(self):
    self.slot2_power_on()
    self.slot1_power_on()
    if self.verbose : self.info()

  def info(self):
  	self.read_status_register()
  	print("#### FMC status ####")
  	if self.get_slot_l12_pwr_en()==1:
  		print("--> slot 1 power  : ON")
  	else:
  		print("--> slot 1 power  : OFF")

  	print("--> slot 1 PG_m2c : {}".format(self.get_slot_l12_pg_m2c()))
  	print("--> slot 1 prsnt  : {}".format(self.get_slot_l12_prst_l()))

  	if self.get_slot_l8_pwr_en()==1:
  		print("--> slot 2 power  : ON")
  	else:
  		print("--> slot 2 power  : OFF")
  	print("--> slot 2 PG_m2c : {}".format(self.get_slot_l8_pg_m2c()))
  	print("--> slot 2 prsnt  : {}".format(self.get_slot_l8_prst_l()))
