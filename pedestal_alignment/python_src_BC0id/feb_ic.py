import time
import sys
import os
import types
from fc7_registers import *
from tools_box import *

###---------------------------------------------###
#     This class is the FEB Internal Control      #
###---------------------------------------------###
class feb_ic:

  def __init__(self,fc7_registers,verbose):
  	self.verbose = verbose
  	self.fc7_registers = fc7_registers
  	self.config = [0x00000000,0x00000000]
  	self.status = 0x00000000
  	self.pulse = 0x00000000

 ## Get function
   # config
  def read_config_register(self):
  	self.config = self.fc7_registers.get_ic_config()
  	return self.config

  def get_i2c_address(self):
  	return int(bf(self.config)[0][0:8])

  def get_parity_error_mask(self):
  	return int(bf(self.config)[0][8:16])

  def get_tx_data(self):
  	return int(bf(self.config)[0][16:24])

  def get_tx_address(self):
  	return int(bf(self.config)[1][0:16])

  def get_tx_len(self):
  	return int(bf(self.config)[1][16:31])

   # status
  def read_status_register(self):
  	self.status = self.fc7_registers.get_ic_status()
  	return self.status

  def get_rx_data(self):
  	return int(bf(self.status)[0:7])
  
  def get_tx_rdy(self):
    return int(bf(self.status)[8])

  def get_rx_empty(self):
    return int(bf(self.status)[9])

   # pulse
  def read_pulse_register(self):
  	self.status = self.fc7_registers.get_ic_pulse()
  	return self.status

  def get_tx_wr(self):
  	return int(bf(self.pulse)[0])

  def get_rx_rd(self):
  	return int(bf(self.pulse)[1])

  def get_tx_start_wr(self):
  	return int(bf(self.pulse)[2])

  def get_rx_start_rd(self):
  	return int(bf(self.pulse)[3])

 ## Set function
   # config
  def write_config_registers(self):
  	self.fc7_registers.set_ic_config(self.config)

  def set_i2c_address(self,value):
  	r= bf(self.config[0])
  	r[0:8] = bf(value)[0:8]
  	self.config[0]= int(r)

  def set_parity_error_mask(self,value):
  	r= bf(self.config[0])
  	r[8:16] = bf(value)[0:8]
  	self.config[0]= int(r)

  def set_tx_data(self,value):
  	r= bf(self.config[0])
  	r[16:24] = bf(value)[0:8]
  	self.config[0]= int(r)

  def set_tx_address(self,value):
  	r= bf(self.config[1])
  	r[0:16] = bf(value)[0:16]
  	self.config[1]= int(r)

  def set_tx_len(self,value):
  	r= bf(self.config[1])
  	r[16:32] = bf(value)[0:16]
  	self.config[1]= int(r)


   # pulse
  def write_pulse_registers(self):
  	self.fc7_registers.set_ic_pulse(self.pulse)

  def set_tx_wr(self,value):
  	r= bf(self.pulse)
  	r[0] = bf(value)[0]
  	self.pulse= int(r)

  def set_rx_rd(self,value):
  	r= bf(self.pulse)
  	r[1] = bf(value)[0]
  	self.pulse= int(r)

  def set_tx_start_wr(self,value):
  	r= bf(self.pulse)
  	r[2] = bf(value)[0]
  	self.pulse= int(r)

  def set_rx_start_rd(self,value):
  	r= bf(self.pulse)
  	r[3] = bf(value)[0]
  	self.pulse= int(r)


  ## high level function
  def pulse_tx_wr(self):
  	self.set_tx_wr(1)
  	self.write_pulse_registers()
  	time.sleep(0.1)
  	self.set_tx_wr(0)
  	self.write_pulse_registers()

  def pulse_rx_rd(self):
  	self.set_rx_rd(1)
  	self.write_pulse_registers()
  	time.sleep(0.1)
  	self.set_rx_rd(0)
  	self.write_pulse_registers()

  def pulse_tx_start_wr(self):
  	self.set_tx_start_wr(1)
  	self.write_pulse_registers()
  	time.sleep(0.1)
  	self.set_tx_start_wr(0)
  	self.write_pulse_registers()

  def pulse_rx_start_rd(self):
  	self.set_rx_start_rd(1)
  	self.write_pulse_registers()
  	time.sleep(0.1)
  	self.set_rx_start_rd(0)
  	self.write_pulse_registers()
