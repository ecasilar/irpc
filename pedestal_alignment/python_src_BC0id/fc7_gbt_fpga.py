import uhal
import time
import sys
import os
import types
from fc7_registers import *
from tools_box import *

###---------------------------------------------###
#        This class configure GBT-FPGA            #
###---------------------------------------------###
class fc7_gbt_fpga:
    def __init__(self,fc7_registers,verbose):
        self.verbose=verbose
        self.fc7_registers = fc7_registers
        self.config = self.fc7_registers.get_config_gbt_fpga()
        self.status = self.fc7_registers.get_status_gbt_fpga()


 ## Get function
   # status
    def read_status_register(self):
        self.config = self.fc7_registers.get_status_gbt_fpga()
        return self.config

    def get_header_flag(self):
        self.read_status_register()
        return bf(self.status)[0]

    def get_header_locked(self):
        self.read_status_register()
        return bf(self.status)[1]

    def get_tx_ready(self):
        self.read_status_register()
        return bf(self.status)[2]

    def get_rx_ready(self):
        self.read_status_register()
        return bf(self.status)[3]

    def get_link_ready(self):
        self.read_status_register()
        return bf(self.status)[4]

    def get_tx_aligned(self):
        self.read_status_register()
        return bf(self.status)[5]

    def get_tx_aligned_computed(self):
        self.read_status_register()
        return bf(self.status)[6]

    def get_rx_frameclk_rdy(self):
        self.read_status_register()
        return bf(self.status)[7]

    def get_rx_error_detected(self):
        self.read_status_register()
        return bf(self.status)[8]

    def get_rx_isdata_sel(self):
        self.read_status_register()
        return bf(self.status)[9]

 ## config
    def read_config_register(self):
        self.config = self.fc7_registers.get_config_gbt_fpga()
        return self.config

    def get_reset(self):
        self.read_config_register()
        return bf(self.config)[0]

    def get_tx_reset(self):
        self.read_config_register()
        return bf(self.config)[1]

    def get_rx_reset(self):
        self.read_config_register()
        return bf(self.config)[2]

    def get_rst_reconfig(self):
        self.read_config_register()
        return bf(self.config)[3]

    def get_tx_pol(self):
        self.read_config_register()
        return bf(self.config)[4]

    def get_rx_pol(self):
        self.read_config_register()
        return bf(self.config)[5]


 ## Set function
   # config

    def write_config(self):
        self.fc7_registers.set_config_gbt_fpga(self.config)

    def set_reset(self,value):
        r= bf(self.config)
        r[0] = bf(value)[0]
        self.config= int(r)

    def set_tx_reset(self,value):
        r= bf(self.config)
        r[1] = bf(value)[0]
        self.config= int(r)

    def set_rx_reset(self,value):
        r= bf(self.config)
        r[2] = bf(value)[0]
        self.config= int(r)

    def set_rst_reconfig(self,value):
        r= bf(self.config)
        r[3] = bf(value)[0]
        self.config= int(r)

    def set_tx_pol(self,value):
        r= bf(self.config)
        r[4] = bf(value)[0]
        self.config= int(r)

    def set_rx_pol(self,value):
        r= bf(self.config)
        r[5] = bf(value)[0]
        self.config= int(r)


## high level function
    def reset(self):
        self.set_reset(1)
        self.set_tx_reset(1)
        self.set_rx_reset(1)
        self.write_config()
        time.sleep(2)
        self.set_reset(0)
        self.write_config()
        time.sleep(1)
        self.set_tx_reset(0)
        self.set_rx_reset(0)
        self.write_config()
        time.sleep(2)
        if self.verbose : self.info()

    def ready(self):
        if self.get_link_ready():
            return True
        else:
            return False

    def init(self):
        print("################################################")
        print("GBT-FPGA reseting...")
        self.set_tx_pol(1)
        self.set_rx_pol(1)
        self.write_config()
        self.reset()
        i=0
        while self.ready()==False :
            if i>10:
                break
            else:
                i = i +1
                time.sleep(0.2)
        if self.ready()==True :
            print(bcolors.GREEN +"GBT-FPGA ready [OK]" +bcolors.ENDC)
        else :
            print(bcolors.FAIL + "GBT-FPGA reseting time out !"+bcolors.ENDC)
            print(bcolors.FAIL + "--> Verify the link"+bcolors.ENDC)


    def info(self):
        self.read_status_register()
        self.read_config_register()
        print("#### GBT_CONFIG REGISTER ####")
        print("GBT_fpga_rst          : {}".format(self.get_reset()))
        print("GBT_fpga_tx_rst       : {}".format(self.get_tx_reset()))
        print("GBT_fpga_rx_rst       : {}".format(self.get_rx_reset()))
        print("GBT_fpga_rst_reconfig : {}".format(self.get_rst_reconfig()))
        print("GBT_fpga_tx_pol       : {}".format(self.get_tx_pol()))
        print("GBT_fpga_rx_pol       : {}".format(self.get_rx_pol()))
        print("## GBT_STATUS REGISTER ##")
        print("Header_flag           : {}".format(self.get_header_flag()))
        print("Header_locked         : {}".format(self.get_header_locked()))
        print("Tx_ready              : {}".format(self.get_tx_ready()))
        print("Rx_ready              : {}".format(self.get_rx_ready()))
        print("Link_ready            : {}".format(self.get_link_ready()))
        print("Tx_aligned            : {}".format(self.get_tx_aligned()))
        print("Tx_aligned_computed   : {}".format(self.get_tx_aligned_computed()))
        print("Rx_frameclk_rdy       : {}".format(self.get_rx_frameclk_rdy()))
        print("Rx_error_detected     : {}".format(self.get_rx_error_detected()))
        print("Rx_isdata_sel         : {}".format(self.get_rx_isdata_sel()))
