import uhal
import time
import sys
import os
import types
from fc7_fifo import *
from fc7_ipbus import *
from fc7_cdce import *
from fc7_registers import *
from fc7_gbt_fpga import *
from fc7_fmc import *
from fc7_fifo_template import *
from fc7_downlink import *
from fc7_uplink import *
from tools_box import *
import numpy as np
import matplotlib.pyplot as plt


###---------------------------------------------###
#     This class is the main class of FC7         #
###---------------------------------------------###

class fc7:
    def __init__(self,verbose):
        #IPBus
        self.ipbus_device = fc7_ipbus()
        #Registers
        self.fpga_registers = fc7_registers(self.ipbus_device,verbose)
        #CDCE
        self.cdce= fc7_cdce(self.fpga_registers,verbose)
        #GBT
        self.gbt_fpga = fc7_gbt_fpga(self.fpga_registers,verbose)
        #FMC
        self.fmc = fc7_fmc(self.fpga_registers,verbose)
        #FIFO transmission
        self.tx_fifo_SC = fc7_fifo_template(self.fpga_registers,"IN",4,5,2,verbose)
        self.tx_fifo_SC_Xdomain = fc7_fifo_template(self.fpga_registers,"NONE",6,7,3,verbose)
        #FIFO Reception SC
        self.rx_fifo_SC  = fc7_fifo_template(self.fpga_registers,"OUT",0,1,1,verbose)
        self.rx_fifo_SC_Xdomain = fc7_fifo_template(self.fpga_registers,"NONE",2,3,0,verbose)
        #FIFO Reception TDC
        self.rx_fifo_TDC_buf = fc7_fifo_template(self.fpga_registers,"NONE",12,13,6,verbose)
        self.rx_fifo_TDC = fc7_fifo_template(self.fpga_registers,"OUT",8,9,5,verbose)
        self.rx_fifo_TDC_Xdomain = fc7_fifo_template(self.fpga_registers,"NONE",10,11,4,verbose)
        #Uplink Com.
        self.uplink = fc7_uplink(self.rx_fifo_SC,self.rx_fifo_TDC,verbose)
        #Downlink Com.
        self.downlink = fc7_downlink(self.tx_fifo_SC,verbose)

    def get_nb_word_in_fifo_SC(self):
        return (self.fpga_registers.get_fifo_frame_nb_words()>>17) & 0xFFF

  #######################################################################
  ## FC7 FIFOs
  #######################################################################
    def fifos_clear(self):
        self.tx_fifo_SC_Xdomain.clear()
        self.tx_fifo_SC.clear()
        self.rx_fifo_SC_Xdomain.clear()
        self.rx_fifo_SC.clear()
        self.rx_fifo_TDC_Xdomain.clear()
        self.rx_fifo_TDC.clear()
        self.rx_fifo_TDC_buf.clear()

  #######################################################################
  ## FC7 TDC RECEPTION MODES
  #######################################################################
    # Acquisition control
    # -------------------
    def start_acquisition(self):
    # start TDC acquisition
        r=bf(self.fpga_registers.get_general_register())
        r[11] = 1
        return self.fpga_registers.set_general_register(int(r))
        
    def stop_acquisition(self):
    # Stop TDC acquisition
        r=bf(self.fpga_registers.get_general_register())
        r[11] = 0
        return self.fpga_registers.set_general_register(int(r))
        
    def acquisition_state(self):
        r=bf(self.fpga_registers.get_general_register())
        if r[11]==1:
            return "running"
        else:
            return "idle" 
            
    def set_acquisition_mode(self,mode):
        r=bf(self.fpga_registers.get_general_register())
        if mode=="Circular_Buffer_Internal":
            r[6:9]=0
        elif mode=="Circular_Buffer_External":
            r[6:9]=3
        elif mode=="TDC_Count":
            r[6:9]=1
        elif mode=="Time_Window":
            r[6:9]=2
        elif mode=="Wait_orbit":
            r[6:9]=4
        else:
            raise(ValueError((bcolors.FAIL +"Mode incorrect !"+ bcolors.ENDC)))
        return self.fpga_registers.set_general_register(int(r))
        
    def get_acquisition_mode(self):
        r=bf(self.fpga_registers.get_general_register())
        if r[6:9]==0:
            return "Circular_Buffer_Internal"
        elif r[6:9]==3:
            return "Circular_Buffer_External"
        elif r[6:9]==1:
            return "TDC_Count"
        elif r[6:9]==2:
            return "Time_Window"
        elif r[6:9]==4:
            return "Wait_orbit"
        else:
            raise(ValueError((bcolors.FAIL +"Mode unknow !"+ bcolors.ENDC)))
    
    # Acquisition parameters [set/get]
    # --------------------------------
    #SET
    def set_nb_frame_acquisition(self,value):
        r=bf(self.fpga_registers.get_general_register())
        r[12:29]=bf(value)[0:18]
        self.fpga_registers.set_general_register(int(r))

    def set_circular_buffer_size(self,nb_frame):
        self.fpga_registers.set_circular_buffer_size(nb_frame)

    def set_time_window(self,time_ms):
        #period clock 40MHz
        value = int(time_ms//0.000025)
        self.fpga_registers.set_time_window_size(value)
        
    def set_general_rst(self,value):
        r=bf(self.fpga_registers.get_general_register())
        r[30]=bf(value)[0]
        self.fpga_registers.set_general_register(int(r))        
    #GET
    def get_nb_frame_acquisition(self):
    #return the number of frames into the fifo buffer
        return int(bf(self.fpga_registers.get_general_register())[12:29])

    def get_time_window(self):
        r=self.fpga_registers.get_time_window_size()
        return (str(r*0.000025)+"ms")

    def get_circular_buffer_size(self):
        return self.fpga_registers.get_circular_buffer_size()    
    
    def get_acquisition_status(self):
    # Status of the recepetion TDC (when True : acquisition is done)
        if bf(self.fpga_registers.get_fifos_status())[14]==1:
            return True
        else:
            return False

    def set_reset_buf(self, value):
    # reset reg of TDC buffer [Reception]
        r=bf(self.fpga_registers.get_fifos_ctrl())
        r[6]= value
        self.fpga_registers.set_fifos_ctrl(int(r))
            
    def set_reset_TDC_path(self, value):
    # reset reg of TDC buffer [Reception]
        r=bf(self.fpga_registers.get_fifos_ctrl())
        r[7]= value
        self.fpga_registers.set_fifos_ctrl(int(r))
  ##================================##
  ##== differents modes launchers ==##
  ##================================##
    def get_TDC_datas(self):
        cpt_timeout = 0
        print("##### Start Acquisition #####")
        self.fc7.start_acquisition()
        while (self.fc7.get_acquisition_status() != True):
            if cpt_timeout==100:
                self.fc7.stop_acquisition()
                return -1
            else:
                time.sleep(0.2)
                cpt_timeout = cpt_timeout + 1
        print("acquisition done : {}".format(self.fc7.get_acquisition_status()))
        self.fc7.stop_acquisition()
        r=self.fc7.uplink.receive_TDC_frame()
        return r
    
    def time_window_acq(self, time_ms):
        self.set_acquisition_mode("Time_Window")
        self.set_time_window(time_ms)
        self.get_TDC_datas()
        
    def TDC_count_acq(self, number_of_datas):
        self.set_acquisition_mode("TDC_Count")
        self.set_nb_frame_acquisition(number_of_datas)
        self.get_TDC_datas()
    
    def Circular_extTrig_acq(self, buffer_size):
        self.set_acquisition_mode("Circular_Buffer_External")
        self.set_circular_buffer_size(buffer_size)
        self.get_TDC_datas()
        
    def Circular_intTrig_acq(self, buffer_size):
        self.set_acquisition_mode("Circular_Buffer_Internal")
        self.set_circular_buffer_size(buffer_size)
        self.get_TDC_datas()
        
    def set_internal_trigger(self, value):
    # Internal trigger to stop acq. in CIRCULAR_BUFFER_INTERNAL MODE [set value = 1 launch pulsed trigger, value = 0 reset pulse generation]
        r=bf(self.fpga_registers.get_general_register())
        r[10] = bf(value)[0]
        return self.fpga_registers.set_general_register(int(r))
 
 
  #######################################################################
  ## FC7 ASIC CALIBRATION
  #######################################################################   
    # Strip Calibration parameters
    #-----------------------------
    def set_calib_enable_trig(self, value):
    ### Define calibration method : with a single trigger generated synchronously (efficiency mode) or without (noise)
        r=bf(self.fpga_registers.get_general_register())
        r[31] = bf(value)[0]      
        return self.fpga_registers.set_general_register(int(r))
        
    def get_calib_enable_trig(self):
        return bf(self.fpga_registers.get_general_register())[31]
        
    def set_short_wdw_delay(self, value):
    # delay of the window acquistion (sweep on this value to detect trigger generated) [firmware with FMC]
        r=bf(self.fpga_registers.get_trigger_out_fmc())
        r[4:14]=bf(value)[0:10]
        self.fpga_registers.set_trigger_out_fmc(int(r))  
        
    def get_short_wdw_delay(self):
        return bf(self.fpga_registers.get_trigger_out_fmc())[4:14]
    
    def set_copy_if_resync(self,value):
        r=bf(self.fpga_registers.get_trigger_out_fmc()) 
        r[31]=bf(value)[0]
        self.fpga_registers.set_trigger_out_fmc(int(r))
    
    def get_copy_if_resync(self):
        return bf(self.fpga_registers.get_trigger_out_fmc())[31]
        
    def get_calibration_done(self):
    # Status of the asic calibration (when '1' : done)
        return bf(self.fpga_registers.get_fifos_status())[15]

    def get_counter_trigger(self):
    # Number of trigger during asic calibration
        return self.fpga_registers.get_counter_trigger()

    def get_nb_word_in_fifo_buf(self):
    # Number of datas into FIFO buffer 
        return bf(self.fpga_registers.get_fifo_frame_nb_words())[0:16]

    def get_rd32b_word_in_fifo_ipbus(self):
        return bf(self.fpga_registers.get_fifos_status())[16:30]

    def start_TDC_counters(self):
    # Start Asic Calibration
        r=bf(self.fpga_registers.get_general_register())
        r[29] = 1
        return self.fpga_registers.set_general_register(int(r))

    def stop_TDC_counters(self):
    # Stop Asic Calibration
        r=bf(self.fpga_registers.get_general_register())
        r[29] = 0
        return self.fpga_registers.set_general_register(int(r))
    
    def start_calibration_with_trigger(self):
        # start calibration with trigger generation
        self.set_trigger_out_ch1(1) # start calibration with trigger gen.
        while (self.get_calibration_done() != 1):
            time.sleep(0.1)
            r=self.get_counter_trigger()
            self.set_trigger_out_ch1(0) #stop calibration
        return r
                
    def get_trigger_efficiency(self, strip, threshold_current):   
        # calibration calculating trigger efficiency at specified threashold value
        cpt_timeout = 0
        trig_100 = []
        self.set_calib_enable_trig(1) # calibration use trigger signal generated ch1 out of FMC if '0' ==> use self.start_TDC_counters() fonction to start calib
        self.set_transmission_mode(3) # single pulse mode
        self.set_time_window(0.00001) # 100 ns calibration
        self.set_short_wdw_delay(44)  # delay of 100 ns window and trigger for detection
        self.set_trigger_out_ch1(0)
        self.feb.configure_10b_dac(strip,threshold_current)
        for i in range(100):
            r = self.start_calibration_with_trigger()
            trig_100.append(r)
        One = trig_100.count(1)
        efficiency = One 
        return efficiency  

    def get_trigger_in_windows(self, window_time_ms = 1, delay = 0):
        # CCount the number of trigger within a time window
        self.fc7.set_time_window(window_time_ms)
        self.fc7.set_short_wdw_delay(delay)
        cpt_timeout = 0
        self.fc7.start_TDC_counters()
        while(self.fc7.get_calibration_done()!=1):
            if cpt_timeout==10:
                self.fc7.stop_TDC_counters()
                return -1
            else:
                time.sleep(0.2)
                cpt_timeout = cpt_timeout + 1
        r=self.fc7.get_counter_trigger()
        self.fc7.stop_TDC_counters()
        return r
    

  #######################################################################
  ## FC7 Transmission signals BC0 / Resync / Trigger
  #######################################################################   
    # BC0 / Resync / trigger enable
    # -----------------------------
    def set_enable_BC0(self, value):
        r=bf(self.fpga_registers.get_FC_transmission())
        r[0]=bf(value)[0]
        self.fpga_registers.set_FC_transmission(int(r))
        
    def set_enable_resync(self, value):
        r=bf(self.fpga_registers.get_FC_transmission())
        r[1]=bf(value)[0]
        self.fpga_registers.set_FC_transmission(int(r))

    def set_trigger_out_ch1(self, value):
        r=bf(self.fpga_registers.get_trigger_out_fmc())
        r[0]=bf(value)[0]
        self.fpga_registers.set_trigger_out_fmc(int(r))
        
    # BC0 / Resync / trigger : enable 2 signals together [use in transmission mode "2" and define desired delay]
    # ----------------------------------------------------------------------------------------------------------
    def set_enable_BC0_Resync(self, value):
    # when set_enable_BC0_Resync(1) ==> BC0 and Resync are both activated on set_enable_BC0(1) command
        r=bf(self.fpga_registers.get_FC_transmission())
        r[2]=bf(value)[0]
        self.fpga_registers.set_FC_transmission(int(r))

    def set_enable_BC0_trigger(self, value):
    # when set_enable_BC0_trigger(1) ==> BC0 and trigger are both activated on set_enable_BC0(1) command
        r = bf(self.fpga_registers.get_FC_transmission())
        r[20] = bf(value)[0]
        self.fpga_registers.set_FC_transmission(int(r))

    # BC0 / Resync / trigger Delay
    # -----------------------------
    def set_reync_delay(self, value):
        r=bf(self.fpga_registers.get_FC_transmission())
        r[5:19]=bf(value)[0:14]
        self.fpga_registers.set_FC_transmission(int(r))

    def set_BC0_delay(self, value):
        r = bf(self.fpga_registers.get_BC0_trigger_delay())
        r[0:14] = bf(value)[0:14]
        self.fpga_registers.set_BC0_trigger_delay(int(r))

    def set_trigger_delay(self, value):
        r = bf(self.fpga_registers.get_BC0_trigger_delay())
        r[15:29] = bf(value)[0:14]
        self.fpga_registers.set_BC0_trigger_delay(int(r))

    # BC0 / Resync / trigger period [use in transmission mode "1"]
    # ------------------------------------------------------------
    def set_resync_period(self,value):
        self.fpga_registers.set_resync_period(value)
    def get_resync_period(self):
        return bf(self.fpga_registers.get_BC0_Resync_period())[16:31] 
        
    def set_BC0_period(self, value):        
        self.fpga_registers.set_BC0_period(value) 

    def get_BC0_period(self):    
        return bf(self.fpga_registers.get_BC0_Resync_period())[0:15] 
        
    def set_trigger_period(self, value):
        r=bf(self.fpga_registers.get_trigger_out_fmc())
        r[14:29]=bf(value)[0:15]
        self.fpga_registers.set_trigger_out_fmc(int(r))

    def get_trigger_period(self, value):
        return bf(self.fpga_registers.get_trigger_out_fmc())[14:29]

    # BC0 cosmic acquisition
    # ------------------------------------------------------------    

    def set_BC0_cosmic(self, value):
        #start/stop comsmic generation
        self.fpga_registers.set_start_orbit(value)

    def start_BC0_cosmic_gen(self, period_ms = 3560, stage1=4,stage2=4,stage3=4,stage4=4):
        # Configurations :  
        #[ max value for stageX : 256 (* 25 ns) / stage1 : val_event, stage2 : Flush + val_event, stage3 : BC0 + val_event stage4 : val_event]
        #============
        #value = int(period_ms//0.000025)

        self.set_BC0_period(period_ms)
        self.set_transmission_mode(0) # cosmic mode
        #stage time values
        self.fpga_registers.set_BC0_cosmic_stage1(stage1)
        self.fpga_registers.set_BC0_cosmic_stage2(stage2)
        self.fpga_registers.set_BC0_cosmic_stage3(stage3)
        self.fpga_registers.set_BC0_cosmic_stage4(stage4)
        # Start BC0 génération
        self.set_BC0_cosmic(1)
        self.set_enable_BC0(1)

        
    def stop_BC0(self):
        # Stop BC0 génération
        self.set_BC0_cosmic(0)
        self.set_enable_BC0(0)


    def start_cosmic_acquisition(self, N_orbits=10):
        # [max Orbit : XX]
        self.fpga_registers.set_BC0_N_orbit(N_orbits)
        self.set_acquisition_mode("Wait_orbit")
        self.start_acquisition()

    def receive_TDC_frame(self):
        if self.get_acquisition_status() == True:
            return self.uplink.receive_TDC_frame()
        else:
            print("Acquisition not finish !")
    # test Resync
    def start_circular_buffer_ext_acq(self, value = 2000):
        self.set_acquisition_mode("Circular_Buffer_External")
        self.set_circular_buffer_size(value)
        self.start_acquisition()


    # def read_fifo_ipbus(self):

   


    # Select transmission mode
    # -----------------------------    
    ##########################################################################################
    def set_transmission_mode(self, value): # used for BC0 / Resync / trigger chan1 generation
    # 0 : BC0_cosmic | 1 : Periodic | 2 : Periodic with Delay | 3 : One pulse
    ##########################################################################################
        r=bf(self.fpga_registers.get_FC_transmission())
        r[3:5]=bf(value)[0:2]
        self.fpga_registers.set_FC_transmission(int(r))
        
    def get_transmission_mode(self):
        return bf(self.fpga_registers.get_FC_transmission())[3:5]


        
  ##==================================##
  ##== differents signal generation ==##
  ##==================================##
    # !!! variable nb_period_ns correspond au nombre de periodes de 25 ns
    def reset_BC0_resync_gen(self):
        #reset generation
        self.set_enable_BC0(0)
        self.set_enable_resync(0) 
        self.set_enable_BC0_Resync(0)

    def reset_transmission(self):
        self.set_enable_BC0(0)
        self.set_enable_resync(0)
        self.set_trigger_out_ch1(0)
        self.set_enable_BC0_Resync(0)
        self.set_enable_BC0_trigger(0)
        self.set_transmission_mode(0)

    def gen_BC0(self, nb_period_ns):
        self.set_enable_BC0(0)
        self.set_enable_BC0_Resync(0)
        self.set_enable_BC0_trigger(0)
        #
        self.set_transmission_mode(1)
        self.set_BC0_period(nb_period_ns)
        self.set_enable_BC0(1)
        
    def gen_resync(self, nb_period_ns):
        self.set_enable_resync(0)
        self.set_enable_BC0_Resync(0)
        #
        self.set_transmission_mode(1)
        self.set_resync_period(nb_period_ns)
        self.set_enable_resync(1)
        
    def gen_BC0_resync_synchronous(self, nb_period_ns):
        self.reset_transmission()
        self.set_transmission_mode(1)
        self.set_BC0_period(nb_period_ns)
        self.set_resync_period(nb_period_ns)
        self.set_enable_BC0_Resync(1)
        self.set_enable_BC0(1)
    
    def gen_BC0_resync_asynchronous(self, nb_period_25ns, nb_delay_resync_ns):
        self.reset_transmission()
        self.set_transmission_mode(2)
        self.set_reync_delay(nb_delay_resync_ns)
        self.set_BC0_period(nb_period_25ns)
        self.set_resync_period(nb_period_25ns)
        self.set_enable_BC0_Resync(1)
        self.set_enable_BC0(1)
        
    ### FMC triggers tests
    #periodic trigger with BC0
    def gen_periodic_BC0_trigger_ch1(self, nb_period_25ns):
        self.reset_transmission()
        self.set_enable_BC0_trigger(1)
        self.set_transmission_mode(1)
        self.set_trigger_period(nb_period_25ns)
        self.set_BC0_period(nb_period_25ns)
        self.set_enable_BC0(1)
    # periodic trigger
    def gen_periodic_trigger_ch1(self, nb_period_25ns):
        self.reset_transmission()
        self.set_transmission_mode(1)
        self.set_trigger_period(nb_period_25ns)
        self.set_trigger_out_ch1(1)

    # generate one pulsed trigger
    def gen_one_trigger_ch1(self):
        self.reset_transmission()
        self.set_transmission_mode(3) ## mode pulsed
        self.set_trigger_out_ch1(1)   ## send trigger
        
        
        
                        ################################
                        ### Transmission Trigger     ###
                        ################################
                        
    #trigger_out fmcl12
    # ---------------------
    # CHAN_1 : Differents modes of transmission : (transmission ; mode : 0 periodic pulse || mode 3 : one pule gen.)
    # CHAN_2 : Busy signal
    
    ###     
    # def set_trigger_out_ch2(self, value):
        # r=bf(self.fpga_registers.get_trigger_out_fmc())
        # r[1]=bf(value)[0]
        # self.fpga_registers.set_trigger_out_fmc(int(r))  
           

    
    def acquisition_info(self):
        print("******************************")
        print("*** ACQUISITION PARAMETERS ***")
        print("******************************")
        print("STATE                 : {}".format(self.acquisition_state()))
        print("MODE                  : {}".format(self.get_acquisition_mode()))
        print("NB_FRAME              : {}".format(self.get_nb_frame_acquisition()))
        print("TIME WINDOW           : {}".format(self.get_time_window()))
        #print("ENABLE TDC COUNTER    : {}".format(self.get_TDC_counters_status()))
        print("CIRCULAR_BUFFER_SIZE  : {}".format(self.get_circular_buffer_size()))
        print("FLAG_ACQ_DONE         : {}".format(self.get_acquisition_status()))
        print("NB_DATA_IN_FIFO_FRAME : {}".format(self.get_nb_word_in_fifo_buf()))

  ###############################################################
  ## HIGH LEVEL FUNCTION
  ###############################################################
  
                        ####################
                        ###  TDC datas   ###
                        ####################  
    #Save TDC datas as row file
    # def save_TDC_datas(self, TDC_data):
    #     fpga0_data = TDC_data["fpga_0"]
    #     fpga1_data = TDC_data["fpga_1"]
    #     fpga2_data = TDC_data["fpga_2"]
    #     #write

    #     with open("TDCdatas.raw", "w") as file:
    #         file.write("#fpga_0\n") 
    #         for x in fpga0_data:
    #             file.write((str(x[0]) + "," +str(x[1]) + "\n"))
    #         file.write("#fpga_1\n") 
    #         for x in fpga1_data:
    #             file.write((str(x[0]) + "," +str(x[1]) + "\n"))
    #         file.write("#fpga_2\n") 
    #         for x in fpga2_data:
    #             file.write((str(x[0]) + "," +str(x[1]) + "\n"))     
    #     file.close()

    ### BC0 id  FW ###
    #Save TDC datas as row file
    def save_TDC_datas(self, Datas):
        for BC0 in Datas.keys():
            #print(BC0)

            fpga0_data = Datas[BC0][0]["fpga_0"]
            fpga1_data = Datas[BC0][0]["fpga_1"]
            fpga2_data = Datas[BC0][0]["fpga_2"]
            # #write
            #file=open("TDCdatas.raw","a") 
            with open("TDCdatas.raw", "a") as file:
                file.write("BC0_id {}\n".format(BC0))

                file.write("#fpga_0\n") 
                for x in fpga0_data:
                   file.write((str(x[0]) + "," +str(x[1]) + "\n"))
                file.write("#fpga_1\n") 
                for x in fpga1_data:
                    file.write((str(x[0]) + "," +str(x[1]) + "\n"))
                file.write("#fpga_2\n") 
                for x in fpga2_data:
                    file.write((str(x[0]) + "," +str(x[1]) + "\n"))     
            file.close()


    # read TDC_data_file
    def read_TDC_datas(self):
      file = open("TDCdatas.raw","r")
      print(file.read())
      file.close()
    
    ########################################################
    # Correction of timestamp
    def TDC_correction(self,TDC_time):
        TDC_corr=[]
        for i in range(1,len(TDC_time)):
            diff =  int(TDC_time[i]) - int(TDC_time[i-1]) # the previous data is higer than the previous
            if diff < 0:
                    diff = diff + 65536
                    TDC_new = int(TDC_time[i]) +  diff
                    TDC_corr.append(TDC_new)
            else:
                TDC_corr.append(TDC_time(i))    
        return TDC_corr    
    #######################################################
    
    #plot TDC datas from raw file : channel / arrival time            
    def read_raw_TDC_file(self, file):
        with open(file,'r') as f:
            mylist = f.read().splitlines()
            r=[]
            for d in mylist:
                r.append(d.split(","))
        graph = {}
        for x in r:
            if x[0][0]=="#":
                title = x[0][1:]
                graph[title] = [[],[]]
            else:
                graph[title][0].append(int(x[0]))
                graph[title][1].append(int(x[1]))
        #plot
        plt.xlabel('time')
        plt.ylabel('channel')
        plt.title('TDC datas')
        plt.grid(True)
        for ref,data in graph.items() :
            #x=self.TDC_correction(data[1])
            x=data[1]
            y=data[0]
            plt.scatter(x,y,label=ref)
        plt.legend(title='Parameter where:')
        plt.show()
        


        
        
  #######################################################################
  ## FC7 BOARD FUNCTIONS
  #######################################################################

    def init(self):
        self.fpga_registers.init()
        self.cdce.init()
        self.fmc.init()
        self.gbt_fpga.init()
        self.fifos_clear()

    def ready(self):
        return(self.gbt_fpga.ready())

    def info(self):
        print("Board ID : {}".format(self.get_boardID()))
        print("Rev ID   : {}".format(self.get_revID()))
        print("Version  : {}".format(self.get_version()))


if __name__ == '__main__':
    project = fc7(False)
    project.set_time_window(1)
    print(project.downlink.send_SC_frame(2,0,0x0010,1))
    print(project.uplink.receive_SC_frame())
