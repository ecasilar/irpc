import uhal
import time
import sys
import os
import types
import argparse
from fc7 import *
from tools_box import *
from feb_v2_cycloneV_r1 import *
from scurve import *
from statistics import mean

## Pedestal search of the selected strip with all asics enabled with a timewindow of 1 ms
# python3 testbench_calibrate_asic.py -strip -timewindow 1 -allasic

class testbench_threshold_asic:

    def __init__(self,verbose):
        self.fc7 = fc7(verbose)
        self.feb = feb_v2_cycloneV_r1(self.fc7,verbose)
        self.common_threshold_range = 1024
        self.scurve = scurve("scurve.raw")

    def get_trigger_in_windows(self, window_time_ms = 1, delay = 0):
        #Window config.
        self.fc7.set_time_window(window_time_ms)
        self.fc7.set_short_wdw_delay(delay)# No delay
                
        cpt_timeout = 0
        self.fc7.start_TDC_counters()
        while(self.fc7.get_calibration_done()!=1):
            if cpt_timeout==10:
                self.fc7.stop_TDC_counters()
                return -1
            else:
                time.sleep(0.2)
                cpt_timeout = cpt_timeout + 1
        r=self.fc7.get_counter_trigger()
        self.fc7.stop_TDC_counters()
        return r
    
    def enable_all_asic_ch(self,fpga, asic):
        all_strips = [x for x in range(0,31,2)]
        self.feb.asic.init(False)
        self.feb.asic.disable_channel(fpga,asic,all_strips)
        self.feb.asic.enable_channel(fpga,asic,all_strips)
        self.feb.asic.configure(fpga,"all")
        print("ASIC ", asic,"of FPGA",fpga," CHANNEL enable: {}".format(self.feb.asic.get_enable_channel(fpga,asic)))
    
    def dac_6b_adjusted_config(self,threashold_50_list):
        threashold_list = []
        #threashold equalisation
        for ref,threashold_50_i in threashold_50_list.items():
            threashold_list.append(threashold_50_i[0])

        # 10bits inputDAC th 50% value
        th_mean = mean(threashold_list)

        ### 6b DAC value
        for strip, th in threashold_50_list.items():
            adjust_n = ((th_mean - th[0])*0.92 + 32*1.5)/1.5
            self.feb.configure_6b_dac(strip, int(adjust_n)) # adjust 6bit DAC
        

    def pedestal_search(self,strip_ref, first, all_asic_ch = False, equalized = False, time_window=1):
        
        ######################################Asic/TDC Config.############################################        
        ### Main loop to scope each strips                
        for strip in strip_ref:
            print("#############################################")
            print("#### ",strip)
                    
            ### Active all ASIC channels
            if all_asic_ch == True:
                fpga = self.feb.lookuptable_strips[strip][self.feb.index_lut_fpga] ## initilize fpga
                asic = self.feb.lookuptable_strips[strip][self.feb.index_lut_asic] ## initialize asic
                #active all TDC
                self.feb.tdc.set_enable(fpga)
                self.feb.tdc.clear_cmd_valid(fpga)
                self.feb.tdc.channel_enable(fpga,"all")
                print( self.feb.asic.get_enable_channel(fpga, asic))
                if self.feb.asic.get_enable_channel(fpga, asic) !=[x for x in range(0,31,2)]: # verifier que les channels ne soient pas déjà activés
                    self.enable_all_asic_ch(fpga, asic) 
      
            ### Active selected ASIC channels One by One
            else:
                fpga = self.feb.lookuptable_strips[strip][self.feb.index_lut_fpga] ## initilize fpga
                asic = self.feb.lookuptable_strips[strip][self.feb.index_lut_asic] ## initialize asic
                self.feb.init_configuration([strip],False) #enable asic channels (mask) and TDC channel

            ### 6bit DAC already equalized ? False ==> set to middle range | True ==> already equalized
            if equalized == False : 
                self.feb.configure_6b_dac(strip,32) #First step : 6b-dac set to middle range (32)

            ### debug
            print("configuration of FPGA")
            print("----------------------")
            print("STRIP : {} [FPGA :{} , ASIC :{}]".format(strip, fpga, asic))
            print("Asic channels status :",self.feb.asic.get_enable_channel(fpga_ref=fpga, asic_ref=asic))
            print("TDC channel status",self.feb.tdc.get_enable_status(fpga_ref=fpga))
            print("----------------------")

            ### Init configuration
            threshold_min = 100
            threshold_current = threshold_min
            self.feb.configure_10b_dac(strip,threshold_current)
            nbr_trigger = self.get_trigger_in_windows(time_window, 15) ## define a maximum trigger value

            ### FIND THREASHOLD RANGE
            print("Find out Threshold range...")
             ### Threashold min
            while(self.get_trigger_in_windows(time_window, 0)>nbr_trigger*0.9):
                print("nombre données : {}" .format(self.get_trigger_in_windows(time_window,0)))
                threshold_current = threshold_current + 10
                self.feb.configure_10b_dac(strip,threshold_current)
            threshold_min = threshold_current - 20
            ### Threashold max
            while(self.get_trigger_in_windows(time_window,0) != 0):
                print("nombre données : {}" .format(self.get_trigger_in_windows(time_window,0)))
                threshold_current = threshold_current + 10
                self.feb.configure_10b_dac(strip,threshold_current)
            threshold_max = threshold_current + 20
            
            ### Sweep 10 bit dac beetween min and max
            print("Threshold min. {} max. {}".format(threshold_min,threshold_max))
            threshold_current = threshold_min
            cpt_zero = 0
            scurve = []
            self.feb.configure_10b_dac(strip,threshold_current)
            while(threshold_current<threshold_max+1):
                percent= int( ((threshold_current-threshold_min)/(threshold_max-threshold_min) )*100)
                print("processing : {}%".format(percent),end="\r")

                ### Mean equalisation on 20 runs for each values
                r=[]        
                for i in range(20):
                    r.append(self.get_trigger_in_windows(time_window,0))

                trigger_mean_value = int(mean(r))
                scurve.append((threshold_current,trigger_mean_value))
                threshold_current = threshold_current + 1

                self.feb.configure_10b_dac(strip,threshold_current)
                time.sleep(0.01)
            if first:
                with open("scurve.raw", "w") as outfile:
                    outfile.write("#{},{},asic_{},ch_{},tdc_{}\n".format(strip,self.feb.lookuptable_strips[strip][0],self.feb.lookuptable_strips[strip][1],self.feb.lookuptable_strips[strip][2],self.feb.lookuptable_strips[strip][3]))
                    for x in scurve:
                        outfile.write((str(x[0]) + "," +str(x[1]) + "\n"))
                        first=False
            else:
                with open("scurve.raw", "a") as outfile:
                    outfile.write("#{},{},asic_{},ch_{},tdc_{}\n".format(strip,self.feb.lookuptable_strips[strip][0],self.feb.lookuptable_strips[strip][1],self.feb.lookuptable_strips[strip][2],self.feb.lookuptable_strips[strip][3]))
                    for x in scurve:
                        outfile.write((str(x[0]) + "," +str(x[1]) + "\n"))


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose", help="increase output verbosity",action="store_true")
    parser.add_argument("-strip", "--strip", type=str, help="target strip")
    parser.add_argument("-timewindow", "--timewindow", type=float, help="time window")
    #parser.add_argument("-allasic_ch", "--allasic", type=bool, help="active all channel ASIC", action="store_true")
    
    ### next software update
    #parser.add_argument("-calib_mode","--calib_mode", type = str,default = "Injection", help="select mode (Injection, noise)" )
    ###

    ###
    
    clear = lambda: os.system('clear')
    clear()
    args = parser.parse_args()
    if args.verbose:
        print("verbosity turned on")

    time_window_ref = args.timewindow
    strip_ref = args.strip
    #all_asic_ch = args.allasic
    #Injection or noise
    #calib_mode_ref = args.calib_mode
    
    
    strip_ref_fpga0_bottom = ["r_strip_0","strip_0","r_strip_1","strip_1","r_strip_2","strip_2","r_strip_3","strip_3","r_strip_4","strip_4","r_strip_5","strip_5","r_strip_6","strip_6","r_strip_7","strip_7"]     
    strip_ref_fpga0_top = ["r_strip_8","strip_8","r_strip_9","strip_9","r_strip_10","strip_10","r_strip_11","strip_11","r_strip_12","strip_12","r_strip_13","strip_13","r_strip_14","strip_14","r_strip_15","strip_15"]       
    strip_ref_fpga1_bottom = ["r_strip_16","strip_16","r_strip_17","strip_17","r_strip_18","strip_18","r_strip_19","strip_19","r_strip_20","strip_20","r_strip_21","strip_21","r_strip_22","strip_22","r_strip_23","strip_23"]
    strip_ref_fpga1_top = ["r_strip_24","strip_24","r_strip_25","strip_25","r_strip_26","strip_26","r_strip_27","strip_27","r_strip_28","strip_28","r_strip_29","strip_29","r_strip_30","strip_30","r_strip_31","strip_31"]
    strip_ref_fpga2_bottom = ["r_strip_32","strip_32","r_strip_33","strip_33","r_strip_34","strip_34","r_strip_35","strip_35","r_strip_36","strip_36","r_strip_37","strip_37","r_strip_38","strip_38","r_strip_39","strip_39"]
    strip_ref_fpga2_top = ["r_strip_40","strip_40","r_strip_41","strip_41","r_strip_42","strip_42","r_strip_43","strip_43","r_strip_44","strip_44","r_strip_45","strip_45","r_strip_46","strip_46","r_strip_47","strip_47"]


    strip_test = ["r_strip_8", "strip_8"]

    #################################### pedestal equalisation 6bit da adjust / All strips activated ###############################
    ################################################################################################################################

    ################################################################################################################################
    
    print("#############################################")
    print("#####      SCRIPT THRESHOLD SEARCH      #####")
    print("         - TARGET {}                    ".format(strip_ref))
    print("#############################################")

    first=True

    ### SELECT STRIPs to calibrate
    if strip_ref == "fpga_0_bottom":
        strip_ref = strip_ref_fpga0_bottom
    elif strip_ref == "fpga_0_top":
        strip_ref = strip_ref_fpga0_top
    elif strip_ref =="fpga_1_bot":
        strip_ref = strip_ref_fpga1_bottom 
    elif strip_ref =="fpga_1_top":
        strip_ref = strip_ref_fpga1_top
    elif strip_ref =="fpga_2_bot":
        strip_ref = strip_ref_fpga2_bottom 
    elif strip_ref =="fpga_2_top":
        strip_ref = strip_ref_fpga2_top
    else:
        strip_ref == args.strip

    
    project = testbench_threshold_asic(args.verbose)
    #####
    
    ################################################################################################################################
    ############################################# STEP 1 : pedestal without equalisation ###########################################
    ################################################################################################################################
    
    
    # #################################################
    # * Pedestal search (Channel by channel) calibration
    # ################################################# 
    # time.sleep(0.5)
    project.pedestal_search(strip_ref = strip_ref, first = True, all_asic_ch = False, equalized = False, time_window = time_window_ref)
    pedestal = project.scurve.read_raw_file("nbr_trigger", "threashold")

    
    
    ################################################################################################################################
    ############################################# STEP 2 : pedestal equalisation 6bit dac adjust ###################################
    ################################################################################################################################
    
    ################################################################################
    ### Interpollation linéaire Scurves / return : liste {'strips' : threashold_50%}
    ################################################################################
    threashold_50_perc = project.scurve.Scurve_50percent_threashold(pedestal)  
    print("threashold_50_perc : {}".format(threashold_50_perc))     
    project.dac_6b_adjusted_config(threashold_50_perc)     # 6bit DAC adjustement 
    

    ################################################################################################################################
    ############################################# STEP 3 : pedestal equalisation 6bit dac adjust ###################################
    ################################################################################################################################
    ################################################################################
    ## Pedestal search with All strips activated and DAC 6bit equalized
    ################################################################################
    project.pedestal_search(strip_ref = strip_ref, first = True, all_asic_ch = True, equalized =True,time_window = time_window_ref)
    pedestal = project.scurve.read_raw_file("nbr_trigger", "threashold")


