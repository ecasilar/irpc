import uhal
import time
import sys
import os
import types
from fc7_registers import *
from tools_box import *

###---------------------------------------------###
#     This class instanciate all FC7 FIFOs        #
###---------------------------------------------###

class fc7_fifo:
    def __init__(self,fc7_registers,verbose):
        self.verbose = verbose
        self.fpga_registers = fc7_registers
        self.tx_fifo_SC = fc7_fifo_template(self.fpga_registers,"IN",4,5,2,self.verbose)
        self.tx_fifo_SC_Xdomain = fc7_fifo_template(self.fpga_registers,"NONE",6,7,3,self.verbose)

        self.rx_fifo_SC  = fc7_fifo_template(self.fpga_registers,"OUT",0,1,1,self.verbose)
        self.rx_fifo_SC_Xdomain = fc7_fifo_template(self.fpga_registers,"NONE",2,3,0,self.verbose)

        self.rx_fifo_TDC = fc7_fifo_template(self.fpga_registers,"NONE",12,13,4,self.verbose)
        self.rx_fifo_TDC_buf = fc7_fifo_template(self.fpga_registers,"OUT",8,9,5,self.verbose)
        self.rx_fifo_TDC_Xdomain = fc7_fifo_template(self.fpga_registers,"NONE",10,11,4,self.verbose)


 ## Get function
    def read_status(self):
        self.status = self.fc7_registers.get_fifos_status()
        self.ctrl   = self.fc7_registers.get_fifos_ctrl()

## Set function
    def write_ctrl(self):
        self.fc7_registers.set_fifos_ctrl(self.ctrl)

    def clear_fifos_rx(self):
        self.clear_rx_fifo_SC()
        self.clear_rx_fifo_SC_cross_domain()

    def clear_fifos_tx(self):
        self.clear_tx_fifo_SC()
        self.clear_tx_fifo_SC_cross_domain()

    def clear_fifos_all(self):
        self.clear_fifos_rx()
        self.clear_fifos_tx()
        if self.verbose: print("clear all fifo done !")

################################################################""
## FIFO SC CROSS DOMAIN
################################################################""

    def clear_rx_fifo_SC_cross_domain(self):
        self.set_rx_fifo_SC_cross_domain_clear(1)
        self.write_ctrl()
        time.sleep(0.1)
        self.set_rx_fifo_SC_cross_domain_clear(0)
        self.write_ctrl()

    def clear_tx_fifo_SC_cross_domain(self):
        self.set_tx_fifo_SC_cross_domain_clear(1)
        self.write_ctrl()
        time.sleep(0.1)
        self.set_tx_fifo_SC_cross_domain_clear(0)
        self.write_ctrl()

    def get_rx_fifo_SC_cross_domain_empty(self):
        self.read_status()
        return int(bf(self.status)[2])

    def get_rx_fifo_SC_cross_domain_full(self):
        self.read_status()
        return int(bf(self.status)[3])

    def set_rx_fifo_SC_cross_domain_clear(self,value):
        r = bf(self.ctrl)
        r[0] = bf(value)[0]
        self.ctrl = int(r)

    def set_tx_fifo_SC_cross_domain_clear(self,value):
        r = bf(self.ctrl)
        r[3] = bf(value)[0]
        self.ctrl = int(r)

    def get_tx_fifo_SC_cross_domain_empty(self):
        self.read_status()
        return int(bf(self.status)[6])

    def get_tx_fifo_SC_cross_domain_full(self):
        self.read_status()
        return int(bf(self.status)[7])

    def get_rx_fifo_SC_cross_domain_clear(self):
        self.read_status()
        return int(bf(self.ctrl)[0])

    def get_tx_fifo_SC_cross_domain_clear(self):
        self.read_status()
        return int(bf(self.ctrl)[3])

    def info_fifo_SC_cross_domain(self):
        self.read_status()
        print("###############################")
        print("## FIFO SC Cross Domain status ##")
        print("###############################")
        print("--> fifo_rx_cross_domain_empty 	: {}".format(self.get_rx_fifo_SC_cross_domain_empty()))
        print("--> fifo_rx_cross_domain_full  	: {}".format(self.get_rx_fifo_SC_cross_domain_full()))
        print("--> fifo_tx_cross_domain_empty 	: {}".format(self.get_tx_fifo_SC_cross_domain_empty()))
        print("--> fifo_tx_cross_domain_full  	: {}".format(self.get_tx_fifo_SC_cross_domain_full()))
        print("--> fifo_rx_cross_domain_clear   : {}".format(self.get_rx_fifo_SC_cross_domain_clear()))
        print("--> fifo_tx_cross_domain_clear   : {}".format(self.get_tx_fifo_SC_cross_domain_clear()))

################################################################""
## FIFO SC
################################################################""

    def clear_rx_fifo_SC(self):
        self.set_rx_fifo_SC_clear(1)
        self.write_ctrl()
        time.sleep(0.1)
        self.set_rx_fifo_SC_clear(0)
        self.write_ctrl()

    def clear_tx_fifo_SC(self):
        self.set_tx_fifo_SC_clear(1)
        self.write_ctrl()
        time.sleep(0.1)
        self.set_tx_fifo_SC_clear(0)
        self.write_ctrl()

    def get_rx_fifo_SC_empty(self):
        self.read_status()
        return int(bf(self.status)[0])

    def get_rx_fifo_SC_full(self):
        self.read_status()
        return int(bf(self.status)[1])

    def get_tx_fifo_SC_empty(self):
        self.read_status()
        return int(bf(self.status)[4])

    def get_tx_fifo_SC_full(self):
        self.read_status()
        return int(bf(self.status)[5])

    def get_rx_fifo_SC_clear(self):
        self.read_status()
        return int(bf(self.ctrl)[1])

    def get_tx_fifo_SC_clear(self):
        self.read_status()
        return int(bf(self.ctrl)[2])

    def set_rx_fifo_SC_clear(self,value):
        r = bf(self.ctrl)
        r[1] = bf(value)[0]
        self.ctrl = int(r)

    def set_tx_fifo_SC_clear(self,value):
        r = bf(self.ctrl)
        r[2] = bf(value)[0]
        self.ctrl = int(r)

    def write_tx_fifo_SC_single(self,value):
        if self.get_tx_fifo_full_SC()==0:
            self.fc7_registers.set_fifo_single_txdata_SC(value)
        else:
            if self.verbose: print("SC_fifo_tx full !")

    def write_tx_fifo_SC_block(self,tab):
        if self.get_tx_fifo_SC_full()==0:
            if self.verbose: print("#####################")
            nb = 0
            nb_frame = 0
            for x in tab:
                if self.verbose: print("TX frame #{} data 32 bits #{} : 0x{:08x}".format(nb_frame,nb,x))
                if nb==3:
                    nb = 0
                    nb_frame = nb_frame + 1
                else:
                    nb = nb + 1
            self.fc7_registers.set_tx_fifo_SC_block(tab)
        else:
            if self.verbose: print("SC_fifo_tx full !")

    def read_rx_fifo_SC_single(self):
        if self.get_rx_fifo_SC_empty()==0:
            return self.fc7_registers.get_rx_fifo_SC_single()
        else:
            if self.verbose: print("fifo_rx_SC empty !")

    def read_rx_fifo_SC_block(self,nb_words):
        if self.get_rx_fifo_SC_empty()==0:
            r=self.fc7_registers.get_rx_fifo_SC_block(nb_words)
            if self.verbose: print("#####################")
            nb = 0
            nb_frame = 0
            for x in r:
                if self.verbose: print("RX frame #{} data 32 bits #{} : 0x{:08x}".format(nb_frame,nb,x))
                if nb==3:
                    nb = 0
                    nb_frame = nb_frame + 1
                else:
                    nb = nb+ 1
            return r
        else:
            if self.verbose: print("fifo_rx_SC empty !")


    def info_fifo_SC(self):
        self.read_status()
        print("#####################")
        print("## FIFOs SC status ##")
        print("#####################")
        print("--> fifo_rx_empty_SC  : {}".format(self.get_rx_fifo_SC_empty()))
        print("--> fifo_rx_full_SC   : {}".format(self.get_rx_fifo_SC_full()))
        print("--> fifo_tx_empty_SC  : {}".format(self.get_tx_fifo_SC_empty()))
        print("--> fifo_tx_full_SC   : {}".format(self.get_tx_fifo_SC_full()))
        print("--> fifo_rx_clear_SC  : {}".format(self.get_rx_fifo_SC_clear()))
        print("--> fifo_tx_clear_SC  : {}".format(self.get_tx_fifo_SC_clear()))


################################################################""
## FIFO FC CROSS DOMAIN
################################################################""

    def clear_rx_fifo_TDC_cross_domain(self):
        self.set_rx_fifo_SC_cross_domain_clear(1)
        self.write_ctrl()
        time.sleep(0.1)
        self.set_rx_fifo_TDC_cross_domain_clear(0)
        self.write_ctrl()

    def clear_tx_fifo_TDC_cross_domain(self):
        self.set_tx_fifo_SC_cross_domain_clear(1)
        self.write_ctrl()
        time.sleep(0.1)
        self.set_tx_fifo_TDC_cross_domain_clear(0)
        self.write_ctrl()

    def get_rx_fifo_TDC_cross_domain_empty(self):
        self.read_status()
        return int(bf(self.status)[2])

    def get_rx_fifo_TDC_cross_domain_full(self):
        self.read_status()
        return int(bf(self.status)[3])

    def set_rx_fifo_TDC_cross_domain_clear(self,value):
        r = bf(self.ctrl)
        r[0] = bf(value)[0]
        self.ctrl = int(r)

    def set_tx_fifo_TDC_cross_domain_clear(self,value):
        r = bf(self.ctrl)
        r[3] = bf(value)[0]
        self.ctrl = int(r)

    def get_tx_fifo_TDC_cross_domain_empty(self):
        self.read_status()
        return int(bf(self.status)[6])

    def get_tx_fifo_TDC_cross_domain_full(self):
        self.read_status()
        return int(bf(self.status)[7])

    def get_rx_fifo_TDC_cross_domain_clear(self):
        self.read_status()
        return int(bf(self.ctrl)[0])

    def get_tx_fifo_TDC_cross_domain_clear(self):
        self.read_status()
        return int(bf(self.ctrl)[3])

    def info_fifo_TDC_cross_domain(self):
        self.read_status()
        print("###############################")
        print("## FIFOs Cross Domain status ##")
        print("###############################")
        print("--> fifo_rx_cross_domain_empty 	: {}".format(self.get_rx_fifo_TDC_cross_domain_empty()))
        print("--> fifo_rx_cross_domain_full  	: {}".format(self.get_rx_fifo_TDC_cross_domain_full()))
        print("--> fifo_tx_cross_domain_empty 	: {}".format(self.get_tx_fifo_TDC_cross_domain_empty()))
        print("--> fifo_tx_cross_domain_full  	: {}".format(self.get_tx_fifo_TDC_cross_domain_full()))
        print("--> fifo_rx_cross_domain_clear   : {}".format(self.get_rx_fifo_TDC_cross_domain_clear()))
        print("--> fifo_tx_cross_domain_clear   : {}".format(self.get_tx_fifo_TDC_cross_domain_clear()))

################################################################""
## FIFO TDC
################################################################""
    def get_rx_fifo_TDC_empty(self):
        self.read_status()
        return int(bf(self.status)[0])

    def get_rx_fifo_TDC_full(self):
        self.read_status()
        return int(bf(self.status)[1])

    def read_rx_fifo_TDC_single(self):
        #if self.get_rx_fifo_FC_empty()==0:
        return self.fc7_registers.get_rx_fifo_TDC_single()
        #else:
        #    if self.verbose: print("fifo_rx_FC empty !")

    def read_rx_fifo_TDC_block(self,nb_words):
        #if self.get_rx_fifo_FC_empty()==0:
        r=self.fc7_registers.get_rx_fifo_TDC_block(nb_words)
        if self.verbose: print("#####################")
        nb = 0
        nb_frame = 0
        for x in r:
            if self.verbose: print("RX frame #{} data 32 bits #{} : 0x{:08x}".format(nb_frame,nb,x))
            if nb==3:
                nb = 0
                nb_frame = nb_frame + 1
            else:
                nb = nb+ 1
            return r
        else:
            if self.verbose: print("fifo_rx_FC empty !")


    def info_fifo_TDC(self):
        pass

################################################################""
################################################################""
################################################################""
################################################################""

    def init(self):
        if self.verbose : self.info()
