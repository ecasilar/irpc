from __future__ import print_function

import uhal
import time
import sys
import os
import types
import cProfile

from fc7 import *
from feb_v2_cycloneV_r1 import *

from feb_ic import *
from feb_sca import *

#Constant definitions
MID   = int("010",2) #2
LEFT  = int("001",2) #1
RIGHT = int("100",2) #4

def DisableTDC():
  sc_write(LEFT+MID+RIGHT,addr=0x0300, data=[0]) #TDC module disable
  #sc_write(LEFT+MID+RIGHT,addr=0x0305, data=[0,0,0]) #meas enable for All TDC channels only

def sc_write(fpga, addr, data):
  WR=1
  send_SC_frame(fpga, WR, address=addr, data=data)


def send_SC_frame(target, rw, address, data):
  project.fpga_registers.set_tx_fifo_SC_block(DL.sc_frame_format_32bits(DL.sc_frame_request(target,rw,address,data)))


def read_FPGA_info():
  #Ensure fifos are flushed
  project.fifos_clear()
  print("----------------------- test FPGA COM ----------------------------")
  #Read FPGA ID of the 3 FPGAs
  sc_read(LEFT+MID+RIGHT, 0x0010, 3, bVerbose=True)


def sc_read(fpga, addr, nWords, bVerbose=False):
  RD = 0
  if bVerbose:
    print("Read",nWords,"word from addr","0x"+hex(addr)[2:].rjust(4,'0'))
  send_SC_frame(fpga, RD, address=addr, data=nWords)
  ReceivedSC = receive_SC_frame()
  llSC=[]
  llSC.append(["0x"+hex(item)[2:].rjust(4,'0') for item in ReceivedSC['fpga_0']])
  llSC.append(["0x"+hex(item)[2:].rjust(4,'0') for item in ReceivedSC['fpga_1']])
  llSC.append(["0x"+hex(item)[2:].rjust(4,'0') for item in ReceivedSC['fpga_2']])

  if bVerbose:
    print("FPGA 0".rjust(15,' ')+'|'+"FPGA 1".rjust(16,' ')+'|'+"FPGA 2".rjust(16,' ')+'|')
    for i in range(max([len(item) for item in llSC])):
      for fpgaID in range(3):
        if i < len(llSC[fpgaID]):
          print(llSC[fpgaID][i].rjust(15,' ')+'|',end='')
        else:
          print(" "*15+'|',end='')
      print()
  return llSC

def receive_SC_frame():
  SC_frames = []
  r = project.fpga_registers.get_rx_fifo_SC_block(project.get_nb_word_in_fifo_SC())
  for i in range(0,len(r),4):
    SC_frames.append(( (r[i]<<96) + (r[i+1]<<64) +(r[i+2]<<32) +r[i+3])>>16 )
    #print(hex(SC_frames[-1]))
  return UL.decode_SC_frame(SC_frames)


if __name__ == "__main__":

  project = fc7(False)
  DL, UL = project.downlink, project.uplink
  feb = feb_v2_cycloneV_r1(project, False)

  project.stop_acquisition()
  project.fifos_clear()

  sca_reg=feb_sca(project.fpga_registers, True)

  #Ensure the uplink bandwidth is released
  DisableTDC()

  #Test communication with the 3 FEB FPGAs
  read_FPGA_info()

  sys.exit()
