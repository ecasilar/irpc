import sys
import os
import types
from tools_box import *
from register import *

###---------------------------------------------###
#     This class instanciate FEB FPGA registers   #
###---------------------------------------------###
class feb_fpga_registers:

    def __init__(self,fpga_id):

        self.fpga_id = fpga_id
        self.fpga_register = {
        
        "GENERAL"        	             : register(0x0000,"rw",16,1,0x0000),
        ### TOP ###
        #-------------------------------------------------------------------#
        "PETIROC_TOP_LOAD"               : register(0x0100,"rw",16,1,0x0000),# bit 0 : load, bit 1 : auto_reload ("auto_reload_periodic config : 0x11")
        "PETIROC_TOP_SR_RST"             : register(0x0101,"rw",16,1,0x0000),
        "PETIROC_TOP_RSTB"               : register(0x0102,"rw",16,1,0x0000),
        "TRIGGER_EXT_PIN_TOP"            : register(0x0103,"rw",16,1,0x0000),# bit 0   
        "PETIROC_TOP_NOR32RAZ_FSM"       : register(0x0104,"rw",16,1,0x0000),
        # FSM stage time
        "FSM_RAZ_stage1_top"             : register(0x0105,"rw",16,1,0x0000),
        "FSM_RAZ_stage2_top"             : register(0x0106,"rw",16,1,0x0000),
        "FSM_RAZ_stage3_top"             : register(0x0107,"rw",16,1,0x0000),
        "FPGA_FEATURES_TOP"              : register(0x0108,"rw",16,1,0x0000), # bit 0 ==> LATCHING sorties PetiROC vers FPGA | bit 1 ==> use FPGA NOR32 (utilise nor32 généré par les entrées du fpga [sequencage reset]) [DEFAULT_VALUE = 0x"10"]
        "BITFLIP_COUNTER_RESET_TOP"      : register(0x0109,"rw",16,1,0x0000), # bit 0
        # 1 / 10 ns period
        "PETIROC_RELOADING_PERIOD_TOP_1" : register(0x010A,"rw",16,1,0x0000), # LSB 
        "PETIROC_RELOADING_PERIOD_TOP_2" : register(0x010B,"rw",16,1,0x0000), # 
        "PETIROC_RELOADING_PERIOD_TOP_3" : register(0x010C,"rw",16,1,0x0000), # MSB
        # PetiROC Mode / Config
        "PETIROC_TYPE_SELECT_TOP"        : register(0x010D,"rw",16,1,0x0001), # Default value : compatibilité PetiROC_2B "0 : 2A | 1:2B | 2 : 2C"
        "PETIROC_TOP_CONFIG"             : register(0x0116,"rw",16,42,[0x0000]),
        # Bitflip counter (irradiation tests)
        "BITFLIP_COUNTER_TOP_1"          : register(0x0140,"ro",16,1,[0x0000]), #LSB
        "BITFLIP_COUNTER_TOP_2"          : register(0x0141,"ro",16,1,[0x0000]), #MSB
        
        "PETIROC_TOP_CONFIG_PREVIOUS"    : register(0x0163,"ro",16,42,[0x0000]),
        #-------------------------------------------------------------------#
        ### BOTTOM ###
        #-------------------------------------------------------------------#
        "PETIROC_BOTTOM_LOAD"            : register(0x0200,"rw",16,1,0x0000),# bit 0 : load, bit 1 : auto_reload ("auto_reload_periodic config : 0x11") 
        "PETIROC_BOTTOM_SR_RST"          : register(0x0201,"rw",16,1,0x0000),
        "PETIROC_BOTTOM_RSTB"            : register(0x0202,"rw",16,1,0x0000),
        "TRIGGER_EXT_PIN_BOT"            : register(0x0203,"rw",16,1,0x0000),# bit 0  
        "PETIROC_BOTTOM_NOR32RAZ_FSM"    : register(0x0204,"rw",16,1,0x0000),
        ### FSM stage ###
        "FSM_RAZ_stage1_bot"             : register(0x0205,"rw",16,1,0x0000),
        "FSM_RAZ_stage2_bot"             : register(0x0206,"rw",16,1,0x0000),
        "FSM_RAZ_stage3_bot"             : register(0x0207,"rw",16,1,0x0000),
        "FPGA_FEATURES_BOT"              : register(0x0208,"rw",16,1,0x0000), # bit 0 ==> LATCHING sorties PetiROC vers FPGA | bit 1 ==> use FPGA NOR32 (utilise nor32 généré par les entrées du fpga [sequencage reset]) [DEFAULT_VALUE = 0x"10"]
        "BITFLIP_COUNTER_RESET_BOT"      : register(0x0209,"rw",16,1,0x0000), # bit 0
        # 1 / 10 ns period
        "PETIROC_RELOADING_PERIOD_BOT_1" : register(0x020A,"rw",16,1,0x0000), # LSB
        "PETIROC_RELOADING_PERIOD_BOT_2" : register(0x020B,"rw",16,1,0x0000), # 
        "PETIROC_RELOADING_PERIOD_BOT_3" : register(0x020C,"rw",16,1,0x0000), # MSB
        # PetiROC Mode / Config
        "PETIROC_TYPE_SELECT_BOT"        : register(0x020D,"rw",16,1,0x0001), # Default value : compatibilité PetiROC_2B "0 : 2A | 1:2B | 2 : 2C"
        "PETIROC_BOTTOM_CONFIG"          : register(0x0216,"rw",16,42,[0x0000]),
        # Bitflip counter (irradiation tests)
        "BITFLIP_COUNTER_BOT_1"          : register(0x0240,"ro",16,1,[0x0000]), #LSB
        "BITFLIP_COUNTER_BOT_2"          : register(0x0241,"ro",16,1,[0x0000]), #MSB
        "PETIROC_BOTTOM_CONFIG_PREVIOUS" : register(0x0263,"ro",16,42,[0x0000]),
        #-------------------------------------------------------------------#
        ### TDC ###
        #-------------------------------------------------------------------#
        "TDC_ENABLE"                     : register(0x0300,"rw",16,1,0x0000),
        "TDC_CMD_VALID"                  : register(0x0301,"rw",16,1,0x0000),
        "TDC_ENABLE_CH0_15"              : register(0x0305,"rw",16,1,0x0000),
        "TDC_ENABLE_CH16_31"             : register(0x0306,"rw",16,1,0x0000),
        "TDC_ENABLE_CH32_33"             : register(0x0307,"rw",16,1,0x0000),
        "TDC_INJECTION_MODE"             : register(0x0308,"rw",3,1,0x0000)
        #-------------------------------------------------------------------#
        }

    def get_register_list(self):
        return self.fpga_register
        
    def get_id(self):
        return self.fpga_id

    def get_address(self,name):
        return self.fpga_register[name].address

    def get_mode(self,name):
        return self.fpga_register[name].mode

    def get_length(self,name):
        return self.fpga_register[name].length

    def get_size(self,name):
        return self.fpga_register[name].size

    def get_value(self,name):
        return self.fpga_register[name].get_value()

    def set_value(self,name,value):
        self.fpga_register[name].set_value(value)




if __name__ == '__main__':
    fpga_0 = feb_fpga_registers("fpga_0")
    print(fpga_0.get_address("GENERAL"))
    print(fpga_0.get_mode("GENERAL"))
    print(fpga_0.get_length("PETIROC_BOTTOM_CONFIG"))
    print(fpga_0.get_size("PETIROC_BOTTOM_CONFIG"))
    print(fpga_0.get_value("PETIROC_BOTTOM_CONFIG"))
