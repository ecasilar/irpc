import time
import sys
import os
import types
from tools_box import *

class register_template:

    def __init__(self,address,mode,length,size,value):
        self.address = address
        self.mode = mode
        self.length = length
        self.size = size
        self.value = value

    def get_value(self):
        return self.value

    def set_value(self,value):
        if isinstance(value,int):
            if self.size==1:
                self.value = value
            else:
                raise(ValueError("value must be a list"))

        elif isinstance(value,list):
            if len(value)==self.size:
                for i,data in enumerate(value):
                    self.value[i] = data
            else:
                raise(ValueError("value list size incorrect"))
        else:
            raise(ValueError("value invalid type"))


    def get_value_bit(self,bit_start,bit_end):
        if self.size==1:
            if ((bit_start>=0 and bit_start<self.length) and (bit_end>=0 and bit_end<self.length) and bit_end>=bit_start) :
                return int(bf(self.value)[bit_start:(bit_end+1)])
            else:
                raise(ValueError("bit number invalid"))
        else:
            raise(ValueError("impossible operation"))


    def set_value_bit(self,bit_start,bit_end,value):
        if self.size==1:
            if ((bit_start>=0 and bit_start<self.length) and (bit_end>=0 and bit_end<self.length) and bit_end>=bit_start) :
                r = bf(self.value)
                r[bit_start:(bit_end+1)] = value
                self.value = int(r)
            else:
                raise(ValueError("bit number invalid"))
        else:
            raise(ValueError("impossible operation"))


    def attribute(self):
        return [self.address,self.mode,self.length,self.size,self.value]

    def info(self):
        print(self.attribute())

if __name__ == '__main__':
    reg1 = register_template(0x0000,"rw",16,2,[0x4455,0x8877])
    print(reg1.get_value())
    reg1.set_value([1122,3344])
    print(reg1.get_value())
    
    reg2 = register(0x0000,"rw",16,1,0x4455)
    reg2.set_value(0x2233)
    print(reg2.get_value())
    print(reg2.get_value_bit(12,15))
    reg2.set_value_bit(12,15,0xFF)
    print(reg2.get_value_bit(12,15))
    reg1.info()
