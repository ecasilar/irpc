import uhal
import time
import sys
import os
import types
import argparse
import os
from fc7 import *
from feb_asics import *
from tools_box import *
from feb_v2_cycloneV_r1 import *

# Exemple of command to configure Asic TOP of all FPGAs.
# python3 testbench_configuration_asic.py -fpga all -asic top -iter 1

class testbench_configuration_asic:

    def __init__(self,fc7_verbose,feb_verbose,check_feb):
        self.fc7 = fc7(fc7_verbose)
        self.feb = feb_v2_cycloneV_r1(self.fc7,feb_verbose)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-vfc7", "--fc7_verbose", help="increase output verbosity",action="store_true")
    parser.add_argument("-vfeb", "--feb_verbose", help="increase output verbosity",action="store_true")
    parser.add_argument("-fpga", "--fpga", type=str, help="target fpga (fpga_0,fpga_1,fpga_2,all)")
    parser.add_argument("-asic", "--asic", type=str, help="target asic (top,bottom,all)")
    parser.add_argument("-iter", "--iter", type=int, help="nb iteration")

    clear = lambda: os.system('clear')
    clear()

    args = parser.parse_args()
    if args.fc7_verbose:
        print("verbosity fc7 turned on")
    if args.feb_verbose:
        print("verbosity feb turned on")

    target = [args.fpga,args.asic]
    nb_iter = args.iter

    clear = lambda: os.system('clear')
    clear()

    project = testbench_configuration_asic(args.fc7_verbose,args.feb_verbose, False)
    project.feb.reset()
    time.sleep(1)
    project.feb.asic.rstb("all","all")
    time.sleep(1)
    project.feb.asic.sr_rst("all","all")
    time.sleep(1)
    cpt_error=[None,None,None,None,None,None]
    project.feb.asic.set_paramaters_from_file("all","all","asics_petiroc2b_config.dat")



    print("nb_iteration : ",nb_iter)
    for i in range(0,nb_iter):
        if target[0]=="all" and target[1]=="all":
            if i==0: cpt_error =[0,0,0,0,0,0]
            result=project.feb.feb_asics.configure(target[0],target[1])
            cpt_index = 0
            for x in result:
                if x==False:
                    cpt_error[cpt_index] = cpt_error[cpt_index] + 1

        elif target[0]=="all" and target[1]=="top":
            if i==0: cpt_error =[0,None,0,None,0,None]
            result=project.feb.feb_asics.configure(target[0],target[1])
            cpt_index = 0
            for x in result:
                if x==False and (cpt_index==0 or cpt_index==2 or cpt_index==4 ):
                    cpt_error[cpt_index] = cpt_error[cpt_index] + 1

        elif target[0]=="all" and target[1]=="bottom":
            if i==0: cpt_error =[None,0,None,0,None,0]
            result=project.feb.feb_asics.configure(target[0],target[1])
            cpt_index = 0
            for x in result:
                if x==False and (cpt_index==1 or cpt_index==3 or cpt_index==5 ):
                    cpt_error[cpt_index] = cpt_error[cpt_index] + 1

        elif target[0]=="fpga_0" and target[1]=="all":
            if i==0: cpt_error =[0,0,None,None,None,None]
            result=project.feb.feb_asics.configure(target[0],target[1])
            cpt_index = 0
            for x in result:
                if x==False and (cpt_index==0 or cpt_index==1):
                    cpt_error[cpt_index] = cpt_error[cpt_index] + 1

        elif target[0]=="fpga_1" and target[1]=="all":
            if i==0: cpt_error =[None,None,0,0,None,None]
            result=project.feb.feb_asics.configure(target[0],target[1])
            cpt_index = 0
            for x in result:
                if x==False and (cpt_index==2 or cpt_index==3):
                    cpt_error[cpt_index] = cpt_error[cpt_index] + 1

        elif target[0]=="fpga_2" and target[1]=="all":
            if i==0: cpt_error =[None,None,None,None,0,0]
            result=project.feb.feb_asics.configure(target[0],target[1])
            cpt_index = 0
            for x in result:
                if x==False and (cpt_index==4 or cpt_index==5):
                    cpt_error[cpt_index] = cpt_error[cpt_index] + 1

        elif target[0]=="fpga_0" and target[1]=="top":
            if i==0: cpt_error =[0,None,None,None,None,None]
            result=project.feb.feb_asics.configure(target[0],target[1])
            cpt_index = 0
            if result==False:
                cpt_error[cpt_index] = cpt_error[cpt_index] + 1

        elif target[0]=="fpga_0" and target[1]=="bottom":
            if i==0: cpt_error =[None,0,None,None,None,None]
            result=project.feb.feb_asics.configure(target[0],target[1])
            cpt_index = 0
            if result==False:
                cpt_error[cpt_index] = cpt_error[cpt_index] + 1

        elif target[0]=="fpga_1" and target[1]=="top":
            if i==0: cpt_error =[None,None,0,None,None,None]
            result=project.feb.feb_asics.configure(target[0],target[1])
            cpt_index = 0
            if result==False:
                cpt_error[cpt_index] = cpt_error[cpt_index] + 1

        elif target[0]=="fpga_1" and target[1]=="bottom":
            if i==0: cpt_error =[None,None,None,0,None,None]
            result=project.feb.asic.configure(target[0],target[1])
            cpt_index = 0
            if result==False:
                cpt_error[cpt_index] = cpt_error[cpt_index] + 1

        elif target[0]=="fpga_2" and target[1]=="top":
            if i==0: cpt_error =[None,None,None,None,0,None]
            result=project.feb.asic.configure(target[0],target[1])
            cpt_index = 0
            if result==False:
                cpt_error[cpt_index] = cpt_error[cpt_index] + 1

        elif target[0]=="fpga_2" and target[1]=="bottom":
            if i==0: cpt_error =[None,None,None,None,None,0]
            result=project.feb.asic.configure(target[0],target[1])
            cpt_index = 0
            if result==False:
                cpt_error[cpt_index] = cpt_error[cpt_index] + 1

        #print("Test configuration petiroc2b: {:0.0%}".format( (((i+1)/(nb_iter)))),end="\r")
    print("")

    print("TEST ITERATION CONFIGURATION PETIROC2B REPORT")
    print("Asic  #                | errors  (%)")
    for index,x in enumerate(cpt_error):
        if index==0:
            fpga=0
            asic="top"
        if index==1:
            fpga=0
            asic="bottom"
        if index==2:
            fpga=1
            asic="top"
        if index==3:
            fpga=1
            asic="bottom"
        if index==4:
            fpga=2
            asic="top"
        if index==5:
            fpga=2
            asic="bottom"
        if x!=None:
            print("-------------------------------------")
            print("ASIC_{} FPGA_{}     | {}/{} ({:0.1%})".format(asic,fpga,cpt_error[index],nb_iter,(cpt_error[index]/nb_iter)))
    print("-------------------------------------")
