import uhal
import time
import sys
import os
import types
import argparse
from fc7 import *
from tools_box import *
from feb_v2_cycloneV_r1 import *


### Initialize FC7 and FEB cards.

class testbench_setup:

    def __init__(self,fc7_verbose,feb_verbose):
        self.fc7 = fc7(fc7_verbose)
        self.feb = feb_v2_cycloneV_r1(self.fc7,feb_verbose)

    def init(self):
        self.fc7.init()
        self.feb.init()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-vfc7", "--fc7_verbose", help="increase output verbosity",action="store_true")
    parser.add_argument("-vfeb", "--feb_verbose", help="increase output verbosity",action="store_true")
    args = parser.parse_args()
    clear = lambda: os.system('clear')
    clear()
    if args.fc7_verbose:
        print("verbosity fc7 turned on")
    if args.feb_verbose:
        print("verbosity feb turned on")

    testbench = testbench_setup(args.fc7_verbose,args.feb_verbose)
    testbench.init()


    
