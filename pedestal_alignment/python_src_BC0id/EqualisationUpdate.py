from __future__ import print_function

import uhal
import time
import sys
import os
import types
import cProfile

from fc7 import *
from feb_v2_cycloneV_r1 import *

from feb_ic import *
from feb_sca import *

import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt
import MongoFeb as mg
import FEBchannels_converter as FEBcc

import json
FIG_ID=[int(time.time()*1000),0]
DAT_ID=FIG_ID

#Constant definitions
MID   = int("010",2) #2
LEFT  = int("001",2) #1
RIGHT = int("100",2) #4

def DisableTDC():
  sc_write(LEFT+MID+RIGHT,addr=0x0300, data=[0]) #TDC module disable
  #sc_write(LEFT+MID+RIGHT,addr=0x0305, data=[0,0,0]) #meas enable for All TDC channels only

def sc_write(fpga, addr, data):
  WR=1
  send_SC_frame(fpga, WR, address=addr, data=data)


def send_SC_frame(target, rw, address, data):
  project.fpga_registers.set_tx_fifo_SC_block(DL.sc_frame_format_32bits(DL.sc_frame_request(target,rw,address,data)))


def test_com_FPGA():
  #Ensure fifos are flushed
  project.fifos_clear()
  print("----------------------- test FPGA COM ----------------------------")
  #Read FPGA ID of the 3 FPGAs
  sc_read(LEFT+MID+RIGHT, 0x0010, 1, bVerbose=True)


def sc_read(fpga, addr, nWords, bVerbose=False):
  RD = 0
  if bVerbose:
    print("Read",nWords,"word from addr","0x"+hex(addr)[2:].rjust(4,'0'))
  send_SC_frame(fpga, RD, address=addr, data=nWords)
  ReceivedSC = receive_SC_frame()
  llSC=[]
  llSC.append(["0x"+hex(item)[2:].rjust(4,'0') for item in ReceivedSC['fpga_0']])
  llSC.append(["0x"+hex(item)[2:].rjust(4,'0') for item in ReceivedSC['fpga_1']])
  llSC.append(["0x"+hex(item)[2:].rjust(4,'0') for item in ReceivedSC['fpga_2']])

  if bVerbose:
    print("FPGA 0".rjust(15,' ')+'|'+"FPGA 1".rjust(16,' ')+'|'+"FPGA 2".rjust(16,' ')+'|')
    for i in range(max([len(item) for item in llSC])):
      for fpgaID in range(3):
        if i < len(llSC[fpgaID]):
          print(llSC[fpgaID][i].rjust(15,' ')+'|',end='')
        else:
          print(" "*15+'|',end='')
      print()
  return llSC


 
def receive_SC_frame():
  SC_frames = []
  r = project.fpga_registers.get_rx_fifo_SC_block(project.get_nb_word_in_fifo_SC())
  for i in range(0,len(r),4):
    SC_frames.append(( (r[i]<<96) + (r[i+1]<<64) +(r[i+2]<<32) +r[i+3])>>16 )
    #print(hex(SC_frames[-1]))
  return UL.decode_SC_frame(SC_frames)

def enable_delay_reset_trigger_forall(delay_in_3ns_unit=4):  #GG add a parameter
    lsAllFpgas=["fpga_0", "fpga_1", "fpga_2"]
    lsAllAsics=["top", "bottom"]
    for fpgaID in lsAllFpgas:
        for asicID in lsAllAsics:
            feb.asic.asic[fpgaID][asicID].enable_delay_reset_trigger(delay_in_3ns_unit) #by steps of ~3ns

def disable_delay_reset_trigger_forall():  #GG add function
    lsAllFpgas=["fpga_0", "fpga_1", "fpga_2"]
    lsAllAsics=["top", "bottom"]
    for fpgaID in lsAllFpgas:
        for asicID in lsAllAsics:
            feb.asic.asic[fpgaID][asicID].disable_delay_reset_trigger() 

def set_PetiROC_type_forall(stype="2C"):
    lsAllFpgas=["fpga_0", "fpga_1", "fpga_2"]
    lsAllAsics=["top", "bottom"]
    for fpgaID in lsAllFpgas:
        for asicID in lsAllAsics:
            feb.asic.set_PetiROC_type(fpgaID,asicID, stype=stype)


def Dac10bConfig(nDacVal=400, lsFpga=["fpga_1"], lsAsic=["top"]):
  for fpgaID in lsFpga:
    for asicID in lsAsic:
      feb.asic.set_10b_dac(fpgaID, asicID, nDacVal)


def DisableAllChannels():
  lsAllFpgas=["fpga_0", "fpga_1", "fpga_2"]
  lsAllAsics=["top", "bottom"]
  for fpgaID in lsAllFpgas:
    for asicID in lsAllAsics:
      feb.asic.disable_channel(fpgaID, asicID, range(32))


def CountROCData(fTime=0.001, bVerbose=False):
  """ Use the FEB FPGAs internal counters to count TDC data for each channel during fTime (counting time in seconds) """
  EnableTDC()
  LaunchDataCounter(nCycles=int(fTime*(10**8))) # nCycles*10ns
  time.sleep(fTime) #Must wait at least the required time
  DisableTDC()
  llCounter=ReadDataCounter(bVerbose=bVerbose)
  return llCounter

def EnableTDC():
  sc_write(LEFT+MID+RIGHT,addr=0x0300, data=[1]) #TDC module enable
  sc_write(LEFT+MID+RIGHT,addr=0x0308, data=[0]) #standard mode
  sc_write(LEFT+MID+RIGHT,addr=0x0305, data=[0xFFFF,0xFFFF,3]) #meas enable for All TDC channels only
  sc_write(LEFT+MID+RIGHT,addr=0x0301, data=[0]) #CMD valid = 0
  sc_write(LEFT+MID+RIGHT,addr=0x0301, data=[1]) #CMD valid = 1
  sc_write(LEFT+MID+RIGHT,addr=0x0301, data=[0]) #CMD valid = 0
  time.sleep(0.005)

def LaunchDataCounter(nCycles=0x00100000):
  sc_write(LEFT+MID+RIGHT, addr=0x030A, data=[nCycles&0xFFFF, nCycles>>16]) #Data counter time window size= 0x00100000 = 10.486ms
  sc_write(LEFT+MID+RIGHT, addr=0x030C, data=[0]) #Ensure data counter disabled
  sc_write(LEFT+MID+RIGHT, addr=0x030C, data=[1]) #Data counter start
  sc_write(LEFT+MID+RIGHT, addr=0x030C, data=[0]) #Ensure data counter disabled

def ReadDataCounter(bVerbose=False):
  if bVerbose:
    sc_read(LEFT+MID+RIGHT, addr=0x0316, nWords=1,bVerbose=True)    #Check data counter valid (timer has reach the required value)
  llRaw=sc_read(LEFT+MID+RIGHT, addr=0x0317, nWords=34*2, bVerbose=bVerbose) #Read data counter for BC0 and Resync channels
  llCounter=[]
  for fpgaID in range(len(llRaw)):
    llCounter.append([])
    for i in range(0, len(llRaw[fpgaID]),2):
      llCounter[-1].append((int(llRaw[fpgaID][i+1],16)<<16)+int(llRaw[fpgaID][i],16))
  return llCounter


def PetirocChannelEnable(lsFpga=["fpga_1"], lsAsic=["top"], lbChannelEnable=[True]*16):
  lnChannel=[]
  for i in range(16):
    if lbChannelEnable[i]:
      lnChannel.append(i*2)

  for fpgaID in lsFpga:
    for asicID in lsAsic:
      feb.asic.enable_channel(fpgaID, asicID, lnChannel)

def loadConfigFromFile(config_jsonFileDirectory,config_jsonFileName):
  fname="%s/%s" % (config_jsonFileDirectory,config_jsonFileName)
  f=open(fname)
  #self.config_state=sf=json.loads(f.read())
  config_state=json.loads(f.read())
  message="Downloaded STATE: {} version: {}".format(config_state["state"],config_state["version"])
  print(message)
  #logging.info(message)
  f.close()
  
  return config_state

def loadConfig(config_statename,config_version): 
  wdd=mg.instance()
  wdd.download(config_statename,config_version,True,odir="/dev/shm/mgroc")
  config_jsonFileName="%s_%s.json" % (config_statename,config_version)
  return loadConfigFromFile("/dev/shm/mgroc", config_jsonFileName)

  
def overwrite_configuration(config_state,asic,asic_num, petiroc_type):
  for ch in range(32):
    asic.set_any_config("mask_discri_charge_ch%d" % ch,config_state["asics"][asic_num]["slc"]["MaskDiscriCharge"][ch])
    asic.set_any_config("input_dac_ch%d" % ch,config_state["asics"][asic_num]["slc"]["InputDac"][ch])
    asic.set_any_config("cmd_input_dac_ch%d" % ch,config_state["asics"][asic_num]["slc"]["InputDacCommand"][ch])
    asic.set_any_config("mask_discri_time_ch%d" % ch,config_state["asics"][asic_num]["slc"]["MaskDiscriTime"][ch])
    asic.set_any_config("6b_dac_ch%d" % ch,config_state["asics"][asic_num]["slc"]["6bDac"][ch])


  asic.set_any_config("EN_10bits_DAC",config_state["asics"][asic_num]["slc"]["EN10bDac"])
  asic.set_any_config("PP_10bits_DAC",config_state["asics"][asic_num]["slc"]["PP10bDac"])
          
  asic.set_any_config("10b_dac_vth_discri_charge",config_state["asics"][asic_num]["slc"]["VthDiscriCharge"])
  asic.set_any_config("10b_dac_vth_discri_time",int(config_state["asics"][asic_num]["slc"]["VthTime"]))

  ##
  asic.set_any_config("EN_ADC"                  ,config_state["asics"][asic_num]["slc"]["EN_adc"])
  asic.set_any_config("PP_ADC"                  ,config_state["asics"][asic_num]["slc"]["PP_adc"])
  asic.set_any_config("sel_startb_ramp_ADC_ext" ,config_state["asics"][asic_num]["slc"]["sel_starb_ramp_adc_ext"])
  asic.set_any_config("usebcompensation"        ,config_state["asics"][asic_num]["slc"]["usebcompensation"])
  asic.set_any_config("EN_bias_DAC_delay"       ,config_state["asics"][asic_num]["slc"]["EN_bias_dac_delay"])
  asic.set_any_config("PP_bias_DAC_delay"       ,config_state["asics"][asic_num]["slc"]["PP_bias_dac_delay"])
  asic.set_any_config("EN_bias_ramp_delay"      ,config_state["asics"][asic_num]["slc"]["EN_bias_ramp_delay"])
  asic.set_any_config("PP_bias_ramp_delay"      ,config_state["asics"][asic_num]["slc"]["PP_bias_ramp_delay"])
  asic.set_any_config("8b_dac_delay"            ,config_state["asics"][asic_num]["slc"]["DacDelay"])
  asic.set_any_config("EN_discri_delay"         ,config_state["asics"][asic_num]["slc"]["EN_discri_delay"])
  asic.set_any_config("PP_discri_delay"         ,config_state["asics"][asic_num]["slc"]["PP_discri_delay"])
  asic.set_any_config("PP_temp_sensor"          ,config_state["asics"][asic_num]["slc"]["PP_temp_sensor"])
  asic.set_any_config("EN_temp_sensor"          ,config_state["asics"][asic_num]["slc"]["EN_temp_sensor"])
  asic.set_any_config("EN_bias_pa"              ,config_state["asics"][asic_num]["slc"]["EN_bias_pa"])
  asic.set_any_config("PP_bias_pa"              ,config_state["asics"][asic_num]["slc"]["PP_bias_pa"])
  asic.set_any_config("EN_bias_discri"          ,config_state["asics"][asic_num]["slc"]["EN_bias_discri"])
  asic.set_any_config("PP_bias_discri"          ,config_state["asics"][asic_num]["slc"]["PP_bias_discri"])
  
  asic.set_any_config("cmd_polarity"            ,config_state["asics"][asic_num]["slc"]["cmd_polarity"])
  asic.set_any_config("latch_discri"            ,config_state["asics"][asic_num]["slc"]["latch"])
  asic.set_any_config("EN_bias_6b_dac"          ,config_state["asics"][asic_num]["slc"]["EN_bias_6bit_dac"])
  ###
  asic.set_any_config("PP_bias_6b_dac"          ,config_state["asics"][asic_num]["slc"]["PP_bias_6bit_dac"])
  ###
  asic.set_any_config("EN_bias_tdc"             ,config_state["asics"][asic_num]["slc"]["EN_bias_tdc"])
  asic.set_any_config("PP_bias_tdc"             ,config_state["asics"][asic_num]["slc"]["PP_bias_tdc"])
  asic.set_any_config("ON_OFF_input_dac"        ,config_state["asics"][asic_num]["slc"]["ON_OFF_input_dac"])
  asic.set_any_config("EN_bias_charge"          ,config_state["asics"][asic_num]["slc"]["EN_bias_charge"])
  asic.set_any_config("PP_bias_charge"          ,config_state["asics"][asic_num]["slc"]["PP_bias_charge"])
  asic.set_any_config("cf_100fF"                ,config_state["asics"][asic_num]["slc"]["Cf3_100fF"])
  asic.set_any_config("cf_200fF"                ,config_state["asics"][asic_num]["slc"]["Cf2_200fF"])
  asic.set_any_config("cf_2_5pF"                ,config_state["asics"][asic_num]["slc"]["Cf1_2p5pF"])
  asic.set_any_config("cf_1_25pF"               ,config_state["asics"][asic_num]["slc"]["Cf0_1p25pF"])
  asic.set_any_config("EN_bias_sca"             ,config_state["asics"][asic_num]["slc"]["EN_bias_sca"])
  asic.set_any_config("PP_bias_sca"             ,config_state["asics"][asic_num]["slc"]["PP_bias_sca"])
  asic.set_any_config("EN_bias_discri_charge"   ,config_state["asics"][asic_num]["slc"]["EN_bias_discri_charge"])
  asic.set_any_config("PP_bias_discri_charge"   ,config_state["asics"][asic_num]["slc"]["PP_bias_discri_charge"])
  asic.set_any_config("EN_bias_discri_adc_time" ,config_state["asics"][asic_num]["slc"]["EN_bias_discri_adc_time"])
  asic.set_any_config("PP_bias_discri_adc_time" ,config_state["asics"][asic_num]["slc"]["PP_bias_discri_adc_time"])
  asic.set_any_config("EN_bias_discri_adc_charge",config_state["asics"][asic_num]["slc"]["EN_bias_discri_adc_charge"])
  asic.set_any_config("PP_bias_discri_adc_charge",config_state["asics"][asic_num]["slc"]["PP_bias_discri_adc_charge"])
  asic.set_any_config("DIS_razchn_int"           ,config_state["asics"][asic_num]["slc"]["DIS_razchn_int"])
  asic.set_any_config("DIS_razchn_ext"           ,config_state["asics"][asic_num]["slc"]["DIS_razchn_ext"])
  asic.set_any_config("SEL_80M"                  ,config_state["asics"][asic_num]["slc"]["SEL_80M"])
  asic.set_any_config("EN_80M"                   ,config_state["asics"][asic_num]["slc"]["EN_80M"])
  asic.set_any_config("EN_slow_lvds_rec"         ,config_state["asics"][asic_num]["slc"]["EN_slow_lvds_rec"])
  #
  asic.set_any_config("PP_slow_lvds_rec"         ,config_state["asics"][asic_num]["slc"]["PP_slow_lvds_rec"])
  #
  asic.set_any_config("EN_fast_lvds_rec"         ,config_state["asics"][asic_num]["slc"]["EN_fast_lvds_rec"])
  asic.set_any_config("PP_fast_lvds_rec"         ,config_state["asics"][asic_num]["slc"]["PP_fast_lvds_rec"])
  asic.set_any_config("EN_transmitter"           ,config_state["asics"][asic_num]["slc"]["EN_transmitter"])
  asic.set_any_config("PP_transmitter"           ,config_state["asics"][asic_num]["slc"]["PP_transmitter"])
  asic.set_any_config("ON_OFF_1mA"               ,config_state["asics"][asic_num]["slc"]["ON_OFF_1mA"])
  asic.set_any_config("ON_OFF_2mA"               ,config_state["asics"][asic_num]["slc"]["ON_OFF_2mA"])
  asic.set_any_config("ON_OFF_ota_mux"           ,config_state["asics"][asic_num]["slc"]["ON_OFF_ota_mux"])
  asic.set_any_config("ON_OFF_ota_probe"         ,config_state["asics"][asic_num]["slc"]["ON_OFF_ota_probe"])
  asic.set_any_config("DIS_trig_mux"             ,config_state["asics"][asic_num]["slc"]["DIS_trig_mux"])
  asic.set_any_config("EN_NOR32_time"            ,config_state["asics"][asic_num]["slc"]["EN_NOR32_time"])
  asic.set_any_config("EN_NOR32_charge"          ,config_state["asics"][asic_num]["slc"]["EN_NOR32_charge"])
  asic.set_any_config("DIS_triggers"             ,config_state["asics"][asic_num]["slc"]["DIS_triggers"])
  asic.set_any_config("EN_dout_oc"               ,config_state["asics"][asic_num]["slc"]["EN_dout_oc"])
  asic.set_any_config("EN_transmit"              ,config_state["asics"][asic_num]["slc"]["EN_transmit"])
  asic.set_any_config("PA_Ccomp<0>"              ,config_state["asics"][asic_num]["slc"]["PA_ccomp_0"])
  asic.set_any_config("PA_Ccomp<1>"              ,config_state["asics"][asic_num]["slc"]["PA_ccomp_1"])
  asic.set_any_config("PA_Ccomp<2>"              ,config_state["asics"][asic_num]["slc"]["PA_ccomp_2"])
  asic.set_any_config("PA_Ccomp<3>"              ,config_state["asics"][asic_num]["slc"]["PA_ccomp_3"])
  asic.set_any_config("Choice_trigger_out"       ,config_state["asics"][asic_num]["slc"]["Choice_Trigger_Out"])
  asic.set_any_config("NC1"                      ,config_state["asics"][asic_num]["slc"]["NC1"])
  asic.set_any_config("NC2"                      ,config_state["asics"][asic_num]["slc"]["NC2"])
  asic.set_any_config("NC3"                      ,config_state["asics"][asic_num]["slc"]["NC3"])
  asic.set_any_config("NC4"                      ,config_state["asics"][asic_num]["slc"]["NC4"])
  return
  if (petiroc_type == 2):
    asic.set_any_config("Delay_reset_trigger"                      ,config_state["asics"][asic_num]["slc"]["Delay_reset_trigger"])
    asic.set_any_config("NC5"                      ,config_state["asics"][asic_num]["slc"]["NC5"])
    asic.set_any_config("NC6"                      ,config_state["asics"][asic_num]["slc"]["NC6"])
    asic.set_any_config("NC7"                      ,config_state["asics"][asic_num]["slc"]["NC7"])
    asic.set_any_config("EN_reset_trigger_delay"   ,config_state["asics"][asic_num]["slc"]["EN_reset_trigger_delay"])
    asic.set_any_config("Delay_reset_ToT"          ,config_state["asics"][asic_num]["slc"]["Delay_reset_ToT"])
    asic.set_any_config("NC8"          ,config_state["asics"][asic_num]["slc"]["NC8"])
    asic.set_any_config("NC9"          ,config_state["asics"][asic_num]["slc"]["NC9"])
    asic.set_any_config("NC10"          ,config_state["asics"][asic_num]["slc"]["NC10"])
    asic.set_any_config("EN_reset_ToT_delay"          ,config_state["asics"][asic_num]["slc"]["EN_reset_ToT_delay"])


      
def ApplyConfig(lsFpga=["fpga_0", "fpga_1", "fpga_2"]):
  for fpgaID in lsFpga:
    #Gen config registers
    ROC_top_config = feb.asic.asic[fpgaID][feb.asic.asic_top_index].gen_config()
    ROC_bot_config = feb.asic.asic[fpgaID][feb.asic.asic_bottom_index].gen_config()

    #Send config registers to FEB FPGAs
    if   fpgaID=="fpga_0": nTargetFPGA = LEFT
    elif fpgaID=="fpga_1": nTargetFPGA = MID
    elif fpgaID=="fpga_2": nTargetFPGA = RIGHT
    else: sys.exit("ERROR : UNKNOWN FPGA ID")

    sc_write(nTargetFPGA, 0x0116, ROC_top_config)
    sc_write(nTargetFPGA, 0x0216, ROC_bot_config)

  nTargetFPGA=0
  if "fpga_0" in lsFpga: nTargetFPGA += LEFT
  if "fpga_1" in lsFpga: nTargetFPGA += MID
  if "fpga_2" in lsFpga: nTargetFPGA += RIGHT

  #PETIROC load cycle
  sc_write(nTargetFPGA, 0x0100, [0]) #ROC TOP LOAD = 0
  sc_write(nTargetFPGA, 0x0200, [0]) #ROC BOT LOAD = 0
  sc_write(nTargetFPGA, 0x0100, [1]) #ROC TOP LOAD = 1
  sc_write(nTargetFPGA, 0x0200, [1]) #ROC BOT LOAD = 1
  sc_write(nTargetFPGA, 0x0100, [0]) #ROC TOP LOAD = 0
  sc_write(nTargetFPGA, 0x0200, [0]) #ROC BOT LOAD = 0

  time.sleep(0.2)


def Dac6bConfig(lnDacVal=[32]*16, lsFpga=["fpga_1"], lsAsic=["top"], lnChannel=range(16)):
  if len(lnDacVal) != len(lnChannel):
    sys.exit("ERROR:Different size of list in Dac6bConfig function")
  for fpgaID in lsFpga:
    for asicID in lsAsic:
      for i in range(len(lnChannel)):
        feb.asic.set_6b_dac(fpgaID,asicID, lnChannel[i]*2, lnDacVal[i])
      for i in range(16):
        feb.asic.set_6b_dac(fpgaID,asicID, i*2+1, 32) #Unused (odd) channels

def sweepDac10b(lDac10bVal=range(350,480,1), lsFpga=["fpga_1"], lsAsic=["top"], bPlot=False, bSaveData=False, sOutFileName=""):
  """ This function perform a sweep of the 10bDac value and return the data counter values """
  print("---- Dac10b sweep : ",len(lDac10bVal),"values between", min(lDac10bVal),"and", max(lDac10bVal),"----")

  lllCounter=[]

  for dac_10b in lDac10bVal:
    #Overload DAC10bits config
    Dac10bConfig(dac_10b, lsFpga, lsAsic)
    ApplyConfig(lsFpga=lsFpga)
    lllCounter.append(CountROCData())

  if bSaveData:
    if sOutFileName=="":
      sOutFileName=str(DAT_ID[0])+"_"+str(DAT_ID[1])
    with open("./SweepData/"+sOutFileName+".csv","w") as outFile:
      outFile.write(",".join(map(str,lDac10bVal))+"\n")
      for fpgaID in range(3):
        for channelID in range(34):
          lCurrentChannelData=[lllCounter[configIndex][fpgaID][channelID] for configIndex in range(len(lDac10bVal))]
          outFile.write(",".join(map(str,lCurrentChannelData))+"\n")
    DAT_ID[1]+=1 #Increment the datNumber

  if bPlot:
    plt.figure(figsize=[12.8,9.6])
    plt.title(sOutFileName)
    for fpgaID in range(3):
      for channelID in range(34):
        lCurrentChannelData=[lllCounter[configIndex][fpgaID][channelID] for configIndex in range(len(lDac10bVal))]
        #Only non-null channels are added to the plot
        if max(lCurrentChannelData)!=0:
          plt.plot(lDac10bVal, lCurrentChannelData, label="FPGA"+str(fpgaID)+" Chan"+str(channelID))

    plt.legend(loc="upper right")
    #plt.ylim(-1,2001)
    #plt.yscale('log')
    if sOutFileName=="":
      sOutFileName=str(FIG_ID[0])+"_"+str(FIG_ID[1])
    plt.savefig("./Plots_petirocC/"+sOutFileName)
    FIG_ID[1]+=1 #Increment the figNumber
    #plt.show()

  return lllCounter

def allchannelEnableScurves(fpga, asic, feb,dircal="./dac6bConfig"):

    #Enable all channels
    lnChannelEnable=range(16)
    lbChannelEnable = [True if n in lnChannelEnable else False for n in lnChannelEnable]
    PetirocChannelEnable(lsFpga=[fpga], lsAsic=[asic], lbChannelEnable=lbChannelEnable)

    #Read 6b and 10b dac configuration from channel by channel Equalisation
    with open("{}/dac6b_{}_{}.txt".format(dircal,fpga,asic),"r") as file:
        mylist = file.read().splitlines()
        value_list=[]
        for d in mylist:
            value_list.append(int(d.split("=")[1]))
        print ("6b :" , value_list)

    #Set 6b dac configuration to all channels
    lnDacVal = value_list[1:] 
    for i in range(32):
      feb.asic.set_6b_dac(fpga,asic, i , 32)
    for i in range(16):
      feb.asic.set_6b_dac(fpga,asic, i*2, lnDacVal[i])

    #get 10b dac value
    dac10b = value_list[0]

    print ("10b :" , dac10b)

    ApplyConfig(lsFpga=[fpga])

    lDac10bVal = range(200,800)
    #Obtain s-curves
    fpgaID = int(fpga[-1])
    lllCounter = sweepDac10b(lDac10bVal=lDac10bVal, lsFpga=[fpga], lsAsic=[asic], bPlot=True, bSaveData=True)
    #lnChannelData = [lllCounter[configIndex][fpgaID][1] for configIndex in range(len(lDac10bVal))]
    #print(lnChannelData)

    return

##GG addition
def EnableAutoResetSM(times=[3,2,2]):
  a=[1]
  a+=times
  sc_write(LEFT+MID+RIGHT, 0x0104,a)
  sc_write(LEFT+MID+RIGHT, 0x0204,a)
## end GG addition

def DisableAutoResetSM():
  sc_write(LEFT+MID+RIGHT, 0x0104, [0])
  sc_write(LEFT+MID+RIGHT, 0x0204, [0])

def OneChannelScurve(sFpga, sAsic, lowth=100,highth=700,step=10,dircal="./dac6bConfig",use32=False,config_statename="TEST",config_version=1,single=True):
  
  res={}
  res["low"]=lowth
  res["high"]=highth
  res["step"]=highth
  res["channels"]=[]
  if (use32):
    #Set all the channels 6bdac to 32
    Dac6bConfig(lsFpga=[sFpga], lsAsic=[sAsic],lnDacVal=[32]*16)
  else:
    Dac6bConfig(lsFpga=[sFpga], lsAsic=[sAsic],lnDacVal=[32]*16)
    config_state=loadConfig(config_statename,config_version)
    #print(config_state)
    for asic_num in range(0,6):
      fpga_number=config_state["asics"][asic_num]["slc"]["nfpga"]
      fpga_name="fpga_"+str(fpga_number)
      if (fpga_name!= sFpga):
        continue
      petiroc_name=config_state["asics"][asic_num]["slc"]["loc"]
      if (petiroc_name!=sAsic):
        continue
      petiroc_number=0
      if petiroc_name=="top":
        petiroc_number=1
      print (fpga_name,petiroc_name,asic_num)
      overwrite_configuration(config_state,feb.asic.asic[fpga_name][petiroc_name],asic_num, petiroc_type=2)


  #If the target is not defined, the first step is the automatic target definition
  if (single):
    print("One channel SCurve")
    lnChannelTransition=[]
    for nChannelIndex in range(16):
      fpgaID = int(sFpga[-1])
      channelID = nChannelIndex if sAsic=="top" else nChannelIndex+16
      lbChannelEnable=[False]*16
      lbChannelEnable[nChannelIndex]=True

      DisableAllChannels() #Disable All channels
      PetirocChannelEnable(lsFpga=[sFpga], lsAsic=[sAsic], lbChannelEnable=lbChannelEnable) #Only enable the current channel
      ApplyConfig()

      ############ 10bdac sweep #################

      ln10bDacVal = range(lowth,highth,step) #Coarse sweep range

      #Create SCurve for the channel
      lllCounter = sweepDac10b(lDac10bVal=ln10bDacVal, lsFpga=[sFpga], lsAsic=[sAsic], bPlot=False, bSaveData=False)

      #print(lllCounter)
      #for fpgaID in range(3):
      #  for channelID in range(34):
      for nChannelIndex in range(16):
        fpgaID = int(sFpga[-1])
        channelID = nChannelIndex if sAsic=="top" else nChannelIndex+16
        #print("Counters ",fpgaID,channelID)
        lCurrentChannelData=[lllCounter[configIndex][fpgaID][channelID] for configIndex in range(len(ln10bDacVal))]
        print(lCurrentChannelData)
        if (lCurrentChannelData[0]!=0):

          chd={}
          chd["fpga"]=fpgaID
          chd["channel"]=channelID
          chd["counters"]=lCurrentChannelData
          res["channels"].append(chd)
  else:
    print("All channel SCurve")
    
    fpgaID = int(sFpga[-1])
    #channelID = nChannelIndex if sAsic=="top" else nChannelIndex+16
    lbChannelEnable=[True]*16


    DisableAllChannels() #Disable All channels
    PetirocChannelEnable(lsFpga=[sFpga], lsAsic=[sAsic], lbChannelEnable=lbChannelEnable) #Only enable the current channel
    ApplyConfig()

    ############ 10bdac sweep #################

    ln10bDacVal = range(lowth,highth,step) #Coarse sweep range

    #Create SCurve for the channel
    lllCounter = sweepDac10b(lDac10bVal=ln10bDacVal, lsFpga=[sFpga], lsAsic=[sAsic], bPlot=False, bSaveData=False)

    #print(lllCounter)
    #for fpgaID in range(3):
    #  for channelID in range(34):
    for nChannelIndex in range(16):
      fpgaID = int(sFpga[-1])
      channelID = nChannelIndex if sAsic=="top" else nChannelIndex+16
      #print("Counters ",fpgaID,channelID)
      lCurrentChannelData=[lllCounter[configIndex][fpgaID][channelID] for configIndex in range(len(ln10bDacVal))]
      print(lCurrentChannelData)
      if (lCurrentChannelData[0]!=0):
        chd={}
        chd["fpga"]=fpgaID
        chd["channel"]=channelID
        chd["counters"]=lCurrentChannelData
        res["channels"].append(chd)
  # Store results
  with open('calib_%s_%s.json' % (sFpga,sAsic), 'w') as outfile:
    json.dump(res, outfile)
def IndependantEqualization(sFpga, sAsic, nDacTarget=-1,searchDac=True):

  #Set all the channels 6bdac to 32
  Dac6bConfig(lsFpga=[sFpga], lsAsic=[sAsic],lnDacVal=[32]*16)

  if nDacTarget == -1:
    #If the target is not defined, the first step is the automatic target definition
    print("Equalization target undefined, launching the target processing...")
    lnChannelTransition=[]
    for nChannelIndex in range(16):
      fpgaID = int(sFpga[-1])
      channelID = nChannelIndex if sAsic=="top" else nChannelIndex+16
      lbChannelEnable=[False]*16
      lbChannelEnable[nChannelIndex]=True

      DisableAllChannels() #Disable All channels
      PetirocChannelEnable(lsFpga=[sFpga], lsAsic=[sAsic], lbChannelEnable=lbChannelEnable) #Only enable the current channel
      ApplyConfig()

      ############ Coarse sweep #################

      ln10bDacVal = range(100,800,10) #Coarse sweep range

      #Create SCurve for the channel
      lllCounter = sweepDac10b(lDac10bVal=ln10bDacVal, lsFpga=[sFpga], lsAsic=[sAsic], bPlot=False, bSaveData=False)
      lnChannelData = [lllCounter[configIndex][fpgaID][channelID] for configIndex in range(len(ln10bDacVal))]

      lnTrans = CalcThresholds(ln10bDacVal, lnChannelData, max(lnChannelData), Limit=0.01)

      print("Channel",nChannelIndex,"transition range coarse estimation:",lnTrans)

      ############ Fine sweep ###################

      ln10bDacVal = range(lnTrans[0]-5,lnTrans[1]+6,1) #Fine sweep range

      #Create SCurve for the channel
      lllCounter = sweepDac10b(lDac10bVal=ln10bDacVal, lsFpga=[sFpga], lsAsic=[sAsic], bPlot=False, bSaveData=False)
      lnChannelData = [lllCounter[configIndex][fpgaID][channelID] for configIndex in range(len(ln10bDacVal))]

      lnTrans = CalcThresholds(ln10bDacVal, lnChannelData, max(lnChannelData), Limit=0.01)

      print("Channel",nChannelIndex,"transition range fine estimation:",lnTrans)

      #Record channel transition mean
      lnChannelTransition.append(mean(lnTrans))

    #Process the target as the middle between min and max transition
    nDacTarget = int((min(lnChannelTransition)+max(lnChannelTransition))/2.0)
    print("Optimal target defined as:",nDacTarget)

  if nDacTarget in [254,255,256]:
    nDacTarget = 257
    print("WARNING: Target shift to avoid 10bdac non-linearity, newTarget=",nDacTarget)
  if nDacTarget in [510,511,512]:
    nDacTarget = 513
    print("WARNING: Target shift to avoid 10bdac non-linearity, newTarget=",nDacTarget)

  print("Launching channel per channel equalization aiming the target :",nDacTarget)
  lnDac6bVal=[]

  if (not searchDac):

    for nChannelIndex in range(16):
      ped=lnChannelTransition[nChannelIndex]
      lnDac6bVal.append(32+int(round((nDacTarget-ped)*1./2.97)))
    else:
      for nChannelIndex in range(16):
        Dac6bConfig(lsFpga=[sFpga], lsAsic=[sAsic],lnDacVal=[32]*16) #Force every channel values to 32
        lnDac6bVal.append(SingleChannelEqualization(sFpga, sAsic, nChannelIndex=nChannelIndex, nDacTarget=nDacTarget))
  saveDac6bConfig(sFpga=sFpga, sAsic=sAsic, nTarget=nDacTarget, lnDac6bVal=lnDac6bVal)

  if -1 in lnDac6bVal:
    sys.exit("ERROR: impossible to reach the target for at least 1 channel")

  return lnDac6bVal

def CalcThresholds(ln10bDacVal, lnChannel, MaxVal, Limit=0.01):

  nLowerThresh = MaxVal*Limit
  nUpperThresh = MaxVal*(1.0-Limit)

  #If the tresholds are not present in channel data, return an empty list
  if min(lnChannel) > nLowerThresh or max(lnChannel) < nUpperThresh:
    return []

  nValIndex = len(ln10bDacVal)-1
  #Detect the lower threshold crossing
  while lnChannel[nValIndex] < nLowerThresh:
    nValIndex -= 1
  #Select the closest value
  if abs(lnChannel[nValIndex]-nLowerThresh) < abs(lnChannel[nValIndex+1]-nLowerThresh):
    nUpperThreshDac = ln10bDacVal[nValIndex]
  else:
    nUpperThreshDac = ln10bDacVal[nValIndex+1]
  
  #Detect the upper threshold crossing
  while lnChannel[nValIndex] < nUpperThresh:
    nValIndex -= 1
  #Select the closest value
  if abs(lnChannel[nValIndex]-nUpperThresh) < abs(lnChannel[nValIndex+1]-nUpperThresh):
    nLowerThreshDac = ln10bDacVal[nValIndex]
  else:
    nLowerThreshDac = ln10bDacVal[nValIndex+1]
  
  lnDacThresholds = [nLowerThreshDac, nUpperThreshDac]

  return lnDacThresholds

def mean(lValues):
  return float(sum(lValues))/float(len(lValues))

def saveDac6bConfig(sFpga, sAsic, nTarget, lnDac6bVal):
  with open("./dac6bConfig/dac6b_"+sFpga+"_"+sAsic+".txt","w") as outFile:
    outFile.write("dac10b Target ="+str(nTarget)+"\n")
    for chanIndex in range(16):
      outFile.write("chan"+str(chanIndex)+"="+str(lnDac6bVal[chanIndex])+"\n")

def SingleChannelEqualization(sFpga, sAsic, nChannelIndex, nDacTarget, MAX_NOISE=3000000, TRANSITION_THRESH=0.1):

  fpgaID = int(sFpga[-1])
  channelID = nChannelIndex if sAsic=="top" else nChannelIndex+16
  fBestScore=1000.0
  nBestDac6b=-1
  Highest6bDacAfterNoise = 0
  lbChannelEnable=[False]*16
  lbChannelEnable[nChannelIndex]=True
  ln6bScanRange=range(64)

  DisableAllChannels() #Disable All channels
  PetirocChannelEnable(lsFpga=[sFpga], lsAsic=[sAsic], lbChannelEnable=lbChannelEnable) #Only enable the current channel
  ApplyConfig()

  #FirstPass usable range (the full transition have to be inside nDacTarget +/- 5dacunit)
  ln10bDacVal=[nDacTarget]
  while True: #break when a transition is found
    if len(ln6bScanRange) == 0:
      lnUsable6bDac = range(max(0,Highest6bDacAfterNoise-3),min(Highest6bDacAfterNoise+4,64))
      print("Transition partially found, dac6b selected",lnUsable6bDac)
      break
      #sys.exit("ERROR: Impossible to find a transition for the current target: "+sFpga+" "+sAsic+" chan"+str(nChannelIndex)+" target="+str(nDacTarget))
    dac6b=int(mean(ln6bScanRange)) #Warning, this works only when there is no internal value removal
    print("Scan with 6bdac value=",dac6b)
    #Config 6bdac value in PETIROC (only for the current channel)
    Dac6bConfig(lsFpga=[sFpga],lsAsic=[sAsic],lnDacVal=[dac6b], lnChannel=[nChannelIndex])
    #Create SCurve for the channel
    lllCounter = sweepDac10b(lDac10bVal=ln10bDacVal, lsFpga=[sFpga], lsAsic=[sAsic], bPlot=False, bSaveData=True)
    lnChannelData = [lllCounter[configIndex][fpgaID][channelID] for configIndex in range(len(ln10bDacVal))]

    #If a transition is found, scan the 3 values around
    if MAX_NOISE * TRANSITION_THRESH <= lnChannelData[0] <= MAX_NOISE * (1.0-TRANSITION_THRESH):
      lnUsable6bDac = range(max(0,dac6b-3),min(dac6b+4,64))
      print("Transition found, dac6b selected",lnUsable6bDac)
      break
    elif lnChannelData[0] < MAX_NOISE * (1.0-TRANSITION_THRESH):
      Highest6bDacAfterNoise = max(Highest6bDacAfterNoise,dac6b)
      ln6bScanRange = list(filter(lambda x:x > dac6b, ln6bScanRange))
    elif lnChannelData[0] > MAX_NOISE * TRANSITION_THRESH:
      ln6bScanRange = list(filter(lambda x:x < dac6b, ln6bScanRange))

  plt.figure(figsize=[12.8,9.6])
  plt.plot([nDacTarget]*2,[0,MAX_NOISE],"--", label="Target", color='black', linewidth=4)

  #Second pass, sweep to get SCurves corresponding to the selected 6b dac
  ln10bDacVal = range(nDacTarget-7,nDacTarget+8,1) 
  for dac6b in lnUsable6bDac:
    #Config 6bdac value in PETIROC (only for the current channel)
    Dac6bConfig(lsFpga=[sFpga],lsAsic=[sAsic],lnDacVal=[dac6b], lnChannel=[nChannelIndex])
    #Create SCurve for the channel
    lllCounter = sweepDac10b(lDac10bVal=ln10bDacVal, lsFpga=[sFpga], lsAsic=[sAsic], bPlot=False, bSaveData=True)
    lnChannelData = [lllCounter[configIndex][fpgaID][channelID] for configIndex in range(len(ln10bDacVal))]

    plt.plot(ln10bDacVal,lnChannelData, label="dac6b="+str(dac6b))

    #Process transition and record 
    lnChannelTransition = CalcThresholds(ln10bDacVal, lnChannelData, MAX_NOISE, Limit=TRANSITION_THRESH)
    if len(lnChannelTransition) != 0: 
      fDiffTargetTrans = mean(lnChannelTransition)-nDacTarget

      plt.plot([mean(lnChannelTransition)]*2,[0,max(lnChannelData)],"--", label="dac6b="+str(dac6b)+"_TransMean")

      if abs(fDiffTargetTrans)<abs(fBestScore):
        fBestScore = fDiffTargetTrans
        nBestDac6b = dac6b

  plt.title("Scurves for different dac6b values for channel "+str(nChannelIndex)+"  Choosen value: "+str(nBestDac6b)+" (diff="+str(fBestScore)+")")
  plt.legend(loc="upper right")
  plt.grid(color='r', linestyle='-', linewidth=2)
  sOutFileName = sFpga+"_"+sAsic+"_"+"Channel"+str(nChannelIndex)
  plt.savefig("./SingleChannelEqualizationPlots/"+sOutFileName)

  #Set the selected best 6bdac value in the PETIROC (only for the current channel)
  Dac6bConfig(lsFpga=[sFpga],lsAsic=[sAsic],lnDacVal=[nBestDac6b], lnChannel=[nChannelIndex])

  return nBestDac6b


if __name__ == "__main__":


  ### GG modifications
  ### Parameters for the script
  ### Python experts can put that in command line argument decoding
  # There is not the corresponding function to do that initialisation in that script ## FC7init=False  #FC7 initialisation should be done once
  p_type=1  # 0: A , 1: B , 2:C
  p2C_autoreset_ON=False
  single=False # scurve channel by channel or all channels at the same time
  nDacTarget=-1 # -1 means find target automatically, otherwise put a DAC value to reach (example 400)
  #nDacTarget=420
  P2B_FSM_parameters=[3,2,2]
  P2C_resetDelay_parameter=15
  CalibDir="./dac6bConfig"
  CalibDir="./dac6bConfigAutoResetFEB6CH187"
  dacsearch=False
  ### end GG modifications

  project = fc7(False)
  DL, UL = project.downlink, project.uplink
  feb = feb_v2_cycloneV_r1(project, False)

  project.stop_acquisition()
  project.fifos_clear()

  sca_reg=feb_sca(project.fpga_registers, True)

  #This function has to be run once for the correct FC7 initialization
  #This function is missing in this script
  #if FC7init:
  #   InitFc7()

  #Ensure the uplink bandwidth is released
  DisableTDC()

  #Test communication with the 3 FEB FPGAs
  test_com_FPGA()

  #"""
  #Initialize all Petiroc
  #"""
  #single = True # set above
  # p_type = 1  # 0: A , 1: B , 2:C  #set above
  if (p_type==1):
    set_PetiROC_type_forall(stype="2B")
    feb.asic.rstb("all","all")
    feb.asic.sr_rst("all","all")
    EnableAutoResetSM(P2B_FSM_parameters)
  if (p_type==2):
      set_PetiROC_type_forall(stype="2C")
      feb.asic.rstb("all","all")
      feb.asic.sr_rst("all","all")
      if p2C_autoreset_ON:
        enable_delay_reset_trigger_forall(P2C_resetDelay_parameter)
        DisableAutoResetSM() #Ensure autoReset SM is disabled for PETIROC 2C
      else:
        EnableAutoResetSM(P2B_FSM_parameters)
        disable_delay_reset_trigger_forall()
  #"""
  for sFpga in ["fpga_0","fpga_1","fpga_2"]:
    for sAsic in ["top", "bottom"]:
  #for sFpga in ["fpga_2"]:
  #  for sAsic in ["bottom"]:
        print("New ASIC")
        print(sFpga, sAsic)
        if  single :
          OneChannelScurve(sFpga, sAsic, lowth=270,highth=360,step=2,use32=False,config_statename="RE31_185_FEB_5_904",config_version=5,single=True)
          #IndependantEqualization(sFpga, sAsic, nDacTarget,dacsearch)
        else :
          OneChannelScurve(sFpga, sAsic, lowth=260,highth=370,step=2,use32=False,config_statename="RE31_185_FEB_5_904",config_version=5,single=False)
          #allchannelEnableScurves(sFpga, sAsic, feb,CalibDir)
  sys.exit()
