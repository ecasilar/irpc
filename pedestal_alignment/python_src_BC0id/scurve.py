import pylint
import time
import sys
import os
import types
import argparse
import os
import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate as sp


###---------------------------------------------###
# This class is used to plot SCURVE from raw file #
###---------------------------------------------###

class scurve:
####
    def __init__(self,file):
        self.file = file

    
#### Plot curve from raw file [used in test files]
    def read_raw_file(self,x_label, y_label):
        with open(self.file,'r') as f:
            mylist = f.read().splitlines()
            r=[]
            for d in mylist:
                r.append(d.split(","))
        graph = {}
        for x in r:
            if x[0][0]=="#":
                title = x[0][1:] + "_" + x[1] + "_" + x[2] + "_" + x[3] + "_" + x[4]
                graph[title] = [[],[]]
            else:
                graph[title][0].append(int(x[0]))
                graph[title][1].append(int(x[1]))                
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.title('Scurve')
        plt.grid(True)
        for ref,data in graph.items() :
            x=data[0]
            y=data[1]
            plt.plot(x,y,label=ref)

        plt.legend(title='Parameter where:')
        plt.show()
        return graph
    
#### Plot and return Threashold at 50% of Scurve after interpolation
    def Scurve_50percent_threashold(self,graph):
        threashold_50_real={}
        for ref,data in graph.items() :
            x_datas = data[0]
            y_datas = data[1]
            y_max = max(y_datas) 
            y_min = min(y_datas) 
            print(y_max)
            print(y_min)
            x_start = 0
            x_stop = 0
            #recherche des indices x(threashold) de chaques extrémités de la pente 
            for i in range(1,len(x_datas)):
                 if y_datas[i] < y_max-(y_max*0.1) and  (y_max-(y_max*0.1)< y_datas[i-1] <= y_max) :
                     x_start = i-1    
                 if y_datas[i] == 0 and y_datas[i-1] > 0:
                     x_stop = i       
            
            # modélisation linéaire de la pente de la S-curve          
            xp = np.linspace(x_datas[x_start],x_datas[x_stop],1000)
            yp = np.linspace(y_max,0,1000)
            half = 500
            #interpolation 1D des valeurs
            curve_real = sp.interp1d(x_datas,y_datas)
            curve_model= sp.interp1d(xp,yp)
            
            #plot
            plt.xlabel('threshold')
            plt.ylabel('nb_triggers')
            plt.plot(x_datas,curve_real(x_datas),':x',label=ref)
            plt.legend(title='Scurves')
            plt.plot(xp,curve_model(xp),'--')
            
            #On récupère la valeur du threashold réel       
            threashold_50_model = int(xp[500])
            for threashold in x_datas:
                if threashold == threashold_50_model:
                    x = ref.split("_")

                    if x[0] == "r":
                        title = str(x[0]+"_"+x[1]+"_"+x[2])
                    else:
                        title = str(x[0]+"_"+x[1])

                    threashold_50_real[title] = []
                    threashold_50_real[title].append(threashold)

        plt.show()
        return threashold_50_real



if __name__ == '__main__':
    obj = scurve("scurve.raw")
    graph=obj.read_raw_file()
    
    for ref,data in graph.items() :
        x=data[0]
        y=data[1]
    threashold_50_real = obj.Scurve_50percent_threashold(graph)


    DACadj_6b=[]
    for strip,th_n in threashold_50_real.items():
        print(th_n[0][0])
