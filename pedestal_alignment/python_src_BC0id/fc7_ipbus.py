import uhal
import time
import sys
import os
import types
from fc7 import *

###---------------------------------------------###
#     This class is the main class of IPBUs       #
###---------------------------------------------###
class fc7_ipbus:

  def __init__(self):
    uhal.setLogLevelTo (uhal.LogLevel.WARNING)
    #This source and the addresses.xml file must be in the same directory
    self.sFilePath = os.path.dirname(os.path.realpath(__file__)) + "/"
    self.device = None

    ## timeout counter
  def connect(self):
  	self.device = uhal.getDevice("FC7_Board", "ipbusudp-2.0://172.16.4.20:50001","file://"+self.sFilePath+"fc7_addresses.xml")
  	self.device.setTimeoutPeriod(3000)
  	print("## Connected to FC7 board #")

  def ReadBlock(self, sRegName,nb_words):
  	while(True):
  		try:
  			readRes =  self.device.getNode(sRegName).readBlock(nb_words)
  			self.device.dispatch()
  			return readRes.value()
  		except:
  			print("IPBus read_block timeout")
  			continue

  def WriteBlock(self, sRegName, nValue):
  	while(True):
  		try:
  			self.device.getNode(sRegName).writeBlock(nValue)
  			self.device.dispatch()
  			break
  		except:
  			print("IPBus write_block timeout")
  			continue

  def WriteReg(self, sRegName, nValue):
    '''Main function to write the firmware ipbus registers'''

    while True:
      try:
        self.device.getNode(sRegName).write(nValue)
        self.device.dispatch()
        break
      except:
        print("IPBus register write problem, retrying...")
        continue

  def ReadReg(self, sRegName):
    '''Main function to read the firmware ipbus registers'''

    while True:
      try:
        readRes = self.device.getNode(sRegName).read()
        self.device.dispatch()
        return readRes.value()
      except:
        print("IPBus register read problem, retrying...")
        continue


if __name__ == '__main__':

  Obj = sca_reg("CRB",8,"rw",0x10)
  fc7 = fc7(True)
  #Obj.info()
  fc7.ipbus_device.ReadReg("USER_OUTPUT_FIFO_TDC")