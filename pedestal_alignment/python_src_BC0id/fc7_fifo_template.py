import uhal
import time
import sys
import os
import types
from fc7_registers import *
from tools_box import *

class fc7_fifo_template:
    def __init__(self,fc7_registers,mode,index_flag_empty,index_flag_full,index_flag_clear,verbose):
        self.verbose = verbose
        self.fc7_registers = fc7_registers
        self.mode=mode
        self.index_flag_empty = index_flag_empty
        self.index_flag_full = index_flag_full
        self.index_flag_clear = index_flag_clear
        self.ctrl = self.fc7_registers.get_fifos_ctrl()
        self.status = self.fc7_registers.get_fifos_status()

 ## Get function
    def read_status(self):
        self.status = self.fc7_registers.get_fifos_status()
        self.ctrl   = self.fc7_registers.get_fifos_ctrl()

    def get_full(self):
        return int(bf(self.fc7_registers.get_fifos_status())[self.index_flag_full])

    def get_empty(self):
        return int(bf(self.fc7_registers.get_fifos_status())[self.index_flag_empty])

    def get_clear(self):
        return int(bf(self.fc7_registers.get_fifos_ctrl())[self.index_flag_clear])


## Set function
    def write_ctrl(self):
        self.fc7_registers.set_fifos_ctrl(self.ctrl)

    def set_clear(self,value):
        v=bf(0x0000)
        v[self.index_flag_clear] = value
        self.ctrl = int(v)
        self.write_ctrl()

## Set function
    def clear(self):
        self.set_clear(1)
        time.sleep(0.1)
        self.set_clear(0)

    def write_single(self,value):
        if self.mode=="IN":
            if self.get_full()==0:
                self.fc7_registers.set_fifo_single_txdata_SC(value)
            else:
                if self.verbose: print("fifo full !")
        else:
            raise(ValueError("FIFO not writable !"))

    def write_block(self,tab):
        if self.mode=="IN":
            if self.get_full()==0:
                self.fc7_registers.set_tx_fifo_SC_block(tab)
            else:
                if self.verbose: print("fifo full !")
        else:
            raise(ValueError("FIFO not writable !"))

    def read_single(self):
        if self.mode=="OUT":
            if self.get_empty()==0:
                return self.fc7_registers.get_rx_fifo_SC_single()
            else:
                if self.verbose: print("fifo  empty !")

        else:
            raise(ValueError("FIFO not readable !"))

    def read_block(self,nb_words):
        if self.mode=="OUT":
            if self.get_empty()==0:
                r=self.fc7_registers.get_rx_fifo_SC_block(nb_words)
                nb = 0
                nb_frame = 0
                for x in r:
                    if self.verbose: print("RX frame #{} data 32 bits #{} : 0x{:08x}".format(nb_frame,nb,x))
                    if nb==3:
                        nb = 0
                        nb_frame = nb_frame + 1
                    else:
                        nb = nb+ 1
                return r
            else:
                if self.verbose: print("fifo empty !")
        else:
            raise(ValueError("FIFO not readable !"))
            
    def read_block_TDC(self,nb_words):
        if self.mode=="OUT":
            if self.get_empty()==0:
                r=self.fc7_registers.get_rx_fifo_TDC_block(nb_words)
                nb = 0
                nb_frame = 0
                for x in r:
                    if self.verbose: print("RX frame #{} data 32 bits #{} : 0x{:08x}".format(nb_frame,nb,x))
                    if nb==3:
                        nb = 0
                        nb_frame = nb_frame + 1
                    else:
                        nb = nb+ 1
                return r
            else:
                if self.verbose: print("fifo empty !")
        else:
            raise(ValueError("FIFO not readable !"))
