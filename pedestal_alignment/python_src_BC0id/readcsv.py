import csv
from ROOT import *
from array import array

fn='1634501332478_2.csv'
fn='../SweepData_AllChannelsEn_RT8_petiroc2c_188_feb6_HVOFF/1634550460393_4.csv'
def plotchannel(fname,fpga,position):
    f=open(fname)
    spr0 = csv.reader(f, delimiter=',')
    
    idx=0
    ch=[]
    vth=None
    ifirst=100
    ilast=0
    for row in spr0:
        if (idx==0):
            vth=row
            print(vth)
        else:
            ch.append(row)
            if (int(ch[idx-1][0])!=0):
                print(idx-1,ch[idx-1][0])
                if (ifirst>96):
                    ifirst=idx-1
            else:
                if (ifirst<96 and ilast==0 and int(ch[idx-1][0])==0):
                    ilast=idx-1
                #al=raw_input()
        idx=idx+1
    print("ECE",ifirst,ilast)
    channel,pedestal,maxnoise,noise=array('d'),array('d'),array('d'),array('d')


    vth = [int(x) for x in vth ]

    c2 = TCanvas( 'c2', 'A Simple Graph Example', 200, 10, 1700, 1500 )
    c2.Divide(4,4)
    gStyle.SetOptFit()
    ipad=1
    gr=[]
    l=[]
    l1=[]
    for ich in range(ifirst,ilast):

        x,y,dx,dy=array('d'),array('d'),array('d'),array('d')
        ncnt=0
        print("ECE 2",ch[ich])
        for ivth in range(0,max(vth)-min(vth)+1):
            ncnt=ncnt+float(ch[ich][ivth])
        #ncnt=ncnt/100
        ncnt=int(ch[ifirst][0])
        print("ECE3",ncnt)

        for i in range(len(vth)):
            x.append(int(vth[i]))
            y.append(float(ch[ich][i])/ncnt)
            dx.append(0)
            dy.append(0)
        #print y
        maxvth=0
        for i in range(len(vth)-1,0,-1):
            if (y[i]>0.5):
                maxvth=x[i]
                break
        minvth=0
        for i in range(len(vth)):
            if (y[i]<0.985):
                minvth=x[i]
                break
        #minvth=205
        #maxvth=450
        print(ich,minvth,maxvth,(maxvth-minvth))
        channel.append(ich)
        pedestal.append(minvth)
        maxnoise.append(maxvth+1)
        noise.append(maxvth-minvth)
        c2.cd(ipad)
        gr.append(TGraphErrors( len(vth), x, y,dx,dy ))

        gr[ipad-1].SetMarkerColor( 1+ich%6 )
        gr[ipad-1].SetMarkerStyle( 20+ich%6 )
        gr[ipad-1].SetTitle( 'Scurve FPGA %d %s channel %d' % (fpga,position,ich) )
        gr[ipad-1].GetXaxis().SetTitle( 'VTH' )
        #gr[ipad-1].GetXaxis().SetTitle( 'HV_{eff} (V)' )
        gr[ipad-1].GetYaxis().SetTitle( 'Efficiency ' )

        gr[ipad-1].GetXaxis().SetRangeUser(minvth-30,maxvth+30)
        gr[ipad-1].Draw( 'AP' )
        l.append(TLine(minvth,0.0,minvth,1.0))
        l[ipad-1].SetLineColor(3);
        l[ipad-1].Draw();
        l1.append(TLine(maxvth,0.0,maxvth,1.0))
        l1[ipad-1].SetLineColor(2);
        l1[ipad-1].Draw();
        c2.Update()
        #c1.GetFrame().SetFillColor( 21 )
        c2.GetFrame().SetBorderSize( 12 )
        c2.Modified()
        c2.Update()
        #val=raw_input()
        ipad=ipad+1
    c2.SaveAs(fname.split("/")[0]+"/Scurve_channels_fpga_%d_%s_%s.png" % (fpga,position,fname.split("/")[1].split(".")[0]))
    c1 = TCanvas( 'c1', 'A Simple Graph Example', 200, 10, 700, 1000 )
    c1.Divide(1,2)
    c1.cd(1)
    grp = TGraphErrors( len(channel), channel,pedestal )

    grp.SetMarkerColor( 1)
    grp.SetMarkerStyle( 20)
    grp.SetTitle( 'Pedestal FPGA %d %s' % (fpga,position))
    grp.GetXaxis().SetTitle( 'Channel' )
    #gr[ipad-1].GetXaxis().SetTitle( 'HV_{eff} (V)' )
    grp.GetYaxis().SetTitle( 'Pedestal (DAC10b)' )
    grp.GetYaxis().SetRangeUser(pedestal[0]-30,pedestal[0]+30)
    
    grp.Draw( 'AP' )
    grp.Fit('pol0')
    c1.Update()
    #c1.GetFrame().SetFillColor( 21 )
    c1.GetFrame().SetBorderSize( 12 )
    c1.Modified()
    c1.Update()
    c1.cd(2)
    grn = TGraphErrors( len(channel), channel,noise )

    grn.SetMarkerColor( 1)
    grn.SetMarkerStyle( 20)
    grn.SetTitle( 'Noise FPGA %d %s' % (fpga,position))
    grn.GetXaxis().SetTitle( 'Channel' )
    #gr4.GetXaxis().SetTitle( 'HV_{eff} (V)' )
    grn.GetYaxis().SetTitle( 'Noise (DAC10b)' )
    grn.GetYaxis().SetRangeUser(noise[0]-15,noise[0]+15)
    
    grn.Draw( 'AP' )
    grn.Fit('pol0')
    c1.Update()
    #c1.GetFrame().SetFillColor( 21 )
    c1.GetFrame().SetBorderSize( 12 )
    c1.Modified()
    c1.Update()
    #val=raw_input()
    c1.SaveAs(fname.split("/")[0]+"/Scurve_summary_fpga_%d_%s_%s.png" % (fpga,position,fname.split("/")[1].split(".")[0]))


plotchannel('dac6bConfig_RE31_186_FEBv2_2_14_904_HV0kV/0kV_fpga_0_bottom.csv',0,'bottom')
plotchannel('dac6bConfig_RE31_186_FEBv2_2_14_904_HV0kV/0p1kV_fpga_0_bottom.csv',0,'bottom')
plotchannel('dac6bConfig_RE31_186_FEBv2_2_14_904_HV0kV/1kV_fpga_0_bottom.csv',0,'bottom')
plotchannel('dac6bConfig_RE31_186_FEBv2_2_14_904_HV0kV/5kV_fpga_0_bottom.csv',0,'bottom')
plotchannel('dac6bConfig_RE31_186_FEBv2_2_14_904_HV0kV/6kV_fpga_0_bottom.csv',0,'bottom')
plotchannel('dac6bConfig_RE31_186_FEBv2_2_14_904_HV0kV/7kV_fpga_0_bottom.csv',0,'bottom')
plotchannel('dac6bConfig_RE31_186_FEBv2_2_14_904_HV0kV/7p1kV_fpga_0_bottom.csv',0,'bottom')
plotchannel('dac6bConfig_RE31_186_FEBv2_2_14_904_HV0kV/7p2kV_fpga_0_bottom.csv',0,'bottom')

plotchannel('dac6bConfig_RE31_186_FEBv2_2_14_904_HV0kV/7kV_333_fpga_0_bottom.csv',0,'bottom')
#plotchannel('dac6bConfig_RE31_186_FEBv2_2_14_904_HV0kV/7kV_301_fpga_0_bottom.csv',0,'bottom')


plotchannel('dac6bConfig_RE31_186_FEBv2_2_14_904_HV0kV/0kV_fpga_0_top.csv',0,'top')
plotchannel('dac6bConfig_RE31_186_FEBv2_2_14_904_HV0kV/0p1kV_fpga_0_top.csv',0,'top')
plotchannel('dac6bConfig_RE31_186_FEBv2_2_14_904_HV0kV/1kV_fpga_0_top.csv',0,'top')
plotchannel('dac6bConfig_RE31_186_FEBv2_2_14_904_HV0kV/5kV_fpga_0_top.csv',0,'top')
plotchannel('dac6bConfig_RE31_186_FEBv2_2_14_904_HV0kV/6kV_fpga_0_top.csv',0,'top')
plotchannel('dac6bConfig_RE31_186_FEBv2_2_14_904_HV0kV/7kV_fpga_0_top.csv',0,'top')
plotchannel('dac6bConfig_RE31_186_FEBv2_2_14_904_HV0kV/7p1kV_fpga_0_top.csv',0,'top')
plotchannel('dac6bConfig_RE31_186_FEBv2_2_14_904_HV0kV/7p2kV_fpga_0_top.csv',0,'top')

plotchannel('dac6bConfig_RE31_186_FEBv2_2_14_904_HV0kV/7kV_333_fpga_0_top.csv',0,'top')
#plotchannel('dac6bConfig_RE31_186_FEBv2_2_14_904_HV0kV/7kV_301_fpga_0_top.csv',0,'top')

#plotchannel('SweepData/1636906254826_0.csv',0,'top')
#plotchannel('SweepData/1636906254826_2.csv',0,'bottom')
#plotchannel('SweepData/1636906254826_4.csv',1,'top')
#plotchannel('SweepData/1636906254826_6.csv',1,'bottom')
#plotchannel('SweepData/1636906254826_8.csv',2,'top')
#plotchannel('SweepData/1636906254826_10.csv',2,'bottom')
#plotchannel('Results/SweepData/1636569437361_4.csv',1,'top')
#plotchannel('Results/SweepData/1635856021966_6.csv',1,'bottom')
#plotchannel('Results/SweepData/1635856021966_8.csv',2,'top')
#plotchannel('Results/SweepData/1635856021966_10.csv',2,'bottom')
#plotchannel('Results/SweepData_AllChannelsEn_TriggerDelay4_petiroc2c_188_feb6_HVOFF/1634814414541_6.csv',1,'bottom')
#plotchannel('Results/SweepData_AllChannelsEn_TriggerDelay4_petiroc2c_188_feb6_HVOFF/1634814414541_8.csv',2,'top')
#plotchannel('Results/SweepData_AllChannelsEn_TriggerDelay4_petiroc2c_188_feb6_HVOFF/1634814414541_10.csv',2,'bottom')
