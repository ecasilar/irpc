# -*- coding: utf-8 -*-
"""
Created on Mon May 10 11:26:51 2021

@author: luciol
"""
import uhal
import time
import signal
import sys
import os
import types
import argparse
import json
import shutil
import threading
import time
from datetime import datetime

from tools_box import *
from fc7 import *
from feb_v2_cycloneV_r1 import *
from feb_ctrl_and_status import *
from testbench_calibrate_asic import *
from feb_fpga_registers import *
from Equalization import *
from Parsers_irradiation_tests import *
from statistics import mean
import matplotlib.pyplot as plt

##########################################
### TESTBENCH FILE for radiation tests ###
##########################################

class testbench_irradiation : 
    
    def __init__(self,verbose):
        self.fc7 = fc7(verbose)
        self.feb = feb_v2_cycloneV_r1(self.fc7,verbose)
        self.stop_loop = False
        self.counter_plot = 0
        self.CRC_ERRORS = {"LEFT"    : 0, # fpga_0
                           "MIDDLE"  : 0, # fpga_1
                           "RIGHT"   : 0} # fpga_2
        self.FAULTS = {"2V" : 1, # actif bas
                       "4V" : 1}
        self.error_detected = {"FAULTS" :0,
                               "CRC" :0} 

        self.scurve = scurve("irradiation_run/plots/scurve_1.raw")

        self.fpga_register = {
        "GENERAL"                        : register(0x0000,"rw",16,1,0x0000),
        ### TOP ###
        #-------------------------------------------------------------------#
        "PETIROC_TOP_LOAD"               : register(0x0100,"rw",16,1,0x0000),# bit 0 : load, bit 1 : auto_reload ("auto_reload_periodic config : 0x11")
        "PETIROC_TOP_SR_RST"             : register(0x0101,"rw",16,1,0x0000),
        "PETIROC_TOP_RSTB"               : register(0x0102,"rw",16,1,0x0000),
        "TRIGGER_EXT_PIN_TOP"            : register(0x0103,"rw",16,1,0x0000),# bit 0   
        "PETIROC_TOP_NOR32RAZ_FSM"       : register(0x0104,"rw",16,1,0x0000),
        # FSM stage time
        "FSM_RAZ_stage1_top"             : register(0x0105,"rw",16,1,0x0000),
        "FSM_RAZ_stage2_top"             : register(0x0106,"rw",16,1,0x0000),
        "FSM_RAZ_stage3_top"             : register(0x0107,"rw",16,1,0x0000),
        "FPGA_FEATURES_TOP"              : register(0x0108,"rw",16,1,0x0000), # bit 0 ==> LATCHING sorties PetiROC vers FPGA | bit 1 ==> use FPGA NOR32 (utilise nor32 généré par les entrées du fpga [sequencage reset]) [DEFAULT_VALUE = 0x"10"]
        "BITFLIP_COUNTER_RESET_TOP"      : register(0x0109,"rw",16,1,0x0000), # bit 0
        # 1 / 10 ns period
        "PETIROC_RELOADING_PERIOD_TOP_1" : register(0x010A,"rw",16,1,0x0000), # LSB 
        "PETIROC_RELOADING_PERIOD_TOP_2" : register(0x010B,"rw",16,1,0x0000), # 
        "PETIROC_RELOADING_PERIOD_TOP_3" : register(0x010C,"rw",16,1,0x0000), # MSB
        "PETIROC_TOP_CONFIG"             : register(0x0117,"rw",16,41,[0x0000]),
        # Bitflip counter (irradiation tests)
        "BITFLIP_COUNTER_TOP_1"          : register(0x0140,"ro",16,1,[0x0000]), #LSB
        "BITFLIP_COUNTER_TOP_2"          : register(0x0141,"ro",16,1,[0x0000]), #MSB
        
        "PETIROC_TOP_CONFIG_PREVIOUS"    : register(0x0164,"ro",16,41,[0x0000]),
        #-------------------------------------------------------------------#
        ### BOTTOM ###
        #-------------------------------------------------------------------#
        "PETIROC_BOTTOM_LOAD"            : register(0x0200,"rw",16,1,0x0000),# bit 0 : load, bit 1 : auto_reload ("auto_reload_periodic config : 0x11") 
        "PETIROC_BOTTOM_SR_RST"          : register(0x0201,"rw",16,1,0x0000),
        "PETIROC_BOTTOM_RSTB"            : register(0x0202,"rw",16,1,0x0000),
        "TRIGGER_EXT_PIN_BOT"            : register(0x0203,"rw",16,1,0x0000),# bit 0  
        "PETIROC_BOTTOM_NOR32RAZ_FSM"    : register(0x0204,"rw",16,1,0x0000),
        ### FSM stage ###
        "FSM_RAZ_stage1_bot"             : register(0x0205,"rw",16,1,0x0000),
        "FSM_RAZ_stage2_bot"             : register(0x0206,"rw",16,1,0x0000),
        "FSM_RAZ_stage3_bot"             : register(0x0207,"rw",16,1,0x0000),
        "FPGA_FEATURES_BOT"              : register(0x0208,"rw",16,1,0x0000), # bit 0 ==> LATCHING sorties PetiROC vers FPGA | bit 1 ==> use FPGA NOR32 (utilise nor32 généré par les entrées du fpga [sequencage reset]) [DEFAULT_VALUE = 0x"10"]
        "BITFLIP_COUNTER_RESET_BOT"      : register(0x0209,"rw",16,1,0x0000), # bit 0
        # 1 / 10 ns period
        "PETIROC_RELOADING_PERIOD_BOT_1" : register(0x020A,"rw",16,1,0x0000), # LSB
        "PETIROC_RELOADING_PERIOD_BOT_2" : register(0x020B,"rw",16,1,0x0000), # 
        "PETIROC_RELOADING_PERIOD_BOT_3" : register(0x020C,"rw",16,1,0x0000), # MSB
        "PETIROC_BOTTOM_CONFIG"          : register(0x0217,"rw",16,41,[0x0000]),
        # Bitflip counter (irradiation tests)
        "BITFLIP_COUNTER_BOT_1"          : register(0x0240,"ro",16,1,[0x0000]), #LSB
        "BITFLIP_COUNTER_BOT_2"          : register(0x0241,"ro",16,1,[0x0000]), #MSB
        "PETIROC_BOTTOM_CONFIG_PREVIOUS" : register(0x0264,"ro",16,41,[0x0000]),
        #-------------------------------------------------------------------#
        ### TDC ###
        #-------------------------------------------------------------------#
        "TDC_ENABLE"                     : register(0x0300,"rw",16,1,0x0000),
        "TDC_CMD_VALID"                  : register(0x0301,"rw",16,1,0x0000),
        "TDC_ENABLE_CH0_15"              : register(0x0305,"rw",16,1,0x0000),
        "TDC_ENABLE_CH16_31"             : register(0x0306,"rw",16,1,0x0000),
        "TDC_ENABLE_CH32_33"             : register(0x0307,"rw",16,1,0x0000),
        "TDC_INJECTION_MODE"             : register(0x0308,"rw",3,1,0x0000)
        #-------------------------------------------------------------------#
        }

        self.lookuptable_strips = {
        "r_strip_0"     : ["fpga_0","bottom",0,16],
        "strip_0"       : ["fpga_0","bottom",2,17],
        "r_strip_1"     : ["fpga_0","bottom",4,18],
        "strip_1"       : ["fpga_0","bottom",6,19],
        "r_strip_2"     : ["fpga_0","bottom",8,20],
        "strip_2"       : ["fpga_0","bottom",10,21],
        "r_strip_3"     : ["fpga_0","bottom",12,22],
        "strip_3"       : ["fpga_0","bottom",14,23],
        "r_strip_4"     : ["fpga_0","bottom",16,24],
        "strip_4"       : ["fpga_0","bottom",18,25],
        "r_strip_5"     : ["fpga_0","bottom",20,26],
        "strip_5"       : ["fpga_0","bottom",22,27],
        "r_strip_6"     : ["fpga_0","bottom",24,28],
        "strip_6"       : ["fpga_0","bottom",26,29],
        "r_strip_7"     : ["fpga_0","bottom",28,30],
        "strip_7"       : ["fpga_0","bottom",30,31],
        "r_strip_8"     : ["fpga_0","top",30,15],
        "strip_8"       : ["fpga_0","top",28,14],
        "r_strip_9"     : ["fpga_0","top",26,13],
        "strip_9"       : ["fpga_0","top",24,12],
        "r_strip_10"    : ["fpga_0","top",22,11],
        "strip_10"      : ["fpga_0","top",20,10],
        "r_strip_11"    : ["fpga_0","top",18,9],
        "strip_11"      : ["fpga_0","top",16,8],
        "r_strip_12"    : ["fpga_0","top",14,7],
        "strip_12"      : ["fpga_0","top",12,6],
        "r_strip_13"    : ["fpga_0","top",10,5],
        "strip_13"      : ["fpga_0","top",8,4],
        "r_strip_14"    : ["fpga_0","top",6,3],
        "strip_14"      : ["fpga_0","top",4,2],
        "r_strip_15"    : ["fpga_0","top",2,1],
        "strip_15"      : ["fpga_0","top",0,0],

        "r_strip_16"    : ["fpga_1","bottom",0,16],
        "strip_16"      : ["fpga_1","bottom",2,17],
        "r_strip_17"    : ["fpga_1","bottom",4,18],
        "strip_17"      : ["fpga_1","bottom",6,19],
        "r_strip_18"    : ["fpga_1","bottom",8,20],
        "strip_18"      : ["fpga_1","bottom",10,21],
        "r_strip_19"    : ["fpga_1","bottom",12,22],
        "strip_19"      : ["fpga_1","bottom",14,23],
        "r_strip_20"    : ["fpga_1","bottom",16,24],
        "strip_20"      : ["fpga_1","bottom",18,25],
        "r_strip_21"    : ["fpga_1","bottom",20,26],
        "strip_21"      : ["fpga_1","bottom",22,27],
        "r_strip_22"    : ["fpga_1","bottom",24,28],
        "strip_22"      : ["fpga_1","bottom",26,29],
        "r_strip_23"    : ["fpga_1","bottom",28,30],
        "strip_23"      : ["fpga_1","bottom",30,31],
        "r_strip_24"    : ["fpga_1","top",30,15],
        "strip_24"      : ["fpga_1","top",28,14],
        "r_strip_25"    : ["fpga_1","top",26,13],
        "strip_25"      : ["fpga_1","top",24,12],
        "r_strip_26"    : ["fpga_1","top",22,11],
        "strip_26"      : ["fpga_1","top",20,10],
        "r_strip_27"    : ["fpga_1","top",18,9],
        "strip_27"      : ["fpga_1","top",16,8],
        "r_strip_28"    : ["fpga_1","top",14,7],
        "strip_28"      : ["fpga_1","top",12,6],
        "r_strip_29"    : ["fpga_1","top",10,5],
        "strip_29"      : ["fpga_1","top",8,4],
        "r_strip_30"    : ["fpga_1","top",6,3],
        "strip_30"      : ["fpga_1","top",4,2],
        "r_strip_31"    : ["fpga_1","top",2,1],
        "strip_31"      : ["fpga_1","top",0,0],

        "r_strip_32"    : ["fpga_2","bottom",0,16],
        "strip_32"      : ["fpga_2","bottom",2,17],
        "r_strip_33"    : ["fpga_2","bottom",4,18],
        "strip_33"      : ["fpga_2","bottom",6,19],
        "r_strip_34"    : ["fpga_2","bottom",8,20],
        "strip_34"      : ["fpga_2","bottom",10,21],
        "r_strip_35"    : ["fpga_2","bottom",12,22],
        "strip_35"      : ["fpga_2","bottom",14,23],
        "r_strip_36"    : ["fpga_2","bottom",16,24],
        "strip_36"      : ["fpga_2","bottom",18,25],
        "r_strip_37"    : ["fpga_2","bottom",20,26],
        "strip_37"      : ["fpga_2","bottom",22,27],
        "r_strip_38"    : ["fpga_2","bottom",24,28],
        "strip_38"      : ["fpga_2","bottom",26,29],
        "r_strip_39"    : ["fpga_2","bottom",28,30],
        "strip_39"      : ["fpga_2","bottom",30,31],
        "r_strip_40"    : ["fpga_2","top",30,15],
        "strip_40"      : ["fpga_2","top",28,14],
        "r_strip_41"    : ["fpga_2","top",26,13],
        "strip_41"      : ["fpga_2","top",24,12],
        "r_strip_42"    : ["fpga_2","top",22,11],
        "strip_42"      : ["fpga_2","top",20,10],
        "r_strip_43"    : ["fpga_2","top",18,9],
        "strip_43"      : ["fpga_2","top",16,8],
        "r_strip_44"    : ["fpga_2","top",14,7],
        "strip_44"      : ["fpga_2","top",12,6],
        "r_strip_45"    : ["fpga_2","top",10,5],
        "strip_45"      : ["fpga_2","top",8,4],
        "r_strip_46"    : ["fpga_2","top",6,3],
        "strip_46"      : ["fpga_2","top",4,2],
        "r_strip_47"    : ["fpga_2","top",2,1],
        "strip_47"      : ["fpga_2","top",0,0]
        }

        
    def init(self):
        self.fc7.init()
        self.feb.init()

##=================================================== ********************** ==================================================================##
##=================================================== Slow Control functions ==================================================================##
##=================================================== ********************** ==================================================================##
    def decode_ctrl_SCA_status(self):
        print(bcolors.BLUE +"\n______________READ_SCA_STATUS_GPIO_CRC/FAULT_______________\n"+bcolors.ENDC) 
          
        ### DECODE CRC and Fault errors From SCA in one request.
        SCA_status = self.feb.feb_ctrl_and_status.sca.get_GPIO_R()
        self.feb.feb_ctrl_and_status.sca.wait_rx_recieved()

        self.CRC_ERRORS["LEFT"] = int(bf(SCA_status)[self.feb.feb_ctrl_and_status.sca.GPIO_direction["CRC_ERROR_LEFT"][1]])
        self.CRC_ERRORS["MIDDLE"] = int(bf(SCA_status)[self.feb.feb_ctrl_and_status.sca.GPIO_direction["CRC_ERROR_MIDDLE"][1]])
        self.CRC_ERRORS["RIGHT"] = int(bf(SCA_status)[self.feb.feb_ctrl_and_status.sca.GPIO_direction["CRC_ERROR_RIGHT"][1]])
        self.FAULTS["2V"] = int(bf(SCA_status)[self.feb.feb_ctrl_and_status.sca.GPIO_direction["FAULT_2V"][1]])
        self.FAULTS["4V"] = int(bf(SCA_status)[self.feb.feb_ctrl_and_status.sca.GPIO_direction["FAULT_4V"][1]])                          
        print('\n')
        print(bcolors.BOLD+"CRC_ERROR_LEFT : {}".format(self.CRC_ERRORS["LEFT"])+ bcolors.ENDC)
        print(bcolors.BOLD+"CRC_ERROR_MIDDLE : {}".format(self.CRC_ERRORS["MIDDLE"])+ bcolors.ENDC)
        print(bcolors.BOLD+"CRC_ERROR_RIGHT : {}".format(self.CRC_ERRORS["RIGHT"])+ bcolors.ENDC)
        print("---")
        print(bcolors.BOLD+"Fault_2V : {}".format(self.FAULTS["2V"])+ bcolors.ENDC)
        print(bcolors.BOLD+"Fault_4V : {}".format(self.FAULTS["4V"])+ bcolors.ENDC)
        print('\n')
        ### CONTROL
        if self.FAULTS["2V"] == 0 or self.FAULTS["4V"] == 0 :
                self.error_detected["FAULTS"] += 1
                print("Restart on Fault Voltage : ")
                for name, value in self.FAULTS.items():
                    if value == 0:
                        print(bcolors.RED + name, " : voltage error detected" + bcolors.ENDC)
                self.restart_on_fault_Voltage()
        for name, value in self.CRC_ERRORS.items():
            if value == 1:  
                self.error_detected["CRC"] += 1
                print(bcolors.RED + "CRC error detected from FPGA : {} ".format(name)+ bcolors.ENDC)
                self.reconf_FPGA(name)
  
    def read_SCA_SEU_counter(self):
        print(bcolors.BLUE +"\n_____________________READ_SCA_SEU_COUNTER______________________\n"+ bcolors.ENDC)
        # SEU counter
        self.SEU_counter = int(self.feb.feb_ctrl_and_status.sca.get_SEU_counter())
        print(bcolors.BOLD + " ==> SEU counter value : ",self.SEU_counter, ""+ bcolors.ENDC)
        print('\n')

    def reset_SEU_counter(self):
        print("\n============================================================")
        print("----------------------RESET SEU_COUNTER---------------------")
        print("============================================================\n")
        self.feb.feb_ctrl_and_status.sca.reset_SEU_counter()

    def restart_on_fault_Voltage(self):
        ### restart 2V or 4V power supplies
        ### Power supplies restarted together
        print(bcolors.BLUE +"RESTART . . ."+ bcolors.ENDC)
        self.feb.feb_ctrl_and_status.sca.init()
        self.feb.feb_ctrl_and_status.deconfigure_FPGA()
        self.feb.feb_ctrl_and_status.power_off_FPGA()
        self.feb.feb_ctrl_and_status.shutdown_2v()
        self.feb.feb_ctrl_and_status.shutdown_4v()
        self.feb.feb_ctrl_and_status.power_up_2v()
        self.feb.feb_ctrl_and_status.power_up_4v()
        self.feb.feb_ctrl_and_status.power_on_FPGA()
        self.feb.feb_ctrl_and_status.configure_FPGA()
        self.feb.feb_ctrl_and_status.reset_FPGA()
        print("RESTART DONE"+ bcolors.ENDC)

    def reconf_FPGA(self, fpga_id):
        #restart a config via N_config sigs
        print(bcolors.BLUE +"FPGA RECONFIGURATION"+ bcolors.ENDC)
        if fpga_id =="LEFT": # fpga_0
            self.feb.feb_ctrl_and_status.sca.set_GPIO(self.feb.feb_ctrl_and_status.sca.GPIO_direction["NCONFIG_LEFT"][1],0)
            self.feb.feb_ctrl_and_status.sca.set_GPIO(self.feb.feb_ctrl_and_status.sca.GPIO_direction["SOFT_RESET_FPGA"][1],1)
            self.feb.feb_ctrl_and_status.sca.set_GPIO(self.feb.feb_ctrl_and_status.sca.GPIO_direction["NCONFIG_LEFT"][1],1)
            self.feb.feb_ctrl_and_status.sca.set_GPIO(self.feb.feb_ctrl_and_status.sca.GPIO_direction["SOFT_RESET_FPGA"][1],0)
            
        if fpga_id =="MIDDLE": # fpga_1
            self.feb.feb_ctrl_and_status.sca.set_GPIO(self.feb.feb_ctrl_and_status.sca.GPIO_direction["NCONFIG_MIDDLE"][1],0)
            self.feb.feb_ctrl_and_status.sca.set_GPIO(self.feb.feb_ctrl_and_status.sca.GPIO_direction["SOFT_RESET_FPGA"][1],1)
            self.feb.feb_ctrl_and_status.sca.set_GPIO(self.feb.feb_ctrl_and_status.sca.GPIO_direction["NCONFIG_MIDDLE"][1],1)
            self.feb.feb_ctrl_and_status.sca.set_GPIO(self.feb.feb_ctrl_and_status.sca.GPIO_direction["SOFT_RESET_FPGA"][1],0)

        if fpga_id =="RIGHT": # fpga_2
            self.feb.feb_ctrl_and_status.sca.set_GPIO(self.feb.feb_ctrl_and_status.sca.GPIO_direction["NCONFIG_RIGHT"][1],0)    
            self.feb.feb_ctrl_and_status.sca.set_GPIO(self.feb.feb_ctrl_and_status.sca.GPIO_direction["SOFT_RESET_FPGA"][1],1)
            self.feb.feb_ctrl_and_status.sca.set_GPIO(self.feb.feb_ctrl_and_status.sca.GPIO_direction["NCONFIG_RIGHT"][1],1)
            self.feb.feb_ctrl_and_status.sca.set_GPIO(self.feb.feb_ctrl_and_status.sca.GPIO_direction["SOFT_RESET_FPGA"][1],0)

        print(bcolors.BLUE +"RECONF DONE"+ bcolors.ENDC)
        
    def read_temperature(self):
        ## Temperature check
        print(bcolors.BLUE + "\n_________________________TEMPERATURES__________________________")
        temp_sensor= self.feb.feb_ctrl_and_status.get_temperature()
        self.temp_sensor_values={}
        for name, value in temp_sensor.items():
            print(name, value[0])
            self.temp_sensor_values[name]=[]
            self.temp_sensor_values[name].append(value[0])
        print("### Check temperatures ###")    
        self.feb.feb_ctrl_and_status.check_temperature()
        print("===============================================================\n" + bcolors.ENDC)
        
    def read_powersupplies(self):
        ## ADC powersuplies values
        print(bcolors.BLUE + "\n__________________Voltage/current ADC read_____________________")
        power_supplies = testbench.feb.feb_ctrl_and_status.get_powersupplies()
        self.power_supplies_val={}
        for name, value in power_supplies.items():
            print(name, value[0])
            self.power_supplies_val[name]=[]
            self.power_supplies_val[name].append(value[0])
        print("### Check Powersupplies ###")
        self.feb.feb_ctrl_and_status.check_powersupplies()
        print("===============================================================\n" + bcolors.ENDC)
        
    def gen_BC0(self, nb_period_25ns):
        # Generate BC0 periodic pulse signal at the desired period. (Frequency of 10kHz = 1/(4000 * 25 ns))
        self.fc7.gen_BC0(nb_period_25ns)

    def stop_BC0(self):
        # Stop BC0 signal
        self.fc7.set_enable_BC0(0)
 

##=================================================== ********************** ==================================================================##
##===================================================      Data Parsing      ==================================================================##
##=================================================== ********************** ==================================================================##
    # read TDC datas from raw file ==> format : {"fpga_0":[channel_tdc, data_counter], "fpga_1":[channel_tdc, data_counter], "fpga_2":[channel_tdc, data_counter]}
    def save_TDC_datas(self, TDC_data, ID):
        #Save TDC datas as row file
        # print(TDC_data)
        # print("_________________________________________________")
        # print(TDC_data[0])
        # print("_________________________________________________")
        # print(TDC_data[0][0]["fpga_0"])
        # print("_________________________________________________")
        fpga0_data = TDC_data[0][0]["fpga_0"]
        fpga1_data = TDC_data[0][0]["fpga_1"]
        fpga2_data = TDC_data[0][0]["fpga_2"]
        #write
        #file=open("/irradiation_run/TDCdatas.raw","a") 
        with open("irradiation_run/TDCdatas.raw", "a") as file:
            file.write("ID :"+ID+"\n")
            file.write("#fpga_0\n") 
            for x in fpga0_data:
                file.write((str(x[0]) + "," +str(x[1]) + "\n"))
            file.write("#fpga_1\n") 
            for x in fpga1_data:
                file.write((str(x[0]) + "," +str(x[1]) + "\n"))
            file.write("#fpga_2\n") 
            for x in fpga2_data:
                file.write((str(x[0]) + "," +str(x[1]) + "\n"))     
        file.close()

    def read_TDC_datas(file_name):
        # read TDC datas from raw file
        with open(file_name,'r') as f:
            mylist = f.read().splitlines()
            r=[]
            data = {}
            cpt = 0
            for d in mylist:
                r.append(d.split(","))
            for x in r:
                print(x[0][0])
                if x[0][0:2] == "ID":
                    pass
                elif x[0][0] =="#":
                    print("ok")
                    title = x[0][1:]
                    if cpt <3 : 
                        data[title] = [[],[]]
                        cpt +=1
                else:
                    data[title][0].append(int(x[0]))
                    data[title][1].append(int(x[1])) 
       
        return data

    # Default inputs of this testbench
    def write_inputs_val(self):
        # Read default inputs format for irradiation tests
        inputs = {"SC_process" : {"time_pr" : 10},
                  "SEU_process" : {"time_pr" : 30,
                                   "fpgas" :["fpga_0","fpga_2"],
                                   "asics" : {"0" :{"top" : 0, "bottom" :0},
                                              "1" :{"top" : 0, "bottom" :0},
                                              "2" :{"top" : 0, "bottom" :0},
                                              }
                                    },
                  "Scurve_process" : {"time_pr" : 300,
                                      "fpgas" :["fpga_0","fpga_2"],
                                      "asics" : {"0" :{"top" : 0, "bottom" :0},
                                                 "1" :{"top" : 0, "bottom" :0},
                                                 "2" :{"top" : 0, "bottom" :0},
                                                 }
                                        }
                  }           
        with open("irradiation_run/inputs.json", 'w') as outfile:
            json.dump(inputs, outfile)

    def read_inputs_val(self):
        with open("irradiation_run/inputs.json",'r') as file:
            data = json.load(file)
        return data

    # Read 6b/10b Dac from Equalized Datas from dac6bConfig
    def readDacDefaultConfig(self,fpga, asic):
        # Read dac values from dac6bConfig folder ==> First run Equalization.py to generate config files
        with open("dac6bConfig/dac6b_{}_{}.txt".format(fpga,asic),"r") as file:
            mylist = file.read().splitlines()
            value=[]
            for d in mylist:
                value.append((d.split("="))[1])
        return value

    def reset_runFolder(self):
        path = os.getcwd()
        ## deleting plot files files
        print(" => Deleting previous run . . .")
        if len(os.listdir(path + "/irradiation_run/plots/")) != 0:
            files = os.listdir(path + "/irradiation_run/plots/")
            for i in range(0, len(files)):
                os.remove(path + "/irradiation_run/plots/" + files[i])
        
        if len(os.listdir(path + "/irradiation_run/SweepData/")) != 0:
            files = os.listdir(path + "/irradiation_run/SweepData/")
            for i in range(0, len(files)):
                os.remove(path + "/irradiation_run/SweepData/" + files[i])

        ## deleting JSON files from the run
        if os.path.exists(path + "/irradiation_run/data_irradiation_sca.json"):
            os.remove(path + "/irradiation_run/data_irradiation_sca.json")
        if os.path.exists(path + "/irradiation_run/data_irradiation_petiROC.json"):
            os.remove(path + "/irradiation_run/data_irradiation_petiROC.json")
        if os.path.exists(path + "/irradiation_run/TDCdatas.raw"):
            os.remove(path + "/irradiation_run/TDCdatas.raw")
        time.sleep(1)


##=================================================== ********************** ==================================================================##
##=================================================== FEB Config and Control ==================================================================##
##=================================================== ********************** ==================================================================##
    def enable_petiroc_auto_reload(self, fpga_ref, asic_ref, period_ms): # enable periodic reconf with a specific period
        self.feb.asic.set_auto_reload(period_ms = period_ms ,fpga_ref = fpga_ref, asic_ref= asic_ref)
        print(bcolors.BLUE +"*** Enable petiROC auto reconfiguration *** \n period = {} ms \n".format(period_ms)+ bcolors.ENDC)   

    def disable_petiroc_auto_reconf(self, fpga_ref): # Disable all ASICS auto reconf. [TOP / BOT]
        self.feb.asic.dis_auto_reload(fpga_ref)
        print(bcolors.BLUE +" ==> Disable petiROC auto reconfiguration. "+ bcolors.ENDC)

    def sc_write(self,fpga, addr, data):
      WR=1
      self.fc7.downlink.send_SC_frame(fpga, WR, address=addr, data=data)

    def sc_read(self,fpga, addr, nWords, bVerbose=False):
      RD = 0
      if bVerbose:
        print("Read",nWords,"word from addr","0x"+hex(addr)[2:].rjust(4,'0'))
      self.fc7.downlink.send_SC_frame(fpga, RD, address=addr, data=nWords)
      ReceivedSC = self.fc7.uplink.receive_SC_frame()
      llSC=[]
      llSC.append(["0x"+hex(item)[2:].rjust(4,'0') for item in ReceivedSC['fpga_0']])
      llSC.append(["0x"+hex(item)[2:].rjust(4,'0') for item in ReceivedSC['fpga_1']])
      llSC.append(["0x"+hex(item)[2:].rjust(4,'0') for item in ReceivedSC['fpga_2']])
      if bVerbose:
        print("FPGA 0".rjust(15,' ')+'|'+"FPGA 1".rjust(16,' ')+'|'+"FPGA 2".rjust(16,' ')+'|')
        for i in range(max([len(item) for item in llSC])):
          for fpgaID in range(3):
            if i < len(llSC[fpgaID]):
              print(llSC[fpgaID][i].rjust(15,' ')+'|',end='')
            else:
              print(" "*15+'|',end='')
          print()
      return llSC

    def Dac10bConfig(self, nDacVal=400, lsFpga=["fpga_1"], lsAsic=["top"]):
        for fpgaID in lsFpga:
            for asicID in lsAsic:
                self.feb.asic.set_10b_dac(fpgaID, asicID, nDacVal)   

    def ApplyConfig(self, lsFpga=["fpga_0", "fpga_1", "fpga_2"]):
      for fpgaID in lsFpga:
        #Gen config registers
        ROC_top_config = self.feb.asic.asic[fpgaID][self.feb.asic.asic_top_index].gen_config()
        ROC_bot_config = self.feb.asic.asic[fpgaID][self.feb.asic.asic_bottom_index].gen_config()
        #Send config registers to FEB FPGAs
        if   fpgaID=="fpga_0": nTargetFPGA = LEFT
        elif fpgaID=="fpga_1": nTargetFPGA = MID
        elif fpgaID=="fpga_2": nTargetFPGA = RIGHT
        else: sys.exit("ERROR : UNKNOWN FPGA ID")
        self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_TOP_CONFIG"].address, ROC_top_config)
        self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_BOTTOM_CONFIG"].address, ROC_bot_config)
      nTargetFPGA=0
      if "fpga_0" in lsFpga: nTargetFPGA += LEFT
      if "fpga_1" in lsFpga: nTargetFPGA += MID
      if "fpga_2" in lsFpga: nTargetFPGA += RIGHT
      #PETIROC load cycle
      self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_TOP_LOAD"].address, [0]) #ROC TOP LOAD = 0
      self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_BOTTOM_LOAD"].address, [0]) #ROC BOT LOAD = 0
      self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_TOP_LOAD"].address, [1]) #ROC TOP LOAD = 1
      self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_BOTTOM_LOAD"].address, [1]) #ROC BOT LOAD = 1
      self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_TOP_LOAD"].address, [0]) #ROC TOP LOAD = 0
      self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_BOTTOM_LOAD"].address, [0]) #ROC BOT LOAD = 0

      time.sleep(0.2)

    def ApplyDefaultConfig(self, lsFpga=["fpga_0", "fpga_1", "fpga_2"], lsAsic=["top","bottom"]):
      ## SET ASIC REGISTER WITH DEFAULT VALUES
      for fpgaID in lsFpga:
        for asicID in lsAsic:
            ## DEFAULT : active all asics channels
            self.PetirocChannelEnable([fpgaID], [asicID], lbChannelEnable=[True]*16)
            ## Get asic default config from datafile
            value = self.readDacDefaultConfig(fpgaID,asicID)
            ### DEFAULT : set 6b/10b values after equalization
            for i in range(16):
                self.feb.asic.asic[fpgaID][asicID].set_6b_dac(i*2,int(value[i+1]))
            #configure middle DAC + 20    
            self.feb.asic.asic[fpgaID][asicID].set_10b_dac_T(int(value[0])+5)
            # Generate config register for a specific asic
            asic_config = self.feb.asic.asic[fpgaID][asicID].gen_config()

            # SEND ASIC REGISTER TO FEB FPGAs
            if   fpgaID=="fpga_0": nTargetFPGA = LEFT
            elif fpgaID=="fpga_1": nTargetFPGA = MID
            elif fpgaID=="fpga_2": nTargetFPGA = RIGHT
            else: sys.exit("ERROR : UNKNOWN FPGA ID")
            if asicID =="top":
                self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_TOP_CONFIG"].address, asic_config)
            if asicID =="bottom":
                self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_BOTTOM_CONFIG"].address, asic_config)        
      #PETIROCs load cycle      
      nTargetFPGA=0
      if "fpga_0" in lsFpga: nTargetFPGA += LEFT
      if "fpga_1" in lsFpga: nTargetFPGA += MID
      if "fpga_2" in lsFpga: nTargetFPGA += RIGHT
      self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_TOP_LOAD"].address, [0]) #ROC TOP LOAD = 0
      self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_BOTTOM_LOAD"].address, [0]) #ROC BOT LOAD = 0
      self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_TOP_LOAD"].address, [1]) #ROC TOP LOAD = 1
      self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_BOTTOM_LOAD"].address, [1]) #ROC BOT LOAD = 1
      self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_TOP_LOAD"].address, [0]) #ROC TOP LOAD = 0
      self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_BOTTOM_LOAD"].address, [0]) #ROC BOT LOAD = 0
      time.sleep(0.2)

    def EnableTDC(self, lsfpga = ["fpga_0","fpga_1","fpga_2"], ls_tdc_channel = [x for x in range(34)]):
        data=0
        l=[]
        for i in ls_tdc_channel:
            data  = data + (1 << i)
        data = bf(data)

        l.append(data[0:16])
        l.append(data[16:32])
        l.append(data[32:34])
        nTargetFPGA=0
        if "fpga_0" in lsfpga: nTargetFPGA += 1
        if "fpga_1" in lsfpga: nTargetFPGA += 2
        if "fpga_2" in lsfpga: nTargetFPGA += 4
        self.sc_write(nTargetFPGA,self.fpga_register["TDC_ENABLE"].address, data=[1]) #TDC module enable
        self.sc_write(nTargetFPGA,self.fpga_register["TDC_INJECTION_MODE"].address, data=[0]) #standard mode
        self.sc_write(nTargetFPGA,self.fpga_register["TDC_ENABLE_CH0_15"].address, data=[data[0:16],data[16:32],data[32:34]]) #meas enable for All TDC channels only => 305/306/307
        self.sc_write(nTargetFPGA,self.fpga_register["TDC_CMD_VALID"].address, data=[0]) #CMD valid = 0
        self.sc_write(nTargetFPGA,self.fpga_register["TDC_CMD_VALID"].address, data=[1]) #CMD valid = 1
        self.sc_write(nTargetFPGA,self.fpga_register["TDC_CMD_VALID"].address, data=[0]) #CMD valid = 0
        time.sleep(0.005)

    def DisableTDC(self, fpga="all"):
        if fpga == "fpga_2":
            self.sc_write(RIGHT,self.fpga_register["TDC_ENABLE"].address, data=[0]) #TDC module disable
        if fpga == "fpga_1":
            self.sc_write(MID,self.fpga_register["TDC_ENABLE"].address, data=[0]) #TDC module disable
        if fpga == "fpga_0":
            self.sc_write(LEFT,self.fpga_register["TDC_ENABLE"].address, data=[0]) #TDC module disable
        if fpga == "all":
            self.sc_write(LEFT+MID+RIGHT,self.fpga_register["TDC_ENABLE"].address, data=[0]) #TDC module disable

    def LaunchDataCounter(self,ntarget = 7 ,nCycles=0x00100000):
        self.sc_write(ntarget, addr=0x030A, data=[nCycles&0xFFFF, nCycles>>16]) #Data counter time window size= 0x00100000 = 10.486ms
        self.sc_write(ntarget, addr=0x030C, data=[0]) #Ensure data counter disabled
        self.sc_write(ntarget, addr=0x030C, data=[1]) #Data counter start
        self.sc_write(ntarget, addr=0x030C, data=[0]) #Ensure data counter disabled

    def PetirocChannelEnable(self,lsFpga=["fpga_1"], lsAsic=["top"], lbChannelEnable=[True]*16):
        lnChannel=[]
        for i in range(16):
            if lbChannelEnable[i]:
              lnChannel.append(i*2)
        for fpgaID in lsFpga:
            for asicID in lsAsic:
              self.feb.asic.enable_channel(fpgaID, asicID, lnChannel)

    def ReadDataCounter(self,bVerbose=False):
        if bVerbose:
            self.sc_read(LEFT+MID+RIGHT, addr=0x0316, nWords=1,bVerbose=True)    #Check data counter valid (timer has reach the required value)
        llRaw=self.sc_read(LEFT+MID+RIGHT, addr=0x0317, nWords=34*2, bVerbose=bVerbose) #Read data counter for BC0 and Resync channels
        llCounter=[]
        for fpgaID in range(len(llRaw)):
            llCounter.append([])
            for i in range(0, len(llRaw[fpgaID]),2):
                llCounter[-1].append((int(llRaw[fpgaID][i+1],16)<<16)+int(llRaw[fpgaID][i],16))
        return llCounter
  
    def CountROCData(self,fTime=0.100, bVerbose=False, injected_strip=""):
        """ Use the FEB FPGAs internal counters to count TDC data for each channel during fTime (counting time in seconds) """
        
        ## remove fpga channel counters if injection on a strip 
        if injected_strip !="":
            AllFpgas = ["fpga_0", "fpga_1", "fpga_2"]
            fpga = self.lookuptable_strips[injected_strip][0]
            fpgas = []
            AllFpgas.remove(fpga)
            fpgas = AllFpgas
            tdc_ch = [x for x in range(33)]
            tdc_sel = self.lookuptable_strips[injected_strip][3]
            tdc_ch[tdc_sel] == 0
            self.EnableTDC(lsfpga = fpgas,ls_tdc_channel =tdc_ch)
        else:
            self.EnableTDC()

        self.LaunchDataCounter(nCycles=int(fTime*(10**8))) # nCycles*10ns
        time.sleep(fTime) #Must wait at least the required time
        self.DisableTDC()
        llCounter=self.ReadDataCounter(bVerbose=bVerbose)
        return llCounter

    def sweepDac10b(self, injected_strip = "", lDac10bVal=range(350,480,1), lsFpga=["fpga_1"], lsAsic=["top"], bPlot=False, bSaveData=False, sOutFileName=""):        
        #Structure which memorize an unique runID and the figID for plot saving
        FIG_ID=[int(time.time()*1000),0]
        DAT_ID=[int(time.time()*1000),0]
        """ This function perform a sweep of the 10bDac value and return the data counter values """
        print("---- Dac10b sweep : ",len(lDac10bVal),"values between", min(lDac10bVal),"and", max(lDac10bVal),"----")
        lllCounter=[]

        for dac_10b in lDac10bVal:
            #Overload DAC10bits config
            self.Dac10bConfig(dac_10b, lsFpga, lsAsic)
            self.ApplyConfig(lsFpga=lsFpga)

            lllCounter.append(self.CountROCData(injected_strip = injected_strip))

        if bSaveData:
            if sOutFileName=="":
                sOutFileName=str(DAT_ID[0])+"_"+str(DAT_ID[1])
            with open("irradiation_run/SweepData/"+sOutFileName+".csv","w") as outFile:
                outFile.write(",".join(map(str,lDac10bVal))+"\n")
                for fpgaID in range(3):
                    for channelID in range(34):
                        lCurrentChannelData=[lllCounter[configIndex][fpgaID][channelID] for configIndex in range(len(lDac10bVal))]
                        outFile.write(",".join(map(str,lCurrentChannelData))+"\n")      
            DAT_ID[1]+=1 #Increment the datNumber            

        # if bPlot:
        #     plt.figure(figsize=[12.8,9.6])
        #     for fpgaID in range(3):
        #         for channelID in range(34):
        #             lCurrentChannelData = [lllCounter[configIndex][fpgaID][channelID] for configIndex in range(len(lDac10bVal))]
        #         #Only non-null channels are added to the plot
        #         if max(lCurrentChannelData)!=0:
        #             plt.plot(lDac10bVal, [lCurrentChannelData], label="FPGA"+str(fpgaID)+" Chan"+str(channelID) +"asic "+ str(lsAsic))
        #     plt.legend(loc="upper right")
        #     plt.ylim(-1,1001)
        #     if sOutFileName =="":
        #         sOutFileName=sOutFileName
        #     plt.savefig("irradiation_run/plots/"+sOutFileName)
        #     plt.show()
        return lllCounter

    def load_feb_config(self,lsfpga, asic = "all"):
        nTargetFPGA=0
        if "fpga_0" in lsfpga: nTargetFPGA += 1
        if "fpga_1" in lsfpga: nTargetFPGA += 2
        if "fpga_2" in lsfpga: nTargetFPGA += 4

        if asic =="top":
            self.sc_write(nTargetFPGA,self.fpga_register["PETIROC_TOP_LOAD"].address,[1]) #top
            self.sc_write(nTargetFPGA,self.fpga_register["PETIROC_TOP_LOAD"].address,[0]) #top
        if asic == "bottom":
            self.sc_write(nTargetFPGA,self.fpga_register["PETIROC_BOTTOM_LOAD"].address,[1]) #bottom
            self.sc_write(nTargetFPGA,self.fpga_register["PETIROC_BOTTOM_LOAD"].address,[0]) #bottom
        if asic =="all":
            self.sc_write(nTargetFPGA,self.fpga_register["PETIROC_TOP_LOAD"].address,[1]) #top
            self.sc_write(nTargetFPGA,self.fpga_register["PETIROC_TOP_LOAD"].address,[0]) #top
            self.sc_write(nTargetFPGA,self.fpga_register["PETIROC_BOTTOM_LOAD"].address,[1]) #bottom
            self.sc_write(nTargetFPGA,self.fpga_register["PETIROC_BOTTOM_LOAD"].address,[0]) #bottom

    def reset_bitflip_counter(self,lsfpga, asic="all"):
        nTargetFPGA=0
        if "fpga_0" in lsfpga: nTargetFPGA += 1
        if "fpga_1" in lsfpga: nTargetFPGA += 2
        if "fpga_2" in lsfpga: nTargetFPGA += 4

        if asic == "top":
            self.sc_write(nTargetFPGA,self.fpga_register["BITFLIP_COUNTER_RESET_TOP"].address,[1])
            self.sc_write(nTargetFPGA,self.fpga_register["BITFLIP_COUNTER_RESET_TOP"].address,[0])
        if asic =="bottom":
            self.sc_write(nTargetFPGA,self.fpga_register["BITFLIP_COUNTER_RESET_BOT"].address,[1])
            self.sc_write(nTargetFPGA,self.fpga_register["BITFLIP_COUNTER_RESET_BOT"].address,[0])
        if asic =="all":
            self.sc_write(nTargetFPGA,self.fpga_register["BITFLIP_COUNTER_RESET_TOP"].address,[1])
            self.sc_write(nTargetFPGA,self.fpga_register["BITFLIP_COUNTER_RESET_TOP"].address,[0]) 
            self.sc_write(nTargetFPGA,self.fpga_register["BITFLIP_COUNTER_RESET_BOT"].address,[1])
            self.sc_write(nTargetFPGA,self.fpga_register["BITFLIP_COUNTER_RESET_BOT"].address,[0])

    def EnableAutoResetFSM(self):
        self.sc_write(LEFT+MID+RIGHT, self.fpga_register["PETIROC_TOP_NOR32RAZ_FSM"].address, [1, int((100-15)/5), 1, 3])
        self.sc_write(LEFT+MID+RIGHT, self.fpga_register["PETIROC_BOTTOM_NOR32RAZ_FSM"].address, [1, int((100-15)/5), 1, 3])

    def DisableAutoResetFSM(self):
        self.sc_write(LEFT+MID+RIGHT, self.fpga_register["PETIROC_TOP_NOR32RAZ_FSM"].address, [0])
        self.sc_write(LEFT+MID+RIGHT, self.fpga_register["PETIROC_BOTTOM_NOR32RAZ_FSM"].address, [0])
    
    def LatchingConfig(self,sConfig="asic"):
        #Enable/DisableFpga latching
        nFPGAConf= 3 if sConfig in ["fpga"] else 2 #FPGA Nor32 is always used
        self.sc_write(LEFT+MID+RIGHT, 0x0108, [nFPGAConf])
        self.sc_write(LEFT+MID+RIGHT, 0x0208, [nFPGAConf])
        #Enable/Disable asic latching
        lsAllFpgas=["fpga_0", "fpga_1", "fpga_2"]
        lsAllAsics=["top", "bottom"]
        for fpgaID in lsAllFpgas:
            for asicID in lsAllAsics:
                if sConfig in ["fpga", "no"]:
                    self.feb.asic.disableLatch(fpgaID, asicID)
                else:
                    self.feb.asic.enableLatch(fpgaID, asicID)
    
    def DisableAllChannels(self, lsFpga=["fpga_0", "fpga_1", "fpga_2"], lsAsic=["top","bottom"]):
        for fpgaID in lsFpga:
            for asicID in lsAsic:
                self.feb.asic.disable_channel(fpgaID, asicID, range(32))

    def Dac6bConfig(self,lnDacVal=[32]*16, lsFpga=["fpga_1"], lsAsic=["top"]):
        for fpgaID in lsFpga:
            for asicID in lsAsic:
                for i in range(16):
                    self.feb.asic.set_6b_dac(fpgaID,asicID, i*2, lnDacVal[i])
                    self.feb.asic.set_6b_dac(fpgaID,asicID, i*2+1, 0) #Unused (odd) channels

    def read_bitflip_counter(self,lsfpga, asic= "all"):
        # Send request to fc7
        # result ={"0" : {"top" : 0, "bot" :0},  # 0 => fpga_0 / 1=> fpga_1 / 2=> fpga_2
        #          "1" : {"top" : 0, "bot" :0},
        #          "2" : {"top" : 0, "bot" :0}}
        result={}
        nTargetFPGA=0
        if "fpga_0" in lsfpga: nTargetFPGA += 1
        if "fpga_1" in lsfpga: nTargetFPGA += 2
        if "fpga_2" in lsfpga: nTargetFPGA += 4

        if asic =="top":
            result1 = self.sc_read(nTargetFPGA,self.fpga_register["BITFLIP_COUNTER_TOP_1"].address,1, False) #top
            result2 = self.sc_read(nTargetFPGA,self.fpga_register["BITFLIP_COUNTER_TOP_2"].address,1,False)
            for i in range(0,3):
                result[i] =[]
                if result1[i] != [] or result2[i]!= []:
                    result[i].append({"top" : ((int((result2[i][0] ),16) << 16 )+  int(result1[i][0],16))})
                    print(result)
            return result

        if asic =="bottom":
            result1 = self.sc_read(nTargetFPGA,self.fpga_register["BITFLIP_COUNTER_BOT_1"].address,1, False) #bottom
            result2 = self.sc_read(nTargetFPGA,self.fpga_register["BITFLIP_COUNTER_BOT_2"].address,1, False)
            for i in range(0,3):
                result[i] =[]
                if result1[i] != [] or result2[i]!= []:
                    result[i].append({"bot" : ((int((result2[i][0] ),16) << 16 )+  int(result1[i][0],16))})
                    print(result)
            return result

        if asic =="all":
            result1_t = self.sc_read(nTargetFPGA,self.fpga_register["BITFLIP_COUNTER_TOP_1"].address,1, False) #top
            result2_t = self.sc_read(nTargetFPGA,self.fpga_register["BITFLIP_COUNTER_TOP_2"].address,1, False)
            result1_b = self.sc_read(nTargetFPGA,self.fpga_register["BITFLIP_COUNTER_BOT_1"].address,1, False) #bottom
            result2_b = self.sc_read(nTargetFPGA,self.fpga_register["BITFLIP_COUNTER_BOT_2"].address,1,False)
            for i in range(0,3):
                result[i] =[]
                if (result1_t[i] != [] or result2_t[i]!= []) and (result1_b[i] != [] or result2_b[i]!= []):
                    result[i].append({"top" : ((int(result2_t[i][0] ,16) << 16 )+  int(result1_t[i][0],16)),
                                    "bot" : ((int(result2_b[i][0] ,16) << 16 )+  int(result1_b[i][0],16))})
                # if result1_t[i] != [] or result2_t[i]!= []:
                #     result[i].append({"top" : ((int(result2_t[i][0] ,16) << 16 )+  int(result1_t[i][0],16))})
                # if result1_b[i] != [] or result2_b[i]!= []:
                #     result[i].append({"bot" : ((int(result2_b[i][0] ,16) << 16 )+  int(result1_b[i][0],16))})
            return result

##=================================================== ********************** ==================================================================##
##=================================================== ,HIGH LEVEL PROCESSES  ==================================================================##
##=================================================== ********************** ==================================================================##
    def SlowControl_process(self,loop_counter_sca):
        print("start time of SC GPIOS / ADC : {} \n".format(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))))
        ## lecture des GPIOs inputs du SCA
        ## decode SCA inputs in one SlowControl request and ctrl reconfig or restart
        testbench.decode_ctrl_SCA_status()
        ## Read SCA SEU counter
        testbench.read_SCA_SEU_counter()
        ## FEB Temperature check
        testbench.read_temperature()
        ## FEB Power_supplies
        testbench.read_powersupplies()
        ## time sca process update and id (loop_counter) of SCA process
        time_sca = time.time()
        date_sca = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time_sca))
        ## Data saved as JSON
        data_sca[loop_counter_sca]=[]
        data_sca[loop_counter_sca].append({
                                     "time" : date_sca,
                                     "CRC_errors" : testbench.CRC_ERRORS,
                                     "FAULTs": testbench.FAULTS,
                                     "SCA_SEU": testbench.SEU_counter,
                                     "FAULTS-err_cnt" : testbench.error_detected["FAULTS"], # count the number of cumulated Faults voltage from the beginning of the test
                                     "CRC_err_cnt" : testbench.error_detected["CRC"],       # count the number of cumulated CRC errors from the beginning of the test
                                     "Temperatures": testbench.temp_sensor_values,
                                     "Power_supplies":testbench.power_supplies_val })
            # Write datas to JSON file
        with open('irradiation_run/data_irradiation_sca.json','w') as outfile:
            json.dump(data_sca,outfile)

    def SEU_process(self, loop_counter_pr, lsFpga):

        print("start time of SEU count : {}".format(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))))

        print("Read bitflip counter . . . \n")
        bitflip_count = testbench.read_bitflip_counter(lsfpga=lsFpga, asic="all")

        ## Load configuration 1st time to set default 
        print("\n ==> loading configuration . . . \n")
        #testbench.load_feb_config(lsFpga, asicID)
        testbench.ApplyDefaultConfig()

        #Reset SEU counter
        print("reset bitflip counter . . . ")
        testbench.reset_bitflip_counter(lsfpga=lsFpga, asic="all")

        ## Load configuration 2nd time to check bitflip
        print("\n ==> loading configuration . . . \n")
        testbench.ApplyDefaultConfig()
        #testbench.load_feb_config(lsFpga, asicID)


        print("Read bitflip counter . . . \n")
        bitflip_count = testbench.read_bitflip_counter(lsfpga=lsFpga, asic="all") # TOP : [fpga_0_1, fpga_1_1, fpga_2_1] [fpga_0_1, fpga_1_1, fpga_2_1]                
        print("BITFLIP COUNTERs = {}".format(bitflip_count))

        ## time pr process update and id (loop_counter) of PR process
        time_pr = time.time()
        date_pr = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time_pr))
        

        ## Datas saved as JSON
        data_pr[loop_counter_pr]=[]
        data_pr[loop_counter_pr].append({"time_pr": date_pr,
                                         "fpga_sel": lsFpga, 
                                         "bitflip_count": bitflip_count})
            # Write datas to JSON file
        with open('irradiation_run/data_irradiation_petiROC.json', 'w') as outfile:
            json.dump(data_pr, outfile)

    def Scurve_process(self, lsFpga,lsAsic):
        #==========================================#
        print("start time of Scurve : {} \n".format(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))))
        #################################
        testbench.DisableAllChannels()

        for fpgaID in lsFpga:
            for asicID in lsAsic:
                testbench.DisableAllChannels()
                print("Start sweep 10bits DAC with All channels activated on : {} asics : {} \n".format(fpgaID,asicID))
                # Active all asic channels
                testbench.PetirocChannelEnable(lsFpga=[fpgaID], lsAsic=[asicID], lbChannelEnable=[True]*16)
                testbench.sweepDac10b(lDac10bVal=range(350,480,1), lsFpga=[fpgaID], lsAsic=[asicID], bPlot=False, bSaveData=True, sOutFileName="{}_{}_{}".format(fpgaID,asicID, time.strftime("%Y-%m-%d_%H:%M:%S", time.localtime(time.time()))))
        
        testbench.DisableAllChannels()

        ## With INJECTION CHANNEL
        # print("Start sweep 10bits DAC with One channels activated on strip 14 : {} asics : {}, asic channel : 4 \n".format("fpga_0","top"))
        # # A sweep with strip 14 activated on fpga 0
        # testbench.PetirocChannelEnable(lsFpga=["fpga_0"], lsAsic=["top"], lbChannelEnable=[0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
        # testbench.sweepDac10b(lDac10bVal=range(350,480,1), lsFpga=["fpga_0"], lsAsic=["top"], bPlot=False, bSaveData=True, sOutFileName="{}_{}_inject-asicCH_{}_{}".format("fpga_0","top","4", time.strftime("%Y-%m-%d_%H:%M:%S", time.localtime(time.time()))))
        # testbench.DisableAllChannels()
        ##################################

    def stop_acquisition(self):
        #===== Acquisition stop and save datas =====#
        #===========================================#
        # Internal trigger set              
        testbench.fc7.set_internal_trigger(1)
        testbench.fc7.set_internal_trigger(0)
        print("*** TDC ACQUISITION STOPPED *** \n")
        # Save datas
        data = testbench.fc7.uplink.receive_TDC_frame()
        if data :
            testbench.save_TDC_datas(data,time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time())))
        # stop
        testbench.fc7.stop_acquisition()
        testbench.DisableTDC()
        #==========================================#

##=================================================== MAIN PROCESS ==================================================================##

if __name__ == '__main__':

    # Testbench initialisation => FC7.init() and feb.init()
    testbench = testbench_irradiation(False)

    ##=== *ARGs PARSER* ===##
    # Add -configured if the setup is already initialized.
    # ==> $ python3 testbench_irradiation.py -configured
    parser = argparse.ArgumentParser()
    parser.add_argument("-configured", "--configured", help="initialize setup",action="store_true")
    args = parser.parse_args()

    ## ARGs
    configured = args.configured

    #Constant definitions
    MID   = int("010",2) #2
    LEFT  = int("001",2) #1
    RIGHT = int("100",2) #4
    lsAllFpgas=["fpga_0", "fpga_1", "fpga_2"]
    lsAllAsics=["top", "bottom"]

    # VARs / datas
    loop_counter = 0
    loop_counter_sca = 0
    loop_counter_seu = 0
    data_sca = {}
    data_pr = {}
    testbench.error_detected = {"FAULTS" :0,"CRC" :0}

    #init times for loop pocesses
    time_sca = time.time()  
    time_seu = time.time()   
    time_scurve = time.time()  

    print(testbench.lookuptable_strips)
    print(testbench.GPIO_direction)
"""
    ## Configuration of setup
    if configured == False:
    ## INIT testbench
        print("*** SETUP INITIALISATION . . . ***")
        testbench.init()
        print("***INITIALISATION DONE ***\n")
        time.sleep(0.5)
    else:
        pass
    # Clear FIFOs
    testbench.fc7.fifos_clear()
     #testbench.feb.reset()
    # Init asic and tdcs
    testbench.feb.asic.init(False)
    testbench.feb.tdc.init(False)

    testbench.reset_runFolder()

#########################################################################
##                      INPUTS OF THE RUN                              ##
#########################################################################
    # read JSON input file
    inputs = testbench.read_inputs_val()

    # FPGAs selected by Process
    fpga_SEU_process = inputs["SEU_process"]["fpgas"]
    fpga_Scurve_process =inputs["Scurve_process"]["fpgas"]

    ## Time processes in sec.
    SCA_process_time = inputs["SC_process"]["time_pr"]
    SEU_process_time = inputs["SEU_process"]["time_pr"]
    Scurve_process_time = inputs["Scurve_process"]["time_pr"]

#########################################################################

    ## Asic default configuration from file ==> 6b and 10b DAC Values [other registers values are fixed by dictionnary in feb_asic.py file]
    testbench.ApplyDefaultConfig(lsFpga = lsAllFpgas, lsAsic = lsAllAsics)
    ## Enable FSM of Asics
    testbench.EnableAutoResetFSM()
    ## reset bitslip counter
    testbench.reset_bitflip_counter(lsfpga=lsAllFpgas, asic="all")

    ## reset acquisition ctrl
    testbench.fc7.stop_acquisition()
    testbench.fc7.fifos_clear()

    # Config acq. parameters
    testbench.fc7.set_acquisition_mode("Circular_Buffer_Internal")
    testbench.fc7.set_circular_buffer_size(100)


    ###################*******************#################
    #****************** MAIN LOOP PROCESS ****************#
    ###################*******************#################
    try:
        print("\n***********************************")
        print("\n***** START IRRADIATION TEST ******\n")
        print("***********************************\n")
        while True :
            #=====#
            if testbench.fc7.acquisition_state() == "idle":
                print("\n Apply asic default configuration for data acquisition of selected FPGAs : {} \n".format(lsAllFpgas))
                testbench.ApplyDefaultConfig(lsFpga = lsAllFpgas, lsAsic = lsAllAsics)
                #print("asic ch : {}".format(testbench.feb.asic.get_enable_channel("fpga_1","top")))
                # Ensure fifos are flushed
                testbench.fc7.stop_acquisition()
                testbench.fc7.fifos_clear()
                # Enable TDCs
                testbench.EnableTDC()
                # Start acquisition
                testbench.fc7.start_acquisition()
                print("\n*** TDC ACQUISITION START *** \n")
                time.sleep(1)
            #=====#


            #***********************************************************************************#
            #***************************** SCA GPIOs read process ******************************#
            #***********************************************************************************#
            
            if (time.time() - time_sca) > SCA_process_time:
                print(bcolors.CYAN + "\n************************************* SCA Read and control ****************************************"+ bcolors.ENDC)
                print(bcolors.CYAN + "************************************* => LOOP : {} \n".format(loop_counter_sca)+ bcolors.ENDC)
                

                testbench.SlowControl_process(loop_counter_sca)
                loop_counter_sca += 1

            
            #***********************************************************************************#
            #************************** PetiROC load config process ****************************#
            #***********************************************************************************#

            if (time.time() - time_seu) > SEU_process_time:
                print(bcolors.CYAN + "\n********************************************* PetiROC BitFlip read counter ****************************************************" + bcolors.ENDC)
                print(bcolors.CYAN + "************************************* => LOOP : {}, FPGAs : {}\n".format(loop_counter_seu,fpga_SEU_process)+ bcolors.ENDC)

                # Stop and save datas before process
                testbench.stop_acquisition()
                ## *** SEU PROCESS *** ##
                testbench.SEU_process(loop_counter_seu,fpga_SEU_process)
                loop_counter_seu += 1
                

            #*************************************************************************************#
            #************************** Pedestal equalization process ****************************#
            #*************************************************************************************#

            if (time.time() - time_scurve) > Scurve_process_time:
                print(bcolors.CYAN + "\n********************************************* PetiROC Equalization ****************************************************" + bcolors.ENDC)
                print(bcolors.CYAN + "************************************* => FPGA : {} \n".format(fpga_Scurve_process)+ bcolors.ENDC)
                
                # Stop and save datas before process
                testbench.stop_acquisition()

                ## *** SCURVE PROCESS *** ##
                testbench.Scurve_process(fpga_Scurve_process,lsAllAsics)
            
                #===== Reset asics states after Scurve =====#
                #===========================================#
                # bitflip counter reset
                testbench.reset_bitflip_counter(lsfpga=lsAllFpgas, asic=lsAllAsics)

                # reset default asic configuration for acquisition
                print("Apply asic default configuration after Pedestal for FPGAs: {} \n".format(lsAllFpgas))
                testbench.ApplyDefaultConfig(lsFpga = lsAllFpgas)
                #===========================================#

            #*** loop frequency ***#
            time.sleep(0.5)        #
            loop_counter +=1       #
            #**********************#

            # Stop and save datas before process
            testbench.stop_acquisition()


    ############################
    ## EXIT MAIN LOOP PROCESS ##
    ############################

    except KeyboardInterrupt:

        # Stop and save datas before process
        testbench.stop_acquisition()
        ##*********************##,
        ## DATAs saving
        ##*********************##
        print("*** Interruption detected ***")
        print(" => Please wait until process is finished . . . ")
        print(" => Datas are saved in IRRADIATION_backup_runs/ folder\n")
        path = os.getcwd()

        ## Copy the run in IRRADIATION_backup_runs folder
        shutil.copytree(path + "/irradiation_run/", path + "/IRRADIATION_backup_runs/{}".format(datetime.now().strftime("%d_%b_%Hh_%Mm")))

        ## deleting plot files files
        print(" => Deleting previous run . . .")
        if len(os.listdir(path + "/irradiation_run/plots/")) != 0:
            files = os.listdir(path + "/irradiation_run/plots/")
            for i in range(0, len(files)):
                os.remove(path + "/irradiation_run/plots/" + files[i])
        
        if len(os.listdir(path + "/irradiation_run/SweepData/")) != 0:
            files = os.listdir(path + "/irradiation_run/SweepData/")
            for i in range(0, len(files)):
                os.remove(path + "/irradiation_run/SweepData/" + files[i])

        ## deleting JSON files from the run
        if os.path.exists(path + "/irradiation_run/data_irradiation_sca.json"):
            os.remove(path + "/irradiation_run/data_irradiation_sca.json")
        if os.path.exists(path + "/irradiation_run/data_irradiation_petiROC.json"):
            os.remove(path + "/irradiation_run/data_irradiation_petiROC.json")
        if os.path.exists(path + "/irradiation_run/TDCdatas.raw"):
            os.remove(path + "/irradiation_run/TDCdatas.raw")
        time.sleep(1)

        print("\n*** QUIT Program **" + bcolors.ENDC)

        
"""