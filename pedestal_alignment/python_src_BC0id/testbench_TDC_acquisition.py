# -*- coding: utf-8 -*-
"""
Created on Wed Feb 24 10:04:18 2021

@author: luciol
"""


import uhal
import time
import sys
import os
import types
import argparse
from fc7 import *
from tools_box import *
from feb_v2_cycloneV_r1 import *


## Commande à lancer pour recuperer les données TDC du FPGA_1 sur le channel 0 en mode Time_Window
## python3 testbench_TDC_acquisition.py -timewindow -fpga fpga_1 -asic top -channel 0

## Commande à lancer pour recuperer les données TDC du FPGA_1 sur tous les channels TDC en mode Circular_buffer_external_trigger
## python3 testbench_TDC_acquisition.py -circularbuf_size 200 -fpga fpga_1 -asic top -channel all

class testbench_TDC_acquisition:

    def __init__(self,fc7_verbose,feb_verbose):
        self.fc7 = fc7(fc7_verbose)
        self.feb = feb_v2_cycloneV_r1(self.fc7,feb_verbose)

        self.fpga_register = {
                "GENERAL"                        : register(0x0000,"rw",16,1,0x0000),
                ### TOP ###
                #-------------------------------------------------------------------#
                "PETIROC_TOP_LOAD"               : register(0x0100,"rw",16,1,0x0000),# bit 0 : load, bit 1 : auto_reload ("auto_reload_periodic config : 0x11")
                "PETIROC_TOP_SR_RST"             : register(0x0101,"rw",16,1,0x0000),
                "PETIROC_TOP_RSTB"               : register(0x0102,"rw",16,1,0x0000),
                "TRIGGER_EXT_PIN_TOP"            : register(0x0103,"rw",16,1,0x0000),# bit 0   
                "PETIROC_TOP_NOR32RAZ_FSM"       : register(0x0104,"rw",16,1,0x0000),
                # FSM stage time
                "FSM_RAZ_stage1_top"             : register(0x0105,"rw",16,1,0x0000),
                "FSM_RAZ_stage2_top"             : register(0x0106,"rw",16,1,0x0000),
                "FSM_RAZ_stage3_top"             : register(0x0107,"rw",16,1,0x0000),
                "FPGA_FEATURES_TOP"              : register(0x0108,"rw",16,1,0x0000), # bit 0 ==> LATCHING sorties PetiROC vers FPGA | bit 1 ==> use FPGA NOR32 (utilise nor32 généré par les entrées du fpga [sequencage reset]) [DEFAULT_VALUE = 0x"10"]
                "BITFLIP_COUNTER_RESET_TOP"      : register(0x0109,"rw",16,1,0x0000), # bit 0
                # 1 / 10 ns period
                "PETIROC_RELOADING_PERIOD_TOP_1" : register(0x010A,"rw",16,1,0x0000), # LSB 
                "PETIROC_RELOADING_PERIOD_TOP_2" : register(0x010B,"rw",16,1,0x0000), # 
                "PETIROC_RELOADING_PERIOD_TOP_3" : register(0x010C,"rw",16,1,0x0000), # MSB
                "PETIROC_TOP_CONFIG"             : register(0x0117,"rw",16,41,[0x0000]),
                # Bitflip counter (irradiation tests)
                "BITFLIP_COUNTER_TOP_1"          : register(0x0140,"ro",16,1,[0x0000]), #LSB
                "BITFLIP_COUNTER_TOP_2"          : register(0x0141,"ro",16,1,[0x0000]), #MSB
                
                "PETIROC_TOP_CONFIG_PREVIOUS"    : register(0x0164,"ro",16,41,[0x0000]),
                #-------------------------------------------------------------------#
                ### BOTTOM ###
                #-------------------------------------------------------------------#
                "PETIROC_BOTTOM_LOAD"            : register(0x0200,"rw",16,1,0x0000),# bit 0 : load, bit 1 : auto_reload ("auto_reload_periodic config : 0x11") 
                "PETIROC_BOTTOM_SR_RST"          : register(0x0201,"rw",16,1,0x0000),
                "PETIROC_BOTTOM_RSTB"            : register(0x0202,"rw",16,1,0x0000),
                "TRIGGER_EXT_PIN_BOT"            : register(0x0203,"rw",16,1,0x0000),# bit 0  
                "PETIROC_BOTTOM_NOR32RAZ_FSM"    : register(0x0204,"rw",16,1,0x0000),
                ### FSM stage ###
                "FSM_RAZ_stage1_bot"             : register(0x0205,"rw",16,1,0x0000),
                "FSM_RAZ_stage2_bot"             : register(0x0206,"rw",16,1,0x0000),
                "FSM_RAZ_stage3_bot"             : register(0x0207,"rw",16,1,0x0000),
                "FPGA_FEATURES_BOT"              : register(0x0208,"rw",16,1,0x0000), # bit 0 ==> LATCHING sorties PetiROC vers FPGA | bit 1 ==> use FPGA NOR32 (utilise nor32 généré par les entrées du fpga [sequencage reset]) [DEFAULT_VALUE = 0x"10"]
                "BITFLIP_COUNTER_RESET_BOT"      : register(0x0209,"rw",16,1,0x0000), # bit 0
                # 1 / 10 ns period
                "PETIROC_RELOADING_PERIOD_BOT_1" : register(0x020A,"rw",16,1,0x0000), # LSB
                "PETIROC_RELOADING_PERIOD_BOT_2" : register(0x020B,"rw",16,1,0x0000), # 
                "PETIROC_RELOADING_PERIOD_BOT_3" : register(0x020C,"rw",16,1,0x0000), # MSB
                "PETIROC_BOTTOM_CONFIG"          : register(0x0217,"rw",16,41,[0x0000]),
                # Bitflip counter (irradiation tests)
                "BITFLIP_COUNTER_BOT_1"          : register(0x0240,"ro",16,1,[0x0000]), #LSB
                "BITFLIP_COUNTER_BOT_2"          : register(0x0241,"ro",16,1,[0x0000]), #MSB
                "PETIROC_BOTTOM_CONFIG_PREVIOUS" : register(0x0264,"ro",16,41,[0x0000]),
                #-------------------------------------------------------------------#
                ### TDC ###
                #-------------------------------------------------------------------#
                "TDC_ENABLE"                     : register(0x0300,"rw",16,1,0x0000),
                "TDC_CMD_VALID"                  : register(0x0301,"rw",16,1,0x0000),
                "TDC_ENABLE_CH0_15"              : register(0x0305,"rw",16,1,0x0000),
                "TDC_ENABLE_CH16_31"             : register(0x0306,"rw",16,1,0x0000),
                "TDC_ENABLE_CH32_33"             : register(0x0307,"rw",16,1,0x0000),
                "TDC_INJECTION_MODE"             : register(0x0308,"rw",3,1,0x0000)
                #-------------------------------------------------------------------#
                }

        
    def init(self):
        self.fc7.init()
        self.feb.init()

##=================================================== ********************** ==================================================================##
##=================================================== Slow Control functions ==================================================================##
##=================================================== ********************** ==================================================================##
        
    def gen_BC0(self, nb_period_25ns):
        # Generate BC0 periodic pulse signal at the desired period. (Frequency of 10kHz = 1/(4000 * 25 ns))
        self.fc7.gen_BC0(nb_period_25ns)

    def stop_BC0(self):
        # Stop BC0 signal
        self.fc7.set_enable_BC0(0)
 

##=================================================== ********************** ==================================================================##
##===================================================      Data Parsing      ==================================================================##
##=================================================== ********************** ==================================================================##
    # read TDC datas from raw file ==> format : {"fpga_0":[channel_tdc, data_counter], "fpga_1":[channel_tdc, data_counter], "fpga_2":[channel_tdc, data_counter]}
    def save_TDC_datas(self, TDC_data, ID):
        # For BC0_id FW
        #Save TDC datas as row file
        # print(TDC_data)
        # print("_________________________________________________")
        # print(TDC_data[0])
        # print("_________________________________________________")
        # print(TDC_data[0][0]["fpga_0"])
        # print("_________________________________________________")
        fpga0_data = TDC_data[0][0]["fpga_0"]
        fpga1_data = TDC_data[0][0]["fpga_0"]
        fpga2_data = TDC_data[0][0]["fpga_0"]
        #write
        #file=open("/irradiation_run/TDCdatas.raw","a") 
        with open("TDCdatas.raw", "a") as file:
            file.write("ID :"+ID+"\n")
            file.write("#fpga_0\n") 
            for x in fpga0_data:
                file.write((str(x[0]) + "," +str(x[1]) + "\n"))
            file.write("#fpga_1\n") 
            for x in fpga1_data:
                file.write((str(x[0]) + "," +str(x[1]) + "\n"))
            file.write("#fpga_2\n") 
            for x in fpga2_data:
                file.write((str(x[0]) + "," +str(x[1]) + "\n"))     
        file.close()

    def read_TDC_datas(file_name):
        # read TDC datas from raw file
        with open(file_name,'r') as f:
            mylist = f.read().splitlines()
            r=[]
            data = {}
            cpt = 0
            for d in mylist:
                r.append(d.split(","))
            for x in r:
                print(x[0][0])
                if x[0][0:2] == "ID":
                    pass
                elif x[0][0] =="#":
                    print("ok")
                    title = x[0][1:]
                    if cpt <3 : 
                        data[title] = [[],[]]
                        cpt +=1
                else:
                    data[title][0].append(int(x[0]))
                    data[title][1].append(int(x[1])) 
        print(data)
       
        return data

    # Read 6b/10b Dac from Equalized Datas from dac6bConfig
    def readDacDefaultConfig(self,fpga, asic):
        # Read dac values from dac6bConfig folder ==> First run Equalization.py to generate config files
        with open("dac6bConfig/dac6b_{}_{}.txt".format(fpga,asic),"r") as file:
            mylist = file.read().splitlines()
            value=[]
            for d in mylist:
                value.append((d.split("="))[1])
        return value

##=================================================== ********************** ==================================================================##
##=================================================== FEB Config and Control ==================================================================##
##=================================================== ********************** ==================================================================##
    def enable_petiroc_auto_reload(self, fpga_ref, asic_ref, period_ms): # enable periodic reconf with a specific period
        self.feb.asic.set_auto_reload(period_ms = period_ms ,fpga_ref = fpga_ref, asic_ref= asic_ref)
        print(bcolors.BLUE +"*** Enable petiROC auto reconfiguration *** \n period = {} ms \n".format(period_ms)+ bcolors.ENDC)   

    def disable_petiroc_auto_reconf(self, fpga_ref): # Disable all ASICS auto reconf. [TOP / BOT]
        self.feb.asic.dis_auto_reload(fpga_ref)
        print(bcolors.BLUE +" ==> Disable petiROC auto reconfiguration. "+ bcolors.ENDC)

    def sc_write(self,fpga, addr, data):
      WR=1
      self.fc7.downlink.send_SC_frame(fpga, WR, address=addr, data=data)

    def sc_read(self,fpga, addr, nWords, bVerbose=False):
      RD = 0
      if bVerbose:
        print("Read",nWords,"word from addr","0x"+hex(addr)[2:].rjust(4,'0'))
      self.fc7.downlink.send_SC_frame(fpga, RD, address=addr, data=nWords)
      ReceivedSC = self.fc7.uplink.receive_SC_frame()
      llSC=[]
      llSC.append(["0x"+hex(item)[2:].rjust(4,'0') for item in ReceivedSC['fpga_0']])
      llSC.append(["0x"+hex(item)[2:].rjust(4,'0') for item in ReceivedSC['fpga_1']])
      llSC.append(["0x"+hex(item)[2:].rjust(4,'0') for item in ReceivedSC['fpga_2']])
      if bVerbose:
        print("FPGA 0".rjust(15,' ')+'|'+"FPGA 1".rjust(16,' ')+'|'+"FPGA 2".rjust(16,' ')+'|')
        for i in range(max([len(item) for item in llSC])):
          for fpgaID in range(3):
            if i < len(llSC[fpgaID]):
              print(llSC[fpgaID][i].rjust(15,' ')+'|',end='')
            else:
              print(" "*15+'|',end='')
          print()
      return llSC

    def Dac10bConfig(self, nDacVal=400, lsFpga=["fpga_1"], lsAsic=["top"]):
        for fpgaID in lsFpga:
            for asicID in lsAsic:
                self.feb.asic.set_10b_dac(fpgaID, asicID, nDacVal)   

    def ApplyConfig(self, lsFpga=["fpga_0", "fpga_1", "fpga_2"]):
      for fpgaID in lsFpga:
        #Gen config registers
        ROC_top_config = self.feb.asic.asic[fpgaID][self.feb.asic.asic_top_index].gen_config()
        ROC_bot_config = self.feb.asic.asic[fpgaID][self.feb.asic.asic_bottom_index].gen_config()
        #Send config registers to FEB FPGAs
        if   fpgaID=="fpga_0": nTargetFPGA = LEFT
        elif fpgaID=="fpga_1": nTargetFPGA = MID
        elif fpgaID=="fpga_2": nTargetFPGA = RIGHT
        else: sys.exit("ERROR : UNKNOWN FPGA ID")
        self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_TOP_CONFIG"].address, ROC_top_config)
        self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_BOTTOM_CONFIG"].address, ROC_bot_config)
      nTargetFPGA=0
      if "fpga_0" in lsFpga: nTargetFPGA += LEFT
      if "fpga_1" in lsFpga: nTargetFPGA += MID
      if "fpga_2" in lsFpga: nTargetFPGA += RIGHT
      #PETIROC load cycle
      self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_TOP_LOAD"].address, [0]) #ROC TOP LOAD = 0
      self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_BOTTOM_LOAD"].address, [0]) #ROC BOT LOAD = 0
      self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_TOP_LOAD"].address, [1]) #ROC TOP LOAD = 1
      self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_BOTTOM_LOAD"].address, [1]) #ROC BOT LOAD = 1
      self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_TOP_LOAD"].address, [0]) #ROC TOP LOAD = 0
      self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_BOTTOM_LOAD"].address, [0]) #ROC BOT LOAD = 0

      time.sleep(0.2)

    def ApplyDefaultConfig(self, lsFpga=["fpga_0", "fpga_1", "fpga_2"], lsAsic=["top","bottom"]):
      ## SET ASIC REGISTER WITH DEFAULT VALUES
      for fpgaID in lsFpga:
        for asicID in lsAsic:
            ## DEFAULT : active all asics channels
            self.PetirocChannelEnable([fpgaID], [asicID], lbChannelEnable=[True]*16)
            ## Get asic default config from datafile
            value = self.readDacDefaultConfig(fpgaID,asicID)
            ### DEFAULT : set 6b/10b values after equalization
            for i in range(16):
                self.feb.asic.asic[fpgaID][asicID].set_6b_dac(i*2,int(value[i+1]))
            #configure middle DAC + 20    
            self.feb.asic.asic[fpgaID][asicID].set_10b_dac_T(int(value[0])-100)
            # Generate config register for a specific asic
            asic_config = self.feb.asic.asic[fpgaID][asicID].gen_config()

            # SEND ASIC REGISTER TO FEB FPGAs
            if   fpgaID=="fpga_0": nTargetFPGA = LEFT
            elif fpgaID=="fpga_1": nTargetFPGA = MID
            elif fpgaID=="fpga_2": nTargetFPGA = RIGHT
            else: sys.exit("ERROR : UNKNOWN FPGA ID")
            if asicID =="top":
                self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_TOP_CONFIG"].address, asic_config)
            if asicID =="bottom":
                self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_BOTTOM_CONFIG"].address, asic_config)        
      #PETIROCs load cycle      
      nTargetFPGA=0
      if "fpga_0" in lsFpga: nTargetFPGA += LEFT
      if "fpga_1" in lsFpga: nTargetFPGA += MID
      if "fpga_2" in lsFpga: nTargetFPGA += RIGHT
      self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_TOP_LOAD"].address, [0]) #ROC TOP LOAD = 0
      self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_BOTTOM_LOAD"].address, [0]) #ROC BOT LOAD = 0
      self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_TOP_LOAD"].address, [1]) #ROC TOP LOAD = 1
      self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_BOTTOM_LOAD"].address, [1]) #ROC BOT LOAD = 1
      self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_TOP_LOAD"].address, [0]) #ROC TOP LOAD = 0
      self.sc_write(nTargetFPGA, self.fpga_register["PETIROC_BOTTOM_LOAD"].address, [0]) #ROC BOT LOAD = 0
      time.sleep(0.2)

    def EnableTDC(self, lsfpga = ["fpga_0","fpga_1","fpga_2"], ls_tdc_channel = [x for x in range(34)]):
        data=0
        l=[]
        for i in ls_tdc_channel:
            data  = data + (1 << i)
        data = bf(data)

        l.append(data[0:16])
        l.append(data[16:32])
        l.append(data[32:34])
        nTargetFPGA=0
        if "fpga_0" in lsfpga: nTargetFPGA += 1
        if "fpga_1" in lsfpga: nTargetFPGA += 2
        if "fpga_2" in lsfpga: nTargetFPGA += 4
        self.sc_write(nTargetFPGA,self.fpga_register["TDC_ENABLE"].address, data=[1]) #TDC module enable
        self.sc_write(nTargetFPGA,self.fpga_register["TDC_INJECTION_MODE"].address, data=[0]) #standard mode
        self.sc_write(nTargetFPGA,self.fpga_register["TDC_ENABLE_CH0_15"].address, data=[data[0:16],data[16:32],data[32:34]]) #meas enable for All TDC channels only => 305/306/307
        self.sc_write(nTargetFPGA,self.fpga_register["TDC_CMD_VALID"].address, data=[0]) #CMD valid = 0
        self.sc_write(nTargetFPGA,self.fpga_register["TDC_CMD_VALID"].address, data=[1]) #CMD valid = 1
        self.sc_write(nTargetFPGA,self.fpga_register["TDC_CMD_VALID"].address, data=[0]) #CMD valid = 0
        time.sleep(0.005)

    def DisableTDC(self, fpga="all"):
        if fpga == "fpga_2":
            self.sc_write(RIGHT,self.fpga_register["TDC_ENABLE"].address, data=[0]) #TDC module disable
        if fpga == "fpga_1":
            self.sc_write(MID,self.fpga_register["TDC_ENABLE"].address, data=[0]) #TDC module disable
        if fpga == "fpga_0":
            self.sc_write(LEFT,self.fpga_register["TDC_ENABLE"].address, data=[0]) #TDC module disable
        if fpga == "all":
            self.sc_write(LEFT+MID+RIGHT,self.fpga_register["TDC_ENABLE"].address, data=[0]) #TDC module disable

    def PetirocChannelEnable(self,lsFpga=["fpga_1"], lsAsic=["top"], lbChannelEnable=[True]*16):
        lnChannel=[]
        for i in range(16):
            if lbChannelEnable[i]:
              lnChannel.append(i*2)
        for fpgaID in lsFpga:
            for asicID in lsAsic:
              self.feb.asic.enable_channel(fpgaID, asicID, lnChannel)

    def load_feb_config(self,lsfpga, asic = "all"):
        nTargetFPGA=0
        if "fpga_0" in lsfpga: nTargetFPGA += 1
        if "fpga_1" in lsfpga: nTargetFPGA += 2
        if "fpga_2" in lsfpga: nTargetFPGA += 4

        if asic =="top":
            self.sc_write(nTargetFPGA,self.fpga_register["PETIROC_TOP_LOAD"].address,[1]) #top
            self.sc_write(nTargetFPGA,self.fpga_register["PETIROC_TOP_LOAD"].address,[0]) #top
        if asic == "bottom":
            self.sc_write(nTargetFPGA,self.fpga_register["PETIROC_BOTTOM_LOAD"].address,[1]) #bottom
            self.sc_write(nTargetFPGA,self.fpga_register["PETIROC_BOTTOM_LOAD"].address,[0]) #bottom
        if asic =="all":
            self.sc_write(nTargetFPGA,self.fpga_register["PETIROC_TOP_LOAD"].address,[1]) #top
            self.sc_write(nTargetFPGA,self.fpga_register["PETIROC_TOP_LOAD"].address,[0]) #top
            self.sc_write(nTargetFPGA,self.fpga_register["PETIROC_BOTTOM_LOAD"].address,[1]) #bottom
            self.sc_write(nTargetFPGA,self.fpga_register["PETIROC_BOTTOM_LOAD"].address,[0]) #bottom

    def EnableAutoResetFSM(self):
        self.sc_write(LEFT+MID+RIGHT, self.fpga_register["PETIROC_TOP_NOR32RAZ_FSM"].address, [1, int((100-15)/5), 1, 3])
        self.sc_write(LEFT+MID+RIGHT, self.fpga_register["PETIROC_BOTTOM_NOR32RAZ_FSM"].address, [1, int((100-15)/5), 1, 3])

    def DisableAutoResetFSM(self):
        self.sc_write(LEFT+MID+RIGHT, self.fpga_register["PETIROC_TOP_NOR32RAZ_FSM"].address, [0])
        self.sc_write(LEFT+MID+RIGHT, self.fpga_register["PETIROC_BOTTOM_NOR32RAZ_FSM"].address, [0])
    
    def LatchingConfig(self,sConfig="asic"):
        #Enable/DisableFpga latching
        nFPGAConf= 3 if sConfig in ["fpga"] else 2 #FPGA Nor32 is always used
        self.sc_write(LEFT+MID+RIGHT, 0x0108, [nFPGAConf])
        self.sc_write(LEFT+MID+RIGHT, 0x0208, [nFPGAConf])
        #Enable/Disable asic latching
        lsAllFpgas=["fpga_0", "fpga_1", "fpga_2"]
        lsAllAsics=["top", "bottom"]
        for fpgaID in lsAllFpgas:
            for asicID in lsAllAsics:
                if sConfig in ["fpga", "no"]:
                    self.feb.asic.disableLatch(fpgaID, asicID)
                else:
                    self.feb.asic.enableLatch(fpgaID, asicID)
    
    def DisableAllChannels(self, lsFpga=["fpga_0", "fpga_1", "fpga_2"], lsAsic=["top","bottom"]):
        for fpgaID in lsFpga:
            for asicID in lsAsic:
                self.feb.asic.disable_channel(fpgaID, asicID, range(32))

    def Dac6bConfig(self,lnDacVal=[32]*16, lsFpga=["fpga_1"], lsAsic=["top"]):
        for fpgaID in lsFpga:
            for asicID in lsAsic:
                for i in range(16):
                    self.feb.asic.set_6b_dac(fpgaID,asicID, i*2, lnDacVal[i])
                    self.feb.asic.set_6b_dac(fpgaID,asicID, i*2+1, 0) #Unused (odd) channels

    def read_bitflip_counter(self,lsfpga, asic= "all"):
        # Send request to fc7
        # result ={"0" : {"top" : 0, "bot" :0},  # 0 => fpga_0 / 1=> fpga_1 / 2=> fpga_2
        #          "1" : {"top" : 0, "bot" :0},
        #          "2" : {"top" : 0, "bot" :0}}
        result={}
        nTargetFPGA=0
        if "fpga_0" in lsfpga: nTargetFPGA += 1
        if "fpga_1" in lsfpga: nTargetFPGA += 2
        if "fpga_2" in lsfpga: nTargetFPGA += 4

        if asic =="top":
            result1 = self.sc_read(nTargetFPGA,self.fpga_register["BITFLIP_COUNTER_TOP_1"].address,1, False) #top
            result2 = self.sc_read(nTargetFPGA,self.fpga_register["BITFLIP_COUNTER_TOP_2"].address,1,False)
            for i in range(0,3):
                result[i] =[]
                if result1[i] != [] or result2[i]!= []:
                    result[i].append({"top" : ((int((result2[i][0] ),16) << 16 )+  int(result1[i][0],16))})
                    print(result)
            return result

        if asic =="bottom":
            result1 = self.sc_read(nTargetFPGA,self.fpga_register["BITFLIP_COUNTER_BOT_1"].address,1, False) #bottom
            result2 = self.sc_read(nTargetFPGA,self.fpga_register["BITFLIP_COUNTER_BOT_2"].address,1, False)
            for i in range(0,3):
                result[i] =[]
                if result1[i] != [] or result2[i]!= []:
                    result[i].append({"bot" : ((int((result2[i][0] ),16) << 16 )+  int(result1[i][0],16))})
                    print(result)
            return result

        if asic =="all":
            result1_t = self.sc_read(nTargetFPGA,self.fpga_register["BITFLIP_COUNTER_TOP_1"].address,1, False) #top
            result2_t = self.sc_read(nTargetFPGA,self.fpga_register["BITFLIP_COUNTER_TOP_2"].address,1, False)
            result1_b = self.sc_read(nTargetFPGA,self.fpga_register["BITFLIP_COUNTER_BOT_1"].address,1, False) #bottom
            result2_b = self.sc_read(nTargetFPGA,self.fpga_register["BITFLIP_COUNTER_BOT_2"].address,1,False)
            for i in range(0,3):
                result[i] =[]
                if (result1_t[i] != [] or result2_t[i]!= []) and (result1_b[i] != [] or result2_b[i]!= []):
                    result[i].append({"top" : ((int(result2_t[i][0] ,16) << 16 )+  int(result1_t[i][0],16)),
                                    "bot" : ((int(result2_b[i][0] ,16) << 16 )+  int(result1_b[i][0],16))})
                # if result1_t[i] != [] or result2_t[i]!= []:
                #     result[i].append({"top" : ((int(result2_t[i][0] ,16) << 16 )+  int(result1_t[i][0],16))})
                # if result1_b[i] != [] or result2_b[i]!= []:
                #     result[i].append({"bot" : ((int(result2_b[i][0] ,16) << 16 )+  int(result1_b[i][0],16))})
            return result

##=================================================== ********************** ==================================================================##
##=================================================== ,HIGH LEVEL PROCESSES  ==================================================================##
##=================================================== ********************** ==================================================================##


    def sc_read(self, fpga, addr, nWords, bVerbose=False):
      RD = 0
      if bVerbose:
        print("Read",nWords,"word from addr","0x"+hex(addr)[2:].rjust(4,'0'))
      self.fc7.downlink.send_SC_frame(fpga, RD, address=addr, data=nWords)
      ReceivedSC = self.fc7.uplink.receive_SC_frame()
      llSC=[]
      llSC.append(["0x"+hex(item)[2:].rjust(4,'0') for item in ReceivedSC['fpga_0']])
      llSC.append(["0x"+hex(item)[2:].rjust(4,'0') for item in ReceivedSC['fpga_1']])
      llSC.append(["0x"+hex(item)[2:].rjust(4,'0') for item in ReceivedSC['fpga_2']])

      if bVerbose:
        print("FPGA 0".rjust(15,' ')+'|'+"FPGA 1".rjust(16,' ')+'|'+"FPGA 2".rjust(16,' ')+'|')
        for i in range(max([len(item) for item in llSC])):
          for fpgaID in range(3):
            if i < len(llSC[fpgaID]):
              print(llSC[fpgaID][i].rjust(15,' ')+'|',end='')
            else:
              print(" "*15+'|',end='')
          print()
      return llSC


    def stop_acquisition(self):
        #===== Acquisition stop and save datas =====#
        #===========================================#

        print("*** TDC ACQUISITION STOPPED *** \n")
        # Save datas
        data = testbench.fc7.uplink.receive_TDC_frame()
        print(data)
        #testbench.save_TDC_datas(data,time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time())))
        # stop
        testbench.fc7.stop_acquisition()
        testbench.DisableTDC()
        #==========================================#

    def start_acquisition(self):
        #print("\n Apply asic default configuration for data acquisition of selected FPGAs : {} \n".format(lsAllFpgas))
        #testbench.ApplyDefaultConfig(lsFpga = lsAllFpgas, lsAsic = lsAllAsics)
        #print("asic ch : {}".format(testbench.feb.asic.get_enable_channel("fpga_1","top")))
        # Ensure fifos are flushed
        testbench.fc7.stop_acquisition()
        testbench.fc7.fifos_clear()
        # Enable TDCs
        testbench.EnableTDC()
        # Start acquisition
        testbench.fc7.start_acquisition()
        print("\n*** TDC ACQUISITION START *** \n")
        time.sleep(1)
##=================================================== MAIN PROCESS ==================================================================##

if __name__ == '__main__':

    # Testbench initialisation => FC7.init() and feb.init()
    testbench = testbench_TDC_acquisition(False, False)

    ##=== *ARGs PARSER* ===##
    # Add -configured if the setup is already initialized.
    # ==> $ python3 testbench_irradiation.py -configured
    parser = argparse.ArgumentParser()
    parser.add_argument("-configured", "--configured", help="initialize setup",action="store_true")
    args = parser.parse_args()

    ## ARGs
    configured = args.configured

    #Constant definitions
    MID   = int("010",2) #2
    LEFT  = int("001",2) #1
    RIGHT = int("100",2) #4
    lsAllFpgas=["fpga_0", "fpga_1", "fpga_2"]
    lsAllAsics=["top", "bottom"]


    ## Configuration of setup
    if configured == False:
    ## INIT testbench
        print("*** SETUP INITIALISATION . . . ***")
        testbench.init()
        print("***INITIALISATION DONE ***\n")
        time.sleep(0.5)
    else:
        pass

    testbench.stop_BC0()
    # Clear FIFOs
    testbench.fc7.fifos_clear()
    #testbench.feb.reset()
    # Init asic and tdcs
    testbench.feb.asic.init(False)
    testbench.feb.tdc.init(False)

    print("----------------------- test FPGA COM ----------------------------")
    #Read FPGA ID of the 3 FPGAs
    testbench.sc_read(LEFT+MID+RIGHT, 0x0010, 1, bVerbose=True)
    print("------------------------------------------------------------------\n")

    ## Asic default configuration from file ==> 6b and 10b DAC Values [other registers values are fixed by dictionnary in feb_asic.py file]
    testbench.ApplyDefaultConfig(lsFpga = ["fpga_1"], lsAsic = ["top","bottom"])
    ## Enable FSM of Asics
    testbench.EnableAutoResetFSM()

    ## reset acquisition ctrl
    testbench.fc7.stop_acquisition()
    testbench.fc7.fifos_clear()

    # Config acq. parameters
    testbench.fc7.set_acquisition_mode("TDC_Count")
    testbench.fc7.set_nb_frame_acquisition(50)


    testbench.stop_BC0()
    #testbench.fc7.start_BC0_cosmic_gen()
    #testbench.fc7.gen_BC0(3560)

    testbench.start_acquisition()
            #=====#
    time.sleep(1)


    # Stop and save datas before process
    testbench.stop_acquisition()

    #testbench.stop_BC0()

        
    ######################### Injection mode = 1 ###########################

    # #disable petiROC channels : self.feb.asic.disable_channel(fpgaID, asicID, lnChannel)
    # testbench.DisableTDC()
    # #testbench.DisableAllChannels()
    # testbench.sc_write(2,testbench.fpga_register["TDC_CMD_VALID"].address, data=[0]) #CMD valid = 0
    # testbench.sc_write(2,testbench.fpga_register["TDC_CMD_VALID"].address, data=[1]) #CMD valid = 1
    # testbench.sc_write(2,testbench.fpga_register["TDC_CMD_VALID"].address, data=[0]) #CMD valid = 0

    # data=0
    # ls_tdc_channel = [x for x in range(34)]
    # for i in ls_tdc_channel:
    #     data  = data + (1 << i)
    # data = bf(data)

    # testbench.sc_write(2,testbench.fpga_register["TDC_ENABLE"].address, data=[1]) #TDC module enable
    # testbench.sc_write(2,testbench.fpga_register["TDC_INJECTION_MODE"].address, data=[1]) #1kHz independant clock input TDC channels
    # testbench.sc_write(2,testbench.fpga_register["TDC_ENABLE_CH0_15"].address, data=[data[0:16],data[16:32],data[32:34]]) #meas enable for All TDC channels only => 305/306/307
    
    # #command valid
    # testbench.sc_write(2,testbench.fpga_register["TDC_CMD_VALID"].address, data=[0]) #CMD valid = 0
    # testbench.sc_write(2,testbench.fpga_register["TDC_CMD_VALID"].address, data=[1]) #CMD valid = 1
    # testbench.sc_write(2,testbench.fpga_register["TDC_CMD_VALID"].address, data=[0]) #CMD valid = 0

    # testbench.start_acquisition()
    # time.sleep(1)
    # testbench.stop_acquisition()


        """ 
    ######################### BC0 COSMIC TEST ###########################
    print("delete previous file . . .")
    path = os.getcwd()
    if os.path.exists(path + "/TDCdatas.raw"):
        os.remove(path + "/TDCdatas.raw")

    # Testbench initialisation => FC7.init() and feb.init()
    testbench = testbench_irradiation(False)

    ##=== *ARGs PARSER* ===##
    # Add -configured if the setup is already initialized.
    # ==> $ python3 testbench_irradiation.py -configured
    parser = argparse.ArgumentParser()
    parser.add_argument("-configured", "--configured", help="initialize setup",action="store_true")
    args = parser.parse_args()

    ## ARGs
    configured = args.configured

    #Constant definitions
    MID   = int("010",2) #2
    LEFT  = int("001",2) #1
    RIGHT = int("100",2) #4
    lsAllFpgas=["fpga_0", "fpga_1", "fpga_2"]
    lsAllAsics=["top", "bottom"]


    ## Configuration of setup
    if configured == False:
    ## INIT testbench
        print("*** SETUP INITIALISATION . . . ***")
        testbench.init()
        print("***INITIALISATION DONE ***\n")
        time.sleep(0.5)
    else:
        pass
    # Clear FIFOs
    testbench.fc7.fifos_clear()
    testbench.feb.reset()
    # Init asic and tdcs
    testbench.feb.asic.init(False)
    testbench.feb.tdc.init(False)
    testbench.DisableTDC()


    testbench.stop_BC0()
    ## Asic default configuration from file ==> 6b and 10b DAC Values [other registers values are fixed by dictionnary in feb_asic.py file]
    testbench.ApplyDefaultConfig(lsFpga = lsAllFpgas, lsAsic = lsAllAsics, threashold_add = 20)
    ## Enable FSM of Asics
    testbench.EnableAutoResetFSM()




    #print("\n************************************* Process 2 ****************************************")

    # print("START BC0 COSMIC \n")

    # testbench.fc7.start_BC0_cosmic_gen()

    # # Start acquisition
    # testbench.fc7.set_acquisition_mode("Circular_Buffer_External")
    # testbench.fc7.set_circular_buffer_size(50)
    # # resync wait after trigger (256*25ns)
    # testbench.fc7.fpga_registers.set_Delay_resync_afterTrigger(value = 1000)
    # print(testbench.fc7.acquisition_info())

    # print(testbench.fc7.fpga_registers.get_BC0_cosmic_config())

    # testbench.fc7.stop_acquisition()
    # testbench.fc7.fifos_clear()

    # testbench.EnableTDC()
    # testbench.fc7.start_acquisition()
    
    # ## CONFIG


    # time.sleep(1)
    # testbench.stop_acquisition()
    """
