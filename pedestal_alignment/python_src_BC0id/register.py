import time
import sys
import os
import types
from tools_box import *

class register:

    def __init__(self,address,mode,length,size,value):
        self.address = address
        self.mode = mode
        self.length = length
        self.size = size
        if isinstance(value,list):
            if size>1 and len(value)==size:
                self.value = value
            elif value[0]==0:
                r=[]
                for i in range(size):
                    r.append(0)
                self.value=r
            else:
                raise(ValueError("value list size incorrect"))

        elif isinstance(value,int):
            if size==1:
                self.value = value
            else:
                raise(ValueError("value must be a list"))

    def get_value(self):
        if self.mode=="rw" or self.mode=="ro":
            return self.value
        else:
            raise(ValueError("Write only register"))


    def set_value(self,value):
        if isinstance(value,int):
            if self.size==1:
                if self.mode=="rw" or self.mode=="w":
                    self.value = value
                else:
                    raise(ValueError("Read only register"))
            else:
                raise(ValueError("value must be a list"))

        elif isinstance(value,list):
            if len(value)==self.size:
                for i,data in enumerate(value):
                    self.value[i] = data
            else:
                raise(ValueError("value list size incorrect"))
        else:
            raise(ValueError("value invalid type"))


    def get_value_bit(self,bit_start,bit_end):
        if self.size==1:
            if ((bit_start>=0 and bit_start<self.length) and (bit_end>=0 and bit_end<self.length) and bit_end>=bit_start) :
                return int(bf(self.value)[bit_start:(bit_end+1)])
            else:
                raise(ValueError("bit number invalid"))
        else:
            raise(ValueError("impossible operation"))


    def set_value_bit(self,bit_start,bit_end,value):
        if self.size==1:
            if ((bit_start>=0 and bit_start<self.length) and (bit_end>=0 and bit_end<self.length) and bit_end>=bit_start) :
                r = bf(self.value)
                r[bit_start:(bit_end+1)] = value
                self.value = int(r)
            else:
                raise(ValueError("bit number invalid"))
        else:
            raise(ValueError("impossible operation"))


    def print(self):
        if self.size==1:
            print("@0x{:04x} -> 0x{:04x}".format(self.address,self.value))
        else:
            for i,x in enumerate(self.value):
                    print("@0x{:04x} -> 0x{:04x}".format(self.address+i,x))

    def attribute(self):
        return [self.address,self.mode,self.length,self.size,self.value]

    def info(self):
        print(self.attribute())

if __name__ == '__main__':
    reg1 = register(0x0000,"rw",16,2,[0x4455,0x8877])
    print(reg1.get_value())
    reg1.set_value([1122,3344])
    print(reg1.get_value())

    reg2 = register(0x0000,"rw",16,1,0x4455)
    reg2.set_value(0x2233)
    print(reg2.get_value())
    print(reg2.get_value_bit(12,15))
    reg2.set_value_bit(12,15,0xFF)
    print(reg2.get_value_bit(12,15))
    reg1.print()
    reg2.print()
