import time
import sys
import os
import types
from tools_box import *
from feb_sca import *
from fc7 import *
from asic_petiroc2b import *


###---------------------------------------------###
#     This class is used to configure PetiROCs    #
###---------------------------------------------###
class feb_asics:

    def __init__(self,fpga,fpga_name,verbose):
        self.verbose = verbose
        self.fpga = fpga
        self.config_words_length = 42

        self.asic_top_index = "top"
        self.asic_bottom_index = "bottom"

        self.fpga_name = fpga_name

        self.asic = {}
        for fpga in self.fpga_name:
            self.asic[fpga] = {"top"    : asic_petiroc2b(False),"bottom" : asic_petiroc2b(False)}


    def init(self,verbose):
        if verbose: print("Initialize all asics (default configuration) and FSM...")
        self.stop_nor32raz_fsm("all","all")
        self.rstb("all","all")
        self.sr_rst("all","all")

        d =[]
        for i in range(32):
            d.append(i)

        for fpga in self.fpga_name:
            self.disable_channel(fpga,"top",d)
            self.disable_channel(fpga,"bottom",d)
            self.configure(fpga,"all")
            ###############################
            self.fpga.write(fpga,"FSM_RAZ_stage1_top",3)
            self.fpga.write(fpga,"FSM_RAZ_stage2_top",1)
            self.fpga.write(fpga,"FSM_RAZ_stage3_top",1)

            self.fpga.write(fpga,"FSM_RAZ_stage1_bot",3)
            self.fpga.write(fpga,"FSM_RAZ_stage2_bot",1)
            self.fpga.write(fpga,"FSM_RAZ_stage3_bot",1)

            #Gerald+Alban change
            #self.start_nor32raz_fsm(fpga,"top")
            #self.start_nor32raz_fsm(fpga,"bottom")
            
	    ##ECE##
            #print("petiroc 2C")
            #self.fpga.write(fpga,"PETIROC_TYPE_SELECT_TOP",2)
            #self.fpga.write(fpga,"PETIROC_TYPE_SELECT_BOT",2)
            ##ECE##


            ###############################
    def print(self,fpga_ref,asic_ref):
        print("**************************")
        print("**** {} asic {}   ***".format(fpga_ref,asic_ref))
        print("**************************")
        print("Channel Enabled      : {}".format(self.get_enable_channel(fpga_ref,asic_ref)))
        print("Channel 6b_dac_value : {}".format(self.get_6b_dac(fpga_ref,asic_ref,self.get_enable_channel(fpga_ref,asic_ref))))
        print("10b_dac_value        : {}".format(self.get_10b_dac(fpga_ref,asic_ref)))
        if self.get_nor32raz_status(fpga_ref,asic_ref)==1:
            print("Nor32Raz FSM enabled")
        else:
            print("Nor32Raz FSM disabled")

 #######################################################
 ## CONTROL ASIC PETIROC
 #######################################################
    def set_paramaters_from_file(self,fpga_ref,asic_ref,file_path):
        f = open(file_path,"r")
        index_fpga,index_asic,reg_name = "","",""
        reg_value =0
        for line in f:
            if line[0]=="#": # specific line
                index_fpga = line[1:-1].split(",")[0]
                index_asic = line[1:-1].split(",")[1]
            else:
                if ((fpga_ref==index_fpga and (asic_ref==index_asic or asic_ref=="all")) or (fpga_ref=="all" and asic_ref=="all")):
                    reg_name  = line[:-1].split(",")[0]
                    reg_value = int(line[:-1].split(",")[1])
                    self.asic[index_fpga][index_asic].config_registers[reg_name][self.asic[index_fpga][index_asic].index_value]
        f.close()

## FSM
    def get_nor32raz_status(self,fpga_ref,asic_ref):
        if asic_ref=="top":
            return self.fpga.read(fpga_ref,"PETIROC_TOP_NOR32RAZ_FSM",1)
        elif asic_ref=="bottom":
            return self.fpga.read(fpga_ref,"PETIROC_BOTTOM_NOR32RAZ_FSM",1)

    def start_nor32raz_fsm(self,fpga_ref,asic_ref):
        """Send Stop_nor32raz_fsm command to asic (broadcast command available)"""

        if asic_ref=="top":
            self.fpga.write(fpga_ref,"PETIROC_TOP_NOR32RAZ_FSM",1)
        elif asic_ref=="bottom":
            self.fpga.write(fpga_ref,"PETIROC_BOTTOM_NOR32RAZ_FSM",1)
        elif asic_ref=="all":
            self.fpga.write(fpga_ref,"PETIROC_TOP_NOR32RAZ_FSM",1)
            self.fpga.write(fpga_ref,"PETIROC_BOTTOM_NOR32RAZ_FSM",1)
        else:
            raise(ValueError(bcolors.FAIL +"Invalid asic name !"+ bcolors.ENDC))


    def stop_nor32raz_fsm(self,fpga_ref,asic_ref):
        """Send Stop_nor32raz_fsm command to asic (broadcast command available)"""

        if asic_ref=="top":
            self.fpga.write(fpga_ref,"PETIROC_TOP_NOR32RAZ_FSM",0)
        elif asic_ref=="bottom":
            self.fpga.write(fpga_ref,"PETIROC_BOTTOM_NOR32RAZ_FSM",0)
        elif asic_ref=="all":
            self.fpga.write(fpga_ref,"PETIROC_TOP_NOR32RAZ_FSM",0)
            self.fpga.write(fpga_ref,"PETIROC_BOTTOM_NOR32RAZ_FSM",0)
        else:
            raise(ValueError(bcolors.FAIL +"Invalid asic name !"+ bcolors.ENDC))

### Resetings
  # SlowControl resetstrip_list

    def sr_rst(self,fpga_ref,asic_ref):
        """Send RS_RSTb command to asic"""
        if self.verbose:print((bcolors.WARNING + "SR_RST asic : {} of FPGA : {}"+ bcolors.ENDC).format(asic_ref,fpga_ref))

        if asic_ref=="top":
            self.fpga.write(fpga_ref,"PETIROC_TOP_SR_RST",1)
            self.fpga.write(fpga_ref,"PETIROC_TOP_SR_RST",0)
        elif asic_ref=="bottom":
            self.fpga.write(fpga_ref,"PETIROC_BOTTOM_SR_RST",1)
            self.fpga.write(fpga_ref,"PETIROC_BOTTOM_SR_RST",0)
        elif asic_ref=="all":
            self.fpga.write(fpga_ref,"PETIROC_TOP_SR_RST",1)
            self.fpga.write(fpga_ref,"PETIROC_BOTTOM_SR_RST",1)
            self.fpga.write(fpga_ref,"PETIROC_TOP_SR_RST",0)
            self.fpga.write(fpga_ref,"PETIROC_BOTTOM_SR_RST",0)
        else:
            raise(ValueError(bcolors.FAIL +"Invalid asic name !"+ bcolors.ENDC))

# General Reset
    def rstb(self,fpga_ref,asic_ref):
        """Send RSTb command to asic"""
        if self.verbose: print((bcolors.WARNING + "RSTb asic : {} of FPGA : {}"+ bcolors.ENDC).format(asic_ref,fpga_ref))

        if asic_ref=="top":
            self.fpga.write(fpga_ref,"PETIROC_TOP_RSTB",0)
            self.fpga.write(fpga_ref,"PETIROC_TOP_RSTB",0x10)
        elif asic_ref=="bottom":
            self.fpga.write(fpga_ref,"PETIROC_BOTTOM_RSTB",0)
            self.fpga.write(fpga_ref,"PETIROC_BOTTOM_RSTB",0x10)
        elif asic_ref=="all":
            self.fpga.write(fpga_ref,"PETIROC_TOP_RSTB",0)
            self.fpga.write(fpga_ref,"PETIROC_BOTTOM_RSTB",0)
            self.fpga.write(fpga_ref,"PETIROC_TOP_RSTB",0x10)
            self.fpga.write(fpga_ref,"PETIROC_BOTTOM_RSTB",0x10)
        else:
            raise(ValueError(bcolors.FAIL +"Invalid asic name !"+ bcolors.ENDC))

    def set_auto_reload(self,period_ms,fpga_ref, asic_ref):
        value = int(period_ms//0.000010)
        if asic_ref =="top":
            self.fpga.write(fpga_ref,"PETIROC_RELOADING_PERIOD_TOP_1",value & 0xffff)
            self.fpga.write(fpga_ref,"PETIROC_RELOADING_PERIOD_TOP_2",(value >> 16) & 0xffff)
            self.fpga.write(fpga_ref,"PETIROC_RELOADING_PERIOD_TOP_3",(value >> 32) & 0xffff)
            self.fpga.write(fpga_ref,"PETIROC_TOP_LOAD",0x11)
        elif asic_ref =="bottom":
            self.fpga.write(fpga_ref,"PETIROC_RELOADING_PERIOD_BOT_1",value & 0xffff)
            self.fpga.write(fpga_ref,"PETIROC_RELOADING_PERIOD_BOT_2",(value >> 16) & 0xffff)
            self.fpga.write(fpga_ref,"PETIROC_RELOADING_PERIOD_BOT_3",(value >> 32) & 0xffff)
            self.fpga.write(fpga_ref,"PETIROC_BOTTOM_LOAD",0x11)

        elif asic_ref =="all":
            self.fpga.write(fpga_ref,"PETIROC_RELOADING_PERIOD_TOP_1",value & 0xffff)
            self.fpga.write(fpga_ref,"PETIROC_RELOADING_PERIOD_TOP_2",(value >> 16) & 0xffff)
            self.fpga.write(fpga_ref,"PETIROC_RELOADING_PERIOD_TOP_3",(value >> 32) & 0xffff)
            self.fpga.write(fpga_ref,"PETIROC_RELOADING_PERIOD_BOT_1",value & 0xffff)
            self.fpga.write(fpga_ref,"PETIROC_RELOADING_PERIOD_BOT_2",(value >> 16) & 0xffff)
            self.fpga.write(fpga_ref,"PETIROC_RELOADING_PERIOD_BOT_3",(value >> 32) & 0xffff)
            self.fpga.write(fpga_ref,"PETIROC_TOP_LOAD",0x11)
            self.fpga.write(fpga_ref,"PETIROC_BOTTOM_LOAD",0x00)

    def dis_auto_reload(self, fpga_ref):  
        self.fpga.write(fpga_ref,"PETIROC_TOP_LOAD",0x00)
        self.fpga.write(fpga_ref,"PETIROC_BOTTOM_LOAD",0x00)

    def reset_bitflip_counter(self, fpga_ref, asic_ref):
        if asic_ref =="top":
            self.fpga.write(fpga_ref,"BITFLIP_COUNTER_RESET_TOP",0x01)
            self.fpga.write(fpga_ref,"BITFLIP_COUNTER_RESET_TOP",0x00)
        if asic_ref =="bottom":
            self.fpga.write(fpga_ref,"BITFLIP_COUNTER_RESET_BOT",0x01)
            self.fpga.write(fpga_ref,"BITFLIP_COUNTER_RESET_BOT",0x00)
        if asic_ref =="all":
            self.fpga.write(fpga_ref,"BITFLIP_COUNTER_RESET_TOP",0x01)
            self.fpga.write(fpga_ref,"BITFLIP_COUNTER_RESET_TOP",0x00)
            self.fpga.write(fpga_ref,"BITFLIP_COUNTER_RESET_BOT",0x01)
            self.fpga.write(fpga_ref,"BITFLIP_COUNTER_RESET_BOT",0x00)

    def get_bitslip_counter(self,fpga_ref, asic_ref):
        if asic_ref =="top":
            result1 = self.fpga.read(fpga_ref,"BITFLIP_COUNTER_TOP_1",1) #LSB
            result2 = self.fpga.read(fpga_ref,"BITFLIP_COUNTER_TOP_2",1) #MSB
            result = (result2 << 16) + result1
            return result
        elif asic_ref == "bottom":
            result1 = self.fpga.read(fpga_ref,"BITFLIP_COUNTER_BOT_1",1)
            result2 = self.fpga.read(fpga_ref,"BITFLIP_COUNTER_BOT_2",1)
            result = (result2 << 16) + result1
            return result
        elif asic_ref =="all":
            result1_top = self.fpga.read(fpga_ref,"BITFLIP_COUNTER_TOP_1",1)
            result2_top = self.fpga.read(fpga_ref,"BITFLIP_COUNTER_TOP_2",1)
            result_top = (result2_top << 16) + result1_top
            result1_bot = self.fpga.read(fpga_ref,"BITFLIP_COUNTER_BOT_1",1)
            result2_bot = self.fpga.read(fpga_ref,"BITFLIP_COUNTER_BOT_2",1)
            result_bot = (result2_bot << 16) + result1_bot
            return result_top, result_bot

### Configuration

    def enable_channel(self,fpga_ref,asic_ref,channel_list):
        for channel in channel_list:
            self.asic[fpga_ref][asic_ref].clear_mask_discri_time(channel)

    def disable_channel(self,fpga_ref,asic_ref,channel_list):
        for channel in channel_list:
            self.asic[fpga_ref][asic_ref].set_mask_discri_time(channel)

    def get_enable_channel(self,fpga_ref,asic_ref):
        r = []
        for i in range(32):
            if (self.asic[fpga_ref][asic_ref].get_mask_discri_time(i)==0):
                r.append(i)
        return r

    def overwrite_channel_mask(self, fpga_ref,asic_ref,channel_list):
        all_channels=[1 for i in range(32)]
        self.disable_channel(fpga_ref,asic_ref, all_channels)
        time.sleep(0.1)
        self.enable_channel(fpga_ref, asic_ref, channel_list)
     

    def set_6b_dac(self,fpga_ref,asic_ref,channel,value):
        self.asic[fpga_ref][asic_ref].set_6b_dac(channel,value)

    def get_6b_dac(self,fpga_ref,asic_ref,list_channel):
        r = []
        for channel in list_channel:
            r.append(self.asic[fpga_ref][asic_ref].get_6b_dac(channel))
        return r

    def set_10b_dac(self,fpga_ref,asic_ref,value):
        self.asic[fpga_ref][asic_ref].set_10b_dac_T(value)

    def get_10b_dac(self,fpga_ref,asic_ref):
        return self.asic[fpga_ref][asic_ref].get_10b_dac_T()

    def enableLatch(self, fpga_ref, asic_ref):
        self.asic[fpga_ref][asic_ref].enableLatch()

    def disableLatch(self, fpga_ref, asic_ref):
        self.asic[fpga_ref][asic_ref].disableLatch()

    def enableNor32(self, fpga_ref, asic_ref):
        self.asic[fpga_ref][asic_ref].enableNor32()

    def disableNor32(self, fpga_ref, asic_ref):
        self.asic[fpga_ref][asic_ref].disableNor32()


    def load_configuration(self,fpga_ref,asic_ref):
        """load asic config command 2 times to read the config asic output"""

        if asic_ref=="top":
            self.fpga.write(fpga_ref,"PETIROC_TOP_LOAD",1)
            self.fpga.write(fpga_ref,"PETIROC_TOP_LOAD",0)
            time.sleep(0.1)
            self.fpga.write(fpga_ref,"PETIROC_TOP_LOAD",1)
            self.fpga.write(fpga_ref,"PETIROC_TOP_LOAD",0)
            time.sleep(0.1)
        elif asic_ref=="bottom":
            self.fpga.write(fpga_ref,"PETIROC_BOTTOM_LOAD",1)
            self.fpga.write(fpga_ref,"PETIROC_BOTTOM_LOAD",0)
            time.sleep(0.1)
            self.fpga.write(fpga_ref,"PETIROC_BOTTOM_LOAD",1)
            self.fpga.write(fpga_ref,"PETIROC_BOTTOM_LOAD",0)
            time.sleep(0.1)

        elif asic_ref=="all":
            self.fpga.write(fpga_ref,"PETIROC_TOP_LOAD",1)
            self.fpga.write(fpga_ref,"PETIROC_TOP_LOAD",0)
            self.fpga.write(fpga_ref,"PETIROC_BOTTOM_LOAD",1)
            self.fpga.write(fpga_ref,"PETIROC_BOTTOM_LOAD",0)
            time.sleep(0.1)
            self.fpga.write(fpga_ref,"PETIROC_TOP_LOAD",1)
            self.fpga.write(fpga_ref,"PETIROC_TOP_LOAD",0)
            self.fpga.write(fpga_ref,"PETIROC_BOTTOM_LOAD",1)
            self.fpga.write(fpga_ref,"PETIROC_BOTTOM_LOAD",0)
            time.sleep(0.1)

        else:
            raise(ValueError(bcolors.FAIL +"Invalid asic name !"+ bcolors.ENDC))

    def send_config_to_FPGA(self,fpga_ref,asic_ref):
        """Write Config in FPGA memory zone"""
        if asic_ref=="top":
            wconfig = self.asic[fpga_ref][asic_ref].gen_config()
            self.fpga.write(fpga_ref,"PETIROC_TOP_CONFIG",wconfig)
        elif asic_ref=="bottom":
            wconfig = self.asic[fpga_ref][asic_ref].gen_config()
            self.fpga.write(fpga_ref,"PETIROC_BOTTOM_CONFIG",wconfig)
        elif asic_ref=="all":
            wconfig = self.asic[fpga_ref][self.asic_top_index].gen_config()
            self.fpga.write(fpga_ref,"PETIROC_TOP_CONFIG",wconfig)
            wconfig = self.asic[fpga_ref][self.asic_bottom_index].gen_config()
            self.fpga.write(fpga_ref,"PETIROC_BOTTOM_CONFIG",wconfig)
        else:
                raise(ValueError(bcolors.FAIL +"Invalid asic name !"+ bcolors.ENDC))


    def configure(self,fpga_ref,asic_ref):
        """Write Config in FPGA memory zone and load in asic"""
        if asic_ref!="all":
            self.send_config_to_FPGA(fpga_ref,asic_ref)
            time.sleep(0.1)
            self.load_configuration(fpga_ref,asic_ref)
            time.sleep(0.1)
            return self.check_config(fpga_ref,asic_ref)

        elif asic_ref=="all":
            result = []
            for key_asic in self.asic[fpga_ref]:
                self.send_config_to_FPGA(fpga_ref,key_asic)
                time.sleep(0.1)
                self.load_configuration(fpga_ref,key_asic)
                time.sleep(0.1)
                result.append(self.check_config(fpga_ref,key_asic))
            return result
        else:
                raise(ValueError(bcolors.FAIL +"Invalid fpga name !"+ bcolors.ENDC))


    def read_previous_config(self,fpga_ref,asic_ref):
        """Read previous asic configuration content in fpga memory zone"""
        if asic_ref=="top": return self.fpga.read(fpga_ref,"PETIROC_TOP_CONFIG_PREVIOUS",self.config_words_length)
        elif asic_ref=="bottom": return self.fpga.read(fpga_ref,"PETIROC_BOTTOM_CONFIG_PREVIOUS",self.config_words_length)


    def read_config(self,fpga_ref,asic_ref):
        """Read asic configuration content in fpga memory zone"""
        if asic_ref=="top": return self.fpga.read(fpga_ref,"PETIROC_TOP_CONFIG",self.config_words_length)
        elif asic_ref=="bottom": return self.fpga.read(fpga_ref,"PETIROC_BOTTOM_CONFIG",self.config_words_length)


    def check_config(self,fpga_ref,asic_ref):
        """Compare asic configuration with the previous asic configuration"""
        current_config = self.read_config(fpga_ref,asic_ref)
        previous_config = self.read_previous_config(fpga_ref,asic_ref)
        compare=zip(current_config,previous_config)
        i=0
        result = True
        if (current_config==[] or previous_config==[]): return False
        for x in compare:
            if x[0]!=x[1]:
                if self.verbose:print("Configuration incorrect data #{} 0x{:04x}!=0x{:04x}".format(i,x[0],x[1]))
                result = False
                i = i + 1

        if (self.verbose and result):
            print(("Configuration asic_{} FPGA_{} "+ bcolors.GREEN +"[OK]" + bcolors.ENDC).format(asic_ref,fpga_ref,))
        elif (self.verbose and not(result)):
            print(("Configuration asic_{} FPGA_{} "+bcolors.FAIL +"[ERROR]" + bcolors.ENDC).format(asic_ref,fpga_ref,))
        return result


    ###################################################################
    ## Last update : 01/09/2021
    def set_PetiROC_type(self, fpga_ref, asic_ref, stype="2B"):
        # set the PetiROC version of the FEB
        # mode value
        if "A" in stype :
            pr_type = 0
        elif "B" in stype : 
            pr_type = 1
        elif "C" in stype :
            pr_type = 2
        # write register of selected asic
        if asic_ref == "top":
            self.fpga.write(fpga_ref,"PETIROC_TYPE_SELECT_TOP",pr_type)
            
        elif asic_ref == "bottom":
            self.fpga.write(fpga_ref,"PETIROC_TYPE_SELECT_BOT",pr_type)
        elif asic_ref == "all":
            self.fpga.write(fpga_ref,"PETIROC_TYPE_SELECT_TOP",pr_type)
            self.fpga.write(fpga_ref,"PETIROC_TYPE_SELECT_BOT",pr_type)
    ###################################################################

if __name__ == '__main__':

    fpga_name = ["fpga_0","fpga_1","fpga_2"]
    fpga_list = {
    fpga_name[0] : 1,
    fpga_name[1] : 2,
    fpga_name[2] : 4,
    "all" : 7,
    }
    fc7 = fc7(False)
    fpga = feb_fpga(fc7,fpga_list)

    asic = feb_asics(fpga,fpga_name,False)

    asic.read_config("fpga_1","top")

    # Write datas to JSON file
  # with open('asic_default_reg.json','w') as outfile:
  #     json.dump(data,outfile)
