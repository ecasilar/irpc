import uhal
import time
import sys
import os
import types
from tools_box import *
import argparse


###---------------------------------------------###
# This class receive and decode frames from GBTx  #
###---------------------------------------------###

class fc7_uplink:

    def __init__(self,rx_fifo_SC,rx_fifo_TDC,verbose):
        self.rx_fifo_SC = rx_fifo_SC
        self.rx_fifo_TDC = rx_fifo_TDC
        self.verbose = verbose


########################################################
## SLOW Control
########################################################
    def receive_SC_frame(self):
        SC_frames = []
        while (self.rx_fifo_SC.get_empty()==0):
            r = self.rx_fifo_SC.read_block(4)
            sc_data = (( (r[0]<<96) + (r[1]<<64) +(r[2]<<32) +r[3])>>16 )
            SC_frames.append(sc_data)
            if self.verbose :
                for i,x in enumerate(r):
                    print("Fifo data received #{} : 0x{:08x}".format(i,x))


        if self.verbose :
            for i,x in enumerate(SC_frames):
                print("SC Frame received 128bits format #{} :0x{:028x}".format(i,x))
        return self.decode_SC_frame(SC_frames)


    def decode_SC_frame(self,rx_sc_frame):
        data_fpga_0 = []
        data_fpga_1 = []
        data_fpga_2 = []
        SC_DATA = {
        "fpga_0" : data_fpga_0,
        "fpga_1" : data_fpga_1,
        "fpga_2" : data_fpga_2}
        cpt_sc_rx_frame = 0
        for frame in rx_sc_frame:
            if bf(frame)[102]==1: # SC FRAME VALID
                if (bf(frame)[96]==1 and bf(frame)[97]==1):
                    data_fpga_2.append( int(bf(frame)[16:32]))
                    data_fpga_2.append( int(bf(frame)[0:16]))
                elif int(bf(frame)[96])==0 and int(bf(frame)[97])==1:
                    data_fpga_2.append( int(bf(frame)[16:32]))
                elif int(bf(frame)[96])==1 and int(bf(frame)[97])==0:
                    data_fpga_2.append( int(bf(frame)[0:16]))
                elif int(bf(frame)[96])==0 and int(bf(frame)[97])==0:
                    pass

                else:
                    print("fc7_uplink bug read test 1")

                if (bf(frame)[98]==1 and bf(frame)[99]==1):
                    data_fpga_1.append( int(bf(frame)[48:64]))
                    data_fpga_1.append( int(bf(frame)[32:48]))
                elif int(bf(frame)[98])==0 and int(bf(frame)[99])==1:
                    data_fpga_1.append( int(bf(frame)[48:64]))
                elif int(bf(frame)[98])==1 and int(bf(frame)[99])==0:
                    data_fpga_1.append( int(bf(frame)[32:48]))
                elif int(bf(frame)[98])==0 and int(bf(frame)[99])==0:
                    pass
                else:
                    print("fc7_uplink bug read test 2")

                if (bf(frame)[100]==1 and bf(frame)[101]==1):
                    data_fpga_0.append( int(bf(frame)[80:96]))
                    data_fpga_0.append( int(bf(frame)[64:80]))
                elif int(bf(frame)[100])==0 and int(bf(frame)[101])==1:
                    data_fpga_0.append( int(bf(frame)[80:96]))
                elif int(bf(frame)[100])==1 and int(bf(frame)[101])==0:
                    data_fpga_0.append( int(bf(frame)[64:80]))
                elif int(bf(frame)[100])==0 and int(bf(frame)[101])==0:
                    pass
                else:
                    print("fc7_uplink bug read test 3")
            else:
                print("invalid SC FRAME")
        if self.verbose:
            for key,values in SC_DATA.items():
                for i,x in enumerate(values):
                    print("data received 16bits format from FPGA_{} #{}: 0x{:04x}".format(key,i,x))
        return SC_DATA


########################################################
## FAST Control
#######################################################

    # def receive_TDC_frame(self):
    #     TDC_frames = []
    #     while (self.rx_fifo_TDC.get_empty()==0):
    #         r = self.rx_fifo_TDC.read_block_TDC(4)
    #         data_int = (( (r[0]<<96) + (r[1]<<64) +(r[2]<<32) +r[3])>>16 )
    #         TDC_frames.append(data_int)
    #         if self.verbose :
    #             for i,x in enumerate(r):
    #                 print("Fifo data received #{} : 0x{:08x}".format(i,x))
    #     if self.verbose :
    #         for i,x in enumerate(TDC_frames):
    #             print("TDC Frame received 128bits format #{} :0x{:028x}".format(i,x))
    #     return self.decode_TDC_frame(TDC_frames)
        


    # def decode_field(self,data_field):
    #     r=[]
    #     if bf(data_field)[30:32]==0:
    #          r.append("fpga_0")
    #     elif bf(data_field)[30:32]==1:
    #         r.append("fpga_1")
    #     elif bf(data_field)[30:32]==2:
    #         r.append("fpga_2")
    #     r.append(bf(data_field)[24:30])
    #     r.append(bf(data_field)[0:24])
    #     return r
        

    
    # def decode_TDC_frame(self,tdc_frame):
    #     data_fpga_0 = []
    #     data_fpga_1 = []
    #     data_fpga_2 = []
    #     TDC_DATA = {
    #     "fpga_0" : data_fpga_0,
    #     "fpga_1" : data_fpga_1,
    #     "fpga_2" : data_fpga_2}
    #     for frame in tdc_frame:
    #         if bf(frame)[96]==1: # TDC FRAME VALID

    #             r=self.decode_field(bf(frame)[0:32])
    #             TDC_DATA[r[0]].append([r[1],r[2]])

    #         if bf(frame)[97]==1: # TDC FRAME VALID
    #             r=self.decode_field(bf(frame)[32:64])
    #             TDC_DATA[r[0]].append([r[1],r[2]])

    #         if bf(frame)[98]==1: # TDC FRAME VALID
    #             r=self.decode_field(bf(frame)[64:96])
    #             TDC_DATA[r[0]].append([r[1],r[2]])
    #     return TDC_DATA
        


#########################
# TESTs BC0_ID ==> a finaliser [ utilisé dans get_TDC_datas (fc7.py)]
#########################


    

    def receive_TDC_frame(self):
        TDC_frames = []
        #print("empty : ", self.rx_fifo_TDC.get_empty())
        while (self.rx_fifo_TDC.get_empty()==0):
            r = self.rx_fifo_TDC.read_block_TDC(8) # Size of a frame is 256 : BC0_id (32) + WB_frame (112)
            data_int = ((r[3]<<128) + (r[4]<< 96) +(r[5]<<64) +(r[6]<<32) + r[7])

            #print("BC0 id : ",int(bf(data_int)[112:144]))

            TDC_frames.append(data_int)
            if self.verbose :
                for i,x in enumerate(r):
                    print("Fifo data received #{} : 0x{:08x}".format(i,x))
        if self.verbose :
            for i,x in enumerate(TDC_frames):
                #print("TDC Frame received 128bits format #{} :0x{:028x}".format(i,x))
                print("TDC Frame received 256bits format #{} :0x{:028x}".format(i,x))
        return self.decode_TDC_frame(TDC_frames)


    def decode_field(self,data_field):
        r=[]
        if bf(data_field)[30:32]==0:
             r.append("fpga_0")
        elif bf(data_field)[30:32]==1:
            r.append("fpga_1")
        elif bf(data_field)[30:32]==2:
            r.append("fpga_2")

        r.append(bf(data_field)[24:30])
        r.append(bf(data_field)[0:24])
        return r


    def decode_TDC_frame(self,tdc_frame):
        BC0_id_i      = 0
        prev_bc0 = 0
        data_fpga_0 = []
        data_fpga_1 = []
        data_fpga_2 = []
        DATA = {}
        TDC_DATA = {
            "fpga_0" : data_fpga_0,
            "fpga_1" : data_fpga_1,
            "fpga_2" : data_fpga_2
        }
        
        for frame in tdc_frame:
            BC0_id = int(bf(frame)[112:144])
            BC0_id_i = prev_bc0
            DATA[BC0_id]=[]


            if BC0_id != BC0_id_i : # si BC0 ID different : on store a un nouvel indice
                DATA[BC0_id]=[]

                data_fpga_0 = []
                data_fpga_1 = []
                data_fpga_2 = []

                TDC_DATA = {
                    "fpga_0" : data_fpga_0,
                    "fpga_1" : data_fpga_1,
                    "fpga_2" : data_fpga_2
                }

                if bf(frame)[96]==1: # TDC FRAME VALID
                    r=self.decode_field(bf(frame)[0:32])
                    TDC_DATA[r[0]].append([r[1],r[2]])

                if bf(frame)[97]==1: # TDC FRAME VALID
                    r=self.decode_field(bf(frame)[32:64])
                    TDC_DATA[r[0]].append([r[1],r[2]])

                if bf(frame)[98]==1: # TDC FRAME VALID
                    r=self.decode_field(bf(frame)[64:96])

                    TDC_DATA[r[0]].append([r[1],r[2]])  

                

                prev_bc0 = BC0_id # BC0_id_i take current value of BC0_id for next loop comparaison
            else:
                if bf(frame)[96]==1: # TDC FRAME VALID
                    r=self.decode_field(bf(frame)[0:32])
                    TDC_DATA[r[0]].append([r[1],r[2]])

                if bf(frame)[97]==1: # TDC FRAME VALID
                    r=self.decode_field(bf(frame)[32:64])
                    TDC_DATA[r[0]].append([r[1],r[2]])

                if bf(frame)[98]==1: # TDC FRAME VALID
                    r=self.decode_field(bf(frame)[64:96])

                    TDC_DATA[r[0]].append([r[1],r[2]])     
            
            DATA[BC0_id].append(TDC_DATA)
            



        return DATA
