from ROOT import *
import json
import math
def plotped(fdir,fpganum,side,debug=False,maxshift=100):
    peds=[]
    f=TFile("calib_fpga%d_%s.root" % (fpganum,side),"RECREATE")
    fj=open("%s/calib_fpga_%d_%s.json" % (fdir,fpganum,side))
    iasic=1+fpganum*2
    if (side=="bottom"):
        iasic=iasic+1
    res=json.load(fj)
    #print res
    print(res["step"])
    print(res["low"])
    print(res["high"])
    nscale=0
    for x in res["channels"]:
        if (x["counters"][0]>nscale):
            nscale=x["counters"][0]
    #print res["channels"][0]
    #print res["channels"][14]
    #print res["channels"][14]["counters"]
    #print len(res["channels"][14]["counters"])
    hl=[]
    c1=TCanvas()
    c2=None
    ip=0;
    meanped=0
    m2ped=0
    pmin=1024
    pmax=0
    gStyle.SetOptFit();
    scfit=TF1("scfit","gaus",res["low"],res["high"]);
    for x in res["channels"]:
        
        hl.append(TH1F("F%d_%d" % (x["fpga"],x["channel"]),"F%d_%d" % (x["fpga"],x["channel"]),len(x["counters"]),res["low"],res["high"]))
        for i in range(len(x["counters"])):
            hl[len(hl)-1].SetBinContent(i+1,x["counters"][i])
        #h.Draw()
        hs=hl[len(hl)-1]
        hs.Scale(1./nscale);
        nmax=0
        for i in range(1,hs.GetNbinsX()):
            if (hs.GetBinContent(i)==0):
                if (hs.GetBinContent(i-1)!=0 and hs.GetBinContent(i+1)!=0):
                    hs.SetBinContent(i,(hs.GetBinContent(i-1)+hs.GetBinContent(i+1))/2.)
            else:
                if (hs.GetBinContent(i)>nmax):
                    nmax=hs.GetBinContent(i)

        
        icolor= ip%4 +1
        istyle= ip//4+1
        hs.SetLineColor(icolor)
        hs.SetLineStyle(istyle)
        hs.SetLineWidth(2)
        c1.cd()
        c1.Draw()
      



        if (len(hl)==1):
            hs.GetYaxis().SetRangeUser(0.,1.1)
            hs.Draw("HIST")
        else:
            hs.Draw("HISTSAME")
        
        hder=TH1F("hder%d" % ip,"derivative",
                  len(x["counters"]),res["low"],res["high"])
        for i in range(1,hs.GetNbinsX()):
            if (hs.GetBinContent(i)-hs.GetBinContent(i+1)>-10):
                hder.SetBinContent(i,hs.GetBinContent(i)-hs.GetBinContent(i+1))
            
        if ( debug and hder.GetRMS()>0.5):
            if (c2 == None):
                c2=TCanvas()
                c2.cd()
            scfit.SetParameter(0,1.);
            scfit.SetParameter(1,hder.GetMean());
            scfit.SetParameter(2,min(1.,hder.GetRMS()));
            hder.Fit("scfit","Q");
            #hs.GetXaxis().SetRangeUser(vthmin-1,scfit.GetParameter(1)+60);
            #gPad.SetLogy();
            #rped=scfit.GetParameter(1)
        #else:
        rped=hder.GetMean()
        binmax = hder.GetMaximumBin();
        rped1=hder.GetXaxis().GetBinCenter(binmax);
        
        if (debug):
            c2.cd()
            c2.Draw()
            hs.Draw("HIST")
            c2.Update()
            val1 = input()
            hder.Draw()
            c2.Update()
            val1 = input()

        print ("Channel %d Derivative Mean: %5.1f Maximum at %5.1f " % (ip,rped,rped1))

        c1.Update()
        ip=ip+1
        meanped=meanped+rped1
        m2ped=m2ped+rped1*rped1
        if (rped1<pmin):
            pmin=rped1
        if (rped1>pmax):
            pmax=rped1
        peds.append(rped1)
    c1.SaveAs("%s/pedestal_fpga%d_%s.pdf" % (fdir,fpganum,side));
    #c1.SaveAs("Run%d_AllStrip%d_%d.pdf" % (run,tdc,asic));

    sped=sorted(peds)
    print(sped)
    meanped=meanped/ip
    rmsped=math.sqrt(m2ped/ip-meanped*meanped)
    print("%d %d RMS %d diff %f" % (pmin,pmax,rmsped,pmax-pmin))
    
    target=int(round(meanped))
    if (maxshift>1):
        for i in range(len(sped)):
            if ((sped[len(sped)-1]-sped[i])<maxshift):
                target=(sped[len(sped)-1]+sped[i])/2.
                target=int(round(target))
                print(i,sped[len(sped)-1],sped[i],target)
                break
    else:
        target=int(input("enter target: "))
    calr=1.9 #2.97
    print ("dac10b Target =",target)
    s="cor_fpga%d_%s_asic%d=[" % (fpganum,side,iasic)
    for i in range(len(peds)):
        print ("chan%d=" %i,int(round(32+(target-peds[i])/calr)))
        s=s+"%d,0" %int(round((target-peds[i])/calr))
        if (i!=len(peds)-1):
            s=s+","
        else:
            s=s+"]"

    print (s)
    print ("THRTRANS[%d]=%d \n %s" %(iasic,int(round(target)),fdir ) )
    val = input()
    f.Write()
    f.Close()
    del c1
    del c2

if __name__ == "__main__":
    
    #plotped("Results/RE31_185_NORES_10_PRC_38_AR_ALL",0,"bottom")
    #plotped("Results/RE31_185_NORES_10_PRC_45_AR",2,"bottom")
    #plotped("Results/RE31_185_NORES_10_PRC_44_AR",0,"bottom")
    #plotped("Results/RE31_185_NORES_10_PRC_44_AR_ALL",0,"bottom")
    mshift=-31*1.9*2
    for i in range(0,3):
    
        plotped("Results/RE31_185_FEB_5_904_v5_A",i,"top",maxshift=mshift)
        plotped("Results/RE31_185_FEB_5_904_v5_A",i,"bottom",maxshift=mshift)
        #plotped("Results/RE31_185_FEB_5_904_v1_Large",i,"bottom",maxshift=-100)
        #plotped("RE31_186_FEB_6_gifOctober_1",i,"bottom")
        #plotped("RE31_186_FEB_6_gifOctober_12",i,"top")
        #plotped("RE31_186_FEB_6_gifOctober_12",i,"bottom")
        #plotped("RE31_186_FEB_6_gifOctober_14",i,"top")
        #plotped("RE31_186_FEB_6_gifOctober_14",i,"bottom")
        #plotped("RE31_186_FEB_6_gifOctober_15_all",i,"top")
        #plotped("RE31_186_FEB_6_gifOctober_15_all",i,"bottom")
        #plotped("RE31_186_FEB_6_gifOctober_15",i,"top")
        #plotped("RE31_186_FEB_6_gifOctober_15",i,"bottom")

        #plotped("RE31_186_FEB_6_gifOctober_15_AR_all",i,"bottom")
        #plotped("RE31_186_FEB_6_gifOctober_15_AR",i,"top")
        #plotped("RE31_186_FEB_6_gifOctober_15_AR",i,"bottom")
