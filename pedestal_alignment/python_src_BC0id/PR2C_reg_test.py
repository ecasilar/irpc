# -*- coding: utf-8 -*-
"""
Created on Mon May 10 11:26:51 2021

@author: luciol
"""
import uhal
import time
import signal
import sys
import os
import types
import argparse
import json
import shutil
import threading
import time
from datetime import datetime

from test_class import test_class
from tools_box import *
from fc7 import *
from feb_v2_cycloneV_r1 import *
from feb_ctrl_and_status import *
from testbench_calibrate_asic import *
from feb_fpga_registers import *
from Equalization import *
from Parsers_irradiation_tests import *
from statistics import mean
import matplotlib.pyplot as plt

##########################################
### TESTBENCH FILE for radiation tests ###
##########################################

class PR2C_reg_test(test_class): 
    
    def __init__(self,verbose):     
               
        super().__init__(verbose)
        

if __name__ == '__main__':

    # # CONSTANTS
    lsAllFpgas=["fpga_0", "fpga_1", "fpga_2"]
    lsAllAsics=["top", "bottom"]

    # DATA
    data_sca = []
    data_fpgas = []

    # # INIT Test_class
    testbench = PR2C_reg_test(False)

    # Apply default configuration of asic
    testbench.ApplyDefaultConfig(lsAllFpgas)

    # Set petiROC 2C type registers 
    #----------------------------------------------------------------------------------------------------------------
    testbench.feb.asic.set_PetiROC_type("fpga_1","top", stype="2C")
    print("register PETIROC_TYPE value : {}\n".format(testbench.feb.fpga.read("fpga_1","PETIROC_TYPE_SELECT_TOP",1)))
    #----------------------------------------------------------------------------------------------------------------

    # Read Bitflip count
    testbench.feb.asic.reset_bitflip_counter("fpga_1","top")
    bitflip_n = testbench.feb.asic.get_bitslip_counter("fpga_1","top")
    print("Bitflip counter value = {}\n".format(bitflip_n))

    # Read asic configuration 
    print(testbench.feb.asic.read_config("fpga_1","top"))

    # Configure delay_reset_trigger
    testbench.feb.asic.asic["fpga_1"]["top"].enable_delay_reset_trigger(8)
    #testbench.feb.asic.asic["fpga_1"]["top"].disable_delay_reset_trigger()
    print(testbench.feb.asic.configure("fpga_1","top"))

    # Read Bitflip after configuration
    bitflip_n = testbench.feb.asic.get_bitslip_counter("fpga_1","top")
    print("Bitflip counter value after configuration= {}\n".format(bitflip_n))

    # Read back asic configuration
    print("---------------------------")
    print("READ Configuration : \n")
    print(testbench.feb.asic.read_config("fpga_1","top"))
    print("\nREAD Previous Configuration : \n")
    print(testbench.feb.asic.read_previous_config("fpga_1","top"))
    print("---------------------------")
    print("CHECK Configuration : \n")
    print(testbench.feb.asic.check_config("fpga_1","top"))
    print("---------------------------")

