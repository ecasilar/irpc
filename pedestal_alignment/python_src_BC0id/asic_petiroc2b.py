import uhal
import time
import sys
import os
import types
from fc7_registers import *
from tools_box import *
import argparse
import json


class asic_petiroc2b:

    def __init__(self,verbose):
        self.verbose = verbose
        #name,[position_bit,nb_bit,default_value,value]
        self.index_value = 3
        self.index_msb_first = 2
        self.index_width = 1
        self.index_position_bit = 0
        self.config_registers = {
        "mask_discri_charge_ch0"  : [0,1,"lsb",1],
      	"mask_discri_charge_ch1"  : [1,1,"lsb",1],
      	"mask_discri_charge_ch2"  : [2,1,"lsb",1],
      	"mask_discri_charge_ch3"  : [3,1,"lsb",1],
      	"mask_discri_charge_ch4"  : [4,1,"lsb",1],
      	"mask_discri_charge_ch5"  : [5,1,"lsb",1],
        "mask_discri_charge_ch6"  : [6,1,"lsb",1],
        "mask_discri_charge_ch7"  : [7,1,"lsb",1],
        "mask_discri_charge_ch8"  : [8,1,"lsb",1],
        "mask_discri_charge_ch9"  : [9,1,"lsb",1],
        "mask_discri_charge_ch10" : [10,1,"lsb",1],
        "mask_discri_charge_ch11" : [11,1,"lsb",1],
        "mask_discri_charge_ch12" : [12,1,"lsb",1],
        "mask_discri_charge_ch13" : [13,1,"lsb",1],
        "mask_discri_charge_ch14" : [14,1,"lsb",1],
        "mask_discri_charge_ch15" : [15,1,"lsb",1],
        "mask_discri_charge_ch16" : [16,1,"lsb",1],
        "mask_discri_charge_ch17" : [17,1,"lsb",1],
        "mask_discri_charge_ch18" : [18,1,"lsb",1],
        "mask_discri_charge_ch19" : [19,1,"lsb",1],
        "mask_discri_charge_ch20" : [20,1,"lsb",1],
        "mask_discri_charge_ch21" : [21,1,"lsb",1],
        "mask_discri_charge_ch22" : [22,1,"lsb",1],
        "mask_discri_charge_ch23" : [23,1,"lsb",1],
        "mask_discri_charge_ch24" : [24,1,"lsb",1],
        "mask_discri_charge_ch25" : [25,1,"lsb",1],
        "mask_discri_charge_ch26" : [26,1,"lsb",1],
        "mask_discri_charge_ch27" : [27,1,"lsb",1],
        "mask_discri_charge_ch28" : [28,1,"lsb",1],
        "mask_discri_charge_ch29" : [29,1,"lsb",1],
        "mask_discri_charge_ch30" : [30,1,"lsb",1],
        "mask_discri_charge_ch31" : [31,1,"lsb",1],
        "input_dac_ch0"            : [32,8,"lsb",0x00],
        "cmd_input_dac_ch0"        : [40,1,"lsb",1],
        "input_dac_ch1"            : [41,8,"lsb",0x00],
        "cmd_input_dac_ch1"        : [49,1,"lsb",1],
        "input_dac_ch2"            : [50,8,"lsb",0x00],
        "cmd_input_dac_ch2"        : [58,1,"lsb",1],
        "input_dac_ch3"            : [59,8,"lsb",0x00],
        "cmd_input_dac_ch3"        : [67,1,"lsb",1],
        "input_dac_ch4"            : [68,8,"lsb",0x00],
        "cmd_input_dac_ch4"        : [76,1,"lsb",1],
        "input_dac_ch5"            : [77,8,"lsb",0x00],
        "cmd_input_dac_ch5"        : [85,1,"lsb",1],
        "input_dac_ch6"            : [86,8,"lsb",0x00],
      	"cmd_input_dac_ch6"        : [94,1,"lsb",1],
      	"input_dac_ch7"            : [95,8,"lsb",0x00],
      	"cmd_input_dac_ch7"        : [103,1,"lsb",1],
      	"input_dac_ch8"            : [104,8,"lsb",0x00],
      	"cmd_input_dac_ch8"        : [112,1,"lsb",1],
      	"input_dac_ch9"            : [113,8,"lsb",0x00],
      	"cmd_input_dac_ch9"        : [121,1,"lsb",1],
      	"input_dac_ch10"           : [122,8,"lsb",0x00],
      	"cmd_input_dac_ch10"       : [130,1,"lsb",1],
      	"input_dac_ch11"           : [131,8,"lsb",0x00],
      	"cmd_input_dac_ch11"       : [139,1,"lsb",1],
      	"input_dac_ch12"           : [140,8,"lsb",0x00],
      	"cmd_input_dac_ch12"       : [148,1,"lsb",1],
      	"input_dac_ch13"           : [149,8,"lsb",0x00],
      	"cmd_input_dac_ch13"       : [157,1,"lsb",1],
      	"input_dac_ch14"           : [158,8,"lsb",0x00],
      	"cmd_input_dac_ch14"       : [166,1,"lsb",1],
      	"input_dac_ch15"           : [167,8,"lsb",0x00],
      	"cmd_input_dac_ch15"       : [175,1,"lsb",1],
      	"input_dac_ch16"           : [176,8,"lsb",0x00],
      	"cmd_input_dac_ch16"       : [184,1,"lsb",1],
      	"input_dac_ch17"           : [185,8,"lsb",0x00],
      	"cmd_input_dac_ch17"       : [193,1,"lsb",1],
      	"input_dac_ch18"           : [194,8,"lsb",0x00],
      	"cmd_input_dac_ch18"       : [202,1,"lsb",1],
      	"input_dac_ch19"           : [203,8,"lsb",0x00],
      	"cmd_input_dac_ch19"       : [211,1,"lsb",1],
      	"input_dac_ch20"           : [212,8,"lsb",0x00],
      	"cmd_input_dac_ch20"       : [220,1,"lsb",1],
      	"input_dac_ch21"           : [221,8,"lsb",0x00],
      	"cmd_input_dac_ch21"       : [229,1,"lsb",1],
      	"input_dac_ch22"           : [230,8,"lsb",0x00],
      	"cmd_input_dac_ch22"       : [238,1,"lsb",1],
      	"input_dac_ch23"           : [239,8,"lsb",0x00],
      	"cmd_input_dac_ch23"       : [247,1,"lsb",1],
      	"input_dac_ch24"           : [248,8,"lsb",0x00],
      	"cmd_input_dac_ch24"       : [256,1,"lsb",1],
      	"input_dac_ch25"           : [257,8,"lsb",0x00],
      	"cmd_input_dac_ch25"       : [265,1,"lsb",1],
      	"input_dac_ch26"           : [266,8,"lsb",0x00],
      	"cmd_input_dac_ch26"       : [274,1,"lsb",1],
      	"input_dac_ch27"           : [275,8,"lsb",0x00],
      	"cmd_input_dac_ch27"       : [283,1,"lsb",1],
      	"input_dac_ch28"           : [284,8,"lsb",0x00],
      	"cmd_input_dac_ch28"       : [292,1,"lsb",1],
      	"input_dac_ch29"           : [293,8,"lsb",0x00],
      	"cmd_input_dac_ch29"       : [301,1,"lsb",1],
      	"input_dac_ch30"           : [302,8,"lsb",0x00],
      	"cmd_input_dac_ch30"       : [310,1,"lsb",1],
      	"input_dac_ch31"           : [311,8,"lsb",0x00],
      	"cmd_input_dac_ch31"       : [319,1,"lsb",1],
      	"input_dac_ch_dummy"       : [320,8,"lsb",0x00],
        "mask_discri_time_ch0"  : [328,1,"lsb",1],
      	"mask_discri_time_ch1"  : [329,1,"lsb",1],
      	"mask_discri_time_ch2"  : [330,1,"lsb",1],
      	"mask_discri_time_ch3"  : [331,1,"lsb",1],
      	"mask_discri_time_ch4"  : [332,1,"lsb",1],
      	"mask_discri_time_ch5"  : [333,1,"lsb",1],
      	"mask_discri_time_ch6"  : [334,1,"lsb",1],
      	"mask_discri_time_ch7"  : [335,1,"lsb",1],
      	"mask_discri_time_ch8"  : [336,1,"lsb",1],
      	"mask_discri_time_ch9"  : [337,1,"lsb",1],
      	"mask_discri_time_ch10" : [338,1,"lsb",1],
      	"mask_discri_time_ch11" : [339,1,"lsb",1],
      	"mask_discri_time_ch12" : [340,1,"lsb",1],
      	"mask_discri_time_ch13" : [341,1,"lsb",1],
      	"mask_discri_time_ch14" : [342,1,"lsb",1],
      	"mask_discri_time_ch15" : [343,1,"lsb",1],
      	"mask_discri_time_ch16" : [344,1,"lsb",1],
      	"mask_discri_time_ch17" : [345,1,"lsb",1],
      	"mask_discri_time_ch18" : [346,1,"lsb",1],
      	"mask_discri_time_ch19" : [347,1,"lsb",1],
      	"mask_discri_time_ch20" : [348,1,"lsb",1],
      	"mask_discri_time_ch21" : [349,1,"lsb",1],
      	"mask_discri_time_ch22" : [350,1,"lsb",1],
      	"mask_discri_time_ch23" : [351,1,"lsb",1],
      	"mask_discri_time_ch24" : [352,1,"lsb",1],
      	"mask_discri_time_ch25" : [353,1,"lsb",1],
      	"mask_discri_time_ch26" : [354,1,"lsb",1],
      	"mask_discri_time_ch27" : [355,1,"lsb",1],
      	"mask_discri_time_ch28" : [356,1,"lsb",1],
      	"mask_discri_time_ch29" : [357,1,"lsb",1],
      	"mask_discri_time_ch30" : [358,1,"lsb",1],
      	"mask_discri_time_ch31" : [359,1,"lsb",1],

        "6b_dac_ch0"            : [360,6,"lsb",0x20],
        "6b_dac_ch1"            : [366,6,"lsb",0x20],
        "6b_dac_ch2"            : [372,6,"lsb",0x20],
        "6b_dac_ch3"            : [378,6,"lsb",0x20],
        "6b_dac_ch4"            : [384,6,"lsb",0x20],
        "6b_dac_ch5"            : [390,6,"lsb",0x20],
        "6b_dac_ch6"            : [396,6,"lsb",0x20],
        "6b_dac_ch7"            : [402,6,"lsb",0x20],
        "6b_dac_ch8"            : [408,6,"lsb",0x20],
        "6b_dac_ch9"            : [414,6,"lsb",0x20],
        "6b_dac_ch10"           : [420,6,"lsb",0x20],
        "6b_dac_ch11"           : [426,6,"lsb",0x20],
        "6b_dac_ch12"           : [432,6,"lsb",0x20],
        "6b_dac_ch13"           : [438,6,"lsb",0x20],
        "6b_dac_ch14"           : [444,6,"lsb",0x20],
        "6b_dac_ch15"           : [450,6,"lsb",0x20],
        "6b_dac_ch16"           : [456,6,"lsb",0x20],
        "6b_dac_ch17"           : [462,6,"lsb",0x20],
        "6b_dac_ch18"           : [468,6,"lsb",0x20],
        "6b_dac_ch19"           : [474,6,"lsb",0x20],
        "6b_dac_ch20"           : [480,6,"lsb",0x20],
        "6b_dac_ch21"           : [486,6,"lsb",0x20],
        "6b_dac_ch22"           : [492,6,"lsb",0x20],
        "6b_dac_ch23"           : [498,6,"lsb",0x20],
        "6b_dac_ch24"           : [504,6,"lsb",0x20],
        "6b_dac_ch25"           : [510,6,"lsb",0x20],
        "6b_dac_ch26"           : [516,6,"lsb",0x20],
        "6b_dac_ch27"           : [522,6,"lsb",0x20],
        "6b_dac_ch28"           : [528,6,"lsb",0x20],
        "6b_dac_ch29"           : [534,6,"lsb",0x20],
        "6b_dac_ch30"           : [540,6,"lsb",0x20],
        "6b_dac_ch31"           : [546,6,"lsb",0x20],

        "EN_10bits_DAC"         : [552,1,"lsb",1],
        "PP_10bits_DAC"         : [553,1,"lsb",1],

        "10b_dac_vth_discri_charge" : [554,10,"msb",0x000],
        "10b_dac_vth_discri_time"   : [564,10,"msb",0x1F4],

        "EN_ADC"                  : [574,1,"lsb",0],
        "PP_ADC"                  : [575,1,"lsb",0],
        "sel_startb_ramp_ADC_ext" : [576,1,"lsb",0],
        "usebcompensation"        : [577,1,"lsb",0],
        "EN_bias_DAC_delay"       : [578,1,"lsb",0],#1],
        "PP_bias_DAC_delay"       : [579,1,"lsb",0],#1],
        "EN_bias_ramp_delay"      : [580,1,"lsb",0],
        "PP_bias_ramp_delay"      : [581,1,"lsb",0],
        "8b_dac_delay"	          : [582,8,"lsb",0x00],
        "EN_discri_delay"         : [590,1,"lsb",0],#1],
        "PP_discri_delay"         : [591,1,"lsb",0],#1],
        "PP_temp_sensor"          : [592,1,"lsb",0],
        "EN_temp_sensor"          : [593,1,"lsb",0],
        "EN_bias_pa"              : [594,1,"lsb",1],
        "PP_bias_pa"              : [595,1,"lsb",1],
        "EN_bias_discri"          : [596,1,"lsb",1],
        "PP_bias_discri"          : [597,1,"lsb",1],
        "cmd_polarity"            : [598,1,"lsb",0],
        "latch_discri"            : [599,1,"lsb",1],
        "EN_bias_6b_dac"          : [600,1,"lsb",1],
        "PP_bias_6b_dac"          : [601,1,"lsb",1],
        "EN_bias_tdc"             : [602,1,"lsb",0],
        "PP_bias_tdc"             : [603,1,"lsb",0],
        "ON_OFF_input_dac"        : [604,1,"lsb",1],
        "EN_bias_charge"          : [605,1,"lsb",0],
        "PP_bias_charge"          : [606,1,"lsb",0],
        "cf_100fF"                : [607,1,"lsb",0],
        "cf_200fF"                : [608,1,"lsb",1],
        "cf_2_5pF"                : [609,1,"lsb",1],
        "cf_1_25pF"               : [610,1,"lsb",0],
        "EN_bias_sca"             : [611,1,"lsb",0],
        "PP_bias_sca"             : [612,1,"lsb",0],
        "EN_bias_discri_charge"   : [613,1,"lsb",0],
        "PP_bias_discri_charge"   : [614,1,"lsb",0],
        "EN_bias_discri_adc_time" : [615,1,"lsb",0],
        "PP_bias_discri_adc_time" : [616,1,"lsb",0],
        "EN_bias_discri_adc_charge" : [617,1,"lsb",0],
        "PP_bias_discri_adc_charge" : [618,1,"lsb",0],
        "DIS_razchn_int"            : [619,1,"lsb",1],
        "DIS_razchn_ext"            : [620,1,"lsb",0],
        "SEL_80M"                   : [621,1,"lsb",1],
        "EN_80M"                    : [622,1,"lsb",1],
        "EN_slow_lvds_rec"          : [623,1,"lsb",1],
        "PP_slow_lvds_rec"          : [624,1,"lsb",1],
        "EN_fast_lvds_rec"          : [625,1,"lsb",0],
        "PP_fast_lvds_rec"          : [626,1,"lsb",0],
        "EN_transmitter"            : [627,1,"lsb",0],
        "PP_transmitter"            : [628,1,"lsb",0],
        "ON_OFF_1mA"                : [629,1,"lsb",0],
        "ON_OFF_2mA"                : [630,1,"lsb",1],
        "NC1"                       : [631,1,"lsb",0],
        "ON_OFF_ota_mux"            : [632,1,"lsb",0],
        "ON_OFF_ota_probe"          : [633,1,"lsb",0],
        "DIS_trig_mux"              : [634,1,"lsb",1],
        "EN_NOR32_time"             : [635,1,"lsb",0],
        "EN_NOR32_charge"           : [636,1,"lsb",0],
        "DIS_triggers"              : [637,1,"lsb",0],
        "EN_dout_oc"                : [638,1,"lsb",0],
        "EN_transmit"               : [639,1,"lsb",0],
        "PA_Ccomp<0>"               : [640,1,"lsb",1],#0],
        "PA_Ccomp<1>"               : [641,1,"lsb",1],#0],
        "PA_Ccomp<2>"               : [642,1,"lsb",1],#0],
        "PA_Ccomp<3>"               : [643,1,"lsb",1],
        "NC2"                       : [644,1,"lsb",1],
        "NC3"                       : [645,1,"lsb",0],
        "NC4"                       : [646,1,"lsb",0],
        "Choice_trigger_out"        : [647,1,"lsb",0],
        "Delay_reset_trigger"       : [648,4,"lsb",0],
        "NC5"                       : [652,1,"lsb",0],
        "NC6"                       : [653,1,"lsb",0],
        "NC7"                       : [654,1,"lsb",0],
        "EN_reset_trigger_delay"    : [655,1,"lsb",1],
        "Delay_reset_ToT"           : [656,4,"lsb",0],
        "NC8"                       : [660,1,"lsb",0],
        "NC9"                       : [661,1,"lsb",0],
        "NC10"                      : [662,1,"lsb",0],
        "EN_reset_ToT_delay"        : [663,1,"lsb",0]

        }


    def parse_data_fromfile(self,file):
      with open(file,'r') as outfile:
        data = json.load(outfile)
        #print(self.config_registers)
      for key, value in self.config_registers.items():
         value[3] = data[key][0]

######################################################################################
# For petiROC_2C
    def enable_delay_reset_trigger(self, value = 0): # pas : 3 ns
      if 0 <= value < 16:
        self.disable_reset_ToT_delay() # disable reset ToT delay
        self.config_registers["Delay_reset_trigger"][self.index_value]=value
        self.config_registers["EN_reset_trigger_delay"][self.index_value]=1

    def enable_delay_reset_ToT(self, value = 0): # pas : 3 ns
      if 0 <= value < 16:
        self.disable_delay_reset_trigger() # disable delay rst trigger
        self.config_registers["Delay_reset_ToT"][self.index_value]=value
        self.config_registers["EN_reset_ToT_delay"][self.index_value]=1

    def disable_delay_reset_trigger(self):
      self.config_registers["EN_reset_trigger_delay"][self.index_value]=0

    def disable_reset_ToT_delay(self):
      self.config_registers["EN_reset_ToT_delay"][self.index_value]=0

######################################################################################

    def config_report(self):
        for key,value in self.config_registers.items():
            print("{} : {}".format(key,value[self.index_value]))


    def set_any_config(self,config_param,value):
        self.config_registers[config_param][self.index_value] = value
        return

    def set_mask_discri_time(self,channel):
        if channel>=0 and channel<32:
            self.config_registers["mask_discri_time_ch"+str(channel)][self.index_value]=1
        else:
            raise(ValueError("Discrimask channel incorrect"))

    def clear_mask_discri_time(self,channel):
        if channel>=0 and channel<32:
            self.config_registers["mask_discri_time_ch"+str(channel)][self.index_value]=0

        else:
            raise(ValueError("Discrimask channel incorrect"))

    def get_mask_discri_time(self,channel):
        if channel>=0 and channel<32:
            return self.config_registers["mask_discri_time_ch" + str(channel)][self.index_value]
        else:
            raise(ValueError("Discrimask channel incorrect"))


    def set_10b_dac_T(self,value):
        if value<1024:
            self.config_registers["10b_dac_vth_discri_time"][self.index_value]= int( bf(value)[0:self.config_registers["10b_dac_vth_discri_time"][self.index_width]])
        else:
            raise(ValueError("value 10b_dac <1024"))

    def get_10b_dac_T(self):
        return self.config_registers["10b_dac_vth_discri_time"][self.index_value]


    def set_6b_dac(self,channel,value):
        if channel>=0 and channel<32:
            if value<64:
                self.config_registers["6b_dac_ch"+str(channel)][self.index_value]= value
            else:
                raise(ValueError("value 6b_dac <64"))
        else:
            raise(ValueError("set_6b_dac channel incorrect"))

    def get_6b_dac(self,channel):
        if channel>=0 and channel<32:
            return self.config_registers["6b_dac_ch" + str(channel)][self.index_value]
        else:
            raise(ValueError("set_6b_dac channel incorrect"))


    def enableLatch(self):
      self.config_registers["latch_discri"][self.index_value]=1

    def disableLatch(self):
      self.config_registers["latch_discri"][self.index_value]=0

    def enableNor32(self):
      self.config_registers["EN_NOR32_time"][self.index_value]=1

    def disableNor32(self):
      self.config_registers["EN_NOR32_time"][self.index_value]=0

######################################################################

    def gen_config(self):
        return self.format_16bits(self.gen_sc_register_petiroc2b())

    def gen_sc_register_petiroc2b(self):
        sc_reg= bf(0x000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000)
        start_bit =0
        end_bit =0
        for key,value in self.config_registers.items():
            start_bit = value[self.index_position_bit]
            end_bit   = value[self.index_position_bit] + value[self.index_width] - 1
            width_data = value[self.index_width]
            if value[self.index_msb_first]=="lsb":
                data = bf(value[self.index_value])
            else:
                for i in range(width_data):
                    inv = width_data - i -1
                    data[i] = bf(value[self.index_value])[inv]
            if (end_bit-start_bit)==0:
                sc_reg[start_bit] = data[0]
            else:
                sc_reg[start_bit:end_bit+1] = data[0:width_data]
        if self.verbose: print("asic_petiroc2b module gen_sc_register_petiroc2b function : 664 bits register value : 0x{:16x}".format(int(sc_reg)))

        return int(sc_reg)

    def format_16bits(self,sc_reg):
        config = []
        data = bf(sc_reg)
        if self.verbose: print("asic_petiroc2b module format_16bits function : 664 bits register value : 0x{:166x}".format(int(data)))
        for i in range(42,0,-1):
            if i==1:
                config.append((int(data[0:8])<<8))
            elif i>1 and i<42:
                config.append(int(data[(i-1)*16-8:(16*i)-8]))
            else:
                config.append(int(data[(i-1)*16-8:i*16-8]))
        if self.verbose: print("asic_petiroc2b module format_16bits function : petiroc Config 16bits format : {}".format(config))
        return config

if __name__ == '__main__':
  asic = asic_petiroc2b(False)
    # Write datas to JSON file
  # with open('asic_default_reg.json','r') as outfile:
  #     data = json.load(outfile)

  # asic.parse_data_fromfile('asic_default_reg.json')
  # print(asic.gen_config())

