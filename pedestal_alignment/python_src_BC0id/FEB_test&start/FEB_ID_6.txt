Filename: FEB_6

################# SCA TEST ################## 

SCA_ID : 18075 

CRC_errors : {'LEFT': 0, 'MIDDLE': 0, 'RIGHT': 0} 

FAULTs : {'2V': 1, '4V': 1} 

SCA_SEU : 0 

Temperatures : {'LM75_SENSOR1': [35.0], 'LM75_SENSOR2': [36.0], 'LM75_SENSOR3': [30.5], 'LM75_SENSOR4': [31.5], 'LM75_SENSOR5': [30.5]} 

______POWER SUPLLIES______ : 

RSSI : [0.31] 

4V_IN : [4.02] 

4V_IN_CURRENT : [2.1] 

2V5_SAFE : [2.5] 

3V3_VCCIO : [3.34] 

2V5_VCCIO : [2.51] 

VH_VCCIO_LEFT : [1.5] 

VH_VCCIO_MIDDLE : [1.5] 

VH_VCCIO_RIGHT : [1.5] 

2V_IN : [1.98] 

2V_IN_CURRENT : [6.36] 

1V5_SAFE : [1.49] 

1V5_VCCIO : [1.49] 

1V1_CORE_LEFT : [1.09] 

1V1_CORE_MIDDLE : [1.09] 

1V1_CORE_RIGHT : [1.09] 

1V1_VCCE_LEFT : [1.09] 

1V1_VCCE_MIDDLE : [1.09] 

1V1_VCCE_RIGHT : [1.09] 

CURRENT_CORE_LEFT : [1.87] 

CURRENT_CORE_MIDDLE : [1.98] 

CURRENT_CORE_RIGHT : [1.87] 

############################################# 

