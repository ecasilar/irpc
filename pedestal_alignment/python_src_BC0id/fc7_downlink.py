import uhal
import time
import sys
import os
import types
from fc7_registers import *
from tools_box import *
import argparse

###--------------------------------------------------###
#   This class is used to transmit GBT frame to GBTx   #
###--------------------------------------------------###
class fc7_downlink:

    def __init__(self,tx_fifo_SC,verbose):
        self.tx_fifo_SC = tx_fifo_SC
        self.verbose = verbose
        self.resync_loc = 79
        self.BC0_loc = 78
        self.resetSCpath = 77
        self.misc    = [67,77]
        self.target  = [64,67]
        self.rsv     = [57,64]
        self.mode    = 56
        self.nb_word = [48,56]
        self.address = [32,48]
        self.Wrdata0 = [16,32]
        self.Wrdata1 = [0,16]
        self.Wrdata_n = [48,64]
        self.Wrdata_n1 = [32,48]
        self.Wrdata_n2 = [16,32]
        self.Wrdata_n3 = [0,16]


    def info(self,sc_frame):
        if self.verbose:
            print("########################################")
            print("SC_frame : 0x{:032x}".format(sc_frame[0]))
            print("FPGA_sel : 0x{:x}".format(bf(sc_frame[0])[self.target[0]+48:self.target[1]+48]))
            print("Wr       : 0x{:x}".format(bf(sc_frame[0])[self.mode+48]))
            print("nb_word  : 0x{:x}".format(bf(sc_frame[0])[self.nb_word[0]+48:self.nb_word[1]+48]))
            print("address  : 0x{:x}".format(bf(sc_frame[0])[self.address[0]+48:self.address[1]+48]))
            nb=0
            for x in sc_frame:
                print("SC_frame 128bits format #{}: 0x{:032x}".format(nb,x))
                nb = nb+1

 ## SC frame create
    def reset_sc_path_frame(self):
        sc_frame_rqst = (0x20000000000000000000 << 48)
        sc_frame =[sc_frame_rqst]
        return sc_frame
    
    # target : FPGA | mode : rd/wr | memory_address : address on the target FPGA | words : nb of words to read/write
    def sc_frame_request(self,target,mode,memory_address,words):
        sc_frame_rqst = bf(0x00000000000000000000)
        sc_frame =[]
        sc_frame_rqst[self.resync_loc] = 0
        sc_frame_rqst[self.BC0_loc] = 0
        sc_frame_rqst[self.resetSCpath] = 0
        sc_frame_rqst[self.misc[0]:self.misc[1]] = 0
        sc_frame_rqst[self.target[0]:self.target[1]] = target
        sc_frame_rqst[self.rsv[0]:self.rsv[1]] = 0
        sc_frame_rqst[self.mode] = mode
        sc_frame_rqst[self.address[0]:self.address[1]] = memory_address
        if mode==0: #read
            sc_frame_rqst[self.nb_word[0]:self.nb_word[1]] = words-1 # nb words to read
            sc_frame_rqst[self.Wrdata0[0]:self.Wrdata1[1]] = 0
            sc_frame_rqst[self.Wrdata1[0]:self.Wrdata1[1]] = 0
            sc_frame.append( (int(sc_frame_rqst))<<48 )
            self.info(sc_frame)
            return sc_frame
        else: #write
            sc_frame_rqst[self.nb_word[0]:self.nb_word[1]] = len(words)-1
            if len(words)==0:
                print((bcolors.FAIL + "FPGA data is empty !" + bcolors.ENDC))
                return -1
            elif len(words)==1:
                sc_frame_rqst[self.Wrdata0[0]:self.Wrdata0[1]] = words[0]
                sc_frame_rqst[self.Wrdata1[0]:self.Wrdata1[1]] = 0
                sc_frame.append( (int(sc_frame_rqst))<<48 )
                self.info(sc_frame)
                return sc_frame
            elif len(words)==2:
                sc_frame_rqst[self.Wrdata0[0]:self.Wrdata0[1]] = words[0]
                sc_frame_rqst[self.Wrdata1[0]:self.Wrdata1[1]] = words[1]
                sc_frame.append( (int(sc_frame_rqst))<<48 )
                self.info(sc_frame)
                return sc_frame
            else:
                sc_frame_rqst[self.Wrdata0[0]:self.Wrdata0[1]] = words[0]
                sc_frame_rqst[self.Wrdata1[0]:self.Wrdata1[1]] = words[1]
                sc_frame.append( (int(sc_frame_rqst))<<48 )
                sc_frame = self.sc_frame_payload(int(sc_frame_rqst),words)
                self.info(sc_frame)
                return sc_frame

    def sc_frame_payload(self,sc_frame_request,words):
        cpt_words = 2
        sc_frame_payload_tmp = bf(0x00000000000000000000)
        sc_frame_payload = []
        sc_frame_payload.append((sc_frame_request<<48))
        while(cpt_words!=len(words)):
            sc_frame_payload_tmp[self.resync_loc] = bf(sc_frame_request)[self.resync_loc]
            sc_frame_payload_tmp[self.BC0_loc] = bf(sc_frame_request)[self.BC0_loc]
            sc_frame_payload_tmp[self.resetSCpath] = bf(sc_frame_request)[self.resetSCpath]
            sc_frame_payload_tmp[self.misc[0]:self.misc[1]] = bf(sc_frame_request)[self.misc[0]:self.misc[1]]
            sc_frame_payload_tmp[self.target[0]:self.target[1]] = bf(sc_frame_request)[self.target[0]:self.target[1]]
            if (len(words)-cpt_words)>=4:
                sc_frame_payload_tmp[self.Wrdata_n[0]:self.Wrdata_n[1]]=words[cpt_words]
                sc_frame_payload_tmp[self.Wrdata_n1[0]:self.Wrdata_n1[1]]=words[cpt_words+1]
                sc_frame_payload_tmp[self.Wrdata_n2[0]:self.Wrdata_n2[1]]=words[cpt_words+2]
                sc_frame_payload_tmp[self.Wrdata_n3[0]:self.Wrdata_n3[1]]=words[cpt_words+3]
                cpt_words = cpt_words + 4
            elif (len(words)-cpt_words)==3:
                sc_frame_payload_tmp[self.Wrdata_n[0]:self.Wrdata_n[1]]=words[cpt_words]
                sc_frame_payload_tmp[self.Wrdata_n1[0]:self.Wrdata_n1[1]]=words[cpt_words+1]
                sc_frame_payload_tmp[self.Wrdata_n2[0]:self.Wrdata_n2[1]]=words[cpt_words+2]
                sc_frame_payload_tmp[self.Wrdata_n3[0]:self.Wrdata_n3[1]]=0x00000000
                cpt_words = cpt_words + 3
            elif (len(words)-cpt_words)==2:
                sc_frame_payload_tmp[self.Wrdata_n[0]:self.Wrdata_n[1]]=words[cpt_words]
                sc_frame_payload_tmp[self.Wrdata_n1[0]:self.Wrdata_n1[1]]=words[cpt_words+1]
                sc_frame_payload_tmp[self.Wrdata_n2[0]:self.Wrdata_n2[1]]=0x00000000
                sc_frame_payload_tmp[self.Wrdata_n3[0]:self.Wrdata_n3[1]]=0x00000000
                cpt_words = cpt_words + 2
            elif (len(words)-cpt_words)==1:
                sc_frame_payload_tmp[self.Wrdata_n[0]:self.Wrdata_n[1]]=words[cpt_words]
                sc_frame_payload_tmp[self.Wrdata_n1[0]:self.Wrdata_n1[1]]=0x00000000
                sc_frame_payload_tmp[self.Wrdata_n2[0]:self.Wrdata_n2[1]]=0x00000000
                sc_frame_payload_tmp[self.Wrdata_n3[0]:self.Wrdata_n3[1]]=0x00000000
                cpt_words = cpt_words + 1
            else:
                print("bug write fc7_downlink!")
            sc_frame_payload.append( (int(sc_frame_payload_tmp))<<48)
        return sc_frame_payload



 ## format to sc_fifo
    def sc_frame_format_32bits(self,sc_frame):
        cpt_frame = 0
        sc_frame_32bits = []
        while(cpt_frame!=len(sc_frame)):
            sc_frame_32bits.append(int(bf(sc_frame[cpt_frame])[96:128]))
            sc_frame_32bits.append(int(bf(sc_frame[cpt_frame])[64:96]))
            sc_frame_32bits.append(int(bf(sc_frame[cpt_frame])[32:64]))
            sc_frame_32bits.append(int(bf(sc_frame[cpt_frame])[0:32]))
            cpt_frame = cpt_frame + 1
        return sc_frame_32bits

    def send_SC_frame(self,target,rw,address,data):
        self.tx_fifo_SC.write_block(self.sc_frame_format_32bits(self.sc_frame_request(target,rw,address,data)))
