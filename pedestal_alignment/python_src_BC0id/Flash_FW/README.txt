#######################
# --- EXPLANATION --- #
#######################

This folder works with "EPCQ_prog.py" code.

It contain 2 kind of files :

.rpd : Those are the Flash binary files to program.
.map : Those are the .rpd file description with addresses of App firmware and golden firmware.

To repogram the Flash follow instructions in top comment of EPCQ_prog.py