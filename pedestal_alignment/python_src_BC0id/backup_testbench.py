# -*- coding: utf-8 -*-
"""
Created on Wed Feb 24 10:04:18 2021

@author: luciol
"""


import uhal
import time
import sys
import os
import types
import argparse
from scurve import *
from TDC_plot import*
from fc7 import *
from testbench_calibrate_asic import * 
from tools_box import *
from feb_v2_cycloneV_r1 import *


class test_fmc_trigger:
    
    def __init__(self,verbose):
        self.fc7 = fc7(verbose)
        self.feb = feb_v2_cycloneV_r1(self.fc7,verbose)
        self.scurve = scurve("efficacity_scurve.raw")

    def get_trigger_windows(self):
        cpt_timeout = 0
        self.fc7.start_TDC_counters()
        while(self.fc7.get_calibration_done()!=1):
            if cpt_timeout==10:
                self.fc7.stop_TDC_counters()
                return -1
            else:
                time.sleep(0.2)
                cpt_timeout = cpt_timeout + 1
        r=self.fc7.get_counter_trigger()
        self.fc7.stop_TDC_counters()
        return r

    def get_trigger_efficiency(self, strip, threshold_current):
        cpt_timeout = 0
        trig_100 = []
        self.fc7.set_trigger_out_ch2(0) 
        project.feb.configure_10b_dac(strip,threshold_current)
        #self.fc7.start_TDC_counters()
        for i in range(100):
            self.fc7.set_trigger_out_ch2(1) 
            #self.fc7.gen_one_trigger_ch1()       ## generate one pulse
            while (project.fc7.get_calibration_done() != 1):
                    time.sleep(0.1)
            
            r=self.fc7.get_counter_trigger()
            trig_100.append(r)
            self.fc7.set_trigger_out_ch2(0) 
        One = trig_100.count(1)
        efficiency = One 
        #self.fc7.stop_TDC_counters()
        return efficiency


    def find_short_wdw(self):
        cpt_timeout = 0
        print("start tdc acquisition")
        for i in range(30,50):
            self.fc7.set_short_wdw_delay(i)

            list_i=[]
            for j in range(500):
                self.fc7.set_trigger_out_ch2(1) #send one trigger channel 2

                if self.fc7.get_calibration_done() == 1 :
                    r=self.fc7.get_counter_trigger()
                    list_i.append(r)
                    self.fc7.set_trigger_out_ch2(0) 
            print("décalage (x25ns) ",i)
            print("nombre de 1 sur 100 tests : ",list_i.count(1))
            
            



    def pedestal_search(self,strip_ref, first):
        self.feb.tdc.set_enable("fpga_1")

        ### Active all ASIC channels
        all_strips = [x for x in range(0,31,2)]
        self.feb.asic.disable_channel("fpga_1","top",all_strips)
        self.feb.asic.enable_channel("fpga_1","top",all_strips)
        self.feb.asic.configure("fpga_1","all")
        print("ASIC CHANNEL EN TOP: {}".format(project.feb.asic.get_enable_channel("fpga_1","top")))

        # # ### 6bit DAC ###############################################################################
        value=[7,19,24,15,25,23,30,24,28,37,37,36,43,49,57,59]
        strip_refb = ["r_strip_24","strip_24","r_strip_25","strip_25","r_strip_26","strip_26","r_strip_27","strip_27","r_strip_28","strip_28","r_strip_29","strip_29","r_strip_30","strip_30","r_strip_31","strip_31"]
        # ### DAC config ASIC
        for i in range(16) : 
            self.feb.configure_6b_dac(strip_refb[i],value[i])
         ###############################################################################

        for strip in strip_ref:
            print("#############################################")
            print("#### ",strip)
            # project.feb.init_configuration([strip],False) #enable asic channel (mask) and TDC channel
            scurve = []

            #############################
            ### TDc channel / channel
            all_channels = [x for x in range(0,34)]
            channel = self.feb.lookuptable_strips[strip][project.feb.index_lut_tdc_channel]
            print(channel)
            #Configure TDCs
            self.feb.tdc.channel_disable("fpga_1",all_channels)
            self.feb.tdc.channel_enable("fpga_1",[channel])
            self.feb.tdc.set_cmd_valid("fpga_1")
            print(self.feb.tdc.get_enable_status("fpga_1"))
            print(self.feb.tdc.get_channel_status("fpga_1"))


            threshold_min = 350
            threshold_current = threshold_min
            #############################
            eff_list = []
            efficacity = self.get_trigger_efficiency(strip,threshold_current) # return efficiency / threashold
            eff_list.append([efficacity, threshold_current])
            
            cpt = 0
            while(efficacity != 0 or cpt < 100):
                cpt = cpt + 1
                threshold_current = threshold_current + 1
                efficacity = self.get_trigger_efficiency(strip,threshold_current)
                eff_list.append([efficacity, threshold_current])
                scurve.append((threshold_current,efficacity))
                print(eff_list)


            print("strip : ",strip, "efficacity/th :",eff_list)

            if first:
                with open("efficacity_scurve.raw", "w") as outfile:
                    outfile.write("#{},{},asic_{},ch_{},tdc_{}\n".format(strip,project.feb.lookuptable_strips[strip][0],project.feb.lookuptable_strips[strip][1],project.feb.lookuptable_strips[strip][2],project.feb.lookuptable_strips[strip][3]))
                    for x in scurve:
                        outfile.write((str(x[0]) + "," +str(x[1]) + "\n"))
                        first=False
            else:
                with open("efficacity_scurve.raw", "a") as outfile:
                    outfile.write("#{},{},asic_{},ch_{},tdc_{}\n".format(strip,project.feb.lookuptable_strips[strip][0],project.feb.lookuptable_strips[strip][1],project.feb.lookuptable_strips[strip][2],project.feb.lookuptable_strips[strip][3]))
                    for x in scurve:
                        outfile.write((str(x[0]) + "," +str(x[1]) + "\n"))


if __name__ == '__main__':

    project = test_fmc_trigger(False)
  
    ### INIT ###  ###############################################################################
    # project.fc7.init()
    # project.feb.init()
 ###############################################################################



    ## RESET #### ###############################################################################
    # print("RESET GENERAL . . .")
    # project.fc7.set_general_rst(1)
    # # time.sleep(1)
    # project.fc7.set_general_rst(0)
    # project.fc7.init()
    # time.sleep(2)
 ###############################################################################


"""
    #### ASIC CONFIGURATION CHAN. #### ###############################################################################
    all_strips = [x for x in range(0,32)]
    project.feb.asic.init(False) # NOR32RAZ
    project.feb.asic.disable_channel("fpga_1","top",all_strips)
    project.feb.asic.disable_channel("fpga_1","bottom",all_strips)
    project.feb.asic.disable_channel("fpga_0","top",all_strips)
    project.feb.asic.disable_channel("fpga_0","bottom",all_strips)
    project.feb.asic.disable_channel("fpga_2","top",all_strips)
    project.feb.asic.disable_channel("fpga_2","bottom",all_strips)


    project.feb.asic.enable_channel("fpga_1","top",[2])
    ### reconf. asic : 

    project.feb.asic.configure("fpga_1","all")
    project.feb.asic.configure("fpga_0","all")
    project.feb.asic.configure("fpga_2","all")
    # # # print("ASIC CHANNEL EN BOT : {}".format(project.feb.asic.get_enable_channel("fpga_1","bottom")))
    print("ASIC CHANNEL EN TOP : {}".format(project.feb.asic.get_enable_channel("fpga_1","top")))
 ##############################################################################
"""
"""
    ### TDC CONFIG #### ###############################################################################
    all_channels = [x for x in range(0,34)]
    channel=[1]
    # # enable TDCs # #

    # # ### disable all
    project.feb.tdc.channel_disable("fpga_1",all_channels)
    project.feb.tdc.channel_disable("fpga_0",all_channels)
    project.feb.tdc.channel_disable("fpga_2",all_channels)

    # ###enable desired
    # #project.feb.tdc.clear_cmd_valid(fpga_ref)
    # #project.feb.tdc.set_cmd_valid(fpga_ref)
    project.feb.tdc.channel_enable("fpga_1", channel)

    project.feb.tdc.set_enable("fpga_1")
    project.feb.tdc.clear_enable("fpga_0")
    project.feb.tdc.clear_enable("fpga_2")
    #project.feb.tdc.clear_enable("fpga_1")
    project.feb.tdc.set_cmd_valid("fpga_0")
    project.feb.tdc.set_cmd_valid("fpga_2")
    project.feb.tdc.set_cmd_valid("fpga_1")
    print(project.feb.tdc.get_enable_status("fpga_1"))
    print(project.feb.tdc.get_channel_status("fpga_1"))
    ###############################################################################
"""

############### DAC value trigger ####################
# project.feb.configure_6b_dac("strip_31",59)
# project.feb.configure_10b_dac("strip_31",453)
#project.feb.configure_10b_dac("strip_31",600)
######################################################




###reset fifo buffer ### ######################################################
# project.fc7.enable_reset_buf()
# project.fc7.disable_reset_buf()
# project.fc7.stop_acquisition()
###############################################################################



#####################################
## BC0 gen.
#####################################
# project.fc7.set_enable_BC0(0)
# project.fc7.set_transmission_mode(1)
# project.fc7.set_BC0_period(500)
# project.fc7.set_resync_period(500)
   # time.sleep(0.1)
# project.fc7.set_enable_BC0(1)
# project.fc7.set_enable_resync(0)



####################################################################################
### MAIN
####################################################################################

# project.fc7.set_time_window(0.0001) #100 ns
# print(project.fc7.get_time_window())
# project.fc7.set_short_wdw_delay(44)

project.fc7.set_trigger_out_ch2(1) ## stop
project.fc7.set_trigger_out_ch2(0) ## stop
time.sleep(0.1)
project.fc7.set_trigger_out_ch2(1) ## stop
project.fc7.set_trigger_out_ch2(0) ## stop

#project.fc7.gen_periodic_trigger_ch1(10000)
time.sleep(1)
#project.fc7.set_trigger_out_ch1(0) ## stop
####### TDC calibration parameters ################

###################################################
strip_ref = ["r_strip_31"]

#### Channel 15 à 0 TDC FPGA middle ASIC TOP #####
#strip_ref = ["r_strip_24","strip_24","r_strip_25","strip_25","r_strip_26","strip_26","r_strip_27","strip_27","r_strip_28","strip_28","r_strip_29","strip_29","r_strip_30","strip_30","r_strip_31","strip_31"]
##################################################

############### Pedestal search #############################
# project.pedestal_search(strip_ref, True)
# pedestal = project.scurve.read_raw_file('threashold', 'efficacity')
#############################################################



    

    
    

    