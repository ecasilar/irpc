import json
from tools_box import *

def readDacDefaultConfig(fpga, asic):
	# Read dac values from dac6bConfig folder ==> First run Equalization.py to generate config files
	with open("dac6bConfig/dac6b_{}_{}.txt".format(fpga,asic),"r") as file:
		mylist = file.read().splitlines()
		value=[]
		for d in mylist:
			value.append((d.split("="))[1])
	return value


def write_inputs_val():
	# Read default inputs format for irradiation tests
	inputs = {"SC_process" : {"time_pr" : 10},
			  "SEU_process" : {"time_pr" : 30,
			  				   "fpgas" :["fpga_0","fpga_2"],
			  				   "asics" : {"0" :{"top" : 0, "bottom" :0},
							  			  "1" :{"top" : 0, "bottom" :0},
							  			  "2" :{"top" : 0, "bottom" :0},
							  			  }
							  	},
			  "Scurve_process" : {"time_pr" : 300,
				  				  "fpgas" :["fpga_0","fpga_2"],
				  				  "asics" : {"0" :{"top" : 0, "bottom" :0},
								  			 "1" :{"top" : 0, "bottom" :0},
								  			 "2" :{"top" : 0, "bottom" :0},
								  			 }
							  		}
			  }			  
	with open("irradiation_run/inputs.json", 'w') as outfile:
		json.dump(inputs, outfile)


def read_inputs_val():
	with open("irradiation_run/inputs.json",'r') as file:
		data = json.load(file)
	return data


def read_TDC_datas(file_name):
	# read TDC datas from raw file
	with open(file_name,'r') as f:
		mylist = f.read().splitlines()
		r=[]
		data = {}
		cpt = 0
		for d in mylist:
			r.append(d.split(","))
		for x in r:
			if x[0][0:2] == "ID":
				pass
			elif x[0][0] =="#":
				title = x[0][1:]
				if cpt <3 : 
					data[title] = [[],[]]
					cpt +=1
			else:
				data[title][0].append(int(x[0]))
				data[title][1].append(int(x[1])) 
	print(data)
   
	return data
    

if __name__ == "__main__":
	## OK
	#print(readDacDefaultConfig("fpga_0","top"))
	## OK
	#write_inputs_val()
	#read_inputs_val()
	## OK
	#read_TDC_datas("IRRADIATION_backup_runs/18_juin_11h_07m/TDCdatas.raw")
	read_TDC_datas("IRRADIATION_backup_runs/22_juin_09h_21m/TDCdatas.raw")

	## OK
	# reg write channel tdc
	# data=0
	# all_ch = [x for x in range(34)]
	# for i in all_ch:
	# 	data  = data + (1 << i)
	# l=[]
	# data = bf(data)
	# l.append(hex(data[0:16]))
	# l.append(hex(data[16:32]))
	# l.append(data[32:34])
	# print(l)
	#data=[0xFFFF,0xFFFF,3]
