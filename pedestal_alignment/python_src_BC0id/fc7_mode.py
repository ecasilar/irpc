import uhal
import time
import sys
import os
import types
from fc7_registers import *
from tools_box import *

class fc7_mode:

  def __init__(self,fc7_registers,verbose):
  	self.verbose = verbose
  	self.fc7_registers = fc7_registers
  	self.ctrl = self.fc7_registers.get_general_register()
	
 ## Get function 
  def read_status(self):
  	self.ctrl = self.fc7_registers.get_general_register()
  	
  def get_disable_filter(self):
  	return int(bf(self.ctrl)[0])
  
  def get_loopback(self):
  	return int(bf(self.ctrl)[1])
  
  def get_rx_pattern(self):
  	return int(bf(self.ctrl)[2])
  
  def get_tx_pattern(self):
  	return int(bf(self.ctrl)[3])
  
  def get_idle_filter(self):
  	return int(bf(self.ctrl)[4])					
## Set function 

  def write_ctrl(self):
  	self.fc7_registers.set_general_register(self.ctrl)
  	
  def set_disable_filter(self,value):
  	r = bf(self.ctrl)
  	r[0] = bf(value)[0]
  	self.ctrl = int(r)
  	
  def set_loopback(self,value):
  	r = bf(self.ctrl)
  	r[1] = bf(value)[0]
  	self.ctrl = int(r)
  	
  def set_rx_pattern(self,value):
  	r = bf(self.ctrl)
  	r[2] = bf(value)[0]
  	self.ctrl = int(r)
  
  def set_tx_pattern(self,value):
  	r = bf(self.ctrl)
  	r[3] = bf(value)[0]
  	self.ctrl = int(r)
  
  def set_idle_filter(self,value):
  	r = bf(self.ctrl)
  	r[3] = bf(value)[0]
  	self.ctrl = int(r)
  	
## High level function
  def reset(self):
  	self.ctrl =0
  	self.write_ctrl()

  def disable_filter_on(self):
  	self.set_disable_filter(1)
  	self.write_ctrl()
  
  def disable_filter_off(self):
  	self.set_disable_filter(0)
  	self.write_ctrl()	
  
  def loopback_on(self):
  	self.set_loopback(1)
  	self.write_ctrl()
  	
  def loopback_off(self):
  	self.set_loopback(0)
  	self.write_ctrl()
  		
  def rx_pattern_on(self):
  	self.set_rx_pattern(1)
  	self.write_ctrl()
  	
  def rx_pattern_off(self):
  	self.set_rx_pattern(0)
  	self.write_ctrl()		
  
  def tx_pattern_on(self):
  	self.set_rx_pattern(1)
  	self.write_ctrl()
  	
  def tx_pattern_off(self):
  	self.set_rx_pattern(0)
  	self.write_ctrl()
  
  def idle_filter_on(self):
  	self.set_idle_filter(1)
  	self.write_ctrl()
  	
  def idle_filter_off(self):
  	self.set_idle_filter(0)
  	self.write_ctrl()
  												
  def info(self):
  	self.read_status()
  	print("#### FC7 MODE ####")
  	print("--> disable_filter	: {}".format(self.get_disable_filter()))
  	print("--> loopback  		: {}".format(self.get_loopback()))
  	print("--> rx_pattern		: {}".format(self.get_rx_pattern()))
  	print("--> tx_pattern  		: {}".format(self.get_tx_pattern()))	
  	print("--> idle_filter 		: {}".format(self.get_idle_filter()))
  	
  	
  	
