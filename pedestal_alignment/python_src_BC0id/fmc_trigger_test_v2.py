# -*- coding: utf-8 -*-
"""
Created on Wed Feb 24 10:04:18 2021

@author: luciol
"""


import uhal
import time
import sys
import os
import types
import argparse
from scurve import *

from fc7 import *
from testbench_calibrate_asic import * 
from tools_box import *
from feb_v2_cycloneV_r1 import *
#from testbench_basics import *

## Calibrate asic strips threshold with Trigger injection

class test_fmc_trigger:
    
    def __init__(self,verbose):
        self.fc7 = fc7(verbose)
        self.feb = feb_v2_cycloneV_r1(self.fc7,verbose)
        self.scurve_eff = scurve("efficacity_scurve.raw") ### Efficiency Scurve
        #self.basics = testbench_basics() 
    
    def get_TDC_datas(self):
        cpt_timeout = 0
        print("##### Start Acquisition #####")
        self.fc7.start_acquisition()
        while (project.fc7.get_acquisition_status() != True):
            if cpt_timeout==10:
                self.fc7.stop_acquisition()
                return -1
            else:
                time.sleep(0.2)
                cpt_timeout = cpt_timeout + 1
        print("acquisition done : {}".format(project.fc7.get_acquisition_status()))
        self.fc7.stop_acquisition()
        r=self.fc7.uplink.receive_TDC_frame()
        return r  
    def get_trigger_windows(self, window_time_ns = 1):
        
        #Window config.
        self.fc7.set_time_window(window_time)
        self.fc7.set_short_wdw_delay(0)# No delay
                
        cpt_timeout = 0
        self.fc7.start_TDC_counters()
        while(self.fc7.get_calibration_done()!=1):
            if cpt_timeout==10:
                self.fc7.stop_TDC_counters()
                return -1
            else:
                time.sleep(0.2)
                cpt_timeout = cpt_timeout + 1
        r=self.fc7.get_counter_trigger()
        self.fc7.stop_TDC_counters()
        return r

    def get_trigger_efficiency(self, strip, threshold_current):
        
        ### WINDOW ACQUISITION PARAMETERS REQUIRED FOR TRIGGER DETECTION
        #project.fc7.set_time_window(0.0001) #100 ns
        #project.fc7.set_short_wdw_delay(44) #1100 ns
        ############################################## 
        cpt_timeout = 0
        trig_100 = []
        self.fc7.set_trigger_out_ch2(0) 
        project.feb.configure_10b_dac(strip,threshold_current)
        for i in range(100):
            self.fc7.set_trigger_out_ch2(1) #start calibration
            while (project.fc7.get_calibration_done() != 1):
                    time.sleep(0.1)
            
            r=self.fc7.get_counter_trigger()
            trig_100.append(r)
            self.fc7.set_trigger_out_ch2(0) #stop calibration
        One = trig_100.count(1)
        efficiency = One 

        return efficiency

    # find delay of the 100 ns window for maximum trigger efficiency
    def find_short_wdw(self):
        cpt_timeout = 0
        print("start tdc acquisition")
        for i in range(30,50):
            self.fc7.set_short_wdw_delay(i)
            list_i=[]
            for j in range(100):
                self.fc7.set_trigger_out_ch2(1) #send one trigger channel 2 / start calib

                if self.fc7.get_calibration_done() == 1 :
                    r=self.fc7.get_counter_trigger()
                    list_i.append(r)
                    self.fc7.set_trigger_out_ch2(0) 
            print("décalage (x25ns) ",i)
            print("nombre de 1 sur 500 tests : ",list_i.count(1))
            
            
    ### Used to find PetiROC / TDC channels efficiency
    def pedestal_efficiency_search(self,strip_ref, first,all_asic = True, all_TDC = False):
        
        fpga = self.feb.lookuptable_strips[strip_ref[0]][project.feb.index_lut_fpga]
        asic = self.feb.lookuptable_strips[strip_ref[0]][project.feb.index_lut_asic]        
        ######################################Asic/TDC Config.############################################
        all_strips = [x for x in range(0,31,2)]
        ### Active all ASIC channels
        if all_asic == True:
            self.feb.asic.init(False) 
            self.feb.asic.disable_channel(fpga,asic,all_strips)
            self.feb.asic.enable_channel(fpga,asic,all_strips)
            self.feb.asic.configure(fpga,asic)
            print("ASIC CHANNEL EN TOP: {}".format(project.feb.asic.get_enable_channel(fpga,asic)))

        all_channels = [x for x in range(0,34)]
        ### Active all TDC channels
        if all_TDC==True:
            self.feb.tdc.set_enable(fpga)
            #Configure TDCs
            self.feb.tdc.channel_disable(fpga,all_channels)
            self.feb.tdc.channel_enable(fpga,all_channels)
            self.feb.tdc.set_cmd_valid(fpga)
            #Status
            print("FPGA status : ", self.feb.tdc.get_enable_status(fpga))
            print("TDC channel status : ",self.feb.tdc.get_channel_status(fpga))
        ##################################################################################################
        
         
        ### CHANNEL / CHANNEL scanning #################################################
        for strip in strip_ref:
            print("#############################################")
            print("#### ",strip)
            # project.feb.init_configuration([strip],False) #enable asic channel (mask) and TDC channel
            scurve = []
            
            ######################################Asic/TDC Config.############################################
            #############################
            ### Asic channel / channel
            if all_asic == False :
                self.feb.init_configuration([strip],False) #enable asic channel (mask) and TDC channel
                
            #############################
            ### TDc channel / channel
            if all_TDC == False : 
                self.feb.tdc.set_enable(fpga)
                channel = self.feb.lookuptable_strips[strip][project.feb.index_lut_tdc_channel]
                print(channel)
                #Configure TDCs
                self.feb.tdc.channel_disable(fpga,all_channels)
                self.feb.tdc.channel_enable(fpga,[channel])
                self.feb.tdc.set_cmd_valid(fpga)
                #Status
                print(self.feb.tdc.get_enable_status(fpga))
                print(self.feb.tdc.get_channel_status(fpga))
            ##################################################################################################
            
            ### efficiency caluculation ######################################################################
            # Detection of a trigger within a 100 ns window
            ##################################################################################################
            threshold_min = 300
            threshold_current = threshold_min
            #############################
            eff_list = []
            efficacity = self.get_trigger_efficiency(strip,threshold_current) # return efficiency / threashold
            eff_list.append([efficacity, threshold_current])
            
            ### Extend window after 0 efficacity
            cpt = 0
            while(efficacity != 0 or cpt < 300):
                cpt = cpt+1
                threshold_current = threshold_current + 1
                efficacity = self.get_trigger_efficiency(strip,threshold_current)
                eff_list.append([efficacity, threshold_current])
                scurve.append((threshold_current,efficacity))
                print(eff_list)
            if first:
                with open("efficacity_scurve.raw", "w") as outfile:
                    outfile.write("#{},{},asic_{},ch_{},tdc_{}\n".format(strip,project.feb.lookuptable_strips[strip][0],project.feb.lookuptable_strips[strip][1],project.feb.lookuptable_strips[strip][2],project.feb.lookuptable_strips[strip][3]))
                    for x in scurve:
                        outfile.write((str(x[0]) + "," +str(x[1]) + "\n"))
                        first=False
            else:
                with open("efficacity_scurve.raw", "a") as outfile:
                    outfile.write("#{},{},asic_{},ch_{},tdc_{}\n".format(strip,project.feb.lookuptable_strips[strip][0],project.feb.lookuptable_strips[strip][1],project.feb.lookuptable_strips[strip][2],project.feb.lookuptable_strips[strip][3]))
                    for x in scurve:
                        outfile.write((str(x[0]) + "," +str(x[1]) + "\n"))


if __name__ == '__main__':

    project = test_fmc_trigger(False)
  
   
####################################################################################
### MAIN Method to plot efficiency of each strips
####################################################################################

########################### Pedestal search efficiency #############################
### Method : 100 ns acq. calibrate to detect one trigger send ch 2 [injection strip 31 fpga middle asic top]
### DAC 6bit loaded with fixed values
####################################################################################

"""
############### DAC value trigger detected strip 31 ch 0 ####################
#project.feb.configure_10b_dac("strip_31",453)   # trigger detected
project.fc7.set_time_window(0.0001)             # 100 ns acq.time
project.fc7.set_short_wdw_delay(45)             # 1100 ns values beetween 44 and 47 = 100% efficiency trigger detected into acq.time = 100 ns
project.fc7.set_trigger_out_ch2(0)              # reset trigger enable
##############################################################
"""

"""
### 6Bits DAC adjustement
###################################################
#### Channel 15 à 0 TDC FPGA middle ASIC TOP #####
strip_ref = ["r_strip_24","strip_24","r_strip_25","strip_25","r_strip_26","strip_26","r_strip_27","strip_27","r_strip_28","strip_28","r_strip_29","strip_29","r_strip_30","strip_30","r_strip_31","strip_31"]
##################################################
##### 6bit DAC setup FPGA_1 asic top ##########################################
value=[7,19,24,15,25,23,30,24,28,37,37,36,43,49,57,59]
### DAC config ASIC
for i in range(16) : 
    project.feb.configure_6b_dac(strip_ref[i],value[i])
###############################################################################
"""

"""
### EFFICIENCY AFTER EQUALISATION
#############################################################
project.pedestal_efficiency_search(strip_ref, True, False)          #all-asic : True  | all-tdc : False
#project.pedestal_efficiency_search(strip_ref, False, False)          #all-asic : False  | all-tdc : False
pedestal = project.scurve.read_raw_file('threashold', 'efficacity')
#############################################################
"""


######################################Asic/TDC Config.############################################

all_strips = [x for x in range(0,31,2)]
### Active all ASIC channels

project.feb.asic.init(False) 
project.feb.asic.disable_channel("fpga_1","top",all_strips)
project.feb.asic.enable_channel("fpga_1","top",all_strips)
project.feb.asic.configure("fpga_1","top")
print("ASIC CHANNEL EN TOP: {}".format(project.feb.asic.get_enable_channel("fpga_1","top")),"\n")

all_channels = [x for x in range(0,16)]
### Active all TDC channels
project.feb.tdc.init(False)
project.feb.tdc.set_enable("fpga_1")
#Configure TDCs
project.feb.tdc.channel_disable("fpga_1",all_channels)
project.feb.tdc.channel_enable("fpga_1",all_channels)
project.feb.tdc.set_cmd_valid("fpga_1")
#Status
print("FPGA status : ", project.feb.tdc.get_enable_status("fpga_1"))
print("TDC channel status : ",project.feb.tdc.get_channel_status("fpga_1"), "\n")
##################################################################################################

project.feb.configure_10b_dac("strip_31",200)   # trigger detected

### Start circular buffer external trigger acquisition

# project.fc7.set_nb_frame_acquisition(10)
# project.fc7.set_acquisition_mode("TDC_Count")

project.fc7.set_circular_buffer_size(10)
project.fc7.set_acquisition_mode("Circular_Buffer_External")

project.fc7.start_acquisition()

##
time.sleep(5)
##
print("###############################")
print("number of data in fifo buffer : {}".format(project.fc7.get_nb_word_in_fifo_frame()))
print("############################### \n")


## external trigger
project.fc7.set_trigger_out_ch2(1)


time.sleep(1)

print("###############################")
print("lecture")
print("############################### \n")

## read TDC datas
data = project.fc7.uplink.receive_TDC_frame()
project.fc7.save_TDC_datas(data)
project.fc7.read_TDC_datas()

print("###############################")
print("number of data in fifo buffer : {}".format(project.fc7.get_nb_word_in_fifo_frame()))
print("############################### \n")

# reset acq.
project.fc7.set_trigger_out_ch2(0)
project.fc7.stop_acquisition()

"""

project.fc7.start_acquisition()

## read TDC datas
data = project.fc7.uplink.receive_TDC_frame()
project.fc7.save_TDC_datas(data)
project.fc7.read_TDC_datas()    

project.fc7.stop_acquisition()
"""