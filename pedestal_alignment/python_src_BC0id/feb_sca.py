import time
import sys
import os
import types
from fc7_registers import *
from feb_ic import *
from tools_box import *

###---------------------------------------------###
#     This class is the FEB Slow control          #
###---------------------------------------------###
class feb_sca:

    def __init__(self,fc7_registers,verbose):
        self.verbose = verbose
        self.fc7_registers = fc7_registers
        self.feb_ic = feb_ic(self.fc7_registers,verbose)
        self.config = [0x00000000,0x00000000,0x00000000]
        self.status = [0x00000000,0x00000000]
        self.pulse  = 0x00000000
        self.config = self.fc7_registers.get_sca_config()
        self.status = self.fc7_registers.get_sca_status()
        self.pulse  = self.fc7_registers.get_sca_pulse()
        self.CRB_value = 0x1c
        self.CRC_value = 0x00
        self.CRD_value = 0x18 # J#0x10
        self.GPIO_direction = { # [value, channel, direction1 to output, 0 for input]
     	"CRC_ERROR_LEFT"   : [0,0,0],
     	"CRC_ERROR_MIDDLE" : [0,1,0],
     	"CRC_ERROR_RIGHT"  : [0,2,0],
     	"NCONFIG_LEFT"     : [0,3,1],
     	"NCONFIG_MIDDLE"   : [0,4,1],
     	"NCONFIG_RIGHT"    : [0,5,1],
     	"INIT_DONE_LEFT"   : [0,6,0],
     	"INIT_DONE_MIDDLE" : [0,7,0],
     	"INIT_DONE_RIGHT"  : [0,8,0],
     	"ENABLE_POWER_FPGA": [0,9,1],
     	"SOFT_RESET_FPGA"  : [0,10,1],
     	"FAULT_4V"         : [0,11,0],
     	"FAULT_2V"         : [0,12,0],
     	"T_ALTERT_FPGA_LEFT"   : [0,13,0],
     	"T_ALTERT_FPGA_MIDDLE" : [0,14,0],
     	"T_ALTERT_FPGA_RIGHT"  : [0,15,0],
     	"T_ALTERT_2V" 	       : [0,16,0],
     	"T_ALTERT_4V"          : [0,17,0],
     	"SPARE_IO_LEFT_3"      : [0,18,0],
     	"SPARE_IO_LEFT_2"      : [0,19,0],
     	"SPARE_IO_LEFT_1"      : [0,20,0],
     	"SPARE_IO_LEFT_0"      : [0,21,0],
     	"SHDN_2V"  	       : [0,22,1],
     	"SPARE_IO_MIDDLE_3"    : [0,23,0],
     	"SPARE_IO_MIDDLE_2"    : [0,24,0],
     	"SPARE_IO_MIDDLE_1"    : [0,25,0],
     	"SPARE_IO_MIDDLE_0"    : [0,26,0],
     	"SHDN_4V"  	       : [0,27,1],
     	"SPARE_IO_RIGHT_3"     : [0,28,0],
     	"SPARE_IO_RIGHT_2"     : [0,29,0],
     	"SPARE_IO_RIGHT_1"     : [0,30,0],
     	"SPARE_IO_RIGHT_0"     : [0,31,0]}


 ## Get function
  # config reg
    def read_config_registers(self):
        self.config = self.fc7_registers.get_sca_config()
        return self.config

    def get_tx_address(self):
      	return int(bf(self.config[0])[0:8])

    def get_tx_channel(self):
      	return int(bf(self.config[0])[8:16])

    def get_tx_command(self):
      	return int(bf(self.config[0])[16:24])

    def get_tx_len(self):
      	return int(bf(self.config[0])[24:32])

    def get_tx_data(self):
      	return int(bf(self.config[1]))

    def get_rx_reset(self):
      	return int(bf(self.config[2])[0])

    def get_tx_reset(self):
      	return int(bf(self.config[2])[1])

      # status reg

    def read_status_registers(self):
      self.status = self.fc7_registers.get_sca_status()
      return self.status




    def get_rx_address(self):
        return int(bf(self.status[0])[0:8])

    def get_rx_control(self):
        return int(bf(self.status[0])[8:16])

    def get_rx_trans_id(self):
        return int(bf(self.status[0])[16:24])

    def get_rx_rdy(self):
      	return int(bf(self.status[0])[31])

    def get_rx_channel(self):
        return int(bf(self.status[1])[0:8])

    def get_rx_len(self):
        return int(bf(self.status[1])[8:16])

    def get_rx_error(self):
        return int(bf(self.status[1])[16:24])

    def get_rx_data(self):
        return int(bf(self.status[2])[0:32])


      # pulse reg
    def read_pulse_register(self):
        self.pulse = self.fc7_registers.get_sca_pulse()
        return self.pulse

    def get_pulse_reset(self):
      	return int(bf(self.pulse)[0])

    def get_pulse_connect(self):
      	return int(bf(self.pulse)[1])

    def get_pulse_start_transaction(self):
      	return int(bf(self.pulse)[2])

     ## Set function

      # config reg
    def write_config_registers(self):
        self.fc7_registers.set_sca_config(self.config)

    def set_tx_address(self,value):
      	r= bf(self.config[0])
      	r[0:8] = bf(value)[0:8]
      	self.config[0]= int(r)

    def set_tx_channel(self,value):
      	r= bf(self.config[0])
      	r[8:16] = bf(value)[0:8]
      	self.config[0]= int(r)

    def set_tx_command(self,value):
      	r= bf(self.config[0])
      	r[16:24] = bf(value)[0:8]
      	self.config[0]= int(r)

    def set_tx_len(self,value):
      	r= bf(self.config[0])
      	r[24:32] = bf(value)[0:8]
      	self.config[0]= int(r)

    def set_tx_data(self,value):
      	r= bf(self.config[1])
      	r[0:32] = bf(value)[0:32]
      	self.config[1]= int(r)

    def set_rx_reset(self,value):
      	r= bf(self.config[2])
      	r[0] = bf(value)[0]
      	self.config[2]= int(r)

    def set_tx_reset(self,value):
      	r= bf(self.config[2])
      	r[1] = bf(value)[0]
      	self.config[2]= int(r)


      ## Pulse reg
    def write_pulse_register(self):
      	self.fc7_registers.set_sca_pulse(self.pulse)

    def set_reset_sca_rx_received(self,value):
        r= bf(self.config[2])
        r[3] = bf(value)[0]
        self.config[2]= int(r)
        
    def set_pulse_reset(self,value):
      	r= bf(self.pulse)
      	r[0] = bf(value)[0]
      	self.pulse= int(r)
      	self.write_pulse_register()

    def set_pulse_connect(self,value):
      	r= bf(self.pulse)
      	r[1] = bf(value)[0]
      	self.pulse= int(r)
      	self.write_pulse_register()

    def set_pulse_start_transaction(self,value):
      	r= bf(self.pulse)
      	r[2] = bf(value)[0]
      	self.pulse= int(r)
      	self.write_pulse_register()

     ## FONCTION CONTROL BLOC SCA FC7
    def pulse_tx_rst(self):
      	self.set_tx_reset(1)
      	self.write_config_registers()
      	time.sleep(0.05)
      	self.set_tx_reset(0)
      	self.write_config_registers()

    def pulse_rx_rst(self):
      	self.set_rx_reset(1)
      	self.write_config_registers()
      	time.sleep(0.05)
      	self.set_rx_reset(0)
      	self.write_config_registers()

    def reset_rx_received_SCA(self):
        self.set_reset_sca_rx_received(1)
        self.set_reset_sca_rx_received(0)
        time.sleep(0.05)

    def wait_rx_recieved(self):
      time.sleep(0.05)
      rdy      = self.get_rx_rdy()
      rx_error = self.get_rx_error()
      #self.feb_ic.pulse_tx_wr() # reg rx_received_o ==> 0
      if rdy !=1 :
        print("rx_received_o : not ready !")
        return False
      elif (rx_error != 0):
        self.print_error(rx_error)
        return False
      else:
        return True

    def print_error(self,value):
      	r = bf(value)
      	if   int(r[0])== 1: print("Generic error flag")
      	elif int(r[1])== 1: print("Invalid channel request")
      	elif int(r[2])== 1: print("Invalid command request")
      	elif int(r[3])== 1: print("Invalid transaction number request")
      	elif int(r[4])== 1: print("Invalid length")
      	elif int(r[5])== 1: print("Channel not enable")
      	elif int(r[6])== 1: print("Channel currently busy")
      	elif int(r[7])== 1: print("Command in treatment")
      	else: print("unknown error")

    def reset(self):
      	self.set_pulse_reset(1)
      	time.sleep(0.05)
      	self.set_pulse_reset(0)
      	self.wait_rx_recieved()

    def connect(self):
      	self.set_pulse_connect(1)
      	time.sleep(0.05)
      	self.set_pulse_connect(0)
      	self.wait_rx_recieved()


    def start_transaction(self):
      #self.feb_ic.pulse_tx_wr() # reg rx_received_o ==> 0
      self.reset_rx_received_SCA() # new reset of rx_received_reg value
      self.set_pulse_start_transaction(1)
      time.sleep(0.05)
      self.set_pulse_start_transaction(0)
      self.wait_rx_recieved()

    def print_config(self):
      	self.read_config_registers()
      	print("## SCA config register ##")
      	print("tx_address : {}".format(hex(self.get_tx_address())))
      	print("tx_channel : {}".format(hex(self.get_tx_channel())))
      	print("tx_command : {}".format(hex(self.get_tx_command())))
      	print("tx_len     : {}".format(hex(self.get_tx_len())))
      	print("tx_data    : {}".format(hex(self.get_tx_data())))
      	print("tx_reset   : {}".format(hex(self.get_tx_reset())))
      	print("rx_reset   : {}".format(hex(self.get_rx_reset())))

    def print_status(self):
      	self.read_status_registers()
      	print("## SCA status register ##")
      	print("rx_address : {}".format(hex(self.get_rx_address())))
      	print("rx_channel : {}".format(hex(self.get_rx_channel())))
      	print("rx_control : {}".format(hex(self.get_rx_control())))
      	print("rx_id      : {}".format(hex(self.get_rx_trans_id())))
      	print("rx_rdy     : {}".format(hex(self.get_rx_rdy())))
      	print("rx_error   : {}".format(hex(self.get_rx_error())))
      	print("rx_len     : {}".format(hex(self.get_rx_len())))
      	print("rx_data    : {}".format(hex(self.get_rx_data())))

    def print_pulse(self):
      	self.read_pulse_register()
      	print("## SCA pulse register ##")
      	print("reset    : {}".format(hex(self.get_pulse_reset())))
      	print("connect  : {}".format(hex(self.get_pulse_connect())))
      	print("start_transaction : {}".format(hex(self.get_pulse_start_transaction())))

     ### fonction d'acces au registres du sca
    def get_id(self):
        self.set_tx_address(0x00)
        self.set_tx_channel(0x14)
        self.set_tx_command(0xD1)
        self.set_tx_len(0x04)
        self.set_tx_data(0x00000001)
        self.write_config_registers()
        self.start_transaction()
        self.read_status_registers()
        return self.get_rx_data()

      ## REGISTRE CTRL_B
    def set_CRB(self,value):
        self.set_tx_address(0x00)
        self.set_tx_channel(0x00)
        self.set_tx_command(0x02)
        self.set_tx_len(0x04)
        self.set_tx_data((value<<24))
        self.write_config_registers()
        self.start_transaction()

    def get_CRB(self):
        self.set_tx_address(0x00)
        self.set_tx_channel(0x00)
        self.set_tx_command(0x03)
        self.set_tx_len(0x04)
        self.set_tx_data(0x00000000)
        self.write_config_registers()
        self.start_transaction()
        self.read_status_registers()
        return (self.get_rx_data()>>24)

      ## REGISTRE CTRL_C
    def set_CRC(self,value):
        self.set_tx_address(0x00)
        self.set_tx_channel(0x00)
        self.set_tx_command(0x04)
        self.set_tx_len(0x04)
        self.set_tx_data((value<<24))
        self.write_config_registers()
        self.start_transaction()

    def get_CRC(self):
        self.set_tx_address(0x00)
        self.set_tx_channel(0x00)
        self.set_tx_command(0x05)
        self.set_tx_len(0x04)
        self.set_tx_data(0x00000000)
        self.write_config_registers()
        self.start_transaction()
        self.read_status_registers()
        return (self.get_rx_data()>>24)

      ## REGISTRE CTRL_D
    def set_CRD(self,value):
        self.set_tx_address(0x00)
        self.set_tx_channel(0x00)
        self.set_tx_command(0x06)
        self.set_tx_len(0x04)
        self.set_tx_data((value<<24))
        self.write_config_registers()
        self.start_transaction()

    def get_CRD(self):
        self.set_tx_address(0x00)
        self.set_tx_channel(0x00)
        self.set_tx_command(0x07)
        self.set_tx_len(0x04)
        self.set_tx_data(0x00000000)
        self.write_config_registers()
        self.start_transaction()
        self.read_status_registers()
        return (self.get_rx_data()>>24)

      ## REGISTRE SEU COUNTER
    def get_SEU_counter(self):
        self.set_tx_address(0x00)
        self.set_tx_channel(0x13)
        self.set_tx_command(0xF1)
        self.set_tx_len(0x04)
        self.set_tx_data(0x00000000)
        self.write_config_registers()
        self.start_transaction()
        self.read_status_registers()
        return self.get_rx_data()

       ## REGISTRE SEU RESET
    def reset_SEU_counter(self):
        self.set_tx_address(0x00)
        self.set_tx_channel(0x13)
        self.set_tx_command(0xF0)
        self.set_tx_len(0x04)
        self.set_tx_data(0x00000000)
        self.write_config_registers()
        self.start_transaction()
        self.read_status_registers()
        return self.get_rx_data()

     ##################################################################################################
    def init(self):
      	print("################################################")
      	self.pulse_tx_rst()
      	self.pulse_rx_rst()
      	self.reset()
      	self.connect()
      	self.configure()
      	time.sleep(1)
      	if self.verbose: self.print()

    def configure(self):
      	step=0
      	nb_step=6
      	self.set_CRB(self.CRB_value)
      	step = step + 1
      	print("Write SCA configuration : {}%".format(round((step/nb_step)*100),0),end="\r")
      	sys.stdout.flush()
      	self.set_CRC(self.CRC_value)
      	step = step + 1
      	print("Write SCA configuration : {}%".format(round((step/nb_step)*100),0),end="\r")
      	sys.stdout.flush()
      	self.set_CRD(self.CRD_value)
      	step = step + 1
      	print("Write SCA configuration : {}%".format(round((step/nb_step)*100),0),end="\r")
      	sys.stdout.flush()
      	self.i2c_configure(0,"100kHz",2,"CMOS")
      	step = step + 1
      	print("Write SCA configuration : {}%".format(round((step/nb_step)*100),0),end="\r")
      	sys.stdout.flush()
      	self.i2c_configure(1,"100kHz",2,"CMOS")
      	step = step + 1
      	print("Write SCA configuration : {}%".format(round((step/nb_step)*100),0),end="\r")
      	sys.stdout.flush()
      	self.gpio_configure()
      	step = step + 1
      	print("Write SCA configuration : {}%".format(round((step/nb_step)*100),0),end="\r")
      	sys.stdout.flush()
      	print("")
      	print(bcolors.GREEN + "SCA Configuration [OK]" + bcolors.ENDC)


    def check_config(self):
        #print(hex(self.get_CRB()),hex(self.CRB_value),hex(self.get_CRC()),hex(self.CRC_value),hex(self.get_CRD()),hex(self.CRD_value))
        if (self.get_CRB()==self.CRB_value and self.get_CRC()==self.CRC_value and self.get_CRD()==self.CRD_value):
            return True
        else:
            return False


    ############### ADC ################

    def read_adc(self,channel):
      	#set mux register
      	self.set_tx_address(0x00)
      	self.set_tx_channel(0x14)
      	self.set_tx_command(0x50)
      	self.set_tx_len(0x04)
      	self.set_tx_data(channel)
      	self.write_config_registers()
      	self.start_transaction()
      	#read adc value
      	self.set_tx_address(0x00)
      	self.set_tx_channel(0x14)
      	self.set_tx_command(0x02)
      	self.set_tx_len(0x04)
      	self.set_tx_data(0x00000001)
      	self.write_config_registers()
      	self.start_transaction()
      	self.read_status_registers()
      	return self.get_rx_data()

    def i2c_configure(self,channel,freq,nbyte,sclmode):
      	tx_data = bf(0x00000000)
      	if channel==0:
      		self.set_tx_channel(0x03)
      	elif channel==1:
      		self.set_tx_channel(0x04)
      	else:
      		print(bcolors.FAIL +"sca i2c channel error !" + bcolors.ENDC)
      	if freq=="100kHz":
      		tx_data[0:2]=0
      	elif freq=="200kHz":
      		tx_data[0:2]=1
      	elif freq=="400kHz":
      		tx_data[0:2]=2
      	elif freq=="1MHz":
      		tx_data[0:2]=3
      	else:
      		print(bcolors.FAIL +"error i2c freq value" + bcolors.ENDC)
      	if sclmode=="Open-drain":
      		tx_data[7]=0
      	elif sclmode=="CMOS":
      		tx_data[7]=1
      	else:
      		print(bcolors.FAIL +"error scl mode value" + bcolors.ENDC)

      	tx_data[2:7]=bf(nbyte)[0:5]
      	self.set_tx_address(0x00)
      	self.set_tx_command(0x30)
      	self.set_tx_len(0x04)

      	self.set_tx_data(int(tx_data)) #  -- SCL->out, N_BYTES->2, SCL->400kHz
      	self.write_config_registers()
      	self.start_transaction()


    def read_i2c_ctrl_register(self,channel):
      	if channel==0:
      		self.set_tx_channel(0x03)
      	elif channel==1:
      		self.set_tx_channel(0x04)
      	else:
      		print(bcolors.FAIL +"sca i2c channel error !" + bcolors.ENDC)
      	self.set_tx_address(0x00)
      	self.set_tx_command(0x31)
      	self.set_tx_len(0x04)
      	self.set_tx_data(0x00000000) #  -- SCL->out, N_BYTES->2, SCL->400kHz
      	self.write_config_registers()
      	self.start_transaction()
      	self.read_status_registers()
      	return self.get_rx_data()


    def read_i2c(self,channel,i2c_address,reg_address):
      	#send command read
      	tx_data = bf(0x00000000)
      	if channel==0:
      		self.set_tx_channel(0x03)
      	elif channel==1:
      		self.set_tx_channel(0x04)
      	else:
      		print(bcolors.FAIL +"sca i2c channel error !" + bcolors.ENDC)
      	self.set_tx_address(0x00)
      	self.set_tx_command(0x82)
      	self.set_tx_len(0x04)
      	tx_data[16:24] = bf(reg_address)[0:8]
      	tx_data[24:32] = bf(i2c_address)[0:8]
      	self.set_tx_data(int(tx_data))
      	self.write_config_registers()
      	self.start_transaction()

      	if channel==0:
      		self.set_tx_channel(0x03)
      	elif channel==1:
      		#read 1
      		self.set_tx_channel(0x04)
      		self.set_tx_address(0x00)
      		self.set_tx_command(0xDE) # multibyte read
      		self.set_tx_len(0x04)
      		tx_data[16:24] = bf(reg_address)[0:8]
      		tx_data[24:32] = bf(i2c_address)[0:8]
      		self.set_tx_data(int(tx_data))
      		self.write_config_registers()
      		self.start_transaction()
      		#read 2
      		self.set_tx_channel(0x04)
      		self.set_tx_address(0x00)
      		self.set_tx_command(0x71) # read data register
      		self.set_tx_len(0x04)
      		tx_data[16:24] = bf(reg_address)[0:8]
      		tx_data[24:32] = bf(i2c_address)[0:8]
      		self.set_tx_data(int(tx_data))
      		self.write_config_registers()
      		self.start_transaction()
      		self.read_status_registers()
      		return self.get_rx_data()

      	else:
      		print(bcolors.FAIL +"sca i2c channel error !"+ bcolors.ENDC)
      		return -1



    def write_i2c(channel,address):
      	print("")


    ############### GPIO ################
    def gpio_configure(self):
      	tx_data = bf(0x00000000)
      	self.set_tx_channel(0x02)
      	self.set_tx_address(0x00)
      	self.set_tx_command(0x20)
      	self.set_tx_len(0x04)
      	for key,value in self.GPIO_direction.items(): # set GPIO direction
      		tx_data[value[1]] = value[2]
      	self.set_tx_data(int(tx_data))
      	self.write_config_registers()
      	self.start_transaction()

    def read_GPIO_direction(self):
      self.set_tx_channel(0x02)
      self.set_tx_address(0x00)
      self.set_tx_command(0x21)
      self.set_tx_len(0x04)
      self.set_tx_data(0x00000000)
      self.write_config_registers()
      self.start_transaction()
      self.read_status_registers()
      return int(self.get_rx_data())

    def get_GPIO_R(self):
      	self.set_tx_channel(0x02)
      	self.set_tx_address(0x00)
      	self.set_tx_command(0x01)
      	self.set_tx_len(0x04)
      	self.set_tx_data(0x00000000)
      	self.write_config_registers()
      	self.start_transaction()
      	self.read_status_registers()
      	return int(self.get_rx_data())

    def get_GPIO_W(self):
      	self.set_tx_channel(0x02)
      	self.set_tx_address(0x00)
      	self.set_tx_command(0x11)
      	self.set_tx_len(0x04)
      	self.set_tx_data(0x00000000)
      	self.write_config_registers()
      	self.start_transaction()
      	self.read_status_registers()
      	return int(self.get_rx_data())

    def set_GPIO(self,channel,value):
      	r= bf(self.get_GPIO_W())
      	self.set_tx_channel(0x02)
      	self.set_tx_address(0x00)
      	self.set_tx_command(0x10)
      	self.set_tx_len(0x04)
      	r[channel]=value
      	self.set_tx_data(int(r))
      	self.write_config_registers()
      	self.start_transaction()


    def print_gpio(self):
      	print("#### SCA configuration ####")
      	reg_b=bf(self.get_CRB())
      	reg_c=bf(self.get_CRC())
      	reg_d=bf(self.get_CRD())
      	print("--> SCA chip ID      : {}".format(hex(self.get_id())))
      	if reg_b[1]==1: print("--> SPI interface    : enable")
      	else : print("--> SPI interface    : disable")
      	if reg_b[2]==1: print("--> GPIO interface   : enable")
      	else : print("--> GPIO interface   : disable")
      	if reg_b[3]==1: print("--> I2C_0 interface  : enable")
      	else : print("--> I2C_0 interface  : disable")
      	if reg_b[4]==1: print("--> I2C_1 interface  : enable")
      	else : print("--> I2C_1 interface  : disable")
      	if reg_b[5]==1: print("--> I2C_2 interface : enable")
      	else : print("--> I2C_2 interface  : disable")
      	if reg_b[6]==1: print("--> I2C_3 interface : enable")
      	else : print("--> I2C_3 interface  : disable")
      	if reg_b[7]==1: print("--> I2C_4 interface : enable")
      	else : print("--> I2C_4 interface  : disable")
      	if reg_c[0]==1: print("--> I2C_5 interface : enable")
      	else : print("--> I2C_5 interface  : disable")
      	if reg_c[1]==1: print("--> I2C_6 interface : enable")
      	else : print("--> I2C_6 interface  : disable")
      	if reg_c[2]==1: print("--> I2C_7 interface : enable")
      	else : print("--> I2C_7 interface  : disable")
      	if reg_c[3]==1: print("--> I2C_8 interface : enable")
      	else : print("--> I2C_8 interface  : disable")
      	if reg_c[4]==1: print("--> I2C_9 interface : enable")
      	else : print("--> I2C_9 interface  : disable")
      	if reg_c[5]==1: print("--> I2C_10 interface : enable")
      	else : print("--> I2C_10 interface : disable")
      	if reg_c[6]==1: print("--> I2C_11 interface : enable")
      	else : print("--> I2C_11 interface : disable")
      	if reg_c[7]==1: print("--> I2C_12 interface : enable")
      	else : print("--> I2C_12 interface : disable")
      	if reg_d[0]==1: print("--> I2C_13 interface : enable")
      	else : print("--> I2C_13 interface : disable")
      	if reg_d[1]==1: print("--> I2C_14 interface : enable")
      	else : print("--> I2C_14 interface : disable")
      	if reg_d[2]==1: print("--> I2C_15 interface : enable")
      	else : print("--> I2C_15 interface : disable")
      	if reg_d[3]==1: print("--> JTAG interface   : enable")
      	else : print("--> JTAG interface   : disable")
      	if reg_d[4]==1: print("--> ADC interface    : enable")
      	else : print("--> ADC interface    : disable")

    def print(self):
        self.print_config()
        self.print_status()
        self.print_pulse()
        self.print_gpio()

        # if channel==0:
        #  self.set_tx_channel(0x03)
        #  self.set_tx_address(0x00)
        #  self.set_tx_command(0x86) # single byte read
        #  self.set_tx_len(0x04)
        #  tx_data[16:24] = bf(reg_address)[0:8]
        #  tx_data[24:32] = bf(i2c_address)[0:8]
        #  self.set_tx_data(int(tx_data))
        #  self.write_config_registers()
        #  self.start_transaction()
        #  self.read_status_registers()
        #  return self.get_rx_data()
