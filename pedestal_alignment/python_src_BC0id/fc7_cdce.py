import uhal
import time
import sys
import os
import types
from tools_box import *

###---------------------------------------------###
#     This class permit FC7 CDCE configuration     #
###---------------------------------------------###

class fc7_cdce:
    def __init__(self,fc7_registers,verbose):
      	self.verbose = verbose
      	self.fc7_registers = fc7_registers
      	self.config_120MHz=[0xEB04032,0x6804031,0x6804030,0xEB04032,0x6886031,0xFC000AA,0x04BE056,0xBD0037F,0x2000997]
      	self.status = self.fc7_registers.get_cdce_status()
      	self.spi_command = self.fc7_registers.get_cdce_spi_command()
      	self.spi_tx_data = self.fc7_registers.get_cdce_spi_tx_data()
      	self.spi_rx_data = self.fc7_registers.get_cdce_spi_rx_data()

 ## Get function
    def read_spi_tx_command(self):
        self.spi_command = self.fc7_registers.get_cdce_spi_command()
        return self.spi_command

    def read_spi_tx_data(self):
      	self.spi_tx_data = self.fc7_registers.get_cdce_spi_tx_data()
      	return self.spi_tx_data

    def read_spi_rx_data(self):
      	self.spi_rx_data = self.fc7_registers.get_cdce_spi_rx_data()
      	return self.spi_rx_data

    def read_status(self):
      	self.status = self.fc7_registers.get_cdce_status()
      	return self.status

    def get_enable(self):
      	return int(bf(self.spi_command)[31])

    def get_ssdelay(self):
        return int(bf(self.spi_command)[18:28])

    def get_hold(self):
      	return int(bf(self.spi_command)[15:18])

    def get_msb_first(self):
        return int(bf(self.spi_command)[14])

    def get_cpha(self):
      	return int(bf(self.spi_command)[13])

    def get_cpol(self):
      	return int(bf(self.spi_command)[12])

    def get_prescaler(self):
        return int(bf(self.spi_command)[0:12])

    def get_pwrdown_b(self):
        return int(bf(self.status)[0])

    def get_ref_sel(self):
        return int(bf(self.status)[1])

    def get_sync(self):
        return int(bf(self.status)[2])

    def get_pll_locked(self):
        return int(bf(self.status)[16])

  ## Set function
    def write_status(self):
        self.fc7_registers.set_cdce_status(self.status)

    def write_spi_tx_command(self):
      	self.fc7_registers.set_cdce_spi_command(self.spi_command)

    def write_spi_tx_data(self):
      	self.fc7_registers.set_cdce_spi_tx_data(self.spi_tx_data)

    def set_pwrdown_b(self,value):
        r= bf(self.status)
        r[0] = bf(value)[0]
        self.status = int(r)

    def set_ref_sel(self,value):
        r= bf(self.status)
        r[1] = bf(value)[0]
        self.status = int(r)

    def set_sync(self,value):
        r= bf(self.status)
        r[2] = bf(value)[0]
        self.status = int(r)

    def set_enable(self,value):
        r= bf(self.spi_command)
        r[31] = bf(value)[0]
        self.spi_command = int(r)

    def set_ssdelay(self,value):
        r= bf(self.spi_command)
        r[18:28] = bf(value)[0:11]
        self.spi_command = int(r)

    def set_hold(self,value):
        r= bf(self.spi_command)
        r[15:18] = bf(value)[0:4]
        self.spi_command = int(r)

    def set_msb_first(self,value):
        r= bf(self.spi_command)
        r[14] = bf(value)[0]
        self.spi_command = int(r)

    def set_cpha(self,value):
        r= bf(self.spi_command)
        r[13] = bf(value)[0]
        self.spi_command = int(r)

    def set_cpol(self,value):
        r= bf(self.spi_command)
        r[12] = bf(value)[0]
        self.spi_command = int(r)

    def set_prescaler(self,value):
        r= bf(self.spi_command)
        r[0:12] = bf(value)[0]
        self.spi_command = int(r)


###############################################################
## HIGH LEVEL FUNCTION
###############################################################
    #read 
    def spi_read(self,addr_reg):
        self.spi_tx_data= (addr_reg << 4) + 0xE
        self.write_spi_tx_data()
        self.spi_command = 0x8FA38014
        self.write_spi_tx_command()
        time.sleep(0.2)
        self.write_spi_tx_command()
        time.sleep(0.2)
        return self.read_spi_rx_data()

    def spi_write(self,addr_reg,value):
        self.spi_tx_data = (value << 4) + addr_reg
        self.write_spi_tx_data()
        self.spi_command = 0x8FA38014
        self.write_spi_tx_command()
        time.sleep(0.2)

    def reset(self):
        self.set_pwrdown_b(0)
        self.write_status()
        time.sleep(0.5)
        self.set_pwrdown_b(1)
        self.write_status()
        time.sleep(0.5)

    def write_config(self,reg_cdce):
        i=0
        nb=9
        for x in reg_cdce:
            self.spi_write(i,x)
            time.sleep(0.2)
            i=i+1
            print("Write CDCE configuration : {}%".format(round((i/nb)*100),0),end="\r")
        print("")

    def read_config(self):
        reg_cdce = [0,0,0,0,0,0,0,0]
        i=0
        nb=8
        for x in reg_cdce:
        	reg_cdce[i]=int(bf(self.spi_read(i))[4:32])
        	i=i+1
        	print("Read CDCE configuration : {}%".format(round((i/nb)*100),0),end="\r")

        print("")
        return reg_cdce

    def check_configuration(self,data_config,data_read):
        if (data_config[0:7]==data_read[0:7]):
            return True
        else:
            return False

    def configure(self):
        r=[0,0,0,0,0,0,0,0,0]
        check =False
        print("################################################")
        while check!=True:
            self.set_sync(1)
            self.set_ref_sel(0)
            self.write_status()
            self.reset()
            self.write_config(self.config_120MHz)
            time.sleep(0.5)
            r=self.read_config()
            check=self.check_configuration(self.config_120MHz,r)
            if check:
                print(bcolors.GREEN + "CDCE configuration [OK] " + bcolors.ENDC)
            else:
                print(bcolors.FAIL + "Error ! restart configuration ..." + bcolors.ENDC)

    def init(self):
        self.configure()
        if self.verbose :self.info()

    def info(self):
        self.read_status()
        print("#### status cdce ####")
        print("--> pwrdown_b  : {}".format(self.get_pwrdown_b()))
        print("--> ref_sel    : {}".format(self.get_ref_sel()))
        print("--> sync       : {}".format(self.get_sync()))
        print("--> pll_locked : {}".format(self.get_pll_locked()))
