Python sources update : 01/10/2021 : 

- Add registers for petiROC_2C and associated functions: asic_petiroc2b.py [add registres 648 to 663]
- Modify FPGA registers range: feb_fpga_registers.py [Asic registers : 42 words]
- Add function to configure Asic type (2A,2B,2C) for FEB compatibility: feb_asics.py [set_PetiROC_type]

-Add little script to test functionnality : PR2C_reg_test.py