import dataProducer
from fc7 import *
from feb_v2_cycloneV_r1 import *


class FEBv2DAQ(dataProducer.dataProducer):
    def __init__(self):
        dataProducer.dataProducer.__init__(self)
        self.fc7_verbosity=False
        self.feb_verbosity=False
        self.connected=True
          
    def daq_initialising(self):
        if self.connected:
          #To change : these functions should return True/False to signal things are OK or not
          self.fc7 = fc7(self.fc7_verbosity)
          self.feb = feb_v2_cycloneV_r1(self.fc7,self.feb_verbosity)  
          self.fc7.init()
          self.feb.init()
        dataProducer.dataProducer.daq_initialising(self)
        
    def daq_configuring(self):
        print ("I need EDAQ input to write that part")
        dataProducer.dataProducer.daq_configuring(self)
    
    def daq_starting(self):
        if self.connected:
          self.fc7.start_acquisition()
          time.sleep(5)
        #Nota Bene : We might need a different run_function for different FC7 aquisition mode 
        #The switch can be done in the line below self.producer_thread = threading.Thread(target=self.run_function)
        dataProducer.dataProducer.daq_starting(self)
           
    def run_function(self):
        print ("Need EDAQ input to know how to loop on event readout")
        print ("     and then combine with Laurent's dataProducer.daq")
        dataProducer.dataProducer.run_function(self)
      
    def daq_stopping(self):
        print ("Way of stopping acquisition might depend on the FC7 acquisition mode")
        if self.connected:
          self.fc7.stop_acquisition()
        dataProducer.dataProducer.daq_stopping(self)
        
    def daq_destroying(self):
        print ("Check with EDAQ if things need to done software-wise when exiting DAQ")
        dataProducer.dataProducer.daq_destroying(self)
      
