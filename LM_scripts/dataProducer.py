import logging
import threading
import random
import time
from transitions import Machine, State


class dataProducer:
    def __init__(self):
        self.fpga0=[]
        self.fpga1=[]
        self.fpga2=[]
        self.BC0=0
        self.running=False
        self.configured=False
        self.run=1000
        self.daqfsm = Machine(model=self, states=[
                              'CREATED', 'INITIALISED', 'CONFIGURED', 'RUNNING', 'CONFIGURED'], initial='CREATED')
        self.daqfsm.add_transition(
            'initialise', 'CREATED', 'INITIALISED', after='daq_initialising')
        self.daqfsm.add_transition('configure', [
                                   'INITIALISED', 'CONFIGURED'], 'CONFIGURED', after='daq_configuring')
        self.daqfsm.add_transition(
            'start', 'CONFIGURED', 'RUNNING', after='daq_starting', conditions='isConfigured')
        self.daqfsm.add_transition(
            'stop', 'RUNNING', 'CONFIGURED', after='daq_stopping', conditions='isConfigured')
        self.daqfsm.add_transition(
            'destroy', 'CONFIGURED', 'CREATED', after='daq_destroying')
        self.state="CREATED"
    def  veto(self):
        return False
    def run_function(self):
        logging.info("Thread %d: starting",self.run)
        time.sleep(2)
        t0=time.time()
        lt=t0
        fout=open("untest%d.dat" % self.run,"bw")
        fpga0=random.sample(range(0xFFFFFFFF),4000)
        fpga1=random.sample(range(0xFFFFFFFF),4000)
        fpga2=random.sample(range(0xFFFFFFFF),4000)

        while (self.running):
            if (self.veto()):
                time.sleep(0.01)
                continue
            if (self.BC0%1000 ==1):
                lt1=time.time()
                print(self.BC0,lt1-lt)
                lt=lt1
            ba=bytearray()
            ba.append(0x28)
            
            flen=int(11+6+(len(fpga0)+len(fpga0)+len(fpga2))*4).to_bytes(4,'big')
            for x in flen:
                ba.append(x)
            for x in self.BC0.to_bytes(4,'big'):
                ba.append(x)
            for x in len(fpga0).to_bytes(2,'big'):
                ba.append(x)
            for x in fpga0:
                for y in x.to_bytes(4,'big'):
                    ba.append(y)
            for x in len(fpga1).to_bytes(2,'big'):
                ba.append(x)
            for x in fpga1:
                for y in x.to_bytes(4,'big'):
                    ba.append(y)
            for x in len(fpga2).to_bytes(2,'big'):
                ba.append(x)
            for x in fpga2:
                for y in x.to_bytes(4,'big'):
                    ba.append(y)
            ba.append(0x29)
            fout.write(ba)
            self.BC0=self.BC0+1
            
            
        logging.info("Thread %d: finishing", self.run)
        fout.close()
    def daq_initialising(self):
        logging.info("Daq is initialised")
    def daq_configuring(self):
        self.configured=True
        logging.info("Daq is configured")
    def isConfigured(self):
        return self.configured
    def daq_starting(self):
        self.run=self.run+1
        self.running=True
        self.producer_thread = threading.Thread(target=self.run_function)
        self.producer_thread.start()
        logging.info("Daq is started")
    def daq_stopping(self):
        self.running=False
        self.producer_thread.join()
        logging.info("Daq is stopped")
    def daq_destroying(self):
        logging.info("Daq is destroyed")
        
