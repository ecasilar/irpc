# Python_DAQ

Python scripts to do a DAQ  for  CMS muons  FEB.

Following instructions are for the lyocms03 machine. 
All commands are to be run in a bash shell from account acqcmsmu.

## Start from newly switched on PC
### Start the database

in the home directory of user acqcmsmu

a) check docker service : sudo service docker status
b) if not running, start docker service : sudo service docker start
c) list docker container : docker ps -a
   If you have a mongodb service running, delete it : docker container rm <CONTAINER ID>
d) start the database server : . start_mongo
e) check database is up and running :
. /usr/local/pmdaq/etc/pmdaq.bashrc 
cd FEB_DAQ/Python_daq/python_daq/
. init.sh 
mgjob  --runs

This should print the list of runs registered in the database.

To start running, see "Start Daq"

## Start Daq 
### Hardware :
lyocms03 PC should be connected by ethernet cable (interface enp3s0) to the microTCA crate.

Low voltage :
4 Volt alimentation with light blue/black cable : set 4.09 V (WARNING, check voltage before connecting to the FEB, should be 4 V at FEB side)
2 Volt alimentation with dark blue cable : set 2.25 V (WARNING, check voltage before connecting to the FEB, should be 2 V at FEB side)
 If using different cables, adjust due LV settings to different voltage drop
 
Alimentation cable : FEB side : red cable connected to P4V and P2V. Black cable connected to GND.
                     ALIM side : red cable connected to positive output, black cable connected to negative output.
		                 The 2 alim should share common negative output, common ground and ground to be connected to negative output.

GBT link : On the microTCA crate, oriented with the ethernet connectors up, connect optical fiber on the last slot on the right with black on left and red on right.
           On the FEB, the GBT link facing you, connect red on left and black on right (i.e. reverse order from microTCA crate)

The FC7 mezzanine, FC7 card being upward in the microTCA crate : most left (first) connector is an output signal for BC0, and middle connector (third) is Resync input signal. External triggers are to be pluged in the Resync input.

###DAQ starting
a) cd FEB_DAQ/Python_daq/python_daq/
b) . init.sh
c) python3

Once python launched.
import FEB_control_FSM
fb=FEB_control_FSM.FEB_control_FSM("Orbit")
fb.initialise()
fb.configure()
fb.start()

you should enter a description of the run you're starting. Enter a blank line when finished.

to stop the run:
fb.stop()

to start a new run :
fb.start()

to change the configuration or a setting, be sure the run is stop.
fb.configure() then fb.start()

the standard configuration parameters are defined in file  FEB_DAQ_user_settings.py.
You can also change parameters by changing corresponding python variable of the FEB_control_FSM (fb) object.


## Monitoring the DAQ running
In a different shell, do
a) cd ~/FEB_DAQ/Python_daq/python_daq/
b) tail -f debug.log


## special TDC time alignement calibration run.
To take a time alignement calibration run, you should connect the BC0 exit signal, send it to a delay, convert to TTL, and send the delayed signal to the Resync input.
Once the hardware is set up, proceed as for a normal run but in python do
fb.set_time_calibration_ON() before doing fb.start()
and
fb.set_time_calibration_OFF() after doing fb.stop()

You don't need to reinitialise or reconfigure between normal runs and special time calibration runs.

## Pedestal measurement and creation of a DB state with the new measured pedestal values.

### measuring pedestals

a) To start the measurement from a known calibration, do the Daq starting up to fb.configure()
b) In another shell, do
cd ~/FEB_DAQ/v0806/python_src
python3 Equalization.py

The results of this calibration will be put under the directory dac6bConfig

### adding the new pedestal setting to the DB
in FEB_DAQ/Python_daq/python_daq/
If you want to start a new set of names for the configuration,
edit the script generate_configuration.py to choose a new database state name and then run

python3 generate_configuration.py

This will automatically create a version 1 of your new configuration state name in the DB.

To import your new pedestals in the database, in FEB_DAQ/Python_daq/python_daq/
edit the script update_configuration.py to specify from which database state you want to update config.
then run
python3 update_configuration.py

NB : You should provide a quantity to say what shift you want for the 10bitDac value from the pedestal value.
The recommended value is 25.

To use your new configuration state, you have two ways :
1) update the file FEB_DAQ_user_settings.py and restart the DAQ from scratch.
2) if the daq is allready running, set
fb.config_statename
   and
fb.config_version
   to match your database name and then do fb.configure() to load the new configuration.

## Updating the configuration
To update the configuration, start python3 and then
import MongoFeb as mg
s=mg.instance()
s.download("YOUR_STATE_TO_CHANGE")
use the s.PR2_XXXX function to change what you want to change and then
s.uploadChanges("STATE_FAMILY_NAME","comment")

usually the "STATE_FAMILY_NAME" is equal to "YOUR_STATE_TO_CHANGE"


##Getting info to the database.
Make sure the DB is running, then, if not already done :
. /usr/local/pmdaq/etc/pmdaq.bashrc 
cd ~/FEB_DAQ/Python_daq/python_daq/
. init.sh 

to print the list of runs and their comments :
mgjob --runs

to print all the available configuration states :
mgroc --states

## Preparation 
  
  you should add to the environement variable PYTHONPATH the directory where Alban's python FEB driver code is located.
  
  To set up your configuration database environnement, put the database password into environement variable DBpassword and then source the correct init_XXXX.sh script from the scripts directory.

## check temperature (works only when daq is stopped)
fb.feb.feb_ctrl_and_status.get_temperature()
