import time

with open("/data/trigger_count.txt", "w") as trig_output:
    trig_output.write(str(0))

wedcs_info = open("test_webdcsInfo.txt","r").readlines()
daqRunningInfo = open("daqRunningInfo.txt", "w")
daqRunningInfo.close()
daqRunningInfo = open("daqRunningInfo.txt", "a")

print("Scan number : " , wedcs_info[0])
print("HV point : " , wedcs_info[1])
print("max trigger : " , wedcs_info[2])

n_trig = 0

import FEB_control_FSM
fb=FEB_control_FSM.FEB_control_FSM("Orbit")
fb.initialise()
time.sleep(3)
fb.configure()
time.sleep(2)
fb.start()
time.sleep(2)

#READ trigger count
while n_trig<=int(wedcs_info[2]):
    time.sleep(3)
    f = open("/data/trigger_count.txt","r").readlines()
    if (len(f)<1) : 
        time.sleep(2)
    else:
        n_trig = int(f[0])
    daqRunningInfo.write("Reading ... , done: "+str(100*(n_trig/float(wedcs_info[2])))+" % \n" )

fb.stop()
time.sleep(2)
fb.destroy()
daqRunningInfo.close()


