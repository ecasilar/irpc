if [ -z "$DBpassword" ];
then
    echo "ERROR"
    echo "You should set environement variable DBpassword to the mongo database password"
    echo "DB administrator knows the password"
else
    export export MGDBLOGIN=acqcmsmu/${DBpassword}@${DBhostname}:${DBhostport}@LYONFEB
fi
