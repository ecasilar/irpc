#this script assumes you have done a ssh tunnel to the machine running the database
#
# ssh -fN -L 27037:lyocms03:27017 loginOnIntermediateHost@IntermediateHost

export DBhostname=localhost
export DBhostport=27037

source  `dirname "${BASH_SOURCE[0]}"`/mongo_init.sh

echo " MGDBLOGIN set to $MGDBLOGIN"
