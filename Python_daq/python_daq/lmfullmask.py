
import json as js
from configuration_helper import *
import MongoFeb as mg
THRTRANS=[]
THRTRANA=[]
for i in range(7):
    THRTRANS.append(0)
    THRTRANA.append(0)

THRTRANS[1]=412
THRTRANA[1]=415
THRTRANS[2]=391
THRTRANA[2]=395
THRTRANS[3]=429
THRTRANA[3]=433
THRTRANS[4]=433
THRTRANA[4]=437
THRTRANS[5]=417
THRTRANA[5]=419
THRTRANS[6]=397
THRTRANA[6]=401



if __name__ == "__main__":

    p_type=1
    p2C_autoreset_ON=False
    P2C_resetDelay_parameter=5
    P2B_FSM_parameters=[3,3,3]
    s=mg.instance()
    s.download("RE31_186_FEB_6_gifOctober",15)
    cor6b=[]
    for i in range(32):
        cor6b.append(0)
    cor6b[0]=-5
    cor6b[2]=-5
    cor6b[28]=-5
    cor6b[30]=-5
    for i in range(1,7):
        s.PR2_ChangeVthTime(THRTRANS[i]+25,0,i)
        s.PR2_ChangeMask(0,i,12,1)
        s.PR2_Change6BDac(0,i,12,1)
        s.PR2_ChangeMask(0,i,14,1)
        s.PR2_Change6BDac(0,i,14,1)
        s.PR2_ChangeMask(0,i,16,1)
        s.PR2_Change6BDac(0,i,16,1)
        s.PR2_ChangeMask(0,i,18,1)
        s.PR2_Change6BDac(0,i,18,1)
        s.PR2_Correct6BDac(0, i, cor6b)

    s.uploadChanges("RE31_186_FEB_6_gifOctober","From version 15 VTHTIME=PED+25 (75 fC) Ch 12,14,16,18 masked,ch 0,2,28,30 6BDAC shifted by -5")


