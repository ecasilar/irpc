import json as js
from configuration_helper import *
import MongoFeb as mg

if __name__ == "__main__":
    #Config to fill
    FEB_type = "FEBv2_2" # FEBv2_1
    FEB_num = "17"
    PR_type = "PR2C" # PR2B
    Chamber = "RE31_186"
    Lab = "904"
    print("State name to copy :", "_".join([Chamber,FEB_type,FEB_num,Lab]))
    s=mg.instance()
    s.createNewState("_".join([Chamber,FEB_type,FEB_num,Lab])) 
    s.addFEB(999,6, "_".join([PR_type,FEB_type]))
    s.update_configuration_daq()
    s.uploadNewState("State with a FEB and 6 ASICs")

