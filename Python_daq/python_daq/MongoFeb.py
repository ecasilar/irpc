#!/usr/bin/env python3
import os
from pymongo import MongoClient
import json
from bson.objectid import ObjectId
import time
import prettyjson as pj




def IP2Int(ip):
    """
    convert IP adress string to int

    :param IP: the IP address
    :return: the encoded integer 
    """
    o = list(map(int, ip.split('.')))
    res = (16777216 * o[3]) + (65536 * o[2]) + (256 * o[1]) + o[0]
    return res


class MongoFeb:
    """
    Main class to access the Mongo DB for FEB asics
    """

    def __init__(self, host,port,dbname,username,pwd):
        """
        connect Mongodb database 

        :param host: Hostanme of the PC running the mongo DB

        :param port: Port to access the base

        :param dbname: Data base name

        :param username: Remote access user

        :param pwd: Remote access password

        """
        self.connection=MongoClient(host,port)
        self.db=self.connection[dbname]
        self.db.authenticate(username,pwd)
        self.state = {}
        self.asiclist = []
        self.bson_id=[]

    def runs(self):
        """
        List all the run informations stored
        """
        res=self.db.runs.find({})
        for x in res:
            if ("run" in x):
                if ("comment" in x and "time" in x):
                    print(time.ctime(x["time"]),x["location"],x["run"],x["comment"])
                else:
                    if ("run" in x):
                        print(x["location"],x["run"],x["comment"])
                #print(x["time"],x["location"],x["run"],x["comment"])

    def getRun(self,location,comment="Not set"):
        """
        Get a new run number for a given setup
        :param location: Setup Name
        :param comment: Comment on the run
        :return: a dictionnary corresponding to the base insertion {run,location,time,comment}
        """
        print("in GetRun:", location)
        #Find entries including full location name in data base
        res=self.db.runs.find({'location':location})
        #res=self.db.runs
        runid={}
        #Loop over the entries
        for x in res:
            #var=raw_input()
            runid=x  # It is enough to store just the last element
        # if we find the same name (First part ) we increase run number by 1.
        if ("location" in runid.keys()):
            if (location.split("_")[0] in runid["location"]):
                runid["run"]=runid["run"]+1
                del runid["_id"]
        else:
            runid["run"]=1000
            runid["location"]=location
        runid["time"]=time.time()
        runid["comment"]=comment
        os.system("mkdir -p /dev/shm/mgjob")
        fname="/dev/shm/mgjob/lastrun.json"
        f=open(fname,"w+")
        f.write(json.dumps(runid, indent=2, sort_keys=True))
        f.close()
        resconf=self.db.runs.insert_one(runid)
        print(resconf)
        return runid
 
    def reset(self):
        """
        Reset connection to download another state
        """
        self.state = {}
        self.asiclist = []
        self.bson_id=[] 
    def createNewState(self,name):
        """
        Create a new state , version is set to 1

        :param name: Name of the state

        """
        self.state["name"]=name
        self.state["version"]=1
        self.state["asics"]=[]
    def addFEB(self,febid,nasic,asictype="PR2B"):
        """
        Add a FEBV1 with asics

        :param  febid: Id of the FEB

        :param nasic: Number of PETIROC asics connected

        :param asictype: "PR2" for PETIROC2A , "PR2B" for PETIROC2B
        """
        #febid=IP2Int(ipname)
        for i in range(nasic):
            asic={}
            asic["address"]="0.0.0.%d" % febid
            asic["dif"]=febid
            asic["num"]=i+1
            asic["slc"]=self.initPR2(i+1,asictype)
            asic["_id"]=None
            print(asic["dif"],asic["num"],asic["_id"]," is added")
            self.asiclist.append(asic)
            
    def uploadFromFile(self,fname):
        """
        Upload a state in DB from a JSON file

        :param fname: File name
        """
        f=open(fname)
        sf=json.loads(f.read())
        f.close()
        self.state["name"]=sf["state"]
        self.state["version"]=sf["version"]
        for x in sf["asics"]:
            result=self.db.asics.insert_one(x)
            x["_id"]=result.inserted_id
        self.bson_id=[]
        for  i in range(len(sf["asics"])):
            self.bson_id.append(sf["asics"][i]["_id"])
        self.state["asics"]=self.bson_id
        self.state["comment"]="Upload from %s" % fname
        resstate=self.db.states.insert_one(self.state)
        print(resstate)
        
    def uploadNewState(self,comment="NEW"):
        """
        Create a new state in the DB with data stored in object memory

        :param comment: A comment on the state

        """
        # First append modified ASICS
        for i in range(len(self.asiclist)):
            if (self.asiclist[i]["_id"]!=None):
                continue
            del self.asiclist[i]["_id"]
            result=self.db.asics.insert_one(self.asiclist[i])
            self.asiclist[i]["_id"]=result.inserted_id
        for  i in range(len(self.asiclist)):
            self.bson_id.append(self.asiclist[i]["_id"])
        self.state["asics"]=self.bson_id
        self.state["comment"]=comment
        resstate=self.db.states.insert_one(self.state)
        print(resstate)
    def states(self):
        """
        List all states in the DB
        """
        cl=[]
        res=self.db.states.find({})
        for x in res:
            if (not ("name" in x)):
                continue
            if ("comment" in x):
                print(x["name"],x["version"],x["comment"])
                cl.append((x["name"],x['version'],x['comment']))
            else:
                print(x["name"],x["version"] )
                cl.append((x["name"],x['version'],"None"))
        return cl
    def download(self,statename,version,toFileOnly=False,odir="/dev/shm/mgroc",pretty=False):
        """
        Download a state configuration to /dev/shm/mgroc/ directory and load it in the MongoFeb object
        
        :param statename: State name
        :param version: State version
        :param toFileOnly: if True and /dev/shm/mgroc/statename_version.json already exists, it exits
        :param odir: Output directory, /dev/shm/mgroc by default
        """        
        os.system("mkdir -p %s" % odir)
        fname="%s/%s_%s.json" % (odir,statename,version)
        if os.path.isfile(fname) and toFileOnly:
            print('%s already download, Exiting' % fname)
            return None
        res=self.db.states.find({'name':statename,'version':version})
        for x in res:
            print(x["name"],x["version"],len(x["asics"])," asics")
            self.state["name"]=x["name"]
            self.state["version"]=x["version"]
            #var=raw_input()
            slc={}
            slc["state"]=statename
            slc["version"]=version
            slc["asics"]=[]
            self.asiclist=[]
            #for y in x["asics"]:
            #    resa=self.db.asics.find_one({'_id':y})
            #print(x["asics"])
            resl=self.db.asics.find({'_id': {'$in': x["asics"]}})
            
            for resa in resl:
                self.asiclist.append(resa)
                #print(resa)
                s={}
                s["slc"]=resa["slc"]
                s["num"]=resa["num"]
                s["dif"]=resa["dif"]
                if ( "address" in resa):
                    s["address"]=resa["address"]
                #print(res["dif"])
                slc["asics"].append(s)

            #os.system("mkdir -p /dev/shm/mgroc")
            #fname="/dev/shm/mgroc/%s_%s.json" % (statename,version)
            #if os.path.isfile(fname):
            #    print('%s already download' % fname)
            #else:
            f=open(fname,"w+")
            #f.write(json.dumps(slc,indent=2, sort_keys=True))
            if (pretty):
                f.write(json.dumps(slc,indent=2,sort_keys=True))
            else:
                f.write(json.dumps(slc,sort_keys=True))
            #f.write(pj.prettyjson(slc, maxlinelength=255))
            f.close()
            return slc
    def initPR2(self, num,version="PR2B"):
        """
        PETIROC 2  initialisation, it creates a default dictionary representation of a PETIROC2

        :param num: Asic number
        :param version: Asic type (PR2 or PR2B)
        :return: the dictionary
        """
	#print("***** init HR2")
        _jasic={}
        _jasic["header"]=num
        _jasic["EN10bDac"] = 1
        _jasic["PP10bDac"] = 1 # 0
        _jasic["EN_adc"] =0
        _jasic["PP_adc"] = 0
        _jasic["sel_starb_ramp_adc_ext"] = 0
        _jasic["usebcompensation"] = 0
        _jasic["EN_bias_dac_delay"] = 0
        _jasic["PP_bias_dac_delay"] = 0
        _jasic["EN_bias_ramp_delay"] = 0
        _jasic["PP_bias_ramp_delay"] = 0
        _jasic["EN_discri_delay"] =0
        _jasic["PP_discri_delay"] = 0
        _jasic["EN_temp_sensor"] = 0
        _jasic["PP_temp_sensor"] = 0
        _jasic["EN_bias_pa"] = 1
        _jasic["PP_bias_pa"] = 1 # 0
        _jasic["EN_bias_discri"] = 1
        _jasic["PP_bias_discri"] = 1 #0
        _jasic["cmd_polarity"] = 0
        _jasic["latch"] = 1
        _jasic["EN_bias_6bit_dac"] =1
        _jasic["PP_bias_6bit_dac"] = 1 #0
        _jasic["EN_bias_tdc"] = 0
        _jasic["PP_bias_tdc"] = 0
        _jasic["ON_OFF_input_dac"] = 1
        _jasic["EN_bias_charge"] = 0
        _jasic["PP_bias_charge"] = 0
        _jasic["Cf3_100fF"] = 0
        _jasic["Cf2_200fF"] = 0
        _jasic["Cf1_2p5pF"] = 0
        _jasic["Cf0_1p25pF"] = 0
        _jasic["EN_bias_sca"] = 0
        _jasic["PP_bias_sca"] = 0
        _jasic["EN_bias_discri_charge"] = 0
        _jasic["PP_bias_discri_charge"] = 0
        _jasic["EN_bias_discri_adc_time"] = 0
        _jasic["PP_bias_discri_adc_time"] = 0
        _jasic["EN_bias_discri_adc_charge"] = 0
        _jasic["PP_bias_discri_adc_charge"] = 0
        _jasic["DIS_razchn_int"] = 1
        _jasic["DIS_razchn_ext"] = 0
        _jasic["SEL_80M"] = 0
        _jasic["EN_80M"] = 0
        _jasic["EN_slow_lvds_rec"] = 1
        _jasic["PP_slow_lvds_rec"] = 1 # 0 
        _jasic["EN_fast_lvds_rec"] = 1
        _jasic["PP_fast_lvds_rec"] = 0
        _jasic["EN_transmitter"] = 0
        _jasic["PP_transmitter"] = 0
        _jasic["ON_OFF_1mA"] =1
        _jasic["ON_OFF_2mA"] = 1
        _jasic["ON_OFF_otaQ"] = 0
        _jasic["ON_OFF_ota_mux"] = 0
        _jasic["ON_OFF_ota_probe"] = 0
        _jasic["DIS_trig_mux"] = 1
        _jasic["EN_NOR32_time"] = 0 #1
        _jasic["EN_NOR32_charge"] = 0
        _jasic["DIS_triggers"] = 0
        _jasic["EN_dout_oc"] = 0
        _jasic["EN_transmit"] = 0 #1
        if ("PR2B" in version):
            _jasic["PA_ccomp_0"] =1
            _jasic["PA_ccomp_1"] =1
            _jasic["PA_ccomp_2"] = 1
            _jasic["PA_ccomp_3"] =1
            _jasic["Choice_Trigger_Out"] =0
            _jasic["NC1"] =0
            _jasic["NC2"] =1
            _jasic["NC3"] =0
            _jasic["NC4"] =0
        if ("PR2C" in version):
            _jasic["PA_ccomp_0"] =1
            _jasic["PA_ccomp_1"] =1
            _jasic["PA_ccomp_2"] = 1
            _jasic["PA_ccomp_3"] =1
            _jasic["Choice_Trigger_Out"] =0
            _jasic["NC1"] =0
            _jasic["NC2"] =1
            _jasic["NC3"] =0
            _jasic["NC4"] =0
            _jasic["NC5"] =0
            _jasic["NC6"] =0
            _jasic["NC7"] =0
            _jasic["NC8"] =0
            _jasic["NC9"] =0
            _jasic["NC10"] =0
            _jasic["Delay_reset_trigger"] =0
            _jasic["EN_reset_trigger_delay"] =0
            _jasic["Delay_reset_ToT"] =0
            _jasic["EN_reset_ToT_delay"] =0
            
        _jasic["DacDelay"] = 0
        idac=[]
        b6dac=[]
        mdc=[]
        mdt=[]
        idc=[]
        for ch in range(32):
            idac.append(125);
            
            mdc.append(1);
            if (ch%2 == 0):
                mdt.append(0)
                b6dac.append(32);
            else:
                mdt.append(1)
                b6dac.append(1);
            idc.append(1)
        if ("FEBv2_2" in version):
            mdt[0]=1
            b6dac[0]=1
            mdt[1]=0
            b6dac[1]=32

        _jasic["InputDac"] = idac;
        _jasic["6bDac"] = b6dac;
        _jasic["MaskDiscriCharge"] = mdc;
        _jasic["MaskDiscriTime"] = mdt;
        _jasic["InputDacCommand"] = idc;
        _jasic["PETIROC_NOR32RAZ_FSM"] = [1,3,2,2];

        _jasic["VthDiscriCharge"] = 300
        _jasic["VthTime"] = 300

        return _jasic


    def uploadChanges(self,statename,comment):
        """
        Upload a new version of the state
        it finds the last version of the state and upload a new one with incremented version number

        :param statename: Name of the state
        :param comment: A comment on the changes
        """
        # Find last version
        res=self.db.states.find({'name':statename})
        print("****")
        print(res)
        print("****")
        last=0
        for x in res:
            if (last<x["version"]):
                last=x["version"]
        if (last==0):
            print(" No state ",statename,"found")
            return
        # First append modified ASICS
        for i in range(len(self.asiclist)):
            if (self.asiclist[i]["_id"]!=None):
                continue
            del self.asiclist[i]["_id"]
            result=self.db.asics.insert_one(self.asiclist[i])
            self.asiclist[i]["_id"]=result.inserted_id
        self.bson_id=[]
        for  a in self.asiclist:
            print(a)
            print(a["_id"])
            self.bson_id.append(a["_id"])
        self.state["asics"]=self.bson_id
        self.state["version"]=last+1
        self.state["comment"]=comment
        resstate=self.db.states.insert_one(self.state)
        print(resstate,self.state["version"],self.state["name"])
        

    def addAsic(self, dif_num, header,version="PR2B"):
        """
        Add a new PETIROC2 to the asic list

        :param dif_num: DIF ID (ipaddr in integer >>16)
        :param header: ASIC number
        :param version: PR2 for 2A , PR2B for 2B
        """
        print("force ASIC")

        thePR2 = self.initPR2(dif_num, header,version)
        self.asiclist.append(thePR2)
        self.asicConf.add(thePR2)

 
    def PR2_ChangeLatch(self, Latch, idif=0, iasic=0):
        """
        Change the Latch mode of specified  asics, modified asics are tagged for upload
        
        :param Latch: Latch value (1/0)
        :param idif: DIF_ID (IP>>16), if 0 all FEBs are changed
        :param iasic: asic number, if 0 all Asics are changed
        """
        for a in self.asiclist:
            if (idif != 0 and a["dif"] != idif):
                continue
            if (iasic != 0 and a["num"] != iasic):
                continue
            try:
                a["slc"]["latch"]=Latch
                a["_id"]=None
            except Exception as e:
                print(e.getMessage())

    def PR2_ChangeVthTime(self, VthTime, idif=0, iasic=0):
        """
        Change the VTHTIME threshold of specified  asics, modified asics are tagged for upload
        
        :param VthTime: Threshold of time discriminators
        :param idif: DIF_ID (IP>>16), if 0 all FEBs are changed
        :param iasic: asic number, if 0 all Asics are changed
        """
        for a in self.asiclist:
            if (idif != 0 and a["dif"] != idif):
                continue
            if (iasic != 0 and a["num"] != iasic):
                continue
            try:
                a["slc"]["VthTime"]=VthTime
                a["_id"]=None
            except Exception as e:
                print(e.getMessage())

    def PR2_ShiftVthTime(self, DeltaVthTime, idif=0, iasic=0):
        """
        Shift the VTHTIME threshold of specified  asics, modified asics are tagged for upload
        
        :param DeltaVthTime: Shift of Threshold of time discriminators
        :param idif: DIF_ID (IP>>16), if 0 all FEBs are changed
        :param iasic: asic number, if 0 all Asics are changed
        """
        for a in self.asiclist:
            if (idif != 0 and a["dif"] != idif):
                continue
            if (iasic != 0 and a["num"] != iasic):
                continue
            try:
                a["slc"]["VthTime"]=a["slc"]["VthTime"]+DeltaVthTime
                a["_id"]=None
            except Exception as e:
                print(e.getMessage())


    def PR2_ChangeDacDelay(self, delay, idif=0, iasic=0):
        """
        Change the DAC delay of specified  asics, modified asics are tagged for upload
        
        :param delay: Dac delay
        :param idif: DIF_ID (IP>>16), if 0 all FEBs are changed
        :param iasic: asic number, if 0 all Asics are changed
        """        
       
        for a in self.asiclist:
            if (idif != 0 and a["dif"] != idif):
                continue
            if (iasic != 0 and a["num"] != iasic):
                continue
            try:
                a["slc"]["DacDelay"]=delay
                a["_id"]=None
            except Exception as e:
                print(e.getMessage())

    def PR2_ChangeAllEnabled(self, idif=0, iasic=0):
        """
        Change all the ENable signals of PETIROC asic

        :param idif: DIF_ID (IP>>16), if 0 all FEBs are changed
        :param iasic: asic number, if 0 all Asics are changed
        """
        for a in self.asiclist:
            if (idif != 0 and a["dif"] != idif):
                continue
            if (iasic != 0 and a["num"] != iasic):
                continue
            try:
                a["slc"]["EN_bias_discri"]=1
                a["slc"]["EN_bias_pa"]=1
                a["slc"]["EN_bias_discri_charge"]=1
                a["slc"]["EN_dout_oc"]=1
                a["slc"]["EN_bias_dac_delay"]=1
                a["slc"]["EN10bdac"]=1
                a["slc"]["EN_bias_discri_adc_charge"]=1
                a["slc"]["EN_bias_sca"]=1
                a["slc"]["EN_bias_6bit_dac"]=1
                a["slc"]["EN_transmit"]=1
                a["slc"]["EN_bias_ramp_delay"]=1
                a["slc"]["EN_bias_charge"]=1
                a["slc"]["EN_fast_lvds_rec"]=1
                a["slc"]["EN_transmitter"]=1
                a["slc"]["EN_adc"]=1
                a["slc"]["EN_NOR32_charge"]=1
                a["slc"]["EN_80M"]=1
                a["slc"]["EN_discri_delay"]=1
                a["slc"]["EN_bias_discri_adc_time"]=1
                a["slc"]["EN_NOR32_time"]=1
                a["slc"]["EN_temp_sensor"]=1

                a["_id"]=None

            except Exception as e:
                print(e.getMessage())

    def PR2_ChangeParam(self,pname,pval,idif=0, iasic=0):
        """
        Change all the ENable signals of PETIROC asic

        :param idif: DIF_ID (IP>>16), if 0 all FEBs are changed
        :param iasic: asic number, if 0 all Asics are changed
        :param pname: parameter name
        :param pval: paramter value
        """
        for a in self.asiclist:
            if (idif != 0 and a["dif"] != idif):
                continue
            if (iasic != 0 and a["num"] != iasic):
                continue
            try:
                a["slc"][pname]=pval
                a["_id"]=None

            except Exception as e:
                print(e.getMessage())

    def PR2_ChangeInputDac(self, idif, iasic, ich, dac):
        """
        Change the InputDAC valu of specified  asics, modified asics are tagged for upload
        
        :param idif: DIF_ID (IP>>16), if 0 all FEBs are changed
        :param iasic: asic number, if 0 all Asics are changed
        :param ich: The channel number
        :param dac: The DAC value
        """
        

        for a in self.asiclist:
            if (idif != 0 and a["dif"] != idif):
                continue
            if (iasic != 0 and a["num"] != iasic):
                continue
            try:
                a["slc"]["InputDac"][ich]=dac
                a["_id"]=None
            except Exception as e:
                print(e)
    def PR2_Change6BDac(self, idif, iasic, ich, dac):
        """
        Change the 6BDAC valu of specified  asics, modified asics are tagged for upload
        
        :param idif: DIF_ID (IP>>16), if 0 all FEBs are changed
        :param iasic: asic number, if 0 all Asics are changed
        :param ich: The channel number
        :param dac: The DAC value
        """
        

        for a in self.asiclist:
            if (idif != 0 and a["dif"] != idif):
                continue
            if (iasic != 0 and a["num"] != iasic):
                continue
            try:
                a["slc"]["6bDac"][ich]=dac
                a["_id"]=None
            except Exception as e:
                print(e)

    def PR2_Correct6BDac(self, idif, iasic, cor):
        """
        Correct the 6BDAC value of specified  asics, modified asics are tagged for upload
        
        :param idif: DIF_ID (IP>>16), if 0 all FEBs are changed
        :param iasic: asic number, if 0 all Asics are changed
        :param cor:  A 32 channels array of corrections to be applied on the 6BDAC values of all channels
        """
       
        for a in self.asiclist:
            if (idif != 0 and a["dif"] != idif):
                continue
            if (iasic != 0 and a["num"] != iasic):
                continue
            try:
                print(a["slc"]["6bDac"])
                for ich in range(32):
                    print(" Dac changed", idif, iasic, ich, cor[ich])
                    ng= a["slc"]["6bDac"][ich]+cor[ich]
                    if (ng<=0):
                        ng=1
                    if (ng>63):
                        ng=63
                    a["slc"]["6bDac"][ich] =ng
                print(a["slc"]["6bDac"])
                a["_id"]=None
            except Exception as e:
                print(e)

    def update_pedestals(self,shift10b=15,fidir="/home/acqcmsmu/FEB_DAQ/v0806/python_src/dac6bConfig_conf1",mask_list=[0,15,6,7,8],version="FEBv2_1"):
        from configuration_helper import readDacDefaultConfig
        for a in self.asiclist:
            fpgaID="fpga_%d" % a["slc"]["nfpga"]
            asicID=a["slc"]["loc"]
            
            veven=readDacDefaultConfig(fpgaID, asicID,fromdir=fidir)[1:]
            vall=[]
            for i in range(16):
                if i in mask_list:
                    a["slc"]["MaskDiscriTime"][i*2] = 1
                    vall.append(1)
                else:
                    vall.append(veven[i])
                vall.append(1)
            a["slc"]["6bDac"] =vall
            a["slc"]["VthTime"] = readDacDefaultConfig(fpgaID, asicID,fromdir=fidir)[0]+shift10b
            if ("FEBv2_2" in version):
                print("VALL 1" , vall[1])
                print("VALL 0" , vall[0])
                print("VERSION" , version)
                a["slc"]["6bDac"][1]=vall[0]
                a["slc"]["6bDac"][0]=1
                a["slc"]["MaskDiscriTime"][0] = 1
                a["slc"]["MaskDiscriTime"][1] = 0
            a["_id"]=None

        return

    def update_configuration_daq(self,shift10b=15,fsm_val=[1,3,2,2]):
        from configuration_helper import readDacDefaultConfig
        asic_num = 0
        for fpgaID in ["fpga_0", "fpga_1", "fpga_2"]:
            for asicID in ["top","bottom"]:
                self.asiclist[asic_num]["slc"]["InputDac"] = [128 for i in range(32)]
                veven=readDacDefaultConfig(fpgaID, asicID)[1:]
                vall=[]
                for i in range(16):
                    vall.append(veven[i])
                    vall.append(1)
                self.asiclist[asic_num]["slc"]["6bDac"] =vall 
                self.asiclist[asic_num]["slc"]["VthTime"] = readDacDefaultConfig(fpgaID, asicID)[0]+shift10b
                self.asiclist[asic_num]["slc"]["PETIROC_NOR32RAZ_FSM"] = fsm_val
                self.asiclist[asic_num]["slc"]['nfpga'] = int(fpgaID.split("_")[1])
                self.asiclist[asic_num]["slc"]['loc'] = asicID
                self.asiclist[asic_num]["_id"] = None
                asic_num += 1
        return

    def minimal_configuration_daq(self,fsm_val=[1,3,2,2]):
        asic_num = 0
        for fpgaID in ["fpga_0", "fpga_1", "fpga_2"]:
            for asicID in ["top","bottom"]:
                self.asiclist[asic_num]["slc"]["InputDac"] = [128 for i in range(32)]
                vall=[]
                for i in range(16):
                    vall.append(32)
                    vall.append(1)
                    self.asiclist[asic_num]["slc"]["MaskDiscriTime"][i*2+1] = 1
                    self.asiclist[asic_num]["slc"]["MaskDiscriTime"][i*2] = 0

                self.asiclist[asic_num]["slc"]["6bDac"] =vall 
                self.asiclist[asic_num]["slc"]["VthTime"] = 600
                self.asiclist[asic_num]["slc"]["PETIROC_NOR32RAZ_FSM"] = fsm_val
                self.asiclist[asic_num]["slc"]['nfpga'] = int(fpgaID.split("_")[1])
                self.asiclist[asic_num]["slc"]['loc'] = asicID
                self.asiclist[asic_num]["_id"] = None
                asic_num += 1
        return


    def PR2_ChangeMask(self, idif, iasic, ich, mask):
        """
        Change PETIROC2 MASKDISCRITIME parameter for one channel

        :param idif: DIF_ID (IP>>16), if 0 all FEBs are changed
        :param iasic: asic number, if 0 all Asics are changed
        :param ich: The channel number
        :param mask: the channel mask
        :warning: 1 = channel inactive, 0=active
        """

        for a in self.asiclist:
            if (idif != 0 and a["dif"] != idif):
                continue
            if (iasic != 0 and a["num"] != iasic):
                continue
            try:
                a["slc"]["MaskDiscriTime"][ich] = mask
                a["_id"]=None
            except Exception as e:
                print(e)
    def PR2_ChangeInputDacCommand(self, idif, iasic, ich, active):
        """
        Change PETIROC2 MASKDISCRITIME parameter for one channel

        :param idif: DIF_ID (IP>>16), if 0 all FEBs are changed
        :param iasic: asic number, if 0 all Asics are changed
        :param ich: The channel number
        :param active: the channel mask
        :warning: 1 = channel active, 0=inactive
        """

        for a in self.asiclist:
            if (idif != 0 and a["dif"] != idif):
                continue
            if (iasic != 0 and a["num"] != iasic):
                continue
            try:
                a["slc"]["InputDacCommand"][ich] = active
                a["_id"]=None
            except Exception as e:
                print(e)
                
    def PR2_SetCCOMP(self, v0,v1,v2,v3, idif=0, iasic=0):
        """
        Change the CCOMP value of specified  asics, modified asics are tagged for upload
        
        :param v0: CCOMP 0 value
        :param v1: CCOMP 1 value
        :param v2: CCOMP 2 value
        :param v3: CCOMP 3 value
        :param idif: DIF_ID (IP>>16), if 0 all FEBs are changed
        :param iasic: asic number, if 0 all Asics are changed
        """        
       
        for a in self.asiclist:
            if (idif != 0 and a["dif"] != idif):
                continue
            if (iasic != 0 and a["num"] != iasic):
                continue
            try:
                a["slc"]["PA_ccomp_0"]=v0
                a["slc"]["PA_ccomp_1"]=v1
                a["slc"]["PA_ccomp_2"]=v2
                a["slc"]["PA_ccomp_3"]=v3
                a["_id"]=None
            except Exception as e:
                print(e.getMessage())


    def PR2_SetCfValue(self, v0,v1,v2,v3, idif=0, iasic=0):
        """
        Change the CCOMP value of specified  asics, modified asics are tagged for upload
        
        :param v0: Cf0_1p25pF value
        :param v1: Cf1_2p5pF value
        :param v2: Cf2_200fF value
        :param v3: Cf3_100fF value
        :param idif: DIF_ID (IP>>16), if 0 all FEBs are changed
        :param iasic: asic number, if 0 all Asics are changed
        """        
       
        for a in self.asiclist:
            if (idif != 0 and a["dif"] != idif):
                continue
            if (iasic != 0 and a["num"] != iasic):
                continue
            try:
                a["slc"]["Cf0_1p25pF"]=v0
                a["slc"]["Cf1_2p5pF"]=v1
                a["slc"]["Cf2_200fF"]=v2
                a["slc"]["Cf3_100fF"]=v3
                a["_id"]=None
            except Exception as e:
                print(e.getMessage())

    def PR2C_EnableAutoReset(self, valtime=8):
        """
        enable the PETIROC 2C auto reset machanism
        and disable the FGPA reset FSM
        
        parameter is the reset time in unit of 5ns
        """
        for a in self.asiclist:
            try:
                a["slc"]["EN_reset_trigger_delay"]=1
                a["slc"]["Delay_reset_trigger"]=valtime
                a["slc"]["EN_reset_ToT_delay"]=0
                a["slc"]["PETIROC_NOR32RAZ_FSM"]=[0,0,0,0]
                a["_id"]=None
            except Exception as e:
                print(e.getMessage())

    def PR2C_DisableAutoReset(self, valtime1=3, valtime2=2, valtime3=2):
        """
        Disable the PETIROC 2C auto reset machanism
        and enable the FGPA reset FSM
        
        parameters are the three time parameter of the FEB FPGA reset FSM in unit of 5ns
        """
        for a in self.asiclist:
            try:
                a["slc"]["EN_reset_trigger_delay"]=0
                a["slc"]["Delay_reset_trigger"]=0
                a["slc"]["EN_reset_ToT_delay"]=0
                a["slc"]["PETIROC_NOR32RAZ_FSM"]=[1,valtime1,valtime2,valtime3]
                a["_id"]=None
            except Exception as e:
                print(e.getMessage())


def instance():
    """
    Create a MongoFeb Object

    :return: The MongoFeb Object
    """
    # create the default access
    login=os.getenv("MGDBLOGIN","NONE")
    if (login != "NONE"):
        
        userinfo=login.split("@")[0]
        hostinfo=login.split("@")[1]
        dbname=login.split("@")[2]
        user=userinfo.split("/")[0]
        pwd=userinfo.split("/")[1]
        host=hostinfo.split(":")[0]
        port=int(hostinfo.split(":")[1])
        #print("MGROC::INSTANCE() ",host,port,dbname,user,pwd)
        _wdd=MongoFeb(host,port,dbname,user,pwd)
        return _wdd
    else:
        if os.path.isfile("/etc/.mongoroc.json"):
            f=open("/etc/.mongoroc.json")
            s=json.loads(f.read())
            _wdd=MongoFeb(s["host"],s["port"],s["db"],s["user"],s["pwd"])
            f.close()
            return _wdd
        else:
            return None
