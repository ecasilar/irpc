
import json as js
from configuration_helper import *
import MongoFeb as mg

cor_fpga0_top_asic1=[16,0,7,0,9,0,4,0,-1,0,6,0,1,0,2,0,0,0,-8,0,-5,0,-11,0,-7,0,-5,0,-5,0,-5,0]
cor_fpga0_bottom_asic2=[14,0,6,0,4,0,8,0,6,0,0,0,6,0,3,0,-4,0,-6,0,-6,0,-4,0,-4,0,-2,0,-10,0,-12,0]
cor_fpga1_top_asic3=[13,0,9,0,2,0,3,0,3,0,-4,0,-8,0,0,0,-4,0,-4,0,-2,0,-5,0,0,0,2,0,1,0,-6,0]
cor_fpga1_bottom_asic4=[8,0,5,0,7,0,-3,0,-1,0,4,0,-5,0,-7,0,-9,0,-5,0,-4,0,-5,0,3,0,2,0,1,0,8,0]
cor_fpga2_top_asic5=[13,0,6,0,0,0,5,0,2,0,-4,0,1,0,-1,0,0,0,0,0,1,0,3,0,-6,0,-2,0,-7,0,-11,0]
cor_fpga2_bottom_asic6=[12,0,13,0,6,0,5,0,3,0,-3,0,1,0,-1,0,0,0,-4,0,-3,0,0,0,-8,0,-9,0,-5,0,-5,0]

if __name__ == "__main__":

    p_type=2
    p2C_autoreset_ON=False
    P2C_resetDelay_parameter=5
    P2B_FSM_parameters=[3,3,3]

    # Create access and download initial state
    s=mg.instance()
    s.download("RE31_186_FEB_6_gifOctober",1) 
    # Mask odd Channels
    for iasic in range(1,7):
        for ch in range(32):
            if (ch%2 ==0):
                continue
            s.PR2_ChangeMask(0,iasic,ch,1)
            s.PR2_Change6BDac(0,iasic,ch,1)
    # Set the RESET mechanism
    if p_type==2 and p2C_autoreset_ON:
        print ("Petiroc 2C Autoreset mode")
        s.PR2C_EnableAutoReset(P2C_resetDelay_parameter)
    else:
        print ("Petiroc 2B style RAZ FSM")
        s.PR2C_DisableAutoReset(P2B_FSM_parameters[0],P2B_FSM_parameters[1],P2B_FSM_parameters[2])
    # Now apply DAC 6Bits corrections
    s.PR2_Correct6BDac(0,1,cor_fpga0_top_asic1)
    s.PR2_Correct6BDac(0,2,cor_fpga0_bottom_asic2)
    s.PR2_Correct6BDac(0,3,cor_fpga1_top_asic3)
    s.PR2_Correct6BDac(0,4,cor_fpga1_bottom_asic4)
    s.PR2_Correct6BDac(0,5,cor_fpga2_top_asic5)
    s.PR2_Correct6BDac(0,6,cor_fpga2_bottom_asic6)

    # Upload changes
    s.uploadChanges("RE31_186_FEB_6_gifOctober","Test alignement LM from version 1")


