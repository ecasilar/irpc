
import json as js
from configuration_helper import *
import MongoFeb as mg



if __name__ == "__main__":

    p_type=2
    p2C_autoreset_ON=True
    P2C_resetDelay_parameter=5
    P2B_FSM_parameters=[3,2,2]
    s=mg.instance()
    s.download("RE31_185_NORES_10_PRC",53) 
    # Mask odd Channels
    for iasic in range(1,7):
        for ch in range(32):
            if (ch%2 ==0):
                continue
            s.PR2_ChangeMask(0,iasic,ch,1)
            s.PR2_Change6BDac(0,iasic,ch,1)
    # Set the RESET mechanism
    if p_type==2 and p2C_autoreset_ON:
        print ("Petiroc 2C Autoreset mode")
        s.PR2C_EnableAutoReset(P2C_resetDelay_parameter)
    else:
        print ("Petiroc 2B style RAZ FSM")
        s.PR2C_DisableAutoReset(P2B_FSM_parameters[0],P2B_FSM_parameters[1],P2B_FSM_parameters[2])

    cor_fpga0_top_asic1=[28,0,45,0,25,0,15,0,40,0,53,0,16,0,21,0,31,0,13,0,13,0,-1,0,-28,0,-1,0,-18,0]
    cor_fpga0_bottom_asic2=[58,0,28,0,16,0,36,0,5,0,18,0,-1,0,-4,0,-8,0,-23,0,-28,0,-24,0,-11,0,-18,0,-18,0,-18,0]
    cor_fpga1_top_asic3=[29,0,51,0,17,0,5,0,1,0,6,0,2,0,-22,0,-15,0,-1,0,-15,0,-4,0,-6,0,-29,0,-4,0,-21,0]
    cor_fpga1_bottom_asic4=[45,0,33,0,27,0,32,0,2,0,19,0,7,0,-8,0,19,0,-13,0,-7,0,7,0,-27,0,-27,0,-4,0,-21,0]
    cor_fpga2_top_asic5=[46,0,26,0,22,0,11,0,4,0,14,0,18,0,2,0,-20,0,-18,0,-7,0,-4,0,-7,0,-5,0,-18,0,-26,0]
    cor_fpga2_bottom_asic6=[44,0,35,0,60,0,41,0,28,0,28,0,12,0,9,0,6,0,-2,0,-22,0,-21,0,-28,0]


    cor_fpga0_top_asic1=[36,0,-3,0,19,0,11,0,-16,0,8,0,38,0,-4,0,-11,0,18,0,1,0,16,0,34,0,-24,0,22,0,-15,0]
    cor_fpga0_bottom_asic2=[25,0,-4,0,-3,0,2,0,-1,0,-2,0,1,0,1,0,2,0,3,0,3,0,3,0,1,0,2,0,2,0,1,0]
    cor_fpga1_top_asic3=[-4,0,18,0,-1,0,-1,0,-1,0,-1,0,-1,0,2,0,0,0,-1,0,1,0,-1,0,-1,0,2,0,-1,0,1,0]
    cor_fpga1_bottom_asic4=[14,0,0,0,-1,0,-1,0,1,0,-2,0,0,0,1,0,-2,0,2,0,1,0,0,0,5,0,4,0,2,0,-9,0]
    cor_fpga2_top_asic5=[13,0,-2,0,-1,0,-1,0,0,0,-1,0,-2,0,0,0,4,0,4,0,0,0,2,0,1,0,0,0,2,0,2,0]
    cor_fpga2_bottom_asic6=[52,0,57,0,35,0,8,0,2,0,28,0,25,0,16,0,19,0,12,0,32,0,28,0,28,0,-24,0,-24,0,-30,0]

    # Change DAC6B +10 on All channels (42)
    #s.PR2_Correct6BDac(0,1,cor_fpga0_top_asic1)
    #s.PR2_Correct6BDac(0,2,cor_fpga0_bottom_asic2)
    #s.PR2_Correct6BDac(0,3,cor_fpga1_top_asic3)
    #s.PR2_Correct6BDac(0,4,cor_fpga1_bottom_asic4)
    #s.PR2_Correct6BDac(0,5,cor_fpga2_top_asic5)
    #s.PR2_Correct6BDac(0,6,cor_fpga2_bottom_asic6)

    THRTRANS=[0 for i in range(7)]
    THRTRANSA=[0 for i in range(7)]
    # aproximate version 53 channel/channel
    THRTRANS[1]=250
    THRTRANS[2]=251
    THRTRANS[3]=257
    THRTRANS[4]=267
    THRTRANS[5]=273
    THRTRANS[6]=247

    # 53 ALL
    THRTRANS[1]=257
    THRTRANS[2]=253
    THRTRANS[3]=257
    THRTRANS[4]=269
    THRTRANS[5]=275
    THRTRANS[6]=247

    # 53 ALL AR
    THRTRANSA[1]=257
    THRTRANSA[2]=259
    THRTRANSA[3]=263
    THRTRANSA[4]=275
    THRTRANSA[5]=283
    THRTRANSA[6]=247


    # change thrhshold
    if (not p2C_autoreset_ON):
        shift=10  # version 54 RF
        shift=15  # version 55 RF
        shift=20  # version 56 RF
    
        for i in range(1,7):
            s.PR2_ChangeVthTime(THRTRANS[i]+shift,0,i)
    else:
        shift=10  # version 57 AR
        shift=15  # version 58 AR
        shift=20  # version 59 AR
    
        for i in range(1,7):
            s.PR2_ChangeVthTime(THRTRANSA[i]+shift,0,i)
    # Mask canal 26 sur ASIC 1
    s.uploadChanges("RE31_185_NORES_10_PRC"," Correction 1.7 THR+20 (+28 effective) AUTORESET")


