#getting the configuration
config_statename="FEB_TEST_DOME_C"
config_version=6

#output_file parameters
runLocation="Developpement"
data_FileDirectory='/opt/data/'

#Orbit parameters
BC0_period_ms=3560  #means roughly 90 microseconds
BC0_time_stage_duration=[4,4,4,4]
BC0_N_orbits=10
BC0_copy_orbits_only_if_trigger=1 #should be 0 or 1

