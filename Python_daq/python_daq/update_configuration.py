import json as js
from configuration_helper import *
import MongoFeb as mg

if __name__ == "__main__":
    #config to fill
    state_name = "RE31_186_FEBv2_2_14_904"
    Vth_shift10b = 90
    voltage = "HV100V"
    autoReset = False
    valtime = 4
    
    
    s=mg.instance()
    s.download(state_name,1)
    if not autoReset:
        s.update_configuration_daq(fsm_val=[1,3,3,3])
    else:
        s.PR2C_EnableAutoReset(valtime)
        
    s.update_pedestals(shift10b=Vth_shift10b,fidir="/home/acqcmsmu/FEB_DAQ/python_src_BC0id_01092021/python_src_BC0id/dac6bConfig_"+state_name+"_"+voltage, mask_list=[], version=state_name)
    s.uploadChanges(state_name,"After pedestals equalization, VthTime at pedestals "+str(Vth_shift10b)+", with double gap")
      
