
import json as js
from configuration_helper import *
import MongoFeb as mg

cor_fpga0_bottom_asic2=[35,0,16,0,8,0,20,0,3,0,10,0,-1,0,-3,0,-5,0,-14,0,-16,0,-13,0,-7,0,-11,0,-10,0,-11,0]





if __name__ == "__main__":

    p_type=2
    p2C_autoreset_ON=True
    P2C_resetDelay_parameter=5
    P2B_FSM_parameters=[3,2,2]

    # Create access and download initial state
    s=mg.instance()
    s.download("RE31_185_NORES_10_PRC",1) 
    # Mask odd Channels
    for iasic in range(1,7):
        for ch in range(32):
            if (ch%2 ==0):
                continue
            s.PR2_ChangeMask(0,iasic,ch,1)
            s.PR2_Change6BDac(0,iasic,ch,1)
    # Set the RESET mechanism
    if p_type==2 and p2C_autoreset_ON:
        print ("Petiroc 2C Autoreset mode")
        s.PR2C_EnableAutoReset(P2C_resetDelay_parameter)
    else:
        print ("Petiroc 2B style RAZ FSM")
        s.PR2C_DisableAutoReset(P2B_FSM_parameters[0],P2B_FSM_parameters[1],P2B_FSM_parameters[2])
    # Now apply DAC 6Bits corrections
    s.PR2_Correct6BDac(0,2,cor_fpga0_bottom_asic2)
    # Upload changes
    s.uploadChanges("RE31_185_NORES_10_PRC","AR on 0 bottom only 1st pass")


