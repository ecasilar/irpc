
import json as js
from configuration_helper import *
import MongoFeb as mg
THRTRANS=[]
for i in range(7):
    THRTRANS.append(0)

THRTRANS[1]=412
THRTRANS[2]=391
THRTRANS[3]=429
THRTRANS[4]=433
THRTRANS[5]=417
THRTRANS[6]=397

if __name__ == "__main__":

    # Threhsold shift
    p_shift=30
    # Download Pedxestal aligned version
    s=mg.instance()
    s.download("RE31_186_FEB_6_gifOctober",15)
    # Change Threshold
    for i in range(1,7):
        s.PR2_ChangeVthTime(THRTRANS[i]+p_shift,0,i)
    # Upload new version 
    s.uploadChanges("RE31_186_FEB_6_gifOctober","Test alignement LM from version 15 VTHTIME=PED+30 (90 fC)")


