def check_int(value,min,max,what):
    if isinstance(value,int):
       if value>=min and value <=max:
          return True
       else: 
          raise(ValueError("{} should be in the range [{},{}]. {} given".format(what,min,max,value)))
    else:
       raise(TypeError("{} should be an int. {} given".format(what,value)))       

def check_bool(value,what):
    if isinstance(value,bool):        
       return True
    else:
       raise(TypeError("{} should be a boolean. {} given".format(what,value)))       

def get_strip_name(strip_number=0,returnSide=True):
    check_int(strip_number,0,47,"get_strip_name::strip_number")
    check_bool(returnSide,"get_strip_name::returnSide")
    name="strip_"
    if returnSide:
        name="r_"+name
    return name+str(strip_number)
def get_strip_number_from_name(strip_name="r_strip_0"):
    twolast=strip_name[-2:]
    if twolast[0]=='_':
       number=int(twolast[1])
    else:
       number=int(twolast)
    check_int(number,0,47,"get_strip_number_from_name found strip number from string")
    return number
def is_it_return_strip_from_strip_name(strip_name="r_strip_0"):
    return strip_name[:2]=='r_'

def get_petiroc_name(petiroc_number=0):
    check_int(petiroc_number,0,1,"get_petiroc_name::petiroc_number")
    if petiroc_number==0:
       return "bottom"
    else:
       return "top"
def get_petiroc_number_from_name(petiroc_name):
    if petiroc_name=="bottom":
       return 0
    if petiroc_name=="top":
       return 1
    raise ValueError("sget_petiroc_number_from_name::petiroc_name should be either 'bottom' or 'top'. {} given".format(petiroc_name))

def get_fpga_name(fpga_number=0):
    check_int(fpga_number,0,2,"get_fpga_name::fpga_number")
    return "fpga_"+str(fpga_number)
def get_fpga_number_from_name(fpga_name="fpga_0"):
    number=int(fpga_name[-1:])
    check_int(number,0,2,"get_fpga_number_from_name found fpga number from string")
    return number



def get_fpga_number(strip_number=0):
    check_int(strip_number,0,47,"get_fpga_number::strip_number")
    return int(strip_number/16)

def get_petiroc_number(strip_number=0):
    check_int(strip_number,0,47,"get_petiroc_number::strip_number")
    return int(strip_number/8)%2

def get_strip_number_local(strip_number=0):
    check_int(strip_number,0,47,"get_strip_number_local::strip_number")
    strip_number_local=strip_number%16
    if strip_number_local>=8:
       strip_number_local=15-strip_number_local
    check_int(strip_number_local,0,7,"get_petiroc_channel debug for strip_number_local")
    return  strip_number_local

def get_petiroc_channel(strip_number=0,returnSide=True):
    check_int(strip_number,0,47,"get_petiroc_channel::strip_number")
    check_bool(returnSide,"get_petiroc_channel::returnSide")
    strip_number_local=get_strip_number_local(strip_number)
    petiroc_channel=strip_number_local*4
    petiroc_number=get_petiroc_number(strip_number)
    if (petiroc_number==0 and not returnSide) or (petiroc_number==1 and returnSide):
       petiroc_channel=petiroc_channel+2
    return petiroc_channel

def get_TDC_channel(strip_number=0,returnSide=True):
    check_int(strip_number,0,47,"get_petiroc_channel::strip_number")
    check_bool(returnSide,"get_petiroc_channel::returnSide")
    if (strip_number%16)>7:
       tdc_shift=0 #top petiroc
    else:
       tdc_shift=16 #bottom petiroc
    strip_number_local=get_strip_number_local(strip_number)
    TDC_channel=strip_number_local*2+tdc_shift
    petiroc_number=get_petiroc_number(strip_number)
    if (petiroc_number==0 and not returnSide) or (petiroc_number==1 and returnSide):
       TDC_channel=TDC_channel+1
    return TDC_channel

def generate_Alban_LookupTable():
    Alban_lookuptable_strips = {}
    for strip in range(48):
        for returnType in [True,False]:
           stripName=get_strip_name(strip,returnType)
           fpga_number=get_fpga_number(strip)
           fpga_name=get_fpga_name(fpga_number)
           petiroc_number=get_petiroc_number(strip)
           petiroc_name=get_petiroc_name(petiroc_number)
           petiroc_channel=get_petiroc_channel(strip,returnType)
           tdc_channel=get_TDC_channel(strip,returnType)
           Alban_lookuptable_strips[stripName]=[fpga_name,petiroc_name,petiroc_channel,tdc_channel]
    return Alban_lookuptable_strips

def generate_petiroc_channel_tdc_channel_mapping():
    table={}
    table["bottom"]={}
    table["top"]={}
    for strip in range(16):
        for returnType in [True,False]:
            petiroc_number=get_petiroc_number(strip)
            petiroc_name=get_petiroc_name(petiroc_number)
            petiroc_channel=get_petiroc_channel(strip,returnType)
            tdc_channel=get_TDC_channel(strip,returnType)
            table[petiroc_name][petiroc_channel]=tdc_channel
    return table

if __name__ == '__main__':
   a=generate_Alban_LookupTable()
   for s,v in a.items():
      print("{:<12} : [{}, {:<8},{:>3},{:>3}]".format(repr(s),repr(v[0]),repr(v[1]),v[2],v[3])) 
   b=generate_petiroc_channel_tdc_channel_mapping()
   for petirocname,channels in b.items():
       print("Petiroc ", petirocname)
       for petiroc,tdc in channels.items():
           print("petiroc {:>3} is tdc {:>3}".format(petiroc,tdc))
