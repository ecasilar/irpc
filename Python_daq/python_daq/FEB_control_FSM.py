import logging
import threading
import random
import time
import argparse
import MongoFeb as mg
import json
import os.path
from transitions import Machine, State
import FEBchannels_converter as FEBcc
from fc7 import *
from feb_v2_cycloneV_r1 import *
from feb_fpga_registers import * 
import FebWriter as FW
import FEB_DAQ_user_settings as user

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.FileHandler("debug.log",mode='w')#,
        #logging.StreamHandler()
    ]
)



class FEB_control_FSM:
      def __init__(self,daqMode):
          # The DAQ FSM by itself
          self.running=False
          self.configured=False          
          self.daqfsm = Machine(model=self, states=['CREATED', 'INITIALISED', 'CONFIGURED', 'RUNNING', 'CONFIGURED'], initial='CREATED')
          self.daqfsm.add_transition('initialise', 'CREATED'                    , 'INITIALISED', after='daq_initialising')
          self.daqfsm.add_transition('configure' , ['INITIALISED', 'CONFIGURED'], 'CONFIGURED' , after='daq_configuring')
          self.daqfsm.add_transition('start'     , 'CONFIGURED',                  'RUNNING',     after='daq_starting', conditions='isConfigured')
          self.daqfsm.add_transition('stop'      , 'RUNNING',                     'CONFIGURED',  after='daq_stopping', conditions='isConfigured')
          self.daqfsm.add_transition('destroy'   , ['INITIALISED', 'CONFIGURED'], 'CREATED',     after='daq_destroying')
          self.state="CREATED"
          self.setDaqMode(daqMode)
          # data needed for non dummy modes
          self.run=1000
          #getting the configuration
          self.config_jsonFileDirectory='/dev/shm/mgroc'
          self.config_jsonFileName='dummy.json'
          self.config_statename=user.config_statename
          self.config_version=user.config_version
          self.feb_petiroc_type=user.feb_petiroc_type
          self.config_source='DB'
          self.pauseconfig=1
          #data needed for Alban FEB interface
          self.fc7_verbosity=False
          self.feb_verbosity=False
          self.fc7_TimeWindow_in_ms=1
          self.fc7_circular_buffer_size=10000
          self.fc7_nbFrame_forTDCcount_Mode=100
          #FEB data output file
          self.runLocation=user.runLocation
          self.runCondition=user.runCondition
          self.data_FileDirectory=user.data_FileDirectory
          self.IPBusReadoutSize=8192 #this is the maximum = the size of the ipbus FIFO
          self.AlbanStylePrintout=False
          #list of channels to enable
          self.fpga_in_daq=set()
          self.all_fpgas_in_daq=0
          self.fpgaTDC_channels_in_daq=[ set(), set(), set() ]
          self.petiroc_channels_in_daq=[ [set(),set()], [set(),set()], [set(),set()] ]
          self.petiroc_to_TDC=FEBcc.generate_petiroc_channel_tdc_channel_mapping()
          #BCO configuration = FC7 configuration
          self.BC0_period_ms=user.BC0_period_ms
          self.BC0_transmission_mode=0 # transmission mode = 0 : BC0_cosmic | 1 : Periodic | 2 : Periodic with Delay | 3 : One pulse
          self.BC0_time_stage1=user.BC0_time_stage_duration[0]
          self.BC0_time_stage2=user.BC0_time_stage_duration[1]
          self.BC0_time_stage3=user.BC0_time_stage_duration[2]
          self.BC0_time_stage4=user.BC0_time_stage_duration[3]
          self.BC0_N_orbits=user.BC0_N_orbits
          self.BC0_copy_orbits_only_if_trigger=user.BC0_copy_orbits_only_if_trigger
          #FEB type FEBv2_1 or FEBv2_2 for mapping
          self.feb_type = user.feb_type
          #description of low level fpga register
          self.fpga_register=feb_fpga_registers("any")
          #debug printout
          self.EDAQ_debug=False
          self.dummy=False

      def getEventNumber(self):
          return self.writer.eventNumber()
      def getRunNumber(self):
          return self.writer.runNumber()

      def check_register_exists(self,fpga_bitword,register):
        if not(isinstance(fpga_bitword,int)):
            raise(TypeError("fpga_bitword for fpga register access should be an int"))
        if fpga_bitword<0 or fpga_bitword>7:
            raise(ValueError("fpga_bitword for fpga register access should be in the range [0,7]"))
        if not(isinstance(register,str)):
            raise(TypeError("fpga_registers register name should be a string"))
        if not (register in self.fpga_register.fpga_register.keys()):
            raise(ValueError("fpga_registers register name unknown"))

      def check_write_data_type(self,wdata):
          for x in wdata:
            if not(isinstance(x,int)):
                raise(TypeError("write fpga_register : data type should be int !"))
            
      def write_fpga_register(self,fpga_bitword,register,data):
          self.check_register_exists(fpga_bitword,register)
          if self.fpga_register.get_mode(register)=="ro":
              raise(ValueError("Attempt to write in a fpga read only register"))
          wdata=data
          if not(isinstance(data,list)):
              wdata=[data]
          self.check_write_data_type(wdata)
          self.fc7.downlink.send_SC_frame(fpga_bitword,1,self.fpga_register.get_address(register),wdata)

          
      def loadConfigFromFile(self):
          fname="%s/%s" % (self.config_jsonFileDirectory,self.config_jsonFileName)
          f=open(fname)
          #self.config_state=sf=json.loads(f.read())
          self.config_state=json.loads(f.read())
          message="Downloaded STATE: {} version: {}".format(self.config_state["state"],self.config_state["version"])
          logging.info(message)
          f.close()

      def loadConfig(self): 
        if self.config_source.lower() == 'test':
            self.config_jsonFileDirectory = "./"
            self.config_jsonFileName = "conf_ece.json"

        if self.config_source=='DB':
          wdd=mg.instance()
          wdd.download(self.config_statename,self.config_version,True,odir=self.config_jsonFileDirectory)
          self.config_jsonFileName="%s_%s.json" % (self.config_statename,self.config_version)
        self.loadConfigFromFile()

      def configure_fromjson(self):
        for fpgaID in ["fpga_0", "fpga_1", "fpga_2"]:
            for asicID in lsAsic:
                self.PetirocChannelEnable([fpgaID], [asicID], lbChannelEnable=[True]*16)
                value = self.readDacDefaultConfig(fpgaID,asicID)
                for i in range(16): self.feb.asic.asic[fpgaID][asicID].set_6b_dac(i*2,int(value[i+1]))
                self.feb.asic.asic[fpgaID][asicID].set_10b_dac_T(int(value[0])+threshold)

  
      def overwrite_configuration(self,asic,asic_num, petiroc_type):
        for ch in range(32):
          asic.set_any_config("mask_discri_charge_ch%d" % ch,self.config_state["asics"][asic_num]["slc"]["MaskDiscriCharge"][ch])
          asic.set_any_config("input_dac_ch%d" % ch,self.config_state["asics"][asic_num]["slc"]["InputDac"][ch])
          asic.set_any_config("cmd_input_dac_ch%d" % ch,self.config_state["asics"][asic_num]["slc"]["InputDacCommand"][ch])
          asic.set_any_config("mask_discri_time_ch%d" % ch,self.config_state["asics"][asic_num]["slc"]["MaskDiscriTime"][ch])
          asic.set_any_config("6b_dac_ch%d" % ch,self.config_state["asics"][asic_num]["slc"]["6bDac"][ch])


        asic.set_any_config("EN_10bits_DAC",self.config_state["asics"][asic_num]["slc"]["EN10bDac"])
        asic.set_any_config("PP_10bits_DAC",self.config_state["asics"][asic_num]["slc"]["PP10bDac"])
          
        asic.set_any_config("10b_dac_vth_discri_charge",self.config_state["asics"][asic_num]["slc"]["VthDiscriCharge"])
        asic.set_any_config("10b_dac_vth_discri_time",int(self.config_state["asics"][asic_num]["slc"]["VthTime"]))

        ##
        asic.set_any_config("EN_ADC"                  ,self.config_state["asics"][asic_num]["slc"]["EN_adc"])
        asic.set_any_config("PP_ADC"                  ,self.config_state["asics"][asic_num]["slc"]["PP_adc"])
        asic.set_any_config("sel_startb_ramp_ADC_ext" ,self.config_state["asics"][asic_num]["slc"]["sel_starb_ramp_adc_ext"])
        asic.set_any_config("usebcompensation"        ,self.config_state["asics"][asic_num]["slc"]["usebcompensation"])
        asic.set_any_config("EN_bias_DAC_delay"       ,self.config_state["asics"][asic_num]["slc"]["EN_bias_dac_delay"])
        asic.set_any_config("PP_bias_DAC_delay"       ,self.config_state["asics"][asic_num]["slc"]["PP_bias_dac_delay"])
        asic.set_any_config("EN_bias_ramp_delay"      ,self.config_state["asics"][asic_num]["slc"]["EN_bias_ramp_delay"])
        asic.set_any_config("PP_bias_ramp_delay"      ,self.config_state["asics"][asic_num]["slc"]["PP_bias_ramp_delay"])
        asic.set_any_config("8b_dac_delay"            ,self.config_state["asics"][asic_num]["slc"]["DacDelay"])
        asic.set_any_config("EN_discri_delay"         ,self.config_state["asics"][asic_num]["slc"]["EN_discri_delay"])
        asic.set_any_config("PP_discri_delay"         ,self.config_state["asics"][asic_num]["slc"]["PP_discri_delay"])
        asic.set_any_config("PP_temp_sensor"          ,self.config_state["asics"][asic_num]["slc"]["PP_temp_sensor"])
        asic.set_any_config("EN_temp_sensor"          ,self.config_state["asics"][asic_num]["slc"]["EN_temp_sensor"])
        asic.set_any_config("EN_bias_pa"              ,self.config_state["asics"][asic_num]["slc"]["EN_bias_pa"])
        asic.set_any_config("PP_bias_pa"              ,self.config_state["asics"][asic_num]["slc"]["PP_bias_pa"])
        asic.set_any_config("EN_bias_discri"          ,self.config_state["asics"][asic_num]["slc"]["EN_bias_discri"])
        asic.set_any_config("PP_bias_discri"          ,self.config_state["asics"][asic_num]["slc"]["PP_bias_discri"])

        asic.set_any_config("cmd_polarity"            ,self.config_state["asics"][asic_num]["slc"]["cmd_polarity"])
        asic.set_any_config("latch_discri"            ,self.config_state["asics"][asic_num]["slc"]["latch"])
        asic.set_any_config("EN_bias_6b_dac"          ,self.config_state["asics"][asic_num]["slc"]["EN_bias_6bit_dac"])
        ###
        asic.set_any_config("PP_bias_6b_dac"          ,self.config_state["asics"][asic_num]["slc"]["PP_bias_6bit_dac"])
        ###
        asic.set_any_config("EN_bias_tdc"             ,self.config_state["asics"][asic_num]["slc"]["EN_bias_tdc"])
        asic.set_any_config("PP_bias_tdc"             ,self.config_state["asics"][asic_num]["slc"]["PP_bias_tdc"])
        asic.set_any_config("ON_OFF_input_dac"        ,self.config_state["asics"][asic_num]["slc"]["ON_OFF_input_dac"])
        asic.set_any_config("EN_bias_charge"          ,self.config_state["asics"][asic_num]["slc"]["EN_bias_charge"])
        asic.set_any_config("PP_bias_charge"          ,self.config_state["asics"][asic_num]["slc"]["PP_bias_charge"])
        asic.set_any_config("cf_100fF"                ,self.config_state["asics"][asic_num]["slc"]["Cf3_100fF"])
        asic.set_any_config("cf_200fF"                ,self.config_state["asics"][asic_num]["slc"]["Cf2_200fF"])
        asic.set_any_config("cf_2_5pF"                ,self.config_state["asics"][asic_num]["slc"]["Cf1_2p5pF"])
        asic.set_any_config("cf_1_25pF"               ,self.config_state["asics"][asic_num]["slc"]["Cf0_1p25pF"])
        asic.set_any_config("EN_bias_sca"             ,self.config_state["asics"][asic_num]["slc"]["EN_bias_sca"])
        asic.set_any_config("PP_bias_sca"             ,self.config_state["asics"][asic_num]["slc"]["PP_bias_sca"])
        asic.set_any_config("EN_bias_discri_charge"   ,self.config_state["asics"][asic_num]["slc"]["EN_bias_discri_charge"])
        asic.set_any_config("PP_bias_discri_charge"   ,self.config_state["asics"][asic_num]["slc"]["PP_bias_discri_charge"])
        asic.set_any_config("EN_bias_discri_adc_time" ,self.config_state["asics"][asic_num]["slc"]["EN_bias_discri_adc_time"])
        asic.set_any_config("PP_bias_discri_adc_time" ,self.config_state["asics"][asic_num]["slc"]["PP_bias_discri_adc_time"])
        asic.set_any_config("EN_bias_discri_adc_charge",self.config_state["asics"][asic_num]["slc"]["EN_bias_discri_adc_charge"])
        asic.set_any_config("PP_bias_discri_adc_charge",self.config_state["asics"][asic_num]["slc"]["PP_bias_discri_adc_charge"])
        asic.set_any_config("DIS_razchn_int"           ,self.config_state["asics"][asic_num]["slc"]["DIS_razchn_int"])
        asic.set_any_config("DIS_razchn_ext"           ,self.config_state["asics"][asic_num]["slc"]["DIS_razchn_ext"])
        asic.set_any_config("SEL_80M"                  ,self.config_state["asics"][asic_num]["slc"]["SEL_80M"])
        asic.set_any_config("EN_80M"                   ,self.config_state["asics"][asic_num]["slc"]["EN_80M"])
        asic.set_any_config("EN_slow_lvds_rec"         ,self.config_state["asics"][asic_num]["slc"]["EN_slow_lvds_rec"])
        #
        asic.set_any_config("PP_slow_lvds_rec"         ,self.config_state["asics"][asic_num]["slc"]["PP_slow_lvds_rec"])
        #
        asic.set_any_config("EN_fast_lvds_rec"         ,self.config_state["asics"][asic_num]["slc"]["EN_fast_lvds_rec"])
        asic.set_any_config("PP_fast_lvds_rec"         ,self.config_state["asics"][asic_num]["slc"]["PP_fast_lvds_rec"])
        asic.set_any_config("EN_transmitter"           ,self.config_state["asics"][asic_num]["slc"]["EN_transmitter"])
        asic.set_any_config("PP_transmitter"           ,self.config_state["asics"][asic_num]["slc"]["PP_transmitter"])
        asic.set_any_config("ON_OFF_1mA"               ,self.config_state["asics"][asic_num]["slc"]["ON_OFF_1mA"])
        asic.set_any_config("ON_OFF_2mA"               ,self.config_state["asics"][asic_num]["slc"]["ON_OFF_2mA"])
        asic.set_any_config("ON_OFF_ota_mux"           ,self.config_state["asics"][asic_num]["slc"]["ON_OFF_ota_mux"])
        asic.set_any_config("ON_OFF_ota_probe"         ,self.config_state["asics"][asic_num]["slc"]["ON_OFF_ota_probe"])
        asic.set_any_config("DIS_trig_mux"             ,self.config_state["asics"][asic_num]["slc"]["DIS_trig_mux"])
        asic.set_any_config("EN_NOR32_time"            ,self.config_state["asics"][asic_num]["slc"]["EN_NOR32_time"])
        asic.set_any_config("EN_NOR32_charge"          ,self.config_state["asics"][asic_num]["slc"]["EN_NOR32_charge"])
        asic.set_any_config("DIS_triggers"             ,self.config_state["asics"][asic_num]["slc"]["DIS_triggers"])
        asic.set_any_config("EN_dout_oc"               ,self.config_state["asics"][asic_num]["slc"]["EN_dout_oc"])
        asic.set_any_config("EN_transmit"              ,self.config_state["asics"][asic_num]["slc"]["EN_transmit"])
        asic.set_any_config("PA_Ccomp<0>"              ,self.config_state["asics"][asic_num]["slc"]["PA_ccomp_0"])
        asic.set_any_config("PA_Ccomp<1>"              ,self.config_state["asics"][asic_num]["slc"]["PA_ccomp_1"])
        asic.set_any_config("PA_Ccomp<2>"              ,self.config_state["asics"][asic_num]["slc"]["PA_ccomp_2"])
        asic.set_any_config("PA_Ccomp<3>"              ,self.config_state["asics"][asic_num]["slc"]["PA_ccomp_3"])
        asic.set_any_config("Choice_trigger_out"       ,self.config_state["asics"][asic_num]["slc"]["Choice_Trigger_Out"])
        asic.set_any_config("NC1"                      ,self.config_state["asics"][asic_num]["slc"]["NC1"])
        asic.set_any_config("NC2"                      ,self.config_state["asics"][asic_num]["slc"]["NC2"])
        asic.set_any_config("NC3"                      ,self.config_state["asics"][asic_num]["slc"]["NC3"])
        asic.set_any_config("NC4"                      ,self.config_state["asics"][asic_num]["slc"]["NC4"])
        if (petiroc_type == 2):
            asic.set_any_config("Delay_reset_trigger"                      ,self.config_state["asics"][asic_num]["slc"]["Delay_reset_trigger"])
            asic.set_any_config("NC5"                      ,self.config_state["asics"][asic_num]["slc"]["NC5"])
            asic.set_any_config("NC6"                      ,self.config_state["asics"][asic_num]["slc"]["NC6"])
            asic.set_any_config("NC7"                      ,self.config_state["asics"][asic_num]["slc"]["NC7"])
            asic.set_any_config("EN_reset_trigger_delay"   ,self.config_state["asics"][asic_num]["slc"]["EN_reset_trigger_delay"])
            asic.set_any_config("Delay_reset_ToT"          ,self.config_state["asics"][asic_num]["slc"]["Delay_reset_ToT"])
            asic.set_any_config("NC8"          ,self.config_state["asics"][asic_num]["slc"]["NC8"])
            asic.set_any_config("NC9"          ,self.config_state["asics"][asic_num]["slc"]["NC9"])
            asic.set_any_config("NC10"          ,self.config_state["asics"][asic_num]["slc"]["NC10"])
            asic.set_any_config("EN_reset_ToT_delay"          ,self.config_state["asics"][asic_num]["slc"]["EN_reset_ToT_delay"])
            ### WARNING put the chip in Noise LM
            #asic.set_any_config("10b_dac_vth_discri_time",300)

      def isConfigured(self):
          return self.configured
     
      def setDaqMode(self,daqMode):
          self.daqMode=daqMode
          if self.daqMode=="DoNothing":
             self.setDoNothingMode()
          if self.daqMode=="TimeWindow":
             self.setTimeWindowMode()
          if self.daqMode=="TDC_Count":
             self.setTDCcountMode()
          if self.daqMode=="Circular_Buffer_External_Trigger":
             self.setExternalCircularBufferMode()
          if self.daqMode=="Circular_Buffer_Internal_Trigger":
             self.setInternalCircularBufferMode()
          if self.daqMode=="Orbit":
             self.setOrbitMode()


      # Do nothing DAQ mode. This allows to test the FSM.
      def setDoNothingMode(self):
          self.daq_initialising=self.printState
          self.daq_configuring=self.do_nothing_configure
          self.daq_starting=self.do_nothing_start
          self.daq_stopping=self.do_nothing_stop
          self.daq_destroying=self.printState

      def do_nothing_configure(self):
          self.configured=True
          self.printState()
      def do_nothing_start(self):
          self.running=True
          self.printState()
      def do_nothing_stop(self):
          self.running=False
          self.printState()

              
      def printState(self):
          print(self.state)
          
      # Talking to the FEB through FC7
      #
      def setOrbitMode(self):
          self.fc7_DaqMode="Wait_orbit"
          self.setAcquisitionWithFEB()         
      def setTimeWindowMode(self):
          self.fc7_DaqMode="Time_Window"
          self.setAcquisitionWithFEB()
      def setTDCcountMode(self):
          self.fc7_DaqMode="TDC_Count"
          self.setAcquisitionWithFEB()
      def setExternalCircularBufferMode(self):
          self.fc7_DaqMode="Circular_Buffer_External"
          self.setAcquisitionWithFEB() 
      def setInternalCircularBufferMode(self):
          self.fc7_DaqMode="Circular_Buffer_Internal"
          self.setAcquisitionWithFEB()
      def startInternalTrigger(self):
          self.fc7.set_internal_trigger(1)
      def stopInternalTrigger(self):
          self.fc7.set_internal_trigger(0)
      def setAcquisitionWithFEB(self):
          self.daq_initialising=self.FEB_initialising
          self.daq_configuring=self.FEB_configuring
          self.daq_starting=self.FEB_starting
          self.daq_stopping=self.FEB_stopping
          self.daq_destroying=self.FEB_destroying
          self.feb_running=self.FEB_acquiring_data
      
      def FEB_initialising(self):
          if self.EDAQ_debug:
            logging.getLogger().setLevel(logging.DEBUG)
          self.fc7 = fc7(self.fc7_verbosity)
          self.feb = feb_v2_cycloneV_r1(self.fc7,self.feb_verbosity)
          self.fc7.set_general_rst(1)
          self.fc7.set_general_rst(0)
          self.fc7.init()
          self.feb.init()
          time.sleep(1)
          logging.info("Still Initialising -1")
          self.fc7.fifos_clear()
          time.sleep(2)
          #self.feb.reset()
          logging.info("Still Initialising 0")
          time.sleep(1)
          self.feb.asic.init(False)
          time.sleep(1)
          logging.info("Still Initialising 1")
          self.feb.asic.rstb("all","all")
          time.sleep(1)
          logging.info("Still Initialising 2 ")
          self.feb.asic.sr_rst("all","all")
          time.sleep(1)
          logging.info("Still Initialising 3")
          self.fc7.stop_acquisition()
          self.fc7.set_acquisition_mode(self.fc7_DaqMode)
          self.fc7.set_time_window(self.fc7_TimeWindow_in_ms)
          self.fc7.set_circular_buffer_size(self.fc7_circular_buffer_size)
          self.fc7.set_nb_frame_acquisition(self.fc7_nbFrame_forTDCcount_Mode)
          #activate the NOR32RAZ FSM. 17,1,3 are values in 5ns units used for irradiation tests 
          #for fpga_name in self.feb.fpga_name:
          self.fc7.acquisition_info()
          #self.feb.reset()
          time.sleep(1)
          #self.feb.reset()
          logging.info("DAQ, FC7 and FEB are initialised")

      def enable_TDC(self):
          self.write_fpga_register(self.all_fpgas_in_daq,"TDC_ENABLE",1)
      def disable_TDC(self):
          self.write_fpga_register(self.all_fpgas_in_daq,"TDC_ENABLE",0)
      def start_BC0(self):
          self.fc7.set_BC0_cosmic(1)
          self.fc7.set_enable_BC0(1)
      def stop_BC0(self):
          self.fc7.set_BC0_cosmic(0)
          self.fc7.set_enable_BC0(0)


      def set_top_petiroc_type(self,fpgaName,petirocType):
          self.feb.asic.fpga.write(fpgaName,"PETIROC_TYPE_SELECT_TOP",petirocType)
      def set_bottom_petiroc_type(self,fpgaName,petirocType):
          self.feb.asic.fpga.write(fpgaName,"PETIROC_TYPE_SELECT_BOT",petirocType)
      def set_petiroc_type(self,fpgaName,asicNumber,petirocType):
          if asicNumber==0:
             self.set_bottom_petiroc_type(fpgaName,petirocType)
          if asicNumber==1:
             self.set_top_petiroc_type(fpgaName,petirocType)


      def FEB_configuring(self):
          self.loadConfig()
          self.write_fpga_register(7,"TDC_ENABLE",0) #start by disabling TDC for all FPGAs
          self.feb.tdc.channel_disable("fpga_0","all")
          self.feb.tdc.channel_disable("fpga_1","all")
          self.feb.tdc.channel_disable("fpga_2","all")
          self.fpga_in_daq=set()
          self.all_fpgas_in_daq=0
          self.fpgaTDC_channels_in_daq=[ set(), set(), set() ]
          self.petiroc_channels_in_daq=[ [set(),set()], [set(),set()], [set(),set()] ]
          for asic_num in range(0,6):
                fpga_number=self.config_state["asics"][asic_num]["slc"]["nfpga"]
                fpga_name=FEBcc.get_fpga_name(fpga_number)
                petiroc_name=self.config_state["asics"][asic_num]["slc"]["loc"]
                petiroc_number=FEBcc.get_petiroc_number_from_name(petiroc_name)
                this_petiroc_type=self.feb_petiroc_type[asic_num]
                if user.codeVersion == "ABC":
                     self.set_petiroc_type(fpga_name,asic_num%2,this_petiroc_type) #FIXME asic_number mapping (here 0,2,4 are bottom and 1,3,5 are top. to reverse change asic_num%2 by (asic_num+1)%2
                     
                if user.codeVersion != "ABC" and this_petiroc_type != 1:
                     message="Software version is not A+B+C compatible and for FPGA {}, request petiroc value = {} which is different from 1 (PETIROC2B)".format(fpga_name,this_petiroc_type)
                     logging.critical(message)
                self.overwrite_configuration(self.feb.asic.asic[fpga_name][petiroc_name],asic_num, petiroc_type=this_petiroc_type)
                MaskPetirocChannels=self.config_state["asics"][asic_num]["slc"]["MaskDiscriTime"]
                for petiroc_channel in range(0,32,2):
                      if MaskPetirocChannels[petiroc_channel]==0 or (petiroc_channel == 0 and "FEBv2_2" in self.feb_type and MaskPetirocChannels[1] == 0):
                            tdc_channel=self.petiroc_to_TDC[petiroc_name][petiroc_channel]
                            self.fpga_in_daq.add(fpga_number)
                            self.fpgaTDC_channels_in_daq[fpga_number].add(tdc_channel)
#                            print("adding channel %d FPGA %d PR %s %d \n" % (tdc_channel,fpga_number,petiroc_name,petiroc_channel))
                            if (petiroc_channel == 0 and "FEBv2_2" in self.feb_type):
                                  self.petiroc_channels_in_daq[fpga_number][petiroc_number].add(1)
                            else:
                                  self.petiroc_channels_in_daq[fpga_number][petiroc_number].add(petiroc_channel)
                            
          for fpga_number in self.fpga_in_daq:
              self.all_fpgas_in_daq+=(1<<fpga_number)
          self.write_fpga_register(self.all_fpgas_in_daq,"PETIROC_TOP_NOR32RAZ_FSM"   ,self.config_state["asics"][0]["slc"]["PETIROC_NOR32RAZ_FSM"])
          self.write_fpga_register(self.all_fpgas_in_daq,"PETIROC_BOTTOM_NOR32RAZ_FSM",self.config_state["asics"][0]["slc"]["PETIROC_NOR32RAZ_FSM"])
          for fpga_number in self.fpga_in_daq:
              fpga_name=FEBcc.get_fpga_name(fpga_number)
              self.feb.tdc.channel_enable(fpga_name,list(self.fpgaTDC_channels_in_daq[fpga_number]))
              print(fpga_name,list(self.fpgaTDC_channels_in_daq[fpga_number]))
          message="WARNING enabling channel 32/33 and 33/34 manually, should it come from database ?"
          logging.warning(message)
          self.write_fpga_register(self.all_fpgas_in_daq,"TDC_ENABLE_CH32_33",3)
          self.write_fpga_register(self.all_fpgas_in_daq,"TDC_CMD_VALID",1)
          self.disable_TDC()
          self.stop_BC0()
          self.fc7.set_BC0_period(self.BC0_period_ms)
          self.fc7.set_transmission_mode(self.BC0_transmission_mode)
          self.fc7.fpga_registers.set_BC0_cosmic_stage1(self.BC0_time_stage1)
          self.fc7.fpga_registers.set_BC0_cosmic_stage2(self.BC0_time_stage2)
          self.fc7.fpga_registers.set_BC0_cosmic_stage3(self.BC0_time_stage3)
          self.fc7.fpga_registers.set_BC0_cosmic_stage4(self.BC0_time_stage4)
          self.fc7.fpga_registers.set_BC0_N_orbit(self.BC0_N_orbits)
          self.fc7.set_copy_if_resync(self.BC0_copy_orbits_only_if_trigger)
          
          for fpga_number in self.fpga_in_daq:
              fpga_name=FEBcc.get_fpga_name(fpga_number)
              for petiroc_name in ["top","bottom"]:
                    #EDAQ recomends not sending the FPGA configuration fully in one row but one PETIROC at a time for power consumption capacity.
                    configOK=self.feb.asic.configure(fpga_name,petiroc_name)
                    time.sleep(self.pauseconfig)
                    if configOK:
                          message="{} petiroc {} configuration OK".format(fpga_name,petiroc_name)
                          logging.info(message)
                    else:
                          message="{} petiroc {} badly configured".format(fpga_name,petiroc_name)
                          logging.critical(message)
                          print("CONFIGURATION FAILED : see log")
          
          #self.feb.reset()
          self.configured=True
          logging.info("Daq, FCT and FEB are configured")

      def writeRunHeader(self):
          runHeaderWordList=[]
          runHeaderWordList.append(int(self.fc7.fpga_registers.get_general_register())) #[0]
          runHeaderWordList.append(int(self.fc7.get_nb_frame_acquisition()))            #[1]
          runHeaderWordList.append(int(self.fc7.fpga_registers.get_time_window_size())) #[2]
          runHeaderWordList.append(int(self.fc7.get_circular_buffer_size()))            #[3]
          runHeaderWordList.append(int(self.fc7.fpga_registers.get_FC_transmission()))  #[4] bit 21:28 ar number of orbits
          self.feb.feb_ctrl_and_status.get_temperature()
          temperatures=self.feb.feb_ctrl_and_status.temperature_value
          temperatures_int_values=[]
          for i in range(1,6):
              key = "LM75_SENSOR"+str(i)
              temperatures_int_values.append(int(2*temperatures[key][0])) # this should be a 9 bit word
          runHeaderWordList.append(( (temperatures_int_values[0]<<18)+(temperatures_int_values[1]<<9)+temperatures_int_values[2])) #[5]
          runHeaderWordList.append(( (temperatures_int_values[3]<<9)+temperatures_int_values[4]))  #[6]
          #reading back channel enabled word for TDC
          chanset=[self.feb.tdc.get_channel_status("fpga_"+str(n)) for n in range(0,3)]
          runHeaderWordList.append((chanset[0][0]<<16)+chanset[0][1])                                                 #[7]
          runHeaderWordList.append((chanset[1][0]<<16)+chanset[1][1])                                                 #[8]
          runHeaderWordList.append((chanset[2][0]<<16)+chanset[2][1])                                                 #[9]
          runHeaderWordList.append((self.all_fpgas_in_daq<<6)+(chanset[0][2]<<4)+(chanset[1][2]<<2)+(chanset[2][2]))  #[10]
          message="Information on potential enabled channel : {}".format(chanset)
          logging.info(message)
          message="runHeader Word List : {}".format(runHeaderWordList)
          logging.debug(message)
          self.writer.writeRunHeader(runHeaderWordList)

      def set_time_calibration_value(self,value):
          self.feb.tdc.set_tdc_injection_mode("fpga_0",value)
          self.feb.tdc.set_tdc_injection_mode("fpga_1",value)
          self.feb.tdc.set_tdc_injection_mode("fpga_2",value)
      def set_time_calibration_ON(self):
          self.set_time_calibration_value(8)
      def set_time_calibration_OFF(self):
          self.set_time_calibration_value(0)

      def FEB_starting(self):
          wdd=mg.instance()
          print("starting a new run, please enter description for future monitoring. Enter blank line to end input")
          lines = []	
          #if not self.AlbanStylePrintout: 
          #    while True:
          #      line = input()
          #      if line:
          #        lines.append(line)
          #      else:
          #        break
          text = '\n'.join(lines)
          newrun=wdd.getRun(self.runLocation.split("_")[0],text)
          self.run=newrun['run']
          os.system("mkdir -p %s" % (self.data_FileDirectory))
          self.writer=FW.FebWriter(self.data_FileDirectory,self.runLocation)
          self.writer.newRun(self.run)
          logging.info("Data are written in file %s." % self.writer.file_name())
          self.writeRunHeader()
          if self.daqMode=="Circular_Buffer_Internal_Trigger":
               self.stopInternalTrigger() # To be sure it is stopped when running in internal circular buffer mode : to start by hand
          self.fc7.stop_acquisition() #start is done in feb_running
          self.start_BC0()
          self.fc7.set_enable_resync(0)
          self.fc7.fifos_clear()
          self.enable_TDC()
          self.running=True
          self.producer_thread = threading.Thread(target=self.feb_running)
          self.producer_thread.start()
          logging.info("Daq is started")
          message = "Run number : "+str(self.run)
          logging.info(message)

      def debugFifo(self):
          a=self.fc7.fpga_registers.get_fifos_status()
          message="{hexadecimal} = {binaire} : done={done}, nwords={nwords}".format(hexadecimal=hex(a),binaire=bin(a),done=bf(a)[14],nwords=bf(a)[16:30])
          logging.debug(message)
      def FEB_acquiring_data(self):
          nacq=0
          ntrig = 0
          while (self.running):
               if (self.veto()):
                  time.sleep(0.01)
                  continue
               self.fc7.start_acquisition()
               nacq=nacq+1
               if self.daqMode=="Circular_Buffer_Internal_Trigger":
                  fb.startInternalTrigger()
                  fb.stopInternalTrigger()
                  time.sleep(0.1)
               dataWait=0
               while self.fc7.get_acquisition_status()==0:
                     if dataWait%10000==0 and dataWait>1E6:
                        message="WAITING DATA {}".format(dataWait)
                        logging.info(message)
                     dataWait=dataWait+1
                     if not self.running:
                           return
                     continue
               if self.EDAQ_debug:
                  message="info dataWait was {}".format(dataWait)
                  logging.debug(message)
               self.writer.newEvent()
               wordInFIFO=self.fc7.get_rd32b_word_in_fifo_ipbus()
               wordRead=0
               if self.AlbanStylePrintout:
                     allReadoutData=[]
               if self.EDAQ_debug:
                     self.debugFifo()
               while wordInFIFO != 0:
                     wordToRead=wordInFIFO
                     if wordToRead>self.IPBusReadoutSize:
                           wordToRead=self.IPBusReadoutSize
                     readoutData=self.fc7.rx_fifo_TDC.read_block_TDC(wordToRead) #this function returns a list of 32 bits words

                     wordRead+=len(readoutData)
                     self.writer.appendEventData(readoutData)
                     if self.AlbanStylePrintout:
                           allReadoutData+=readoutData
                     wordInFIFO=self.fc7.get_rd32b_word_in_fifo_ipbus()
               if wordRead==0:
                  message="problem at fifo readout number {}, event written {}.".format( nacq, self.writer.eventNumber() )
                  logging.error(message)
                  time.sleep(1)
               else:
                  if not self.dummy:
                     self.writer.writeEvent()
               if (nacq%100==0 or self.EDAQ_debug):
                   message="Info : Event {} (acquisition number {}) have read {} words = {} potential TDC frames.".format(self.writer.eventNumber(),nacq,wordRead, 3*wordRead/8)
                   logging.info(message)
               if (nacq%10==0):
                   with open("/data/trigger_count.txt", "w") as trig_output:
                       trig_output.write(str(self.writer.eventNumber()))

               if self.AlbanStylePrintout:
                     #Alban decoding
                     TDC_frames=self.TDC_pack_all(allReadoutData)
                     decoded_frames=self.fc7.uplink.decode_TDC_frame(TDC_frames)
                     for keys in decoded_frames.keys():
                         for i in decoded_frames[keys][0]["fpga_0"]:
                                ntrig += i[0]//33
                                with open("/data/trigger_count.txt", "w") as trig_output:
                                    trig_output.write(str(ntrig))
                     self.fc7.save_TDC_datas(decoded_frames)
               self.fc7.stop_acquisition()
          logging.info("Thread %d: finishing", self.run)
      def FEB_stopping(self):
          self.running=False
          self.producer_thread.join()
          self.fc7.stop_acquisition()
          self.disable_TDC()
          self.stop_BC0()
          self.writer.endRun()
          self.fc7.acquisition_info()
          logging.info("Daq is stopped")
      def FEB_destroying(self):
          self.configured=False
          logging.info("Daq is destroyed")


      def addStripToEnable(self,stripNumber=0,directStrip=True,returnStrip=True):
          fpga_number=FEBcc.get_fpga_number(stripNumber)
          petiroc_number=FEBcc.get_petiroc_number(stripNumber)
          self.fpga_in_daq.add(fpga_number)
          if directStrip:
             self.fpgaTDC_channels_in_daq[fpga_number].add(FEBcc.get_TDC_channel(stripNumber,False))
             self.petiroc_channels_in_daq[fpga_number][petiroc_number].add(FEBcc.get_petiroc_channel(stripNumber,False))
          if returnStrip:
             self.fpgaTDC_channels_in_daq[fpga_number].add(FEBcc.get_TDC_channel(stripNumber,True))
             self.petiroc_channels_in_daq[fpga_number][petiroc_number].add(FEBcc.get_petiroc_channel(stripNumber,True))
        
      def TDC_pack_all(self,readout):
            length=len(readout)
            message="DEBUG {} /8={}".format(length,length/8)
            logging.debug(message)
            s=[8*i for i in range(int(length/8))]
            packed=[readout[i:8+i] for i in s]
            TDC_frames=[ ((r[3]<<128) + (r[4]<< 96) +(r[5]<<64) +(r[6]<<32) + r[7]) for r in packed]
            return TDC_frames

      def  veto(self):
          return False
 
      
if __name__ == '__main__':
   daqMode="DoNothing"
   parser = argparse.ArgumentParser()
   parser.add_argument("-dummy", "--do_nothing_daq_mode", help="Run the DAQ FSM without doing any real action (default)",action="store_true")
   parser.add_argument("-timewindow", "--time_window_daq_mode", help="Run the DAQ FC7 in time window mode",action="store_true")
   parser.add_argument("-TDCcount", "--TDC_COunt_daq_mode", help="Run the DAQ FC7 in TDC count mode",action="store_true")
   parser.add_argument("-external_trigger_circular_buffer", "--external_trigger_circular_buffer_daq_mode", help="Run the DAQ FC7 in circular external trigger mode",action="store_true")
   parser.add_argument("-internal_trigger_circular_buffer", "--internal_trigger_circular_buffer_daq_mode", help="Run the DAQ FC7 in circular internal trigger mode",action="store_true")
   parser.add_argument("-orbit", "--orbit_wait_daq_mode", help="Run the DAQ FC7 in wait orbit mode",action="store_true")
   args = parser.parse_args()
   needStateConfig=False
   if args.do_nothing_daq_mode:
      daqMode="DoNothing"
   elif args.time_window_daq_mode:
      daqMode="TimeWindow"
      needStateConfig=True
   elif args.TDC_COunt_daq_mode:
      daqMode="TDC_Count"
      needStateConfig=True
   elif args.external_trigger_circular_buffer_daq_mode:
       daqMode="Circular_Buffer_External_Trigger"
       needStateConfig=True
   elif args.internal_trigger_circular_buffer_daq_mode:
       daqMode="Circular_Buffer_Internal_Trigger"
       needStateConfig=True
   elif args.orbit_wait_daq_mode:
       daqMode="Orbit"
       needStateConfig=True       
   print("daqMode is "+daqMode)
   
   fb=FEB_control_FSM(daqMode)
   if needStateConfig:
      #fb.config_statename="UN_TEST_FEBV2"
      fb.config_statename="FEB_TEST_DOME_C"
      fb.config_version=2
      fb.config_source="DB"
   fb.AlbanStylePrintout=True
   fb.initialise()
   time.sleep(2)
   fb.configure()
   time.sleep(2)
   fb.start()
   time.sleep(5)
   fb.stop()
   time.sleep(2)
   fb.destroy()
   

