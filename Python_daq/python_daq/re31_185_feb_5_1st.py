
import json as js
from configuration_helper import *
import MongoFeb as mg



if __name__ == "__main__":

    p_type=1
    p2C_autoreset_ON=False
    P2C_resetDelay_parameter=5
    P2B_FSM_parameters=[3,2,2]
    s=mg.instance()
    s.download("RE31_185_FEB_5_904",4) 
    # Mask odd Channels
    for iasic in range(1,7):
        for ch in range(32):
            if (ch%2 ==0):
                continue
            s.PR2_ChangeMask(0,iasic,ch,1)
            s.PR2_Change6BDac(0,iasic,ch,1)
    # Set the RESET mechanism
    if p_type==2 and p2C_autoreset_ON:
        print ("Petiroc 2C Autoreset mode")
        s.PR2C_EnableAutoReset(P2C_resetDelay_parameter)
    else:
        print ("Petiroc 2B style RAZ FSM")
        s.PR2C_DisableAutoReset(P2B_FSM_parameters[0],P2B_FSM_parameters[1],P2B_FSM_parameters[2])

    # First pass
    cor_fpga0_top_asic1=[69,0,28,0,9,0,40,0,28,0,18,0,7,0,-6,0,47,0,18,0,-14,0,-9,0,-7,0,-25,0,-28,0,-23,0]
    cor_fpga0_bottom_asic2=[1,0,11,0,-25,0,6,0,16,0,-8,0,-19,0,-10,0,-13,0,8,0,37,0,24,0,25,0,-1,0,12,0,11,0]
    cor_fpga1_top_asic3=[12,0,21,0,-23,0,3,0,-1,0,13,0,6,0,-14,0,0,0,-20,0,1,0,23,0,14,0,22,0,12,0,5,0]
    cor_fpga1_bottom_asic4=[25,0,17,0,-1,0,-3,0,-13,0,-25,0,-13,0,-9,0,-12,0,4,0,-6,0,-7,0,-25,0,-9,0,-7,0,-23,0]
    cor_fpga2_top_asic5=[13,0,1,0,-29,0,-8,0,-10,0,1,0,7,0,8,0,3,0,9,0,16,0,-3,0,28,0,29,0,13,0,25,0]
    cor_fpga2_bottom_asic6=[5,0,-2,0,-13,0,8,0,15,0,-14,0,-13,0,9,0,-12,0,-23,0,-13,0,-5,0,23,0,-2,0,-6,0,-11,0]

    THRTRANS=[0 for i in range(7)]
    THRTRANSA=[0 for i in range(7)]
    #2nd pass
    cor_fpga0_top_asic1=[57,0,22,0,5,0,28,0,5,0,9,0,-1,0,-1,0,1,0,-1,0,-1,0,-2,0,-2,0,-1,0,-2,0,-2,0]
    THRTRANS[1]=345
    cor_fpga0_bottom_asic2=[-1,0,1,0,-1,0,1,0,1,0,1,0,1,0,1,0,-1,0,1,0,-1,0,-1,0,1,0,1,0,-1,0,-1,0]
    THRTRANS[2]=344

    cor_fpga1_top_asic3=[0,0,0,0,-1,0,0,0,0,0,-1,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,0,-1,0,0,0,0,0]
    THRTRANS[3]=297
    cor_fpga1_bottom_asic4=[3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,-1,0,-1,0,0,0,0,0]
    THRTRANS[4]=289 
    cor_fpga2_top_asic5=[0,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,0,0,0,-1,0,0,0,0,0,-1,0,0,0,0,0,0,0]
    THRTRANS[5]=331 

    cor_fpga2_bottom_asic6=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,0,0,0]
    THRTRANS[6]=331

    applyCor=True
    # After 2nd pass
    THRTRANS[1]=345 
    THRTRANS[2]=345 
    THRTRANS[3]=297 
    THRTRANS[4]=289 
    THRTRANS[5]=331 
    THRTRANS[6]=331 
    applyCor=False
    applyThr=False
    # 2nd pass all
    THRTRANS[1]=347 
    THRTRANS[2]=345 
    THRTRANS[3]=299 
    THRTRANS[4]=290 
    THRTRANS[5]=333 
    THRTRANS[6]=331
    applyThr=True
    # Change DAC6B +10 on All channels (42)
    if (applyCor):
        s.PR2_Correct6BDac(0,1,cor_fpga0_top_asic1)
        s.PR2_Correct6BDac(0,2,cor_fpga0_bottom_asic2)
        s.PR2_Correct6BDac(0,3,cor_fpga1_top_asic3)
        s.PR2_Correct6BDac(0,4,cor_fpga1_bottom_asic4)
        s.PR2_Correct6BDac(0,5,cor_fpga2_top_asic5)
        s.PR2_Correct6BDac(0,6,cor_fpga2_bottom_asic6)

    if (applyThr):
        shift=7  # version 54 RF
        shift=10  # version 55 RF
        shift=15  # version 56 RF
    
        for i in range(1,7):
            s.PR2_ChangeVthTime(THRTRANS[i]+shift,0,i)

    # Mask canal 26 sur ASIC 1
    s.uploadChanges("RE31_185_FEB_5_904","LM after second pass VTH+%d" % shift)


