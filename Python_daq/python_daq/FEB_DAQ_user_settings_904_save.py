#getting the configuration
config_statename="904_HV100"
config_version=7

#output_file parameters
runLocation="Cosmic904"
data_FileDirectory='/data/raw/'

#Orbit parameters
BC0_period_ms=3560  #means roughly 90 microseconds
BC0_time_stage_duration=[4,4,4,4]
BC0_N_orbits=10
BC0_copy_orbits_only_if_trigger=1 #should be 0 (all readout) or 1 (readout only triggered data)


