#getting the configuration

codeVersion = "ABC"

wedcs_info = open("test_webdcsInfo.txt","r").readlines()
print("Scan number : " , wedcs_info[0])
print("HV point : " , wedcs_info[1])
print("max trigger : " , wedcs_info[2])
runLoc = "_".join(["TestBeamaAtGifOctober","HV",str(int(wedcs_info[1])), "SN" , str(int(wedcs_info[0])) , "MaxTrig" , str(int(wedcs_info[2]))])

config_statename="RE41_189_FEB_7_904"
#config_version=2 # Shifter Change only This !! 
feb_petiroc_type = [1,1,1,1,1,1] #A:0 , B:1, C:2
#config_statename="RE41_189_FEB_7_904"
config_version=3
#feb_petiroc_type = [2,2,2,2,2,2] #A:0 , B:1, C:2


#output_file parameters
#runLocation="Cosmic904"
runLocation=runLoc
runCondition=runLoc
print(runLocation)
data_FileDirectory='/data/raw/'

#Orbit parameters
#BC0_period_ms=3560  #means roughly 90 microseconds
BC0_time_stage_duration=[4,4,4,4]
BC0_N_orbits=10
BC0_copy_orbits_only_if_trigger=1 #should be 0 (all readout) or 1 (readout only triggered data)
