import json as js
from register import *

def gen_empty_config():
    daq_conf = {}

    daq_conf["global"] = {}    # for configurations global to all petirocs and fbgas

    for fpgaID in ["fpga_0", "fpga_1", "fpga_2"]:
        daq_conf[fpgaID] = {} # for configurations related to corresponding fpga
        for asicID in ["top","bottom"]:
            daq_conf[fpgaID][asicID] = {} # for configurations related to corresponding fpga
    return daq_conf

def readDacDefaultConfig(fpga, asic,fromdir="/home/acqcmsmu/FEB_DAQ/v0806/python_src/dac6bConfig_conf1"):
    # Read dac values from dac6bConfig folder ==> First run Equalization.py to generate config files
    #with open("../../../shared_folder/python_src/dac6bConfig/dac6b_{}_{}.txt".format(fpga,asic),"r") as file:
    with open("{}/dac6b_{}_{}.txt".format(fromdir,fpga,asic),"r") as file:
        mylist = file.read().splitlines()
        value_list=[]
        for d in mylist:
            value_list.append(int(d.split("=")[1]))
    return value_list

def update_input_6b_10b_daq(s):
    asic_num = 0
    for fpgaID in ["fpga_0", "fpga_1", "fpga_2"]:
        for asicID in ["top","bottom"]:
            s.asiclist[asic_num]["InputDac"] = [128 for i in range(32)]
            s.asiclist[asic_num]["6bDac"] = readDacDefaultConfig(fpgaID, asicID)[1:]
            s.asiclist[asic_num]["10bDac"] = readDacDefaultConfig(fpgaID, asicID)[0]
            s.asiclist[asic_num]['nfpga'] = int(fpgaID.split("_")[1])
            s.asiclist[asic_num]['loc'] = asicID
            asic_num += 1
    return

def fill_RestConf(daq_conf):
    for fpgaID in ["fpga_0", "fpga_1", "fpga_2"]:
        for asicID in ["top","bottom"]:
            daq_conf[fpgaID][asicID]["PETIROC_"+asicID.upper()+"_NOR32RAZ_FSM"] = [1,3,2,2]
    return daq_conf


def dump_configtoJson(daq_conf):
    with open('conf_ece.json', 'w') as fp:
        js.dump(daq_conf, fp)
